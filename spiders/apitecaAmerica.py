import scrapy
import csv
import codecs


class WorkSpider(scrapy.Spider):
    name = 'aptekaAmica'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            "url","Product_Name","cat","Main_Image","Product_Price","Producent","Kod_Producent","Description"


        ]
    };

    urls = [
        "https://apteka-amica.pl/",

    ]


    def start_requests(self):

        # f = codecs.open("/home/vishwa/aptekaamica.xml", "w+", encoding='utf8')
        # contents = "\n".join(['<?xml version="1.0" encoding="utf-8"?><offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1">','</offers>'])
        # f.write(contents)
        # f.flush()
        # f.close()

        i = 0
        for u in self.urls:

            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        details = response.xpath('//div[@class="box orange"]//li//@href').extract()

        for link in details:

            yield scrapy.Request(url=link,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True)


    def parseInside(self,response):

        pagingUrls = []
        try:

            paging= response.xpath('//div[@id="pagination"]//li//a/@href').extract_first()

            lastpgnum = response.xpath('//div[@id="pagination"]//li//a//text()').extract()
            valuelastpg=lastpgnum[-2]
            pagingother= paging.split('=')[0]
            i = 0
            while i < int(valuelastpg)+1:
                pgurl = "https://apteka-amica.pl"+pagingother+"=" + str(i)
                pagingUrls.append(pgurl)
                i += 1
        except:
            pass
            # pagingUrls=response.url

        for pagesofprdt in pagingUrls:
            yield scrapy.Request(url=pagesofprdt,callback=self.toAllPrdt,headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True)


    def toAllPrdt(self,response):

        prducturl = response.xpath('//ul[@class="thumblist-vertical"]//li//div[1]/a')
        prdlinks = prducturl.xpath('./@href').extract()

        for prdlink in prdlinks:

            yield scrapy.Request(url=prdlink, callback=self.prdDetails, headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True)

    def prdDetails(self,response):

        url=response.url
        id=url.split('/')[-1].split('-')[0]
        prdname= response.css('.product-title').xpath('./text()').extract_first()
        cat = response.url.split('https://apteka-amica.pl/')
        cat1=''.join(cat).split('/')[0]

        image = response.css('.product-photos').xpath('.//img/@data-pagespeed-lazy-src').extract_first()
        q= response.css('.product-price').xpath('./text()').extract_first()
        s=q.split('z')[0]
        prdprice = s.replace(',', '.')

        producer= ''
        kodproducer= ''
        bloz7= ''

        tabledds=response.xpath('//div[@id="tab-description-head"]//dd/text()').extract()
        tabledts=response.xpath('//div[@id="tab-description-head"]//dt/text()').extract()

        for j in range(len(tabledds)):
            dd=tabledds[j]
            dt=tabledts[j]

            if dt=='Producent':
                producer=dd
            elif dt=='BLOZ 7':
                bloz7=dd
            elif dt=='Kod EAN':
                kodproducer=dd
        descr= response.xpath('//div[@id="tab-description-body"]//text()').extract()
        descri='\n'.join(descr)

        # line = '<o id="%s" price="%s" url="%s"><cat><![CDATA[%s]]></cat><name><![CDATA[%s]]></name><imgs><main url="%s"/></imgs><desc><![CDATA[%s]]></desc><attrs><a name="Producent"><![CDATA[%s]]></a><a name="Kod_producenta"><![CDATA[%s]]></a><a name="EAN"><![CDATA[%s]]></a><a name="BLOZ_7"><![CDATA[%s]]></a></attrs></o>' % (
        #     id, prdprice, response.url, cat1, prdname, image, descri, producer, "AAAA", kodproducer, bloz7
        # )
        #
        #
        # f = codecs.open("/home/vishwa/aptekaamica.xml", "r", encoding='utf8')
        # contents = f.readlines()
        # contents = [a for a in contents if a.strip() != '']
        # f.close()
        #
        # contents.insert(len(contents) - 1, line.strip())
        #
        # f = codecs.open("/home/vishwa/aptekaamica.xml", "w", encoding='utf8')
        # contents = "\n".join(contents)
        # f.write(contents)
        # f.flush()
        # f.close()
        yield {
            "url": response.url,
            "id" : id,
            "prdname": prdname

        }