#  # -*- coding: utf-8 -*-
# import scrapy
# import csv
# import json
# import copy
# from scrapy.http import FormRequest
# import os,sys
# import pymysql.cursors
# import requests
#
# # csvfile = "rarecarateData1.csv"
# # csvRow = ('id', 'shape','color','carat','symmetry','polish','cut','clarity','price','web_url','retailer')
# # with open(csvfile, "wb") as fp1:
# #     wr = csv.writer(fp1, dialect='excel')
# #     wr.writerow(csvRow)
#
# csv.register_dialect('myDialect', delimiter='/', quoting=csv.QUOTE_NONE)
# connection = pymysql.connect(host='localhost',
#                              user='root',
#                              password='root',port=3306,
#                              db='scrape', use_unicode=True, charset="utf8")
#
# shapes = [1,10,6,2,4,8,5,9,3,7]
# data = {"diamond":{"hasMedia":"false",
#                    "shapes":[1,10,6,2,4,8,5,9,3,7],
#                    "priceMin":350,"priceMax":2000000,
#                    "caratMin":0.15,"caratMax":15,
#                    "cutMax":3,"cutMin":-1,
#                    "colorMax":1,"colorMin":1,
#                    "clarityMax":1,"clarityMin":1,
#                    "fluorescenceMax":4,"fluorescenceMin":0,
#                    "certificateLabs":[0],
#                    "polishMax":3,"polishMin":1,
#                    "symmetryMax":3,"symmetryMin":1,"tableWidthPercentageMin":0,"tableWidthPercentageMax":100,"depthPercentageMin":0,"depthPercentageMax":100,"lengthToWidthRatioMin":1,"lengthToWidthRatioMax":2.75,"lengthMin":3,"lengthMax":20,"widthMin":3,"widthMax":20,"heightMin":2,"heightMax":12,"crownAngleMin":23,"crownAngleMax":40,"pavilionAngleMin":38,"pavilionAngleMax":43,"girdleThicknessPercentageMin":1.5,"girdleThicknessPercentageMax":7,"girdleThicknessMax":8,"girdleThicknessMin":1,"pricePerCaratMin":0,"pricePerCaratMax":50000,"pairSearch":"false","isLabGrown":"false","dealScoreRatings":[],"qualityScoreRankings":[],"shippingDays":-1},"setting":{"priceMin":0,"priceMax":1000000,"styles":[],"metals":[]},"retailer":{"showOnline":"true","showLocal":"true","postalCode":"00800","distance":75,"retailers":[35,23,32,17,27,55,54,30,24,107,39,53,38,50,15,103,52,105,12],"localRetailers":[],"features":[]}}
# headers = {
#                     'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0',
#                     'Accept': 'application/json, text/javascript, */*; q=0.01',
#                     'Accept-Language': 'en-US,en;q=0.5',
#                     'Content-Type': 'application/json',
#                     'Origin': 'https://www.rarecarat.com',
#                     'Connection': 'keep-alive',
#                     'TE': 'Trailers',
#                 }
#
# def get_copy_data(shape,color,clarity,minCarat=0.15,maxCarat=15.0,minPrice=200,maxPrice=2000000):
#     data_new = copy.deepcopy(data)
#     data_new['diamond']['shapes'] = [shape]
#     data_new['diamond']['colorMax'] = color
#     data_new['diamond']['colorMin'] = color
#     data_new['diamond']['clarityMax'] = clarity
#     data_new['diamond']['clarityMin'] = clarity
#     data_new['diamond']['caratMin'] = minCarat
#     data_new['diamond']['caratMax'] = maxCarat
#     data_new['diamond']['priceMin'] = minPrice
#     data_new['diamond']['priceMax'] = maxPrice
#     # data_new['diamond']['cutMax'] = cutMax
#     return data_new
#
# def parse_body_to_save_diamond(response):
#     # pass
#     # pass body here to yield and save to csv
#     # url = 'https://webapi.rarecarat.com/diamonds2'
#     # r = requests.get(url)
#     # res_json = r.json()
#     # # res_json = json.loads(requests.)
#     # print(res_json)
#     # print(requests)
#
#     data = json.loads(response.body)
#     dataset = data['diamonds']
#     for dt in dataset:
#         id = dt['id']
#         depthurl = 'https://webapi.rarecarat.com/diamonds/'+str(id)+'?embed=retailer,retailer.ratings,retailer.features,retailer.facts,retailer.appraisals,description&diamondId='+str(id)+'&onlineRetailers=&localRetailers='
#         shapenum = dt['shape']
#         shape = ''
#         if shapenum == 1:
#             shape = 'Round'
#         elif shapenum == 10:
#             shape = 'Cushion'
#         elif shapenum == 6:
#             shape = 'Oval'
#         elif shapenum == 2:
#             shape = 'Princess'
#         elif shapenum == 4:
#             shape = 'Emerald'
#         elif shapenum == 8:
#             shape = 'Radiant'
#         elif shapenum == 5:
#             shape = 'Pear'
#         elif shapenum == 9:
#             shape = 'Asscher'
#         elif shapenum == 3:
#             shape = 'Marquise'
#         elif shapenum == 7:
#             shape = 'Heart'
#
#         colornum = dt['color']
#         color = ''
#         if colornum == 1:
#             color = 'D'
#         elif colornum == 2:
#             color = 'E'
#         elif colornum == 3:
#             color = 'F'
#         elif colornum == 4:
#             color = 'G'
#         elif colornum == 5:
#             color = 'H'
#         elif colornum == 6:
#             color = 'I'
#         elif colornum == 7:
#             color = 'J'
#         elif colornum == 8:
#             color = 'K'
#
#         carat = dt['carat']
#         symmetry = dt['symmetry']
#         polish = dt['polish']
#         cutnum = dt['cut']
#         cut = ''
#         if cutnum == 3:
#             cut = 'Good'
#         elif cutnum == 2:
#             cut = 'Very Good'
#         elif cutnum == 1:
#             cut = 'Ideal'
#
#         claritynum = dt['clarity']
#         clarity = ''
#         if claritynum == 8:
#             clarity = 'SI2'
#         elif claritynum == 7:
#             clarity = 'SI1'
#         elif claritynum == 6:
#             clarity = 'VS2'
#         elif claritynum == 5:
#             clarity = 'VS1'
#         elif claritynum == 4:
#             clarity = 'VVS2'
#         elif claritynum == 3:
#             clarity = 'VVS1'
#         elif claritynum == 2:
#             clarity = 'IF'
#         elif claritynum == 1:
#             clarity = 'FL'
#
#         price = dt['price']
#         try:
#             wburl = dt['url']
#         except:
#             wburl = ''
#
#         try:
#             retailer = dt['retailer']['name']
#         except:
#             retailer = ''
#         measurements = ''
#         depth = ''
#         fluorescencenum = dt['fluorescence']
#         fluorescence = ''
#         if fluorescencenum == 4:
#             fluorescence = 'Very Strong'
#         elif fluorescencenum == 3:
#             fluorescence = 'Strong'
#         elif fluorescencenum == 2:
#             fluorescence = 'Medium'
#         elif fluorescencenum == 1:
#             fluorescence = 'Faint'
#         elif fluorescencenum == 0:
#             fluorescence = 'None'
#
#         certificateNum = 'GIA'
#         print(cut)
#
#         csvfile = "rarecarateData1.csv"
#         csvRow = (id, shape,color,carat,symmetry,polish,cut,clarity,price,wburl,retailer)
#         with open(csvfile, "a") as fp:
#             wr = csv.writer(fp, dialect='excel')
#             wr.writerow(csvRow)
#
#         with connection.cursor() as cursor:
#             sql = "SELECT * FROM `RareDiamond` WHERE `id`=%s"
#             cursor.execute(sql, (id))
#             result = cursor.fetchone()
#
#             if result is None:
#                 sql = "INSERT INTO RareDiamond (id,shape,color,carat,symmetry,polish,cut,clarity,price," \
#                       "url,retailer,measurements,depth,fluorescence,certificate,depthurl)" \
#                       "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s , %s,%s,%s,%s,%s,%s,%s)"
#                 cursor.execute(sql,
#                                (id,shape,color,carat,symmetry,polish,cut,clarity,price,wburl,
#                                 retailer,measurements,depth,fluorescence,certificateNum,depthurl)
#                                )
#                 connection.commit()
#
#             else:
#                 sql = "UPDATE RareDiamond SET id=%s ,shape = %s,color = %s ,carat = %s,symmetry = %s,polish = %s," \
#                       "cut = %s," \
#                       "clarity = %s," \
#                       "price = %s,url = %s,retailer= %s, measurements= %s, depth= %s,fluorescence= %s," \
#                       "certificate= %s,depthurl= %s " \
#                       "WHERE id=%s"
#                 cursor.execute(sql,(id,shape,color,carat,symmetry,polish,cut,clarity,price,wburl,
#                                     retailer,measurements,depth,fluorescence,certificateNum,depthurl,id))
#                 connection.commit()
#
#
# class WorkSpider(scrapy.Spider):
#     name = 'rarecarat'
#
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': [
#             "id","shape","color","carat","symmetry","polish","cut","clarity",
#             "price","url","measurements","depth"
#             # "shape","min_carat","max_carat","color","clarity","min_price","max_price",
#             "Total_Diamonds"
#         ]
#     };
#
#     def start_requests(self):
#
#         url = 'https://webapi.rarecarat.com/diamonds2'
#
#         for col in range(1,9):
#             for clar in range(1,9):
#                 for shape in shapes:
#                     data_new=get_copy_data(shape,col,clar)
#                     yield scrapy.Request(url,method='POST', body=json.dumps(data_new),headers= headers,callback=self.recursive_find_partitions,
#                                      meta={'shape':shape,'color':col,'clarity':clar,'min_carat':0.15,'max_carat':15},dont_filter=True)
#                 # break
#
#     def recursive_find_partitions(self,response):
#         color = response.meta['color']
#         clarity = response.meta['clarity']
#         min_carat = response.meta['min_carat']
#         max_carat = response.meta['max_carat']
#         shape = response.meta['shape']
#         data = json.loads(response.body)
#         totl= int(data['totals']['total'])
#         if totl>100:
#             max_carat_temp = (max_carat + min_carat) / 2.0
#             max_carat_temp = int(max_carat_temp * 100) / 100.0
#             if max_carat_temp != min_carat and max_carat_temp != max_carat:
#                 data_new = get_copy_data(shape, color, clarity, min_carat, max_carat_temp)
#                 url_1 = "https://webapi.rarecarat.com/diamonds2"
#                 yield scrapy.Request(url_1, callback=self.recursive_find_partitions, headers=headers,body=json.dumps(data_new),method='POST',
#                                      meta={'first': False, 'shape':shape,'color': color, 'clarity': clarity, 'min_carat': min_carat,
#                                            'max_carat': max_carat_temp}, dont_filter=True)
#                 url_2 = "https://webapi.rarecarat.com/diamonds2"
#                 data_new=get_copy_data(shape,color,clarity,max_carat_temp,max_carat)
#                 yield scrapy.Request(url_2, callback=self.recursive_find_partitions, headers=headers,body=json.dumps(data_new),method='POST',
#                                      meta={'first': False,'shape':shape, 'color': color, 'clarity': clarity,
#                                            'min_carat': max_carat_temp,
#                                            'max_carat': max_carat}, dont_filter=True)
#             else:
#                 if min_carat!=max_carat:
#                     data_new = get_copy_data(shape, color, clarity, min_carat, min_carat)
#                     url_1 = "https://webapi.rarecarat.com/diamonds2"
#                     yield scrapy.Request(url_1, callback=self.recursive_find_partitions, headers=headers,
#                                          body=json.dumps(data_new), method='POST',
#                                          meta={'first': False, 'shape': shape, 'color': color, 'clarity': clarity,
#                                                'min_carat': min_carat,
#                                                'max_carat': min_carat}, dont_filter=True)
#                     url_2 = "https://webapi.rarecarat.com/diamonds2"
#                     data_new = get_copy_data(shape, color, clarity, max_carat, max_carat)
#                     yield scrapy.Request(url_2, callback=self.recursive_find_partitions, headers=headers,
#                                          body=json.dumps(data_new), method='POST',
#                                          meta={'first': False, 'shape': shape, 'color': color, 'clarity': clarity,
#                                                'min_carat': max_carat,
#                                                'max_carat': max_carat}, dont_filter=True)
#                 else:
#                     # this means for the selected parameters still diamond count is greater than 100 and it can not be broken down with carat any more
#                     # now you need to create another method to scrape recursive
#                     # its should get constants
#                     # shape,color,clarity,min_carat,max_carat then as you recursively varied carat. you need to vary price
#                     url_2 = "https://webapi.rarecarat.com/diamonds2"
#                     data_new = get_copy_data(shape, color, clarity, max_carat, max_carat,200,1000000)
#                     yield scrapy.Request(url_2, callback=self.recursive_find_partitions_price, headers=headers,
#                                          body=json.dumps(data_new), method='POST',
#                                          meta={'first': False, 'shape': shape, 'color': color, 'clarity': clarity,
#                                                'carat': max_carat,
#                                                'min_price': 200,'max_price':1000000}, dont_filter=True)
#                     url_2 = "https://webapi.rarecarat.com/diamonds2"
#                     data_new = get_copy_data(shape, color, clarity, max_carat, max_carat, 1000000, 2000000)
#                     yield scrapy.Request(url_2, callback=self.recursive_find_partitions_price, headers=headers,
#                                          body=json.dumps(data_new), method='POST',
#                                          meta={'first': False, 'shape': shape, 'color': color, 'clarity': clarity,
#                                                'carat': max_carat,
#                                                'min_price': 1000000,'max_price':2000000}, dont_filter=True)
#                     # call price partition
#                     # uncomment below yield when implemented
#                     # yield scrapy.Request(url_2, callback=self.recursive_find_partitions_price, headers=headers,
#                     #                      body=json.dumps(data_new), method='POST',
#                     #                      meta={'first': True, 'shape': shape, 'color': color, 'clarity': clarity,
#                     #                            'min_carat': max_carat_temp,
#                     #                            'max_carat': max_carat,'min_price':200,'max_price':2000000}, dont_filter=True)
#
#         else :
#             # don't call site again
#             # we already doing a loads of calls
#             # in the response we already have the diamonds
#             yield {
#                 "shape": shape,
#                 "min_carat": min_carat,
#                 "max_carat": max_carat,
#                 "color": color,
#                 "clarity": clarity,
#                 "min_price": 200,
#                 "max_price": 2000000,
#                 "Total_Diamonds": totl
#             }
#             parse_body_to_save_diamond(response)
#
#     def recursive_find_partitions_price(self, response):
#         color = response.meta['color']
#         clarity = response.meta['clarity']
#         carat = response.meta['carat']
#         max_price = response.meta['max_price']
#         min_price = response.meta['min_price']
#         shape = response.meta['shape']
#         data = json.loads(response.body)
#         totl = int(data['totals']['total'])
#
#         if totl>100:
#             max_price_temp = int((min_price + max_price) / 2.0)
#             if max_price_temp != min_price and max_price_temp != max_price:
#                 data_new = get_copy_data(shape, color, clarity, carat, carat,min_price,max_price_temp)
#                 url_1 = "https://webapi.rarecarat.com/diamonds2"
#                 yield scrapy.Request(url_1, callback=self.recursive_find_partitions_price, headers=headers,body=json.dumps(data_new),method='POST',
#                                      meta={'first': False, 'shape':shape,'color': color, 'clarity': clarity, 'carat': carat,
#                                            'min_price': min_price,'max_price':max_price_temp}, dont_filter=True)
#                 url_2 = "https://webapi.rarecarat.com/diamonds2"
#                 data_new=get_copy_data(shape,color,clarity,carat,carat,max_price_temp,max_price)
#                 yield scrapy.Request(url_2, callback=self.recursive_find_partitions_price, headers=headers,body=json.dumps(data_new),method='POST',
#                                      meta={'first': False,'shape':shape, 'color': color, 'clarity': clarity,
#                                            'carat': carat,
#                                            'min_price': max_price_temp,'max_price':max_price}, dont_filter=True)
#             else:
#                 if min_price!=max_price:
#                     data_new = get_copy_data(shape, color, clarity, carat, carat,min_price,min_price)
#                     url_1 = "https://webapi.rarecarat.com/diamonds2"
#                     yield scrapy.Request(url_1, callback=self.recursive_find_partitions, headers=headers,
#                                          body=json.dumps(data_new), method='POST',
#                                          meta={'first': False, 'shape': shape, 'color': color, 'clarity': clarity,
#                                                'carat': carat,
#                                                'min_price': min_price,'max_price':min_price}, dont_filter=True)
#                     url_2 = "https://webapi.rarecarat.com/diamonds2"
#                     data_new = get_copy_data(shape, color, clarity, carat, carat, max_price, max_price)
#                     yield scrapy.Request(url_2, callback=self.recursive_find_partitions, headers=headers,
#                                          body=json.dumps(data_new), method='POST',
#                                          meta={'first': False, 'shape': shape, 'color': color, 'clarity': clarity,
#                                                'carat': carat,
#                                                'min_price': max_price,'max_price':max_price}, dont_filter=True)
#                 else:
#                     yield {
#                         "shape": shape,
#                         "min_carat": carat,
#                         "max_carat": carat,
#                         "color": color,
#                         "clarity": clarity,
#                         "min_price": min_price,
#                         "max_price": max_price,
#                         "Total_Diamonds": totl
#                     }
#         else:
#             # don't call site again
#             # we already doing a loads of calls
#             # in the response we already have the diamonds
#             yield {
#                 "shape": shape,
#                 "min_carat": carat,
#                 "max_carat": carat,
#                 "color": color,
#                 "clarity": clarity,
#                 "min_price":min_price,
#                 "max_price":max_price,
#                 "Total_Diamonds": totl
#             }
#             parse_body_to_save_diamond(response)
#
#
#     def parse(self, response):
#         data = json.loads(response.body)
#         dataset = data['diamonds']
#         totl = int(data['totals']['total'])
#         mincarat = response.meta['min_carat']
#         maxcaret = response.meta['max_carat']
#         color = response.meta['color']
#         clarity = response.meta['clarity']
#
#         # yield {
#         #     "min_carat": mincarat,
#         #     "max_carat": maxcaret,
#         #     "color": color,
#         #     "clarity": clarity,
#         #     "Total_Diamonds": totl
#         # }
#
#         for dt in dataset:
#             id = dt['id']
#             depthurl = 'https://webapi.rarecarat.com/diamonds/'+str(id)+'?embed=retailer,retailer.ratings,retailer.features,retailer.facts,retailer.appraisals,description&diamondId='+str(id)+'&onlineRetailers=&localRetailers='
#             shape = dt['shape']
#             color = dt['color']
#             carat = dt['carat']
#             symmetry = dt['symmetry']
#             polish = dt['polish']
#             cut = dt['cut']
#             clarity = dt['clarity']
#             price = dt['price']
#             try:
#                 wburl = dt['url']
#             except:
#                 wburl = ''
#             # id, shape, color, carat, symmetry, polish, cut, clarity, price, measurement, depth
#             yield scrapy.Request(depthurl, callback=self.parseDepth_measure,
#                                  meta={"id": id,"shape":shape,"color":color,"carat":carat,"symmetry":symmetry,"polish":polish,"cut":cut,
#                                        "clarity":clarity,"price": price,"url":wburl,"Total_Diamonds":totl})
#
#     def parseDepth_measure(self,response):
#         data = json.loads(response.body)
#         id = response.meta['id']
#         shape = response.meta['shape']
#         color = response.meta['color']
#         carat = response.meta['carat']
#         symmetry = response.meta['symmetry']
#         polish = response.meta['polish']
#         cut = response.meta['cut']
#         clarity = response.meta['clarity']
#         price = response.meta['price']
#         wburl = response.meta['url']
#         totl = response.meta['Total_Diamonds']
#         try:
#             measurements = data['measurements']
#         except:
#             measurements = ''
#         try:
#             depth = data['depthPercentage']
#         except:
#             depth = ''
#
#         yield {
#             "id": id,"shape":shape,"color":color,"carat":carat,"symmetry":symmetry,"polish":polish,"cut":cut,"clarity":clarity,
#             "price": price,"url":wburl,"measurements": measurements,"depth":depth,"Total_Diamonds": totl
#         }
#
#         with connection.cursor() as cursor:
#             sql = "SELECT * FROM `RareDiamond` WHERE `id`=%s"
#             cursor.execute(sql, (id))
#             result = cursor.fetchone()
#
#             if result is None:
#                 sql = "INSERT INTO RareDiamond (id,shape,color,carat,symmetry,polish,cut,clarity,price,url,measurements,depth)" \
#                       "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s , %s,%s,%s)"
#                 cursor.execute(sql,
#                                (id,shape,color,carat,symmetry,polish,cut,clarity,price,wburl,measurements,depth)
#                                )
#                 connection.commit()
#
#             else:
#                 sql = "UPDATE RareDiamond SET id=%s ,shape = %s,color = %s ,carat = %s,symmetry = %s,polish = %s,cut = %s,clarity = %s," \
#                       "price = %s,url = %s, measurements= %s, depth= %s" \
#                       "WHERE id=%s"
#                 cursor.execute(sql,(id,shape,color,carat,symmetry,polish,cut,clarity,price,wburl,measurements,depth,id))
#                 connection.commit()
