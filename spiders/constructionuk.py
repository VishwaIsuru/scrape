import scrapy
import csv
from csv import reader

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36',
    # 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    # 'Accept-Language': 'en-US,en;q=0.5',
    # 'Connection': 'keep-alive',
}

class workspider(scrapy.Spider):
    name = "constructionuk"

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "url","region","category","companyName","telephone","fax","mobile","website","contact","address"
            # 'url','region','cat','result'
                               ]
    };

    def start_requests(self):
        regions = []
        catkeys = []
        catvals = []
        with open('regionukcons.csv', 'r', encoding="utf8",errors='ignore') as read_obj:
            csv_reader = reader(read_obj)
            for row in csv_reader:
                region = row[1]
                regions.append(region)
        with open('catageryukcons.csv', 'r', encoding="utf8",errors='ignore') as read_obj1:
            csv_reader = reader(read_obj1)
            for row in csv_reader:
                catkey = row[0]
                catval = row[1]
                catkeys.append(catkey)
                catvals.append(catval)

        print('Region count ',len(regions))
        print('Category count ', len(catkeys))
        for i in range(11, 12):
            for j in range(750,len(catkeys)):
                ur = 'https://www.construction.co.uk/d_m/'+str(catkeys[j])+',-1/england-'+str(regions[i].replace('-',' ').replace('&',' ').replace(' ','-').lower())+'/'+str(catvals[j].replace('-',' ').replace('&',' ').replace(' ','-').lower())
                yield scrapy.Request(url=ur, callback=self.parse_inside, dont_filter=True,headers=headers,
                                     meta={'region': regions[i],'category': catvals[j]},
                                     )

    def parse_inside(self,response):
        url = response.url
        region = response.meta['region']
        category = response.meta['category']
        # results = response.xpath('//div[@class="currentPageNumber"]//text()').extract_first().split('(')[1].split(' ')[0]
        # yield {
        #     'url': url,
        #     'region': region,
        #     'cat': category,
        #     'result': results
        # }
        pgcount = response.xpath('//div[@class="currentPageNumber"]//text()').extract_first().split('Page 1 of ')[-1].split(' ')[0]
        for i in range(1,int(pgcount)+1):
            nw = ',-'+str(i)
            pgurl = url.replace(',-1',nw)
            yield scrapy.Request(url=pgurl, callback=self.parse_inside_pages, dont_filter=True, headers=headers,
                                 meta={'region': region, 'category': category},)

    def parse_inside_pages(self, response):
        url = response.url
        region = response.meta['region']
        category = response.meta['category']
        consturls = response.xpath('//div[@class="defaultListInfo"]//a//@href').extract()
        for curl in consturls:
            yield scrapy.Request(url=curl, callback=self.parse_inside_construc, dont_filter=True, headers=headers,
                                 meta={'region': region, 'category': category})

    def parse_inside_construc(self,response):
        url = response.url
        region = response.meta['region']
        category = response.meta['category']
        companyName = response.xpath('//div[@class="listingCompanyName right"]//h1//text()').extract_first()
        try:
            telephone = response.xpath('//div[@id="hTel"]//@onclick').extract_first().split(',')[1]
        except:
            telephone = ''
        fax = ''.join([x.strip() for x in response.xpath('//div[contains(text(),"Fax")]/following-sibling::div//text()').extract()])
        try:
            mobile = response.xpath('//div[contains(text(),"Mobile")]/following-sibling::div//@onclick').extract_first().split(',')[2]
        except:
            mobile = ''
        website = ''.join([x.strip() for x in response.xpath('//div[contains(text(),"Website")]/following-sibling::div//text()').extract()])
        contact = ''.join([x.strip() for x in response.xpath('//div[contains(text(),"Contact")]/following-sibling::div//text()').extract()])
        address = ','.join([x.strip() for x in response.xpath('//div[@itemprop="address"]//text()').extract()])
        # email = response.xpath('//div[contains(text(),"Email")]/following-sibling::div//text()').extract()

        yield {
            "url": url,
            "region": region,
            "category": category,
            "companyName": companyName,
            "telephone": telephone,
            "fax": fax,
            "mobile": mobile,
            "website": website,
            "contact": contact,
            "address": address
        }