import scrapy

class WorkSpider(scrapy.Spider):
    name = 'amazonPgAsin'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "prd_link", "asin"
        ]
    };

    urls = [
        "https://www.amazon.com/s?k=Dupont&i=fashion-womens-intl-ship&crid=B367NF7WD5J1&sprefix=dupont%2Cfashion-womens-intl-ship%2C356&ref=nb_sb_noss",
        ]

    def start_requests(self):
        header = {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(
                31111111111111) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
            "Referer": "https://www.amazon.com",
            "Origin": "https://www.amazon.com",
            "Connection": "keep-alive",
        }

        for url in self.urls:
            yield scrapy.Request(url=url, headers=header, callback=self.parseInside, dont_filter=True,meta={'page':1})

    def parseInside(self, response):
        next_page = response.meta['page'] + 1
        # '//div[@class="a-section a-spacing-small a-spacing-top-small"]//a[@class="a-link-normal s-underline-text s-underline-link-text s-link-style a-text-normal"]'

        prddivs = response.xpath('//h2[@class="a-size-mini a-spacing-none a-color-base s-line-clamp-2"]//a[@class="a-link-normal s-underline-text s-underline-link-text s-link-style a-text-normal"]')
        for prddiv in prddivs.xpath('./@href').extract():
            prdlink = 'https://www.amazon.com' + prddiv
            if '/dp/' in prdlink:
                prdlinkneed = prdlink
                asin = prdlinkneed.split('/dp/')[1].split('/')[0]
                yield {"prd_link": prdlink, "asin": asin}

        pages = response.xpath('//a[@class="s-pagination-item s-pagination-button"]')
        print (next_page)
        # print(pages)
        for page in pages:
            print("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWww")
            text = page.css('a').xpath('./text()').extract_first()
            pageNum = int(text)
            if pageNum == next_page:
                header = {
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(
                        pageNum) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
                    "Referer": "https://www.amazon.com",
                    "Origin": "https://www.amazon.com",
                    "Connection": "keep-alive",
                }

                l='https://www.amazon.com' +page.xpath('./@href').extract_first()
                yield scrapy.Request(url=l, callback=self.parseInside, headers=header, meta={'page': next_page})
                break
