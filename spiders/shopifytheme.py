import scrapy

class workspider(scrapy.Spider):
    name = "shopithemesql"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Url",
            "ThemeTitle","Industry", "ShortDescription",
            "Price", "FeatureImageDesktop", "FeatureImageMobile",
            "BuyThemeLink", "ThemeDeveloperLink",
            "ThemeFeaturetitle1", "ThemeFeatureShortDescription1",
            "ThemeFeaturetitle2", "ThemeFeatureShortDescription2",
            "ThemeFeaturetitle3", "ThemeFeatureShortDescription3",
            "ThemeFeaturetitle4", "ThemeFeatureShortDescription4",
            "ThemeFeaturetitle5", "ThemeFeatureShortDescription5",
            "ThemeFeaturetitle6", "ThemeFeatureShortDescription6",
            "StoreUsingFeatureImage1", "StoreUsingLinkToStore1",
            "StoreUsingFeatureImage2", "StoreUsingLinkToStore2",
            "StoreUsingFeatureImage3", "StoreUsingLinkToStore3",
            "StoreUsingFeatureImage4", "StoreUsingLinkToStore4",
            "ThemeDeveloperName","ViewDemo"
                ]
    };
    urls = ["https://themes.shopify.com/themes?industry=art-photography","https://themes.shopify.com/themes?industry=clothing-fashion",
            "https://themes.shopify.com/themes?industry=jewelry-accessories","https://themes.shopify.com/themes?industry=electronics",
            "https://themes.shopify.com/themes?industry=food-drink","https://themes.shopify.com/themes?industry=home-garden",
            "https://themes.shopify.com/themes?industry=furniture","https://themes.shopify.com/themes?industry=health-beauty",
            "https://themes.shopify.com/themes?industry=sports-recreation","https://themes.shopify.com/themes?industry=toys-games",
            "https://themes.shopify.com/themes?industry=other","https://themes.shopify.com/collections/trending-themes"]

    def start_requests(self):

        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )

    def parse(self, response):
        allthemepages = []

        thems = response.css('.filtered-themes-total').xpath('.//text()').extract_first()

        if thems is not None:
            them = thems.replace('\n', '').replace('  ', '').split(' ')[-2]
            s= int(them)/24+2
            thmpages = int(s)

            for i in range(1,thmpages):
                thmpgurl = response.url+'&page='+str(i)
                allthemepages.append(thmpgurl)
                yield scrapy.Request(url=thmpgurl, callback=self.toAllThemes,
                                     headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

        else:
            thmpgurl = response.url
            yield scrapy.Request(url=thmpgurl, callback=self.toAllThemes,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

    def toAllThemes(self, response):

        allthemes = response.xpath('//a[@class="theme__link"]/@href').extract()

        for themes in allthemes:
            themelink = 'https://themes.shopify.com'+themes
            yield scrapy.Request(url=themelink, callback=self.toStyleUrls,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

    def toStyleUrls(self,response):

        styles = response.xpath('//span[@class="example-presets-selector__option-label"]//text()').extract()
        lsp = response.url.split('?')[-1]
        them = response.url.split('?')[0].split('/')[4]
        for style in styles:
            sty = style.replace('\n            ','').replace('\n              ','').replace('  ','')
            # print(sty)
            stylesurl = 'https://themes.shopify.com/themes/'+them+'/styles/'+sty.lower()+'?'+lsp
            yield scrapy.Request(url=stylesurl, callback=self.toAllThemesData, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 dont_filter=True)

    def toAllThemesData(self, response):
        print('aaaaaaaaaaaaaaaaaaaaaaaaAllllllllThemeeeeeeeeeeeeeeeeeeeeeeeeees')
        print('dddddddddddddddddddddddddd')
        themetitle = response.xpath('//h1[@class="section-heading__heading theme-meta__theme-name"]//text()').extract_first()
        industry1 = response.url.split('=')[1].split('&')[0]
        shortdisc = response.css('.theme-meta__tagline').xpath('.//text()').extract_first()
        price = response.xpath('//p[@class="heading--3 gutter-bottom--half"]//text()').extract_first()
        desimg = response.xpath('//img[@class="main-screenshot__image"]//@src').extract_first()
        mobileimg = response.xpath('//div[@class="iphone__screenshot"]//img//@src').extract_first()
        try:
            buythemelink = response.xpath('//a[@id="AddTheme"]/@href').extract_first()
        except:
            buythemelink = ''
        try:
            themedevlink = response.xpath('//a[@rel="nofollow"]/@href').extract_first()
        except:
            themedevlink = response.xpath('//p[@class="text-major theme-text-major gutter-bottom--reset"]//text()').extract_first()

        themedevname = response.xpath('//a[@rel="nofollow"]//text()').extract_first()
        if themedevname is None:
            themedevname = 'Shopify'
        if themedevlink is None:
            themedevlink = 'This theme is officially supported by Shopify'
        themfettitle1 = response.xpath('//div[@class="social-proof__feature-text"][1]//h3//text()').extract_first()
        thefetshordiscr1 = response.xpath('//div[@class="social-proof__feature-text"][1]//p//text()').extract_first()
        themfettitle2 = response.css('div.grid__item--tablet-up-half:nth-child(3) > div:nth-child(1) > div:nth-child(2) > h3:nth-child(1)').xpath('.//text()').extract_first()
        thefetshordiscr2 = response.css('div.grid__item--tablet-up-half:nth-child(3) > div:nth-child(1) > div:nth-child(2) > p:nth-child(2)').xpath('.//text()').extract_first()
        themfettitle3 = response.css('div.grid__item--tablet-up-half:nth-child(4) > div:nth-child(1) > div:nth-child(2) > h3:nth-child(1)').xpath('.//text()').extract_first()
        thefetshordiscr3 = response.css('div.grid__item--tablet-up-half:nth-child(4) > div:nth-child(1) > div:nth-child(2) > p:nth-child(2)').xpath('.//text()').extract_first()
        themfettitle4 = response.css('div.grid__item--tablet-up-half:nth-child(5) > div:nth-child(1) > div:nth-child(2) > h3:nth-child(1)').xpath('.//text()').extract_first()
        thefetshordiscr4 = response.css('div.grid__item--tablet-up-half:nth-child(5) > div:nth-child(1) > div:nth-child(2) > p:nth-child(2)').xpath('.//text()').extract_first()
        themfettitle5 = response.css('div.grid__item--tablet-up-half:nth-child(6) > div:nth-child(1) > div:nth-child(2) > h3:nth-child(1)').xpath('.//text()').extract_first()
        thefetshordiscr5 = response.css('div.grid__item--tablet-up-half:nth-child(6) > div:nth-child(1) > div:nth-child(2) > p:nth-child(2)').xpath('.//text()').extract_first()
        themfettitle6 = response.css('div.grid__item--tablet-up-half:nth-child(7) > div:nth-child(1) > div:nth-child(2) > h3:nth-child(1)').xpath('.//text()').extract_first()
        thefetshordiscr6 = response.css('div.grid__item--tablet-up-half:nth-child(7) > div:nth-child(1) > div:nth-child(2) > p:nth-child(2)').xpath('.//text()').extract_first()

        storefeaturelink1 = response.xpath('//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][1]//a/@href').extract_first()
        strofeatureimg1 = response.xpath('//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][1]//img/@src').extract_first()
        storefeaturelink2 = response.xpath(
            '//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][2]//a/@href').extract_first()
        strofeatureimg2 = response.xpath(
            '//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][2]//img/@src').extract_first()
        storefeaturelink3 = response.xpath(
            '//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][3]//a/@href').extract_first()
        strofeatureimg3 = response.xpath(
            '//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][3]//img/@src').extract_first()
        storefeaturelink4 = response.xpath(
            '//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][4]//a/@href').extract_first()
        strofeatureimg4 = response.xpath(
            '//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][4]//img/@src').extract_first()
        viewdemo = 'https://themes.shopify.com'+response.xpath('//a[@class="marketing-button marketing-button--secondary theme-preview-link"]//@href').extract_first()
        industry = ''
        if 'art-photography' in industry1:
            industry = 'Art & Photography'
        if 'clothing-fashion' in industry1:
            industry = 'Clothing & Fashion'
        if 'jewelry-accessories' in industry1:
            industry = 'Jewelry & Accessories'
        if 'electronics' in industry1:
            industry = 'Electronics'
        if 'food-drink' in industry1:
            industry = 'Food & Drink'
        if 'home-garden' in industry1:
            industry = 'Home & Garden'
        if 'furniture' in industry1:
            industry = 'Furniture'
        if 'health-beauty' in industry1:
            industry = 'Health & Beauty'
        if 'sports-recreation' in industry1:
            industry = 'Sports & Recreation'
        if 'toys-games' in industry1:
            industry = 'Toys & Games'
        if 'other' in industry1:
            industry = 'Other'
        if 'trending-themes' in industry1:
            industry = 'Trending this week'

        yield {
            "Url": response.url,"ThemeTitle": themetitle,"Industry": industry,"ShortDescription":shortdisc,
            "Price": price,"FeatureImageDesktop":desimg,"FeatureImageMobile": mobileimg,
            "BuyThemeLink": buythemelink,"ThemeDeveloperLink":themedevlink,
            "ThemeFeaturetitle1":themfettitle1,"ThemeFeatureShortDescription1":thefetshordiscr1,
            "ThemeFeaturetitle2": themfettitle2, "ThemeFeatureShortDescription2": thefetshordiscr2,
            "ThemeFeaturetitle3": themfettitle3, "ThemeFeatureShortDescription3": thefetshordiscr3,
            "ThemeFeaturetitle4": themfettitle4, "ThemeFeatureShortDescription4": thefetshordiscr4,
            "ThemeFeaturetitle5": themfettitle5, "ThemeFeatureShortDescription5": thefetshordiscr5,
            "ThemeFeaturetitle6": themfettitle6, "ThemeFeatureShortDescription6": thefetshordiscr6,
            "StoreUsingFeatureImage1": strofeatureimg1,"StoreUsingLinkToStore1": storefeaturelink1,
            "StoreUsingFeatureImage2": strofeatureimg2, "StoreUsingLinkToStore2": storefeaturelink2,
            "StoreUsingFeatureImage3": strofeatureimg3, "StoreUsingLinkToStore3": storefeaturelink3,
            "StoreUsingFeatureImage4": strofeatureimg4, "StoreUsingLinkToStore4": storefeaturelink4,
            "ThemeDeveloperName": themedevname,"ViewDemo":viewdemo
        }
