import scrapy

class workspider(scrapy.Spider):
    name = "shopithemecheckbox"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "checkbox","themes","Themecount","Category","Type"
                ]
    };
    urls = ["https://themes.shopify.com/themes?sort_by=most_recent&price%5B%5D=free","https://themes.shopify.com/themes?sort_by=most_recent&price%5B%5D=paid",
            "https://themes.shopify.com/themes?sort_by=most_recent&number-of-products%5B%5D=optimized-for-single-product-stores","https://themes.shopify.com/themes?sort_by=most_recent&number-of-products%5B%5D=built-for-small-catalogs",
            "https://themes.shopify.com/themes?sort_by=most_recent&number-of-products%5B%5D=built-for-medium-catalogs","https://themes.shopify.com/themes?sort_by=most_recent&number-of-products%5B%5D=built-for-large-catalogs",
            "https://themes.shopify.com/themes?sort_by=most_recent&layout-style%5B%5D=wide-layout","https://themes.shopify.com/themes?sort_by=most_recent&layout-style%5B%5D=grid-style-layout",
            "https://themes.shopify.com/themes?sort_by=most_recent&layout-style%5B%5D=collage-style-layout","https://themes.shopify.com/themes?sort_by=most_recent&layout-style%5B%5D=editorial-style-layout",
            "https://themes.shopify.com/themes?sort_by=most_recent&layout-style%5B%5D=row-style-layout","https://themes.shopify.com/themes?sort_by=most_recent&layout-style%5B%5D=one-page-layout",
            "https://themes.shopify.com/themes?sort_by=most_recent&product-page%5B%5D=product-page-tabs","https://themes.shopify.com/themes?sort_by=most_recent&product-page%5B%5D=product-pickup-availability",
            "https://themes.shopify.com/themes?sort_by=most_recent&product-page%5B%5D=product-image-zoom","https://themes.shopify.com/themes?sort_by=most_recent&product-page%5B%5D=product-size-chart",
            "https://themes.shopify.com/themes?sort_by=most_recent&product-page%5B%5D=optimized-for-large-images","https://themes.shopify.com/themes?sort_by=most_recent&product-page%5B%5D=product-page-gallery",
            "https://themes.shopify.com/themes?sort_by=most_recent&product-page%5B%5D=related-products","https://themes.shopify.com/themes?sort_by=most_recent&home-page%5B%5D=slideshow",
            "https://themes.shopify.com/themes?sort_by=most_recent&home-page%5B%5D=header-slideshow","https://themes.shopify.com/themes?sort_by=most_recent&home-page%5B%5D=slideshow-with-video",
            "https://themes.shopify.com/themes?sort_by=most_recent&home-page%5B%5D=home-page-collection-grid","https://themes.shopify.com/themes?sort_by=most_recent&home-page%5B%5D=customer-testimonials",
            "https://themes.shopify.com/themes?sort_by=most_recent&home-page%5B%5D=parallax-effect","https://themes.shopify.com/themes?sort_by=most_recent&home-page%5B%5D=home-page-video",
            "https://themes.shopify.com/themes?sort_by=most_recent&navigation%5B%5D=sidebar-menu","https://themes.shopify.com/themes?sort_by=most_recent&navigation%5B%5D=horizontal-menu",
            "https://themes.shopify.com/themes?sort_by=most_recent&navigation%5B%5D=multi-level-menu","https://themes.shopify.com/themes?sort_by=most_recent&navigation%5B%5D=multi-column-menu",
            "https://themes.shopify.com/themes?sort_by=most_recent&navigation%5B%5D=slide-out-menu","https://themes.shopify.com/themes?sort_by=most_recent&marketing-and-social-media%5B%5D=marketing-popup",
            "https://themes.shopify.com/themes?sort_by=most_recent&marketing-and-social-media%5B%5D=promotional-banner","https://themes.shopify.com/themes?sort_by=most_recent&marketing-and-social-media%5B%5D=twitter-feed",
            "https://themes.shopify.com/themes?sort_by=most_recent&marketing-and-social-media%5B%5D=disqus-support-for-blog-comments"
            ]

    def start_requests(self):

        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )

    def parse(self, response):
        allthemepages = []

        thems = response.css('.filtered-themes-total').xpath('.//text()').extract_first()

        if thems is not None:
            them = thems.replace('\n', '').replace('  ', '').split(' ')[-2]
            s = int(them)/24+2
            thmpages = int(s)

            for i in range(1,thmpages):
                thmpgurl = response.url+'&page='+str(i)
                allthemepages.append(thmpgurl)
                yield scrapy.Request(url=thmpgurl, callback=self.toAllThemes,
                                     headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

        else:
            thmpgurl = response.url
            yield scrapy.Request(url=thmpgurl, callback=self.toAllThemes,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

    def toAllThemes(self, response):

        allthemes = response.xpath('//a[@class="theme__link"]/@href').extract()

        themes = ','.join(allthemes)
        chechbox = response.url.split('=')[2].split('&')[0]
        themecount = len(allthemes)

        category = ''
        type = ''
        if 'free' in chechbox:
            type = 'Free'
            category = 'Price'
        if 'paid' in chechbox:
            type = 'Paid'
            category = 'Price'

        if 'optimized-for-single-product-stores' in chechbox:
            type = 'Single product'
            category = 'Number of products'
        if 'built-for-small-catalogs' in chechbox:
            type = 'Small catalogs'
            category = 'Number of products'
        if 'built-for-medium-catalogs' in chechbox:
            type = 'Medium catalogs'
            category = 'Number of products'
        if 'built-for-large-catalogs' in chechbox:
            type = 'Large catalogs'
            category = 'Number of products'

        if 'wide-layout' in chechbox:
            type = 'Wide'
            category = 'Layout style'
        if 'grid-style-layout' in chechbox:
            type = 'Grid'
            category = 'Layout style'
        if 'collage-style-layout' in chechbox:
            type = 'Collage'
            category = 'Layout style'
        if 'editorial-style-layout' in chechbox:
            type = 'Editorial'
            category = 'Layout style'
        if 'row-style-layout' in chechbox:
            type = 'Row'
            category = 'Layout style'
        if 'one-page-layout' in chechbox:
            type = 'One page'
            category = 'Layout style'

        if 'product-page-tabs' in chechbox:
            type = 'Tabs'
        if 'product-pickup-availability' in chechbox:
            type = 'Pickup Availability'
        if 'product-image-zoom' in chechbox:
            type = 'Image zoom'
            category = 'Product Page'
        if 'product-size-chart' in chechbox:
            type = 'Size chart'
            category = 'Product Page'
        if 'optimized-for-large-images' in chechbox:
            type = 'High-Definition images'
            category = 'Product Page'
        if 'product-page-gallery' in chechbox:
            type = 'Gallery'
            category = 'Product Page'
        if 'related-products' in chechbox:
            type = 'Related products'
            category = 'Product Page'

        if 'slideshow' in chechbox:
            type = 'Slideshow'
            category = 'Home page'
        if 'header-slideshow' in chechbox:
            type = 'Header slideshow'
            category = 'Home page'
        if 'slideshow-with-video' in chechbox:
            type = 'Slideshow with video'
            category = 'Home page'
        if 'home-page-collection-grid' in chechbox:
            type = 'Collection grid'
            category = 'Home page'
        if 'customer-testimonials' in chechbox:
            type = 'Customer testimonials'
            category = 'Home page'
        if 'parallax-effect' in chechbox:
            type = 'Parallax effect'
            category = 'Home page'
        if 'home-page-video' in chechbox:
            type = 'Video'
            category = 'Home page'

        if 'sidebar-menu' in chechbox:
            type = 'Sidebar menu'
            category = 'Navigation'
        if 'horizontal-menu' in chechbox:
            type = 'Horizontal menu'
            category = 'Navigation'
        if 'multi-level-menu' in chechbox:
            type = 'Multi-level menu'
            category = 'Navigation'
        if 'multi-column-menu' in chechbox:
            type = 'Multi-column menu'
            category = 'Navigation'
        if 'slide-out-menu' in chechbox:
            type = 'Slide-out menu'
            category = 'Navigation'

        if 'marketing-popup' in chechbox:
            type = 'Marketing popup'
            category = 'Marketing and social media'
        if 'promotional-banner' in chechbox:
            type = 'Promotional banner'
            category = 'Marketing and social media'
        if 'twitter-feed' in chechbox:
            type = 'Twitter feed'
            category = 'Marketing and social media'
        if 'disqus-support-for-blog-comments' in chechbox:
            type = 'Disqus blog comments'
            category = 'Marketing and social media'

        yield {
            "checkbox": chechbox,
            "themes": allthemes,
            "Themecount": themecount,
            "Category": category,
            "Type": type
        }