import scrapy
import csv
import pandas as pd
import requests

class WorkSpider(scrapy.Spider):
    name = 'ebayfinditpartssearchterms'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Search list","Product link","Part Title",
            "Seller","Price"
        ]
    };

    def start_requests(self):
        col_list = ["Search List 2"]
        df = pd.read_csv("FIp Scrape File 12_29_2021.csv", usecols=col_list)
        urls = df["Search List 2"]

        for i in range(0,len(urls)+1):
            u = 'https://www.ebay.com/sch/i.html?_from=R40&_trksid=p2047675.m570.l1313&_nkw='+urls[i]+'&_sacat=0'
            yield scrapy.Request(url=u,callback=self.parse,meta={'searhterm':urls[i]},
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parse(self, response):
        searchterm = response.meta['searhterm']
        firstitem1 = response.css('li.s-item:nth-child(2) > div:nth-child(1) > div:nth-child(2) > a:nth-child(1)')
        firstitem = firstitem1.xpath('.//@href').extract_first()
        if firstitem is None:
            firstitem1 = response.css('li.s-item--watch-at-corner:nth-child(1) > div:nth-child(1) > div:nth-child(2) > a')
            firstitem = firstitem1.xpath('.//@href').extract_first()

        yield scrapy.Request(url=firstitem, callback=self.parseProduct, meta={'searhterm': searchterm},
                             headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parseProduct(self,response):
        searchterm = response.meta['searhterm']
        productname = ''.join([x.strip() for x in response.xpath('//h1[@itemprop="name"]//text()').extract()])
        price = ''.join([x.strip() for x in response.xpath('//span[@itemprop="price"]//text()').extract()])
        seller = ''.join([x.strip() for x in response.xpath('//span[@class="ux-textspans--PSEUDOLINK ux-textspans--BOLD"]//text()').extract()])

        yield {
            'Search list': searchterm,
            'Product link': response.url,
            'Part Title': productname,
            'Seller': seller,
            'Price': price,
        }