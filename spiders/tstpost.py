import scrapy
import pandas as pd
import csv

class WorkSpider(scrapy.Spider):
    name = 'amazongetasin'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "asin","prd_link"
        ]
    };

    def start_requests(self):
        col_list = ["lastcat", "sndpgurl"]
        df = pd.read_csv("amazon_55k_Toys_and_Games_cat.csv", usecols=col_list, encoding='unicode_escape')
        brands = df["lastcat"]
        # prttype = df["partType"]

        header = {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(
                313311111121111) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
            "Referer": "https://www.amazon.com",
            "Origin": "https://www.amazon.com",
            "Connection": "keep-alive",
        }
        # cookie = {"sp-cdn": "L5Z9:CA"}  # "CN for china"  CA for canada

        for i in range(0, len(brands)):
            brand = brands[i]
            print(brand)
            u = str(brand)+'&s=date-desc-rank'
            yield scrapy.Request(url=u, callback=self.parseInside,
                                 headers=header,
                                 meta={'page':1}, dont_filter=True)

    def parseInside(self, response):
        # brand = response.meta['brand']
        next_page = response.meta['page'] + 1
        # '//div[@class="a-section a-spacing-small a-spacing-top-small"]//a[@class="a-link-normal s-underline-text s-underline-link-text s-link-style a-text-normal"]'

        prddivs = response.xpath('//h2[contains(@class, "a-size-mini a-spacing-none a-color-base s-line-clamp-")]//a[@class="a-link-normal s-underline-text s-underline-link-text s-link-style a-text-normal"]')
        for prddiv in prddivs.xpath('./@href').extract():
            prdlink = 'https://www.amazon.com' + prddiv
            if '/dp/' in prdlink:
                prdlinkneed = prdlink
                asin = prdlinkneed.split('/dp/')[1].split('/')[0]
                print('asin',asin)
                yield {"asin": asin,"prd_link": prdlink}

        pages = response.xpath('//a[@class="s-pagination-item s-pagination-button"]')
        print (next_page)
        # print(pages)
        for page in pages:
            print("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWww")
            text = page.css('a').xpath('./text()').extract_first()
            pageNum = int(text)
            if pageNum == next_page and pageNum < 101:
                header = {
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(
                        pageNum) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
                    "Referer": "https://www.amazon.com",
                    "Origin": "https://www.amazon.com",
                    "Connection": "keep-alive",
                }

                l='https://www.amazon.com' +page.xpath('./@href').extract_first()
                yield scrapy.Request(url=l, callback=self.parseInside, headers=header, meta={'page': next_page})
                break
