import scrapy
import csv

class WorkSpider(scrapy.Spider):
    name = 'segunda'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Url","Phone","Price","Name","Ubicacion"


        ]
    };

    urls = [
        # "https://www.vivanuncios.com.mx/s-venta-inmuebles/estado-de-mexico/page-47/v1c1097l1014p47?dw=house,apartment&pr=4000000,&df=ownr",
        # "https://www.vivanuncios.com.mx/s-venta-inmuebles/distrito-federal/page-38/v1c1097l1008p38?dw=house,apartment&pr=4000000,&df=ownr"

    ]
    # i = 1
    # while i < 40:
    #     ur = "https://www.vivanuncios.com.mx/s-venta-inmuebles/distrito-federal/page-"+str(i)+"/v1c1097l1008p"+str(i)+"?dw=house,apartment&pr=4000000,&df=ownr"
    #     urls.append(ur)
    #     i += 1
    i = 1
    while i < 48:
        ur = "https://www.vivanuncios.com.mx/s-venta-inmuebles/estado-de-mexico/page-" + str(i) + "/v1c1097l1014p" + str(i) + "?dw=house,apartment&pr=4000000,&df=ownr"
        urls.append(ur)
        i += 1

    def start_requests(self):


        i=0
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,meta={'Index':i}, dont_filter=True)
            i+=1

    def parse(self, response):

        # pagedetailurl = response.xpath('')
        # response.xpath(
        #     '//div[@class="left-column central"]/div[@id="tileRedesign"]//div[@class="tile-desc one-liner"]/a[@class="href-link tile-title-text"]/@href').extract()

        details = response.xpath('//div[@class="left-column central"]/div[@id="tileRedesign"]//div[@class="tile-desc one-liner"]/a[@class="href-link tile-title-text"]')
        links = details.xpath('.//@href').extract()

        # print(links)
        # print links
        for link in links:
            l = 'https://www.vivanuncios.com.mx' + link
            yield scrapy.Request(url=l, callback=self.parseInside, headers={'User-Agent': 'Mozilla Firefox 12.0'})
        # print l ,dont_filter=True
    def parseInside(self,response):

        # drugname=  response.xpath('//div[@class="col-md-12 title"]/h1/text()').extract()
        t=response.xpath('//script[@type="application/ld+json"]').extract()
        t = ''.join(t)
        phone=''
        try:
            phone=t.split('"telephone":"')[1].split('"}')[0]
        except:
            pass
        price=''
        try:
            price=''.join([x.strip() for x in response.xpath('//h3/span/span[@class="value"]/span[@class="ad-price"]/text()').extract()]).split()[0]
        except:
            pass
        q = response.xpath('//script').extract()
        q = ''.join(q)
        name=''
        try:
            name=q.split('"sellerName":"')[1].split('",')[0]
        except:
            pass
        ubicacion=''
        try:
            ubicacion=q.split('"geoLocDisplayName":"')[1].split('",')[0]
        except:
            pass


        yield {
            "Url" : response.url,
            "Phone": phone,
            "Price": price,
            "Name":name,
            "Ubicacion":ubicacion


        }