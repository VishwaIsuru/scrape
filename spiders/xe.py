import scrapy
import csv

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1',
    'Cache-Control': 'max-age=0',
    'TE': 'trailers',
}
cookies = {
    'property_preferences': '%257B%2522cpOpen%2522%253Afalse%252C%2522comparisonListHeight%2522%253A0%257D',
    '__XE_COOKIE': 'b961e9a15678568d7f0e4b202b9c4e22',
    'xe-data-events-user-id': 'bc5760c1-109c-4c31-965c-3877b9604707',
    '__utma': '175768868.430027483.1628180136.1629450118.1629458103.11',
    '__utmz': '175768868.1628180136.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)',
    '_ga_S00JV588D8': 'GS1.1.1629458097.13.1.1629459339.0',
    '_ga': 'GA1.2.1772021936.1628180136',
    '_fbp': 'fb.1.1628180137615.1779921616',
    '_hjid': 'eeea8de6-d087-4be7-a436-06f41dde4d4e',
    '__gads': 'ID=0822c7cf48ab0fd4:T=1628180138:S=ALNI_Mb-pVYoeORS0LxW7Vw73xVkEeMbZg',
    'intercom-id-bewzsb79': 'e668f345-356c-4046-8b75-1440c6f6a2bc',
    'intercom-session-bewzsb79': '',
    '__SID': 'B53B026A-0195-11EC-B00D-583BF5AEB7B2',
    '__utmc': '175768868',
    '_gid': 'GA1.2.851164482.1629431683',
    '__utmb': '175768868.28.10.1629458103',
    '_hjAbsoluteSessionInProgress': '0',
    '_hjIncludedInSessionSample': '1',
    'reese84': '3:LC20U9ifp6s/9CscJY0U1w==:r4J9iOEjY2xXQaPyG82scCJAFEYty93dwg1Xh9tcyPg1IeU/cQVpLlP50lSWg4bUwBl0fLd8BrMUToCOe6rjsvtjt8hWUqcoMtPghwpVmDyeoIKOSTa0rEWhIjlSgfP1SO2OZ9iE2k+HpF+ZBeudzbK9FgN2Wo4OnHFXLRlezHEoeeCvuytrCybVZ7OsJV91pRU2+nTzW78MhRjRrHda9Ps2KCZpq+2WK82WgkHgQP5oHxVMSkbP9vabzleueUESpeaiW0CVDrTqwfdWkNENmJisdbL+CYYjTcJ5ScZGwJQ0qyCBKiHPclpTBj84Thnt5zXbNDZnEEeNOdmGl9l5ah+qJP0zZEGs1X7h105mXFFyh4L0vfGcwopIBraJXfbwJtLIpVGaPmEDqTLr7k20A4USdNHTqTvXHnUif7+cFdw=:k4xBFYfDbW9SrpmLu+QZIhnM654EdComd/lJRoVJblQ=',
}


class workspider(scrapy.Spider):
    name = "xe"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "url","property Type","Location","city","price","Bedroom","Bathroom","Square Meters",
            "Floor","Details","Description","Advertisement creation","Last modification","Advertisement hits"
                ]
    };
    urls = []

    for k in range(11):
        ur = 'https://www.xe.gr/property/en/search?Geo.area_id_new__hierarchy=82194&System.item_type=re_prof&Transaction.price.from=109000&Transaction.price.to=120000&Transaction.type_channel=117518&per_page=24&page='+str(k)
        urls.append(ur)
        ur1 = 'https://www.xe.gr/property/en/search?Geo.area_id_new__hierarchy=82194&System.item_type=re_prof&Transaction.price.from=120000&Transaction.price.to=140000&Transaction.type_channel=117518&per_page=24&page='+str(k)
        urls.append(ur1)
        # ur2 = 'https://www.xe.gr/property/en/search?Geo.area_id_new__hierarchy=82194&System.item_type=re_prof&Transaction.price.from=140000&Transaction.price.to=170000&Transaction.type_channel=117518&per_page=24&page='+str(k)
        # urls.append(ur2)
        # ur3 = 'https://www.xe.gr/property/en/search?Geo.area_id_new__hierarchy=82194&System.item_type=re_prof&Transaction.price.from=140000&Transaction.price.to=170000&Transaction.type_channel=117518&per_page=24&page='+str(k)
        # urls.append(ur3)
        # ur4 = 'https://www.xe.gr/property/en/search?Geo.area_id_new__hierarchy=82194&System.item_type=re_prof&Transaction.price.from=170000&Transaction.price.to=210000&Transaction.type_channel=117518&per_page=24&page='+str(k)
        # urls.append(ur4)
        # ur5 = 'https://www.xe.gr/property/en/search?Geo.area_id_new__hierarchy=82194&System.item_type=re_prof&Transaction.price.from=59000&Transaction.price.to=69000&Transaction.type_channel=117518&per_page=24&page='+str(k)
        # urls.append(ur5)
        # ur6 = 'https://www.xe.gr/property/en/search?Geo.area_id_new__hierarchy=82194&System.item_type=re_prof&Transaction.price.from=210000&Transaction.price.to=250000&Transaction.type_channel=117518&per_page=24&page='+str(k)
        # urls.append(ur6)
        # ur7 = 'https://www.xe.gr/property/en/search?Geo.area_id_new__hierarchy=82194&System.item_type=re_prof&Transaction.price.from=79000&Transaction.price.to=89000&Transaction.type_channel=117518&per_page=24&page='+str(k)
        # urls.append(ur7)
        # ur8 = 'https://www.xe.gr/property/en/search?Geo.area_id_new__hierarchy=82194&System.item_type=re_prof&Transaction.price.from=89000&Transaction.price.to=109000&Transaction.type_channel=117518&per_page=24&page='+str(k)
        # urls.append(ur8)
        # ur9 = 'https://www.xe.gr/property/en/search?Geo.area_id_new__hierarchy=82271&System.item_type=re_prof&Transaction.price.from=370000&Transaction.price.to=380000&Transaction.type_channel=117518&per_page=24&page='+str(k)
        # urls.append(ur9)
        # ur10 = 'https://www.xe.gr/property/en/search?Geo.area_id_new__hierarchy=82271&Item.area.from=41&Item.area.to=45&System.item_type=re_residence&Transaction.price.from=80000&Transaction.price.to=90000&Transaction.type_channel=117518&per_page=24&page='+str(k)
        # urls.append(ur10)
        # ur11 = 'https://www.xe.gr/property/en/search?Geo.area_id_new__hierarchy=82271&Item.area.from=46&Item.area.to=49&System.item_type=re_residence&Transaction.price.from=80000&Transaction.price.to=90000&Transaction.type_channel=117518&per_page=24&page='+str(k)
        # urls.append(ur11)
        # ur12 = 'https://www.xe.gr/property/en/search?Geo.area_id_new__hierarchy=82271&Item.area.from=50&Item.area.to=50&System.item_type=re_residence&Transaction.price.from=80000&Transaction.price.to=90000&Transaction.type_channel=117518&per_page=24&page='+str(k)
        # urls.append(ur12)


    def start_requests(self):

        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers= headers,cookies= cookies
                                 )

    def parse(self, response):
        allprd = response.xpath('//a[@class="articleLink "]//@href').extract()
        print(allprd)
        for prd in allprd:
            proplink = 'https://www.xe.gr'+prd
            yield scrapy.Request(url=proplink, callback=self.toAllThemes,
                                 headers= headers, cookies = cookies,dont_filter=True)

    def toAllThemes(self, response):
        ptype = response.xpath('//div[@class="info-title"]//h1//text()').extract_first()
        city = ''.join([x.strip() for x in response.xpath('//span[@class="geo"]//text()').extract()])
        price = ''.join([x.strip() for x in response.xpath('//div[@class="price"]//h1//text()').extract()])
        area = response.xpath('//div[@class="area"]//h1//text()').extract_first()
        bedroom = ''
        bathroom = ''
        brooms = response.xpath('//div[@class="baths"]')
        for brm in brooms:
            lable = brm.xpath('.//span[@class="label"]//text()').extract_first()
            rmcont = brm.xpath('.//h1//text()').extract_first()
            if 'Bedrooms' in lable:
                bedroom = rmcont
            if 'Bathrooms' in lable:
                bathroom = rmcont
        detailss = []
        detail = response.xpath('//ul[@class="spec"]//li')
        for det in detail:
            dettex = ''.join([x.strip() for x in det.xpath('.//text()').extract()])
            detailss.append(dettex)
        details = ','.join(detailss)
        floor = ''
        for h in detailss:
            if 'Floor' in h:
                floor = h.replace('Floor:','')
        description = ''.join([x.strip() for x in response.xpath('//div[@class="description-content"]//text()').extract()])
        adcreation = ''
        lstmodifiction = ''
        adhitsby = ''
        adstats = response.xpath('//div[@class="stats-content"]//div')
        for ads in adstats:
            adtext = ads.xpath('.//text()').extract_first()
            if 'Advertisement creation:' in adtext:
                adcreation = adtext.replace('Advertisement creation:','')
            if 'Last modification:' in adtext:
                lstmodifiction = adtext.replace('Last modification:','')
            if 'Advertisement hits by now:' in adtext:
                adhitsby = adtext.replace('Advertisement hits by now:','')

        yield {
            "url": response.url,
            "property Type" : ptype,
            "Location" : 'Thessaloniki',
            "city" : city,
            "price" : price,
            "Bedroom": bedroom,
            "Bathroom": bathroom,
            "Square Meters" : area,
            "Floor": floor,
            "Details": details,
            "Description": description,
            "Advertisement creation": adcreation,
            "Last modification": lstmodifiction,
            "Advertisement hits": adhitsby
        }