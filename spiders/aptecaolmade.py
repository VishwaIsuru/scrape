import scrapy
import csv
import codecs


class WorkSpider(scrapy.Spider):
    name = 'aptekapomo'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            "url","Product_Name","cat","Main_Image","Product_Price","Kod_Producent","Description"


        ]
    };

    urls = [
        # "https://www.aptekaolmed.pl",
        "https://aptekapomocna24.pl"

    ]


    def start_requests(self):

        # f = codecs.open("/home/vishwa/aptekpomo.xml", "w+", encoding='utf8')
        # contents = "\n".join(['<?xml version="1.0" encoding="utf-8"?><offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1">','</offers>'])
        # f.write(contents)
        # f.flush()
        # f.close()

        i = 0
        for u in self.urls:

            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        details = response.xpath('//div[@id="menu_categories"]/ul/li//a')

        links = details.xpath('./@href').extract()



        for link in links:
            l = 'https://aptekapomocna24.pl' + link
            # print(l)
            yield scrapy.Request(url=l,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True)

            # break



    def parseInside(self,response):

        pagingUrls = []
        try:

            paging= response.css('#paging_setting_top > div:nth-child(2) > div:nth-child(1) > a:nth-child(4)').xpath('./text()').extract_first()
            last =response.xpath('//div[@class="search_paging"]//@href').extract_first()
            lsurl=last.split('all')[0]

            i = 0
            while i < int(paging):
                pgurl = "https://aptekapomocna24.pl"+lsurl+str(i)
                pagingUrls.append(pgurl)
                i += 1
        except:
            pagingUrls.append(response.url)
            # pass

        for pagesofprdt in pagingUrls:
            yield scrapy.Request(url=pagesofprdt,callback=self.toAllPrdt,headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True)


    def toAllPrdt(self,response):

        # print (response.url)

        # yield {
        #     "url": response.url
        # }

        prducturl = response.xpath('//div[@id="search"]/div/div/a[@class="product-name"]')

        prdlinks = prducturl.xpath('./@href').extract()

        for prdlink in prdlinks:
            linn = 'https://aptekapomocna24.pl' + prdlink
            yield scrapy.Request(url=linn, callback=self.prdDetails, headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True)
    #
    def prdDetails(self,response):

        prdname= response.css('.product_info > h1:nth-child(1)').xpath('./text()').extract()
        cat = response.css('#breadcrumbs').xpath('.//text()').extract()
        cat = cat[-2]
        image = response.xpath('//a[@class="projector_medium_image"]/img/@src').extract_first()
        imageurl = 'https://aptekapomocna24.pl'+image
        prdprice= response.xpath('//strong[@id="projector_price_value"]/text()').extract_first()

        kodproducer= 'EAN'
        descri= '\n'.join([x.strip('') for x in response.css('#component_projector_longdescription').xpath('.//text()').extract() if x.strip()!=''])

        # line = '<o  price="%s" url="%s"><cat><![CDATA[%s]]></cat><Product_Price><![CDATA[%s]]></Producr_Price><Main_Image><main url="%s"/></Main_Image><Description><![CDATA[%s]]></Description><attrs><a name="Kod_producenta"><![CDATA[%s]]></a></attrs></o>' % (
        #     prdprice,response.url, cat, prdname, imageurl, descri, kodproducer)

        yield {
            "url":response.url,
            "Product_Name":prdname,
            "cat":cat,
            "Main_Image":imageurl,
            "Product_Price":prdprice,
            "Kod_Producent":kodproducer,
            "Description":descri
        }

        # f = codecs.open("/home/vishwa/aptekpomo.xml", "r", encoding='utf8')
        # contents = f.readlines()
        # contents = [a for a in contents if a.strip() != '']
        # f.close()
        #
        # contents.insert(len(contents) - 1, line.strip())
        #
        # f = codecs.open("/home/vishwa/aptekpomo.xml", "w", encoding='utf8')
        # contents = "\n".join(contents)
        # f.write(contents)
        # f.flush()
        # f.close()
