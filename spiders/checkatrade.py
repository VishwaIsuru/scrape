import scrapy
import csv
from csv import reader
import json
import requests

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Accept': '	text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
}

class workspider(scrapy.Spider):
    name = "checkatrade"

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': ['postalcode','name','trade','operatingLocationText','number',
                               'locality','region','recentMeanScore','totalReviews','uniqueName'
                               ]
    };

    def start_requests(self):

        postcodes = []
        with open('postcodeNew.csv', 'r', encoding="utf8",errors='ignore') as read_obj1:
            csv_reader = reader(read_obj1)
            for row in csv_reader:
                postcode = row[2]
                postcodes.append(postcode)

        for i in range(1, 2000):
            for j in range(1,182):
                ur = 'https://search.checkatrade.com/api/v1/trade-card/search?filter=categoryId[eq]:'+str(i)+';postcode[eq]:'+postcodes[j]+'&page=1&size=10&sort=relevance.desc'
                yield scrapy.Request(url=ur, callback=self.parse_inside, dont_filter=True,headers=headers,
                                     meta={'trade':i,'postalcode':postcodes[j],}
                                     )

    def parse_inside(self,response):
        url = response.url
        trade = response.meta['trade']
        postalcode = response.meta['postalcode']
        data = json.loads(response.body)

        totalpgs  = data['meta']['totalPages']
        totalresult = data['meta']['totalResults']
        # print(totalpgs)
        if int(totalresult) > 0:
            for p in range(1,int(totalpgs)+1):
                pgurl = url.replace('page=1','page='+str(p))
                # print('pagge ', pgurl)
                yield scrapy.Request(url=pgurl, callback=self.parse_inside_trade, dont_filter=True, headers=headers,
                                     meta={'trade': trade, 'postalcode': postalcode }
                                     )

    def parse_inside_trade(self,response):
        trade = response.meta['trade']
        postalcode = response.meta['postalcode']
        data = json.loads(response.body)

        items = data['items']
        for item in items:
            name = item['name']
            operatingLocationText = item['operatingLocationText']
            number = item['contact']['primary']['label']
            locality = item['address']['locality']
            region = item['address']['region']
            recentMeanScore = item['reviewsSummary']['recentMeanScore']
            totalReviews = item['reviewsSummary']['totalReviews']
            uniqueName = item['uniqueName']

            yield {
                'postalcode': postalcode,
                'name': name,
                'trade': trade,
                'operatingLocationText': operatingLocationText,
                'number': number,
                'locality': locality,
                'region': region,
                'recentMeanScore': recentMeanScore,
                'totalReviews': totalReviews,
                'uniqueName': uniqueName
            }