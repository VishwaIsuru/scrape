import scrapy
import csv

class WorkSpider(scrapy.Spider):
    name = 'growjob'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'Name','Country','Funding'
        ]
    };

    urls = ["https://growjo.com/industry/Fintech/1",
            "https://growjo.com/industry/Fintech/2",
            "https://growjo.com/industry/Fintech/3","https://growjo.com/industry/Fintech/4",
            "https://growjo.com/industry/Fintech/5","https://growjo.com/industry/Fintech/6"
            ]

    def start_requests(self):
        i = 0
        for u in self.urls:

            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):
        # l = response.xpath("//*[contains(text(), 'Total Funding:')]//text()").getall()
        g = response.xpath('//tr')
        for k in g:
            name = ''.join([x.strip() for x in k.xpath('.//td[2]//text()').extract()])
            country = k.xpath('.//td[5]//text()').extract_first()
            funfind = k.xpath('.//td[6]//text()').extract_first()

            yield {
                'Name':name,
                'Country': country,
                "Funding": funfind
            }