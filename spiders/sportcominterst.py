import scrapy
import csv
from datetime import datetime
import pandas as pd

class workspider(scrapy.Spider):
    name = "sportscomint"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Id","Url","Collage","Offer","Status","Date","RecruitedBy"
        ]
    };
    urls = []

    def start_requests(self):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'Cache-Control': 'max-age=0',
        }
        col_list = ["url","ViewComplittemlistUrl"]
        df = pd.read_csv("sportcom21to25Allprt1.csv", usecols=col_list)
        inturls = df["ViewComplittemlistUrl"]
        idurl = df['url']
        l = len(inturls)
        for i in range(0, l):
            pid = idurl[i].split('/')[4]
            yield scrapy.Request(inturls[i], callback=self.parse,headers = headers,meta={'id':pid})
        # yield scrapy.Request("https://247sports.com/Recruitment/JC-Latham-113384/RecruitInterests/", callback=self.parse, headers=headers,)

    def parse(self, response):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'Cache-Control': 'max-age=0',
        }
        pid = response.meta['id']
        url = response.url

        interslist = response.xpath('//ul[@class="recruit-interest-index_lst"]//li')
        for li in interslist:
            collage = li.xpath('.//div[@class="first_blk"]/a//text()').extract_first()
            offrspn = ''.join([x.strip('') for x in li.xpath('.//span[@class="offer"]//text()').extract()])
            if 'Yes' in offrspn:
                offer = 1
            else:
                offer = 0
            status = ''.join([x.strip('') for x in li.xpath('.//div[@class="first_blk"]//span[@class="status"]//text()').extract()]).replace('Status:','')
            date = status.split('(')[-1].replace(')','')
            recruitedby = ''.join([x.strip('') for x in li.xpath('.//ul[@class="interest-details_lst"]//text()').extract()])

            if collage is not None:
                yield {
                    "Id": pid,
                    "Url": url,
                    "Collage": collage,
                    "Offer": offer,
                    "Status": status,
                    "Date": date,
                    "RecruitedBy": recruitedby
                }
