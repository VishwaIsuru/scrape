import scrapy
import csv
import codecs

class WorkSpider(scrapy.Spider):
    name = 'amazonBrandSub2024'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'lastcat','pagin','sndpgurl'
        ]
    };

    def start_requests(self):
        u = "https://www.amazon.com/s?k=international+navistar&i=automotive&crid=2V3Q9AMIV5FEP&sprefix=international+navistar%2Cautomotive%2C74&ref=nb_sb_noss_1"
        yield scrapy.Request(url=u, callback=self.parse,
                         headers={'User-Agent': 'Mozilla Firefox 12.0'},
                         )


    def parse(self, response):
        fclinks = response.xpath('//li[@class="a-spacing-micro s-navigation-indent-2"]//@href').extract()
        sncatTexts = []
        for fc in fclinks:
            fscat = 'https://www.amazon.com' + fc
            yield scrapy.Request(url=fscat, callback=self.parseSecnd,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},meta={'subcattexts':sncatTexts}
                                 )

    def parseSecnd(self,response):
        snlinks = response.xpath('//li[@class="a-spacing-micro s-navigation-indent-2"]//@href').extract()
        sncatTexts = response.xpath('//li[@class="a-spacing-micro s-navigation-indent-2"]//text()').extract()
        subcattexts = response.meta['subcattexts']
        if len(snlinks) > 0 and subcattexts != sncatTexts:
            for u in snlinks:
                yield scrapy.Request(url='https://www.amazon.com'+str(u), callback=self.parseSecnd,
                                     headers={'User-Agent': 'Mozilla Firefox 12.0'},meta={'subcattexts':sncatTexts}
                                     )
        else:
            try:
                lspg = response.xpath('//span[@class="s-pagination-item s-pagination-disabled"]//text()').extract_first()
            except:
                lspg = '3'
            try:
                sndpaglin = response.xpath('//a[@class="s-pagination-item s-pagination-button"]//@href').extract_first()
            except:
                sndpaglin = response.url

            yield {
                'lastcat': response.url,
                'pagin': lspg,
                'sndpgurl': sndpaglin
            }

