import scrapy
import csv
import pandas as pd
import requests
import json
import csv

class workspider(scrapy.Spider):
    name = "sport247jsoncoach"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Url",
                ]
    };

    def start_requests(self):
        for i in range(1, 100):
            couchurl = 'https://247sports.com/Coach.json?Page='+str(i)

            headers = {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.5',
                'Connection': 'keep-alive',
                'Host': '247sports.com',
                # 'TE': 'Trailers',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100100 Firefox/87.0'
            }

        # for u in self.urls:
            yield scrapy.Request(url=couchurl, callback=self.parse,headers=headers,dont_filter=True,meta= {'page':i})

    def parse(self, response):
        pgnum = response.meta['page']
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.5',
            'Connection': 'keep-alive',
            'Host': '247sports.com',
            # 'TE': 'Trailers',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100100 Firefox/87.0'
        }

        resurl = response.url

        responseply = requests.get(resurl, headers=headers)
        print(responseply)
        d= responseply.status_code
        print(d)
        if (d== 200):
            plyjsont = json.loads(responseply.content)
            print(plyjsont)

            jsonfile = 'sports247coachset1.json'
            with open(jsonfile, 'a') as f:
                f.write(json.dumps(plyjsont))
                f.write(",")
                f.close()

