# import scrapy
# import csv
# from datetime import datetime
# import pandas as pd
#
#
# csvRow = ['ID','College','Offer','Status','Date','Recruited By','URL']
# with open('ESPNSigningStatus.csv', "wb") as fp1:
#     wr = csv.writer(fp1, dialect='excel',delimiter='\t',lineterminator='\n')
#     wr.writerow(csvRow)
#
# class workspider(scrapy.Spider):
#     name = "espnfootball"
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': [
#             "ID","Firstname","Lastname","Position","Height","Weight","HighSchool","HomeTown","State",
#             "GraduationClass","Committed College","Class","ESPN300","ESPNRating",
#             "ESPNStars","ESPN Regional Ranking","ESPN Positional Ranking","ESPN State Ranking","URL"
#         ]
#     };
#     urls = [
#         # "http://www.espn.com/college-sports/football/recruiting/playerrankings/_/class/2006/view/state/order/true/state/alabama"
#     ]
#     years = ['2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020','2021','2022']
#     stateof = ['alaska','alabama','arkansas','arizona','colorado','connecticut','washington-d.c.','delaware','florida','georgia',
#                'hawaii','iowa','idaho','illinois','indiana','kansas','kentucky','louisiana','massachusetts','maryland','maine',
#                'michigan','minnesota','missouri','mississippi','montana','north-carolina','north-dakota','nebraska','new-hampshire',
#                'new-jersey','new-mexico','nevada','new-york','ohio','oklahoma','oregon','pennsylvania','rhode-island','south-carolina',
#                'south-dakota','tennessee','texas','utah','virginia','vermont','washington','wisconsin','west-virginia','wyoming',
#                'puerto-rico','china']
#
#     for y in years:
#         uy = "http://www.espn.com/college-sports/football/recruiting/playerrankings/_/class/"+str(y)+"/view/state/order/true/state/"
#         for st in stateof:
#             u = uy+str(st)
#             urls.append(u)
#
#     for yr in years:
#         clfn = "http://www.espn.com/college-sports/football/recruiting/playerrankings/_/class/"+str(yr)+"/view/state/order/true"
#         urls.append(clfn)
#
#     def start_requests(self):
#         for url in self.urls:
#             yield scrapy.Request(url, callback=self.parse)
#
#     def parse(self, response):
#         allplyrurls = response.xpath('//div[@class="name"]//a//@href').extract()
#         for plurl in allplyrurls:
#             yield scrapy.Request(plurl, callback=self.parseInside)
#
#     def parseInside(self,response):
#         id= response.url.split('/id/')[-1]
#         names = response.xpath('//div[@class="player-name"]//h1//text()').extract_first()
#         fsname = names.split(' ')[0]
#         lsname = names.split(' ')[-1]
#         position = response.css('.bio > ul:nth-child(2) > li:nth-child(3) > a:nth-child(2)').xpath('.//text()').extract_first()
#         biodtl = ''.join([x.strip('') for x in response.css('.bio > p:nth-child(1)').xpath('.//text()').extract()])
#         height = biodtl.split('|')[0].split(',')[0].replace('\n\t\t\t\t\t\t\t','')
#         weight = biodtl.split('|')[0].split(',')[-1].replace('\n\t\t\t\t\t\t\t','')
#         highschool = response.css('.bio > ul:nth-child(2) > li:nth-child(2) > a:nth-child(2)').xpath('.//text()').extract_first()
#         hometowns = response.css('.bio > ul:nth-child(2) > li:nth-child(1) > a:nth-child(2)').xpath('.//text()').extract_first()
#         hometown = hometowns.split(',')[0]
#         state = hometowns.split(',')[-1]
#         graclass = biodtl.split('|')[-1].replace(' Class of ','')
#         commitedcollage = response.css('.bio > ul:nth-child(2) > li:nth-child(4) > a:nth-child(2)').xpath('.//text()').extract_first()
#         espn300 = response.css('.rank > span:nth-child(1)').xpath('.//text()').extract_first()
#         espnrating = response.xpath('//ul[@class="mod-rating"]//li[1]//text()').extract_first().replace('\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t','')
#         espnstarscunts = response.xpath('//ul[@class="mod-rating"]//li[2]//@class').extract_first()
#         espnstarscunt = ''
#         if 'five' in espnstarscunts:
#             espnstarscunt = '5'
#         if 'four' in espnstarscunts:
#             espnstarscunt = '4'
#         if 'three' in espnstarscunts:
#             espnstarscunt = '3'
#         if 'two' in espnstarscunts:
#             espnstarscunt = '2'
#         if 'one' in espnstarscunts:
#             espnstarscunt = '1'
#
#         espregionalranking = response.css('.regional > span:nth-child(1)').xpath('.//text()').extract_first()
#         esppositionrankin = response.css('td.position > span:nth-child(1)').xpath('.//text()').extract_first()
#         espstateranking = response.css('.state > span:nth-child(1)').xpath('.//text()').extract_first()
#
#         try:
#             signdate = response.xpath('//div[@class="bio"]//li[4]//text()').extract()[3]
#         except:
#             signdate = ''
#
#         schlists = response.xpath('//div[@class="mod-content no-footer tabular"]//tr')
#         for school in schlists:
#             schoolnm = school.xpath('.//a//text()').extract_first()
#             schstatus = school.xpath('.//td[2]//text()').extract_first()
#
#             print (schoolnm)
#             schoolcomit =''
#             comitdate = ''
#             if schstatus is not None and 'Signed' in schstatus:
#                 schoolcomit = 'Committed'
#                 comitdate = signdate
#                 # or 'Committed' in schstatus
#
#             offrimg = school.xpath('.//td[3]//@src').extract_first()
#             offer = ''
#             if offrimg is not None and 'https://a.espncdn.com/i/recruiting/collegesites/icon-check.png' in offrimg:
#                 offer = 1
#             print (offer)
#             print (schstatus)
#             print (schoolcomit)
#             req = ''
#             csvfilnm1 = 'ESPNSigningStatus.csv'
#             csvRow = (id, schoolnm,offer,schoolcomit,comitdate,req,response.url)
#             with open(csvfilnm1, "a+") as fp:
#                 wr = csv.writer(fp, dialect='excel',delimiter='\t',lineterminator='\n')
#                 wr.writerow(csvRow)
#
#         yield {
#             "ID": id,
#             "Firstname": fsname,
#             "Lastname": lsname,
#             "Position": position,
#             "Height": height,
#             "Weight": weight,
#             "HighSchool": highschool,
#             "HomeTown": hometown,
#             "State": state,
#             "GraduationClass": graclass,
#             "Committed College": commitedcollage,
#             "Class": graclass,
#             "ESPN300": espn300,
#             "ESPNRating": espnrating,
#             "ESPNStars": espnstarscunt,
#             "ESPN Regional Ranking": espregionalranking,
#             "ESPN Positional Ranking": esppositionrankin,
#             "ESPN State Ranking": espstateranking,
#             "URL": response.url
#         }