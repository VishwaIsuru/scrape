import scrapy
import csv
import pandas as pd
import requests
import json
import csv

class workspider(scrapy.Spider):
    name = "sport247jsoninstireqdepthdtl2"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Url",
                ]
    };

    def start_requests(self):
        col_list = ["Key"]
        df = pd.read_csv("plyprimaryreqmentjsoninstitypeCompine.csv", usecols=col_list)
        urls = df["Key"]
        print(len(urls))
        # hist1 = []24560 133921
        for i in range(70000, len(urls)):
            k= str(urls[i]).replace('.0','')
            plyrurl = 'https://247sports.com/Recruitment/' + str(k) + '/RecruitInterestDepthDetail.json'
            # print (str(urls[i]))
            # plyrurl = urls[i]
            headers = {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.5',
                'Connection': 'keep-alive',
                'Host': '247sports.com',
                # 'TE': 'Trailers',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100100 Firefox/87.0'
            }

        # for u in self.urls:
            yield scrapy.Request(url=plyrurl, callback=self.parse,headers=headers,dont_filter=True,meta= {'page':i})

    def parse(self, response):
        pgnum = response.meta['page']
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.5',
            'Connection': 'keep-alive',
            'Host': '247sports.com',
            # 'TE': 'Trailers',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100103 Firefox/87.0'
        }

        resurl = response.url

        responseply = requests.get(resurl, headers=headers)
        print(responseply)
        d= responseply.status_code
        print(d)
        if (d== 200):
            plyjsont = json.loads(responseply.content)
            print(plyjsont)

            for j in range(70000,129237,20000):
                # print(j) 129237
                if (pgnum>j and pgnum<j+20000):
                    jsonfile = 'sport247jsoninstireqdepthdtl'+str(j)+'.json'
                    with open(jsonfile, 'a') as f:
                        f.write(json.dumps(plyjsont))
                        f.write(",")
                        f.close()

        if (d != 200):
            print('jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj')

            csvRow = (str(resurl), str(d))
            csvfile = "plyprmaryreqmjsonerror2.csv"
            with open(csvfile, "a+") as fp:
                wr = csv.writer(fp, dialect='excel', lineterminator='\n')
                wr.writerow(csvRow)
