import csv
import pandas as pd
import scrapy
import re
import json
import csv
import re

csv.register_dialect('myDialect', delimiter='/', quoting=csv.QUOTE_NONE)

cookies = {
    'general_session': '2004E33CF41611EBB17D9DB7B1AAC5B6',
}

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
}

class workspider(scrapy.Spider):
    name = "nhs"
    # custom_settings = {
    #     # specifies exported fields and order
    #     'FEED_EXPORT_FIELDS': ["Link"
    #                            ]
    # };
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': ["Link",
                               "Job Name",    "Job Reference",    "Employer" ,   "Department",    "Location",    "Salary",    "Job Type",
                               "Working Pattern",    "Pay Scheme",    "Pay Band",    "Staff Group",    "Specialty",    "Closing Date",
                                "Job Description",    "Person Specification","Date Posted",
                                "Contact Name1","Job Title1","Job Division1", "Email1", "Telephone1",
                                "Contact Name2","Job Title2","Job Division2", "Email2", "Telephone2",
                                "Contact Name3","Job Title3","Job Division3", "Email3", "Telephone3",
                                "Contact Name4","Job Title4","Job Division4", "Email4", "Telephone4",
                                "Contact Name5","Job Title5","Job Division5", "Email5", "Telephone5",
                               ]
    };
    #
    # def start_requests(self):
    #     for i in range(1,1152):
    #         url = f"https://www.jobs.nhs.uk/xi/search_vacancy?action=page&page={i}"
    #         yield scrapy.Request(url=url, callback=self.parse_inside,
    #                              headers=headers,cookies=cookies)
    #
    # def parse_inside(self, response):
    #     print(response.body)
    #     links=response.css('.vacancy h2 a').xpath('./@href').extract()
    #     links=['https://www.jobs.nhs.uk'+link for link in links]
    #
    #     for link in links:
    #         yield {
    #             'Link':link
    #         }

    def start_requests(self):
        df = pd.read_csv('nhslink3.csv', header=0, delimiter=',')
        i = 0
        for index, row in df.iterrows():
            url=row["Link"]
            # url ='https://www.jobs.nhs.uk/xi/direct_apply/?vac_ref=916296420' nhsBetaDataLoseSalary.csv
            print(url)
            yield scrapy.Request(url=url, callback=self.parse_inside,
                                 headers=headers,cookies=cookies)
            i+=1
            # breakS

    def parse_inside(self,response):
        url = response.url
        contact_names = ['']*5
        contact_emails = ['']*5
        contact_tels = ['']*5
        contact_name_index = 0
        contact_email_index = 0
        contact_tel_index = 0
        try:
            job_name = ''.join(response.css('.vacancyContainer h1')[0].xpath('./text()').extract()).strip()
            job_reference = ''.join(response.css('.vacancyContainer h2.h2Strong strong').xpath('./text()').extract()).strip()
            left_datas = response.css('.vacancyContainer .info .pairedData').xpath('./*//text()').extract()
            employer = ''
            department = ''
            location = ''
            salary = ''
            paidata = response.xpath('//dl[@class="pairedData"]//dt//text()').extract()
            paidatatext = response.xpath('//dl[@class="pairedData"]//dd//text()').extract()
            k=0
            for pdt in paidata:
                if pdt == 'Employer:':
                    employer = paidatatext[k]
                if pdt == 'Location:':
                    location = paidatatext[k]
                if pdt == 'Salary:':
                    salary = paidatatext[k]
                if pdt == 'Department:':
                    department = paidatatext[k]
                k+=1
            # i=0
            # for data in left_datas:
            #     if data.strip().replace(':', '') == 'Employer':
            #         employer=left_datas[i+1].strip()
            #     elif data.strip().replace(':', '') == 'Location':
            #         location=left_datas[i+1].strip()
            #     elif data.strip().replace(':', '') == 'Salary':
            #         salary=left_datas[i].strip()
            #     elif data.strip().replace(':', '')  == 'Department':
            #         department=left_datas[i+1].strip()

            right_datas = response.css('.vacancyContainer .pairedData').xpath('./*//text()').extract()

            job_type = ''
            working_pattern = ''
            pay_scheme = ''
            pay_band = ''
            staff = ''
            speciality = ''
            closing_date = ''
            contact_name = ''
            contact_email = ''
            contact_phone = ''

            i=0
            for data in right_datas:
                if data.strip().replace(':','') == 'Job Type':
                    job_type=right_datas[i+1].strip().strip()
                elif data.strip().replace(':','') == 'Working pattern':
                    p=1
                    while True:
                        try:
                            if right_datas[i + p].replace(':','').strip() in ['Pay Scheme','Pay Band','Staff Group','Specialty/Function','Closing Date','Email Address','Telephone']:
                                break
                            working_pattern += right_datas[i + p].strip()+'\n'
                            p+=1
                        except:
                            break
                elif data.strip().replace(':','') == 'Pay Scheme':
                    pay_scheme = right_datas[i + 1].strip().strip()
                elif data.strip().replace(':','') == 'Pay Band':
                    pay_band = right_datas[i + 1].strip().strip()
                elif data.strip().replace(':','') == 'Staff Group':
                    staff = right_datas[i + 1].strip().strip()
                elif data.strip().replace(':', '') == 'Specialty/Function':
                    speciality = right_datas[i + 1].strip().strip()
                elif data.strip().replace(':', '') == 'Closing Date':
                    closing_date = right_datas[i + 1].strip().strip()
                elif data.strip().replace(':', '') == 'Contact Name':
                    contact_name = right_datas[i + 1].strip().strip()
                elif data.strip().replace(':', '') == 'Email Address':
                    contact_email = right_datas[i + 1].strip().strip()
                elif data.strip().replace(':', '') == 'Telephone':
                    contact_phone = right_datas[i + 1].strip().strip()
                    contact_phone=re.sub('[^0-9/]*', '', contact_phone)
                i+=1
            if contact_name!='':
                names = contact_name.split('/')
                for i in range(len(names)):
                    contact_names[i]=names[i].strip()
                contact_name_index=len(names)
            if contact_email!='':
                mails = contact_email.split('/')
                for i in range(len(mails)):
                    contact_emails[i] = mails[i]
                contact_email_index=len(mails)
            if contact_phone!='':
                tels = contact_phone.split('/')
                for i in range(len(tels)):
                    contact_tels[i]=tels[i]
                contact_tel_index=len(tels)
            job_specification = ""
            person_specification = ""
            links = response.css('.vacancyContainer .mainDocs.panel').xpath('.//li/a')
            for link in links:
                text = link.xpath('./text()').extract_first()
                l ='https://www.jobs.nhs.uk'+ link.xpath('./@href').extract_first()

                if 'Job Description' in text:
                    job_specification=l
                elif 'Person' in text:
                    person_specification=l

            des = ' '.join(response.css('.description').xpath('.//text()').extract())
            emails = re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", des)
            emails = [email for email in emails if email not in contact_email]
            nname=''
            if emails:
                nname = emails[0].split('@')[0].split('.')[0]
            contact_email_index=contact_email_index if contact_email_index>contact_name_index else contact_name_index
            if emails:
                for i in range(len(emails)):
                    contact_emails[contact_email_index] = emails[i]
                    nname = emails[i].split('@')[0].split('.')[0]
                    if '.' in emails[i].split('@')[0]:
                        last_name=emails[i].split('@')[0].split('.')[1]
                        last_name=re.sub("[^a-zA-Z]+", "", last_name)
                        nname+=' '+last_name
                    contact_names[contact_email_index] = nname.title()
                    contact_email_index += 1

            a = re.sub('[^0-9 -]', '', des)
            b = re.sub('  +', '@@@', a)
            b = b.replace(' ','').replace('-','')
            numbers=re.findall('[0-9]+', b)
            numbers = [num for num in numbers if len(num) == 10 or len(num) == 11]
            numbers = [num for num in numbers if num not in contact_phone]
            for number in numbers:
                contact_tels[contact_tel_index]=number
                contact_tel_index+=1
            numbers = '\n'.join(numbers)
            # print(contact_names+" "+contact_tels+" "+contact_emails)
            yield {
                "Link":url,
                "Job Name":job_name, "Job Reference":job_reference, "Employer":employer, "Department":department, "Location":location, "Salary":salary, "Job Type":job_type,
                "Working Pattern":working_pattern.strip(), "Pay Scheme":pay_scheme, "Pay Band":pay_band, "Staff Group":staff, "Specialty":speciality, "Closing Date":closing_date,
                "Job Description":job_specification,"Date Posted": ' ',
                "Person Specification":person_specification,
                  "Contact Name1":contact_names[0], "Job Title1":"", "Job Division1":"", "Email1":contact_emails[0], "Telephone1":contact_tels[0],
                  "Contact Name2":contact_names[1], "Job Title2":"", "Job Division2":"", "Email2":contact_emails[1], "Telephone2":contact_tels[1],
                  "Contact Name3":contact_names[2], "Job Title3":"", "Job Division3":"", "Email3":contact_emails[2], "Telephone3":contact_tels[2],
                  "Contact Name4":contact_names[3], "Job Title4":"", "Job Division4":"", "Email4":contact_emails[3], "Telephone4":contact_tels[3],
                  "Contact Name5":contact_names[4], "Job Title5":"", "Job Division5":"", "Email5":contact_emails[4], "Telephone5":contact_tels[4],
            }

        except Exception as e:

            try:
                redirecturl = response.xpath('//div[@class="content "]//div[@class="buttons standAlone"]//a/@href').extract_first()
                redirectcrurl = "https://www.jobs.nhs.uk"+str(redirecturl)
                yield scrapy.Request(url=redirectcrurl, callback=self.parse_continue,
                                     headers=headers, cookies=cookies)

            except:
                redirectcrurl = response.url
                yield scrapy.Request(url=redirectcrurl, callback=self.parse_inside,
                                     headers=headers, cookies=cookies)

            print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
            print(redirectcrurl)


    def parse_continue(self,response):

        link = response.url
        print('fdfdfdfffffffffffffffffffffffffffffffffffff')
        print(link)
        date_posted = response.css('#date_posted').xpath('./text()').extract()
        job_name = response.css('#heading').xpath('./text()').extract()
        job_reference = response.url.split('/')[-1]
        employer = response.css('#employer_name').xpath('./text()').extract()
        department = ''
        try:
            loc1 = response.css('#employer_address_line_1').xpath('./text()').extract_first()
        except:
            loc1 = ''
        try:
            loc2 = response.css('#employer_address_line_2').xpath('./text()').extract_first()
        except:
            loc2 = ''
        try:
            loc3 = response.css('#employer_town').xpath('./text()').extract_first()
        except:
            loc3 = ''
        try:
            loc4 = response.css('#employer_county').xpath('./text()').extract_first()
        except:
            loc4 = ''
        try:
            loc5 = response.css('#employer_postcode').xpath('./text()').extract_first()
        except:
            loc5 = ''

        location = str(loc1)+","+str(loc2)+","+str(loc3)+","+str(loc4)+","+str(loc5)
        try:
            salary = response.xpath("//h3[contains(text(), 'Salary')]/following-sibling::p[1]/text()").extract()
        except:
            try:
                salary = response.css('#range_salary').xpath('./text()').extract_first()
            except:
                salary = response.css('#negotiable_salary').xpath('./text()').extract_first()
        job_type = response.css('#contract_type').xpath('./text()').extract()
        working_pattern = '\n'.join([x.strip().replace(',', '') for x in response.xpath(
            "//h3[contains(text(), 'Working pattern')]/following-sibling::p[1]/text()").extract() if x.strip() != ''])
        working_pattern = re.sub('  +', '', working_pattern)
        working_pattern = re.sub('\n+', '\n', working_pattern).strip()
        pay_scheme = response.css('#payscheme-type').xpath('./text()').extract()
        pay_band = response.css('#payscheme-band').xpath('./text()').extract()
        staff = ' '
        speciality = ' '
        closing_date = response.css('#closing_date').xpath('./text()').extract_first().replace('The closing date is ','')
        job_specification = '\n'.join([x.strip() for x in response.xpath("//text()[following::h2/text()='Person Specification' and preceding::h2/text()='Job description']").extract()])
        person_specification = '\n'.join([x.strip() for x in response.xpath("//text()[following::h2/text()='Employer details' and preceding::h2/text()='Person Specification']").extract()])
        contctname1 = response.css('#contact_details_name').xpath('./text()').extract_first()
        contemail = response.css('#contact_details_email').xpath('./a/text()').extract()
        contctnum1 = response.css('#contact_details_number').xpath('./text()').extract()
        contavnm2 = 'technical officer'
        contceml2 = response.css('#technical_support_contact_details_email').xpath('./text()').extract()
        contactph3 = response.css('#technical_support_contact_details_number').xpath('./text()').extract()

        yield {
            "Link": link ,
            "Job Name": job_name, "Job Reference": job_reference, "Employer": employer, "Department": department,
            "Location": location, "Salary": salary, "Job Type": job_type,
            "Working Pattern": working_pattern, "Pay Scheme": pay_scheme, "Pay Band": pay_band,
            "Staff Group": staff, "Specialty": speciality, "Closing Date": closing_date,
            "Job Description": job_specification, "Date Posted": date_posted,
            "Person Specification": person_specification,
            "Contact Name1": contctname1, "Job Title1": "", "Job Division1": "", "Email1": contemail,
            "Telephone1": contctnum1,
            "Contact Name2": contavnm2, "Job Title2": "", "Job Division2": "", "Email2": contceml2,
            "Telephone2": contactph3,
            "Contact Name3": " ", "Job Title3": "", "Job Division3": "", "Email3": "",
            "Telephone3": "",
            "Contact Name4": "", "Job Title4": "", "Job Division4": "", "Email4": "",
            "Telephone4": "",
            "Contact Name5": "", "Job Title5": "", "Job Division5": "", "Email5": "",
            "Telephone5": "",
        }