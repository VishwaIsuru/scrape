import scrapy
import csv
import pandas as pd
import requests

class WorkSpider(scrapy.Spider):
    name = 'rockauto'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            # "LC","PN","DESC","RockAuto Title","RockAuto Price"
            # amazonRockauto
            "LC","PN","DESC","RockAuto Title","RockAuto Price","Amazon Title","Amazon Price"
        ]
    };

    def start_requests(self):
        col_list = ["LC","PN","DESC","RockAuto Title","RockAuto Price"]
        df = pd.read_csv("rocoautoData28.csv", usecols=col_list)
        pn = df["PN"]
        lc = df["LC"]
        desc = df["DESC"]
        rocktitle = df["RockAuto Title"]
        rockprice = df["RockAuto Price"]

        for i in range(0,len(pn)+1):
            # u = 'https://www.rockauto.com/en/partsearch/?partnum='+pn[i]
            u = 'https://www.amazon.com/s?k='+pn[i]
            yield scrapy.Request(url=u,callback=self.parserock,meta={'pn':pn[i],'lc':lc[i],'desc':desc[i],'rctitle':rocktitle[i],
                                                                     'rcprice':rockprice[i]},
                                 headers={
                                     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:105.0) Gecko/20'+str(i)+'00101 Firefox/195.0',
                                 }
                                 )

    def parserock(self, response):
        pn = response.meta['pn']
        lc = response.meta['lc']
        desc = response.meta['desc']
        rctitle = response.meta['rctitle']
        rcprice = response.meta['rcprice']

        # rocktitle = ''.join([x for x in response.xpath('//td[@class="listing-border-top-line listing-inner-content"]//text()').extract()]).replace('\n','').replace('  ','')
        # rockprice = response.xpath('//span[contains(@id, "dprice")]//text()').extract_first()
        #
        # yield {
        #     "LC": lc, "PN": pn, "DESC": desc, "RockAuto Title": rocktitle, "RockAuto Price": rockprice
        # }

        # amazon part scrape
        a = response.xpath(
            '//a[@class="a-size-base a-link-normal s-underline-text s-underline-link-text s-link-style a-text-normal"]')
        for k in a:
            if 'spons' not in k.xpath('.//@href').extract_first():
                prdlink = k.xpath('.//@href').extract_first()
                price = k.xpath('.//span[@class="a-offscreen"]//text()').extract_first()
                yield scrapy.Request(url='https://www.amazon.com'+prdlink, callback=self.parseamzondata,
                                     meta={'lc':lc,'pn':pn,'desc':desc,'rctitle':rctitle,'rcprice':rcprice,'priceamzon': price},
                                     headers={
                                         "User-Agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(11111121111) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
                                     }
                                     )
                break

    def parseamzondata(self,response):
        lc = response.meta['lc']
        pn = response.meta['pn']
        desc = response.meta['desc']
        rctitle = response.meta['rctitle']
        rcprice = response.meta['rcprice']
        priceamzon = response.meta['priceamzon']
        title = ''.join([x.strip() for x in response.xpath('//h1[@id="title"]//text()').extract()])

        yield {
            "LC": lc,"PN":pn,"DESC":desc,"RockAuto Title":rctitle,"RockAuto Price":rcprice,"Amazon Title":title,"Amazon Price":priceamzon
        }