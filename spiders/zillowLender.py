import scrapy
from scrapy.http import FormRequest
import csv
import pandas as pd
import json
import requests

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:107.0) Gecko/20100101 Firefox/107.0",
    "Accept": "application/json",
    "Accept-Language": "en-US,en;q=0.5",
    # "Accept-Encoding": "gzip, deflate, br",
    "Referer": "https://www.zillow.com/",
    "Content-Type": "text/plain",
    "Origin": "https://www.zillow.com",
    "Connection": "keep-alive",
    "Sec-Fetch-Dest": "empty",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Site": "same-site",
    # Requests doesn"t support trailers
    # "TE": "trailers",
}


class workspider(scrapy.Spider):
    name = "zillowlender"

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'URL','Person Name','Broker Name','Address','Mobile Number',
            'Office Number','Fax Number','Reviews','Ratting','Website'
    ]
    };

    def start_requests(self):
        # with open('usa_city_state.csv', newline='') as csvfile:
        #     spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        #     for row in spamreader:
        #         print(row[0])
        with open('usa_city_state.csv', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                city = row['city']

                params = {
                    "partnerId": "RD-CZMBMCZ",
                }
                data = {"sort": "Relevance", "pageSize": 20, "page": 1,"partnerId": "RD-CZMBMCZ",
                        "fields": ["companyName", "totalReviews", "rating", "screenName", "imageId", "individualName",
                                   "employerScreenName", "nmlsId", "recentReviews", "confirmedReviews"],
                        "location": ""+city+"",
                        "language": "English", "debug": False}

                request_body = json.dumps(data)
                yield scrapy.Request(url='https://mortgageapi.zillow.com/getLenderDirectoryListings', callback=self.parse_inside,
                                     method="POST", dont_filter=True,
                                     body=request_body, headers=headers,
                                     meta= {'city':city}
                                     )

    def parse_inside(self,response):
        prdurl = response.url
        city = response.meta['city']
        data = json.loads(response.body)
        total = data['totalLenders']
        print(total)
        pgs = int(int(total)/20)
        pgall = pgs+2
        for pg in range(1,pgall):
            data = {"sort": "Relevance", "pageSize": 20, "page": pg, "partnerId": "RD-CZMBMCZ",
                    "fields": ["companyName", "totalReviews", "rating", "screenName", "imageId", "individualName",
                               "employerScreenName", "nmlsId", "recentReviews", "confirmedReviews"],
                    "location": ""+city+"",
                    "language": "English", "debug": False}

            request_body = json.dumps(data)
            yield scrapy.Request(url='https://mortgageapi.zillow.com/getLenderDirectoryListings',
                                 callback=self.parse_inside_allpages,
                                 method="POST", dont_filter=True,
                                 body=request_body, headers=headers,
                                 # params= params
                                 )

    def parse_inside_allpages(self,response):
        data = json.loads(response.body)
        for lenders in data['lenders']:
            try:
                companyName = lenders['companyName']
            except:
                companyName = ''
            # print(companyName)
            persondata = lenders['screenName']
            # print(persondata)
            try:
                fnm = lenders['individualName']['firstName']
            except:
                fnm = ''
            try:
                mnm = lenders['individualName']['middleName']
            except:
                mnm = ''
            try:
                lsnm = lenders['individualName']['lastName']
            except:
                lsnm = ''
            personNameAll = fnm+' '+mnm+' '+lsnm
            rating = lenders['rating']
            totalReviews = lenders['totalReviews']
            personurl = 'https://www.zillow.com/lender-profile/'+persondata+'/'
            data = {'partnerId': 'RD-CZMBMCZ',"fields":["aboutMe","address","cellPhone","contactLenderFormDisclaimer","companyName",
                              "employerMemberFDIC","employerScreenName","equalHousingLogo","faxPhone",
                              "hideCellPhone","imageId","individualName","languagesSpoken","memberFDIC",
                              "nationallyRegistered","nmlsId","nmlsType","officePhone","rating","screenName",
                              "stateLicenses","stateSponsorships","title","recentReviews","confirmedReviews",
                              "totalReviews","website"],"lenderRef":{"screenName":""+persondata+""}}
            # print(data)
            request_body = json.dumps(data)
            yield scrapy.Request(url='https://mortgageapi.zillow.com/getRegisteredLender',
                                 callback=self.parse_inside_persondata,
                                 method="POST", dont_filter=True,
                                 body=request_body, headers=headers,
                                 meta={'personurl':personurl,'personname':personNameAll,'companyname':companyName,
                                       'reviews':totalReviews,'rattings':rating}
                                 )

    def parse_inside_persondata(self,response):
        personurl = response.meta['personurl']
        personNameAll = response.meta['personname']
        companyName = response.meta['companyname']
        totalReviews = response.meta['reviews']
        rating = response.meta['rattings']
        # print(response.body)
        data = json.loads(response.body)
        addressAll = ''
        celPhoneAll = ''
        officePhoneAll = ''
        faxNumberAll = ''
        website = ''

        address = data['lender']['address']['address']
        try:
            address2 = data['lender']['address']['address2']
        except:
            address2 = ''
        #     try:
        city = data['lender']['address']['city']
        #     except:
        #         city = ''
        #     try:
        stateAbbreviation = data['lender']['address']['stateAbbreviation']
        #     except:
        #         stateAbbreviation = ''
        #     try:
        zipCode = data['lender']['address']['zipCode']
        #     except:
        #         zipCode = ''
        #
        addressAll = address+','+address2+','+city+','+stateAbbreviation+' '+zipCode
        try:
            cellPhonearea = data['lender']['cellPhone']['areaCode']
        except:
            cellPhonearea = ''
        try:
            celnum = data['lender']['cellPhone']['number']
        except:
            celnum = ''
        try:
            celpref = data['lender']['cellPhone']['prefix']
        except:
            celpref = ''
        #
        celPhoneAll = '('+cellPhonearea+')'+celpref+'-'+celnum
        try:
            offPhonearea = data['lender']['officePhone']['areaCode']
        except:
            offPhonearea = ''
        try:
            offnum = data['lender']['officePhone']['number']
        except:
            offnum = ''
        try:
            offpref = data['lender']['officePhone']['prefix']
        except:
            offpref = ''
        #
        officePhoneAll = '('+offPhonearea+')'+offpref+'-'+offnum
        try:
            faxPhonearea = data['lender']['faxPhone']['areaCode']
        except:
            faxPhonearea = ''
        try:
            faxnum = data['lender']['faxPhone']['number']
        except:
            faxnum = ''
        try:
            faxpref = data['lender']['faxPhone']['prefix']
        except:
            faxpref = ''
        #
        faxNumberAll = '(' + faxPhonearea + ')' + faxpref + '-' + faxnum
        try:
            website = data['lender']['website']
        except:
            website = ''
        #
        yield {
            'URL': personurl,
            'Person Name': personNameAll,
            'Broker Name': companyName,
            'Address': addressAll,
            'Mobile Number': celPhoneAll,
            'Office Number': officePhoneAll,
            'Fax Number': faxNumberAll,
            'Reviews': totalReviews,
            'Ratting': rating,
            'Website': website
        }