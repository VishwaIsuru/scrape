import scrapy
import csv
import codecs

class WorkSpider(scrapy.Spider):
    name = 'aptekacentry'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            "url","Product_Name","cat","Main_Image","Product_Price",
            "Producent","Kod_Producent","ID","EAN"

        ]
    };

    urls = [
        "https://aptekacenturia24.pl/",
    ]

    def start_requests(self):

        f = codecs.open("./aptekacentry24.xml", "w+", encoding='utf8')
        contents = "\n".join([
                                 '<?xml version="1.0" encoding="utf-8"?><offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1">',
                                 '</offers>'])
        f.write(contents)
        f.flush()
        f.close()

        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        links = response.xpath('//ul[@class="cat"]//a//@href').extract()
        print(len(links))

        for link in links:
            l = 'https://aptekacenturia24.pl/' + link
            # print (l)
            yield scrapy.Request(url=l, callback=self.parseInside, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 dont_filter=True)


    def parseInside(self, response):
        nwurl = response.url
        pagingUrls = []

        try:
            paging = response.xpath('//table[@class="range table"]//tr[1]//td[2]//text()').extract()[-1]
            valuelastpg = int(paging)/10+1

            i = 0
            while i < int(valuelastpg):
                pgurl = nwurl+ "?page=" + str(i)
                pagingUrls.append(pgurl)
                i += 1
        except:
            pagingUrls.append(response.url)

        for pagesofprdt in pagingUrls:
            yield scrapy.Request(url=pagesofprdt, callback=self.toAllPrdt,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

    def toAllPrdt(self, response):

        prdlinks = response.xpath('//tbody//tr//td[2]//a//@href').extract()
        catg = response.xpath('//ul[@class="cat_path"]//li//a//text()').extract()[-1]

        for prdlink in prdlinks:
            linn = 'https://aptekacenturia24.pl/'+prdlink
            yield scrapy.Request(url=linn, callback=self.prdDetails, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 dont_filter=True,meta={'catga': catg})

    def prdDetails(self,response):

        print('I m herrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr')
        prdname = response.xpath('//div[@class="produkt"]//h1//text()').extract_first()
        id = response.url.split('i')[-1].split('.')[0]
        cat = response.meta['catga']
        imageurl = response.xpath('//div[@class="zdjecie"]//a//@href').extract_first().replace('javascript:okno(','').replace(')','').replace("'","").replace('&','-')
        prdprice1 = response.xpath('//div[@class="cena"]//h4//text()').extract_first().replace(',','.')
        prdprice = prdprice1.split()[0]
        ean = ''
        producer = ''
        dts = response.xpath('//div[@class="list"]//dl//dt//text()').extract()
        dds = response.xpath('//div[@class="list"]//dl//dd//text()').extract()
        for k in range(len(dts)):
            dttxt = dts[k]
            ddtxt = dds[k]
            if dttxt == 'EAN:':
                ean = ddtxt
            if dttxt == 'producent:':
                producer = ddtxt.replace('"','')

        kodproducer = 'BBBB'
        # descri = '\n'.join([x.strip('') for x in
        #                     response.xpath('//div[@id="component_projector_longdescription"]//p//text()').extract() if
        #                     x.strip() != ''])
        # id = response.url.split('https://www.aptekaolmed.pl/product-pol-')[1].split('-')[0]
        linkxml = response.url.replace('&','-')
        line = '<o id="%s" price="%s" url="%s"><cat><![CDATA[%s]]></cat><name><![CDATA[%s]]></name><imgs><main url="%s"/></imgs><attrs><a name="Producent"><![CDATA[%s]]></a><a name="Kod_producenta"><![CDATA[%s]]></a><a name="EAN"><![CDATA[%s]]></a></attrs></o>' % (
            id, prdprice, linkxml, cat, prdname, imageurl, producer, kodproducer,ean
        )

        yield {
            "url": response.url,
            "Product_Name": prdname,
            "cat": cat,
            "Main_Image": imageurl,
            "Product_Price": prdprice,
            "Producent": producer,
            "Kod_Producent": kodproducer,
            "ID": id,
            "EAN": ean
        }

        f = codecs.open("./aptekacentry24.xml", "r", encoding='utf8')
        contents = f.readlines()
        contents = [a for a in contents if a.strip() != '']
        f.close()

        contents.insert(len(contents) - 1, line.strip())

        f = codecs.open("./aptekacentry24.xml", "w", encoding='utf8')
        contents = "\n".join(contents)
        f.write(contents)
        f.flush()
        f.close()


