import scrapy
from scrapy.http import FormRequest
import csv
import pandas as pd
import json
import requests


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:103.0) Gecko/20100101 Firefox/103.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1',
}

class workspider(scrapy.Spider):
    name = "gelbeseiten"

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'url','seller','address','branch','opens','telephone','email','website','other info','branches'
    ]
    };

    def start_requests(self):
        col_list = ['prdink', 'ss']
        df = pd.read_csv("D:\\Songs\\amazonWeekly\\amazonWeekly_28_05_23\\gelbeseiten_5.csv",
                         usecols=col_list)

        crossreffs = df["ss"]
        prdlinks = df["prdink"]
        # print(len(partnumrs))
        for i in range(0, len(prdlinks)):
            prtnum = prdlinks[i]
            crosfer = crossreffs[i]
            yield scrapy.Request(url=prdlinks[i], callback=self.parse_inside, dont_filter=True,
                                 meta={'prtnumbr':prtnum,'crosfer': crosfer}
                                 )

    def parse_inside(self,response):
        prdurl = response.url
        seller = ''.join([x for x in response.xpath('//h1//text()').extract()]).replace('\n','').replace('\t','')
        address = ''.join([x for x in response.xpath('//address[@class="mod-TeilnehmerKopf__adresse :: d-none d-md-block"]//text()').extract()]).replace('\n','').replace('\t','')
        branch = ''.join([x for x in response.xpath('//div[@class="mod-TeilnehmerKopf__branchen"]//text()').extract()]).replace('\n','').replace('\t','')
        opensat = ''.join([x for x in response.xpath('//div[@class="mod-TeilnehmerKopf__oeffnungszeiten"]//text()').extract()]).replace('\n','').replace('\t','')
        tele = ''.join([x for x in response.xpath('//span[@class="mod-TeilnehmerKopf--secret_suffix"]//text()').extract()]).replace('\n','').replace('\t','')
        email = ''.join([x for x in response.xpath('//div[@id="email_versenden"]//@data-link').extract()]).replace('\n','').replace('\t','').replace('mailto:','').replace('?subject=Anfrage über Gelbe Seiten','')
        website = ''.join([x for x in response.xpath('//div[@class="mod-Kontaktdaten__list-item contains-icon-homepage"]//a//@href').extract()]).replace('\n','').replace('\t','')
        otherinfo = ''.join([x for x in response.xpath('//section[@id="beschreibung"]//text()').extract()]).replace('\n','').replace('\t','')
        branchas = ', '.join([x for x in response.xpath('//section[@id="branchen_und_stichworte"]//text()').extract()]).replace('\n','').replace('\t','')

        yield {
            'url' : prdurl,
            'seller' : seller,
            'address' : address,
            'branch' : branch,
            'opens' : opensat,
            'telephone' : tele,
            'email' : email,
            'website' : website,
            'other info' : otherinfo,
            'branches' : branchas
        }
