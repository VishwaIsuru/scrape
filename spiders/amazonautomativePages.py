import scrapy
import pandas as pd

class WorkSpider(scrapy.Spider):
    name = 'amazonautomativepage'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'fstcsvlink','previouspagenumber','lsturl','lastpage'
        ]
    };

    def start_requests(self):
        col_list = ["link"]
        df = pd.read_csv("AmazonAutolastlinks1.csv", usecols=col_list)
        urls = df["link"]
        d=len(urls)+1
        for i in range(0,d):
            yield scrapy.Request(url=urls[i], callback=self.parseInside, dont_filter=True,
                                 headers={
                                     'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100'+str(i)+' Firefox/74.0',
                                     # 'Accept-Encoding': 'gzip, deflate, br', 'Connection': 'keep-alive'
                                 },
                                 meta={'page': 1})

    def parseInside(self, response):
        next_page = response.meta['page'] + 1
        print(response.url)
        pages = response.xpath('//ul[@class="a-pagination"]//li//text()').extract()
        pagesli = response.xpath('//ul[@class="a-pagination"]//li//a//@href').extract()
        try:
            lspg = pages[-3]
        except:
            lspg = '1'
        try:
            sndpglink = pagesli[1]
        except:
            sndpglink = ''
        if int(lspg)>1:
            nwpg = 'page='+str(lspg)
            lspglink = 'https://www.amazon.com'+sndpglink.replace('page=2',nwpg)
        else:
            lspglink = response.url
        csvlink = response.url
        yield scrapy.Request(url=lspglink, callback=self.parseInsidelspg, dont_filter=True,meta={'csvlink':csvlink,'previouspg':lspg},
                             headers={
                                 'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0',
                                 # 'Accept-Encoding': 'gzip, deflate, br', 'Connection': 'keep-alive'
                             }
                             )

    def parseInsidelspg(self,response):
        csvlink = response.meta['csvlink']
        previouspage = response.meta['previouspg']
        pageslst = response.xpath('//ul[@class="a-pagination"]//li//text()').extract()
        try:
            lspg = pageslst[-3]
        except:
            lspg = '1'

        yield {
            'fstcsvlink': csvlink,
            'previouspagenumber': previouspage,
            'lsturl': response.url,
            'lastpage': lspg,

        }
        # pages = response.css('.a-pagination').xpath('.//li[@class="a-normal"]')
        # print (next_page)
        # # print(pages)
        # for page in pages:
        #     print("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWww")
        #     text = page.css('a').xpath('./text()').extract_first()
        #     pageNum = int(text)
        #     if pageNum == next_page:
        #         header = {
        #             "User-Agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(
        #                 pageNum) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
        #             "Referer": "https://www.amazon.com",
        #             "Origin": "https://www.amazon.com",
        #             "Connection": "keep-alive",
        #         }
        #
        #         l='https://www.amazon.com' +page.xpath('./a/@href').extract_first()
        #         yield scrapy.Request(url=l, callback=self.parseInside, headers=header, meta={'page': next_page})
        #         break
