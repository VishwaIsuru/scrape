import scrapy
import csv
from csv import reader
from scrapy_scrapingbee import ScrapingBeeRequest

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
}

class workspider(scrapy.Spider):
    name = "ricadoscrape"

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'Url','Main Category','Subcategory 1','Subcategory 2','Subcategory 3','Subcategory 4',
            'Name','Price','Description'
        ]
    };

    def start_requests(self):
        maincats = []
        subcat1s = []
        subcat2s = []
        subcat3s = []
        subcat4s = []
        links = []
        with open('ricadocatagery.csv', 'r', encoding="utf8",errors='ignore') as read_obj:
            csv_reader = reader(read_obj)
            for row in csv_reader:
                maincat = row[0]
                maincats.append(maincat)
                subcat1 = row[1]
                subcat1s.append(subcat1)
                subcat2 = row[2]
                subcat2s.append(subcat2)
                subcat3 = row[3]
                subcat3s.append(subcat3)
                subcat4 = row[4]
                subcat4s.append(subcat4)
                link = row[5]
                links.append(link)
        for i in range(1, len(links)):
            ur = links[i]
            yield ScrapingBeeRequest(url=ur, callback=self.parse_inside, dont_filter=True,headers=headers,
                                     params={'render_js':True,'premium_proxy':False,'country_code':'us'},
                                     meta={'maincat':maincats[i],'sub1cat': subcat1s[i],'sub2cat':subcat2s[i],
                                           'sub3cat':subcat3s[i],'sub4cat':subcat4s[i]})

    def parse_inside(self,response):
        url = response.url
        maincat = response.meta['maincat']
        sub1cat = response.meta['sub1cat']
        sub2cat = response.meta['sub2cat']
        sub3cat = response.meta['sub3cat']
        sub4cat = response.meta['sub4cat']
        allpages = response.xpath('//span[@class="MuiButton-label MuiFlatPageButton-label MuiFlatPagination-label"]//text()').extract()
        try:
            lspage = allpages[-1]
        except:
            lspage = '1'
        for j in range(1,int(lspage)+1):
            pgurl = url+'?page='+str(j)
            yield ScrapingBeeRequest(url=pgurl, callback=self.parse_inside_products, dont_filter=True, headers=headers,
                                     params={'render_js': True, 'premium_proxy': False, 'country_code': 'us'},
                                     meta={'maincat':maincat,'sub1cat':sub1cat,
                                           'sub2cat':sub2cat,'sub3cat':sub3cat,'sub4cat':sub4cat})

    def parse_inside_products(self, response):
        maincat = response.meta['maincat']
        sub1cat = response.meta['sub1cat']
        sub2cat = response.meta['sub2cat']
        sub3cat = response.meta['sub3cat']
        sub4cat = response.meta['sub4cat']
        productlinks =response.xpath('//a[@class="MuiGrid-root link--2OHFZ MuiGrid-item MuiGrid-grid-xs-6 MuiGrid-grid-sm-4 MuiGrid-grid-md-3"]//@href').extract()
        for l in productlinks:
            link = 'https://www.ricardo.ch'+l
            yield ScrapingBeeRequest(url=link, callback=self.parseInsidePrdData,dont_filter=True,
                                     params={'render_js':True,'premium_proxy':False,'country_code':'us'},
                                     meta={'maincat': maincat, 'sub1cat': sub1cat,
                                           'sub2cat': sub2cat, 'sub3cat': sub3cat, 'sub4cat': sub4cat}
                                     )

    def parseInsidePrdData(self, response):
        maincat = response.meta['maincat']
        sub1cat = response.meta['sub1cat']
        sub2cat = response.meta['sub2cat']
        sub3cat = response.meta['sub3cat']
        sub4cat = response.meta['sub4cat']
        name = response.xpath('//h1[@class="style_title__O6YYD"]//text()').extract_first()
        price = response.xpath("//p[contains(@class, 'jss77 style_price__')]//text()").extract_first() or response.xpath("//div[contains(@class, 'jss77 style_price__')]//text()").extract_first()
        description = '\n'.join([x.strip() for x in response.xpath('//div[@class="style_userDescription__2hRc9"]//text()').extract()]).replace('\n\n','')
        breadcrumb = response.xpath('//li[@itemprop="itemListElement"]//text()').extract()

        yield {
            'Url': response.url,
            'Main Category': maincat,
            'Subcategory 1': sub1cat,
            'Subcategory 2': sub2cat,
            'Subcategory 3': sub3cat,
            'Subcategory 4': sub4cat,
            'Name': name,
            'Price': price,
            'Description': description
        }