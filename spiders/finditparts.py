import scrapy
import csv
import pandas as pd
import requests

class WorkSpider(scrapy.Spider):
    name = 'finditparts'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Product link","Product Name","Part Number","Manufacture","MSRP","Actual Price",
            "Description","Part Description","Cross Reference","Cross Reference List"
        ]
    };

    def start_requests(self):
        col_list = ["prdlink"]
        df = pd.read_csv("finditpartslinksFilter.csv", usecols=col_list)
        urls = df["prdlink"]

        for i in range(0,15445):
            u = urls[i]
            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parse(self, response):

        prdlink = response.css('#stellar_phase_2').xpath('.//h1//text()').extract_first().replace('\n','').replace('  ','')
        partnumber = response.xpath('//dt[contains(text(),"Part Number")]/following-sibling::dd[1]/text()').extract_first()
        manufacture = response.xpath('//dt[contains(text(),"Manufacturer")]/following-sibling::dd[1]/text()').extract_first()
        description = response.xpath('//dt[contains(text(),"Description")]/following-sibling::dd//h2//text()').extract_first()
        try:
            partdescrip = response.xpath('//h3[contains(text(),"FREIGHTLINER part description")]/following-sibling::div[1]//text()').extract_first().replace('\n','').replace('  ','')
        except:
            partdescrip = ''
        try:
            d1 = response.xpath('//text()[contains(., "Cross Reference")]').extract_first()
        except:
            d1 = 'Cross Reference'
        crossreferense = response.xpath('//h5[contains(text(),"'+str(d1)+'")]/following-sibling::p[2]//text()').extract_first()
        crossreferenselist = ','.join([x.strip() for x in response.xpath('//ul[@class="list-unstyled cross-reference-list"]//a//text()').extract()])
        msrp = response.xpath('//p[@class="product_info__msrp"]//text()').extract_first()
        try:
            price = '$'+response.xpath('//span[@class="price_tag__dollars"]//text()').extract_first()
        except:
            price = ''

        # images = []
        # nomalimg = response.xpath('//picture[@class="product_image_picture"]//img//@src').extract_first()
        # images.append(nomalimg)
        # thumb = response.xpath('//div[@class="product_page__thumbnail"]//a')
        # for th in thumb:
        #     thmimg = th.xpath('.//@href').extract_first()
        #     images.append(thmimg)
        #
        # imgfol = partnumber.replace('/', '').replace(':', '').replace(' ', '').replace('"', '')
        # for i in range(0, len(images)):
        #     r = requests.get(images[i])
        #     folder = "./finditpartsImages/" +imgfol
        #     imgName = folder + "_" + str(i) + ".jpg"
        #     if r.status_code == 200:
        #         with open(imgName, 'wb') as f:
        #             f.write(r.content)

        yield {
            "Product link": response.url,
            "Product Name": prdlink,
            "Part Number": partnumber,
            "Manufacture": manufacture,
            "MSRP": msrp,
            "Actual Price": price,
            "Description": description,
            "Part Description": partdescrip,
            "Cross Reference": crossreferense,
            "Cross Reference List": crossreferenselist
        }