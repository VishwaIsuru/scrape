# import scrapy
# import csv
# # from sshtunnel import SSHTunnelForwarder
# import pymysql
# from datetime import datetime
#
# # server = SSHTunnelForwarder(
# #     '158.220.104.93',
# #     ssh_username='root',
# #     ssh_password='wnw5UHBq75Yb4fB',
# #     remote_bind_address=('127.0.0.1', 3306)
# # )
# # server.start()
#
# conn = pymysql.connect(
#     host='localhost',
#     # port=server.local_bind_port,
#     user='root',
#     password='root',
#     db='scrape'
# )
# #
# cursor = conn.cursor()
#
# class workspider(scrapy.Spider):
#     name = "unamityNarApril_mysql"
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': [
#             'horseid','formdate','formhorseJocky'
#                 ]
#     };
#     urls = []
#     M = ['04']  # ,'02','03','04','05','06','07','08','09','10','11','12'
#     D = ['01']
#     for d1 in D:
#         for m in M:
#             for y in range(2023, 2024):
#                 ur1 = 'https://nar.umanity.jp/racedata_top.php?date='+str(y)+str(m)+str(d1)
#                 urls.append(ur1)
#
#     def start_requests(self):
#         for u in self.urls:
#             yield scrapy.Request(url=u, callback=self.parse,
#                                  # headers= headers,cookies= cookies
#                                  )
#             # break
#
#     def parse(self,response):
#         allplaces = response.xpath('//div[@class="tab_place line_ylw_934"]//ul[@class="clearfix"]//a//@href').extract()
#         for place in allplaces:
#             placelink = 'https://nar.umanity.jp/'+place
#             yield scrapy.Request(url=placelink, callback=self.toAllPlace,
#                                  # headers= headers, cookies = cookies,dont_filter=True
#                                  )
#
#     def toAllPlace(self,response):
#         eventURL = response.url
#         allraces = response.xpath('//td[@class="left"]//a//@href').extract()
#         for race in allraces:
#             racelink = 'https://nar.umanity.jp/' + race
#             yield scrapy.Request(url=racelink, callback=self.toAllRace,
#                                  # headers= headers, cookies = cookies,dont_filter=True
#                                  )
#
#     def toAllRace(self, response):
#         eventURL = response.url
#         eventId1 = eventURL.split('=')[-1]
#         meetingDate = ''.join([x for x in response.xpath('//time[@datetime]//text()').extract()])
#         meetingVenue = response.xpath('//li[@class="on"]//text()').extract_first().strip()
#         d = ''.join([x for x in response.xpath('//div[@class="detail2"]//text()').extract()])
#         eventTrackSurface = d.split('発走')[1].strip().split(' ')[0].replace('｜','')
#         meetingTotalEvent = len(response.xpath('//div[@class="race_box"]//ul[@class="clearfix"]//li').extract())
#         eventTrackCondition = ''.join([x for x in response.css('.tx_gre').xpath('./text()').extract()])
#         eventNumber = response.xpath('//div[@class="race_box"]//li[@class="on"]//text()').extract_first()
#         raceyear = eventId1[0:4]
#         eventClass = response.xpath('//div[@class="detail"]//h2//text()').extract_first().strip().replace('Ａ','A').replace('Ｂ','B').replace('Ｃ','C').\
#             replace('Ｄ','D').replace('Ｅ','E').replace('Ｇ','G').replace('Ｉ','I').replace('Ｊ','J').replace('Ｐ','P').\
#             replace('Ｒ','R').replace('Ｔ','T').\
#             replace('Ｗ','W').replace('Ｙ','Y').replace('１','1').replace('２','2').\
#             replace('３','3').replace('４','4').replace('５','5').replace('６','6').replace('７','7').replace('８','8').replace('９','9')
#         eventNumber = eventClass.split(' ')[0]
#         eventName = ' '.join(eventClass.split(' ')[1:]).replace('Ａ','A').replace('Ｂ','B').replace('Ｃ','C').\
#             replace('Ｄ','D').replace('Ｅ','E').replace('Ｇ','G').replace('Ｉ','I').replace('Ｊ','J').replace('Ｐ','P').\
#             replace('Ｒ','R').replace('Ｔ','T').\
#             replace('Ｗ','W').replace('Ｙ','Y').replace('１','1').replace('２','2').\
#             replace('３','3').replace('４','4').replace('５','5').replace('６','6').replace('７','7').replace('８','8').replace('９','9')
#         eventDistance = d.split('発走')[1].strip().split(' ')[1].replace('右','')
#         eventDistanceUnit = eventDistance[-1]
#         eventTime = d.split('発走')[0].strip()
#         eventTotalRunner = len(response.xpath('//table[@class="table_base"]//tr[@class="odd_row" or @class="even_row"]').extract())
#         prizemo = response.css('.detail > div:nth-child(4)').xpath('.//text()').extract_first().strip()
#         try:
#             eventPrizeMoney = prizemo.split('本賞金：')[1].split('円')[0]+ ' yen'
#         except:
#             eventPrizeMoney = prizemo.split('別定｜本賞金：')[1].split('円')[0]+ ' yen'
#         eventPriceMoneyCurrency = 'yen'
#         reddot = response.xpath('//h2[@class="point f14"]//text()').extract_first().strip()
#         nmm = response.xpath('//div[@class="detail"]//h2//text()').extract_first().strip()
#         div3 = ''.join([x for x in response.css('.detail > div:nth-child(3)').xpath('.//text()').extract()])
#         eventWeather = response.xpath('//div[@class="condition"]//img//@src').extract_first().replace('/common/img/ico_','').replace('_s.gif','')
#         eventStatus = ''
#         eventStakesLevel = ''
#         eventCreatedAt = ''
#         eventUpdatedAt = ''
#
# #         with connection.cursor() as cursor:
#         tim = datetime.now()
#         sql = "INSERT INTO unamityNARRaceDetail_from_april_2023 (eventId, eventURL, eventName, eventNumber, eventTime, " \
#               "eventTotalRunner, eventStatus, eventClass,eventStakesLevel, meetingVenue, eventTrackCondition, " \
#               "eventTrackSurface, eventWeather, eventDistance,eventDistanceUnit, eventPrizeMoney, " \
#               "eventPriceMoneyCurrency, eventCreatedAt, eventUpdatedAt,created,updated)" \
#               "VALUES (N%s, N%s, N%s, N%s, N%s, %s, N%s, N%s, N%s, N%s, N%s, N%s, N%s, N%s, N%s , N%s, N%s, N%s, N%s,%s,%s)"
#
#         sql = sql % ("'"+eventId1+"'", "'"+eventURL+"'", "'"+eventName+"'", "'"+eventNumber+"'", "'"+eventTime+"'",
#                      eventTotalRunner, "'"+eventStatus+"'","'"+eventClass+"'","'"+eventStakesLevel+"'",
#                      "'"+meetingVenue+"'", "'"+eventTrackCondition+"'", "'"+eventTrackSurface+"'","'"+eventWeather+"'",
#                      "'"+eventDistance+"'","'"+eventDistanceUnit+"'", "'"+eventPrizeMoney+"'", "'"+eventPriceMoneyCurrency+"'",
#                      "'"+eventCreatedAt+"'", "'"+eventUpdatedAt+"'", "'"+str(tim)+"'","'"+str(tim)+"'")
#
#         print(sql)
#         cursor.execute(sql)
#         conn.commit()
#
#         horsetb = response.xpath('//table[@class="table_base"]//tr[@class="odd_row" or @class="even_row" or @class="odd" or @class="even"]')
#         for hr in horsetb:
#             horseNumber1 = hr.xpath('.//td[3]//text()').extract_first().strip()
#             horseName1 = hr.xpath('.//td[4]//a//text()').extract_first().strip()
#             horseSex1 = ''.join([x for x in hr.xpath('.//td[5]//text()').extract()]).strip()
#             horseAge1 = int(''.join(filter(str.isdigit, horseSex1)))
#             horseWeightCarried1 = hr.xpath('.//td[6]//text()').extract_first()
#             horseWeightCarriedUnit1 = 'Kg'
#             horseJocky1 = ''.join([x for x in hr.xpath('.//td[7]//text()').extract()])
#             horseTrainer1 = ''.join([x for x in hr.xpath('.//td[8]//text()').extract()])
#             horseWeight1 = hr.xpath('.//td[10]//text()').extract_first()
#             horseTime1 = hr.xpath('.//td[13]//text()').extract_first()
#             horseStartingPrice1 = hr.xpath('.//td[11]//text()').extract_first()
#             horseMargin1 = ''.join([x for x in hr.xpath('.//td[14]//text()').extract()]).replace('１','1 ').replace('２','2 ').\
#             replace('３','3 ').replace('４','4 ').replace('５','5 ').replace('６','6 ').replace('７','7 ').replace('８','8 ').replace('９','9 ')
#             horsePosition1 = hr.xpath('.//td[1]//text()').extract_first()
#             horselink = 'https://nar.umanity.jp/'+hr.xpath('.//td[4]//a//@href').extract_first()
#             yield scrapy.Request(url=horselink, callback=self.parse_inside_horse, dont_filter=True,
#                                  meta={'eventId':eventId1,'horseNumber':horseNumber1,'horseName':horseName1,'horseSex':horseSex1,
#                                        'horseAge':horseAge1,'horseStartingPrice':horseStartingPrice1,'horsePosition':horsePosition1,
#                                        'horseMargin': horseMargin1,'horseWeightCarried': horseWeightCarried1,'horseWeightCarriedUnit':horseWeightCarriedUnit1,
#                                        'horseJocky': horseJocky1,'horseTrainer': horseTrainer1,'horseWeight': horseWeight1,
#                                        'horseTime': horseTime1
#                                        }
#                                  )
#
#     def parse_inside_horse(self,response):
#         horseurl = response.url
#         horseid = horseurl.split('=')[-1]
#         eventId = response.meta['eventId']
#         horseNumber = response.meta['horseNumber']
#         horseName = response.meta['horseName']
#         horseSex = response.meta['horseSex']
#         horseAge = response.meta['horseAge']
#         horseStartingPrice = response.meta['horseStartingPrice']
#         horsePosition = response.meta['horsePosition']
#         horseMargin = response.meta['horseMargin']
#         horseWeightCarried = response.meta['horseWeightCarried']
#         horseWeightCarriedUnit = response.meta['horseWeightCarriedUnit']
#         horseJocky = response.meta['horseJocky']
#         horseTrainer = response.meta['horseTrainer']
#         horseWeight = response.meta['horseWeight']
#         horseWeightUnit = 'Kg'
#         horseTime = response.meta['horseTime']
#         horseOwner = ''.join([x for x in response.xpath('//th[contains(text(),"馬主")]//following-sibling::td//text()').extract()])
#         horsePriceMoneyWon = ''.join([x for x in response.xpath('//th[contains(text(),"本賞金")]//following-sibling::td//text()').extract()])
#         horsePriceMoneyWonCurrency = horsePriceMoneyWon[-1:]
#         lp = response.xpath('//td[@width="250"]//text()').extract()
#         horseDOB = lp[-1].strip()[-11:]
#         horseYOB = horseDOB.strip()[:4]
#         horseColour = lp[-1].strip()[:-11]
#         horseDam = response.css('td.father-cell:nth-child(1)').xpath('.//text()').extract_first().strip()
#         horseDamDam = response.xpath('//td[@class="father-cell"]//text()').extract()[1]
#         horseDamSire = response.xpath('//td[@class="mother-cell"]//text()').extract()[0]
#         horseSire = response.xpath('//td[@class="mother-cell"]//text()').extract()[1]
#         horseSireDam = response.xpath('//td[@class="father-cell"]//text()').extract()[2]
#         horseSireSire = response.xpath('//td[@class="mother-cell"]//text()').extract()[2]
#         horseCOB = ''
#         horseSireYOB = ''
#         horseDamYOB = ''
#         horseDamSireYOB = ''
#         horseSireSireYOB = ''
#         horseSireDamYOB = ''
#         horseDamDamYOB = ''
#         horseDamDamSire = ''
#         horseDamDamSireYOB = ''
#         horseComment = ''
#         horseCreatedAt = ''
#         horseUpdatedAt = ''
#         horseBarrierNumber = horseNumber
#
#         # with connection.cursor() as cursor:
#         sql = "INSERT INTO unamityNARHorse_from_april_2023 (horseid, eventId, horseurl, horseNumber, horseName, horseSex, horseCOB, " \
#               "horseAge, horseDOB, horseYOB,horseColour,horseSire, horseSireYOB, horseDam, horseDamYOB, " \
#               "horseDamSire, horseDamSireYOB, horseSireSire,horseSireSireYOB,horseSireDam, horseSireDamYOB, " \
#               "horseDamDam, horseDamDamYOB, horseDamDamSire, horseDamDamSireYOB,horseStartingPrice, horsePosition, " \
#               "horseMargin, horsePriceMoneyWon, horsePriceMoneyWonCurrency,horseWeightCarried, " \
#               "horseWeightCarriedUnit, horseJocky, horseTrainer, horseOwner, horseComment,horseCreatedAt, " \
#               "horseUpdatedAt, horseWeight, horseWeightUnit, horseTime, horseBarrierNumber)" \
#               "VALUES (N%s, N%s, N%s, N%s, N%s, N%s, N%s, %s, N%s , N%s, N%s, N%s, N%s, N%s, N%s , N%s," \
#               " N%s, N%s, N%s, N%s, N%s, N%s, N%s," \
#               " N%s, N%s, N%s, N%s , N%s, N%s, N%s, N%s, N%s, N%s , N%s, N%s, N%s, N%s, N%s , N%s, N%s, N%s, N%s)"
#
#         sql = sql % ("'"+horseid+"'", "'"+eventId+"'", "'"+horseurl+"'", "'"+horseNumber+"'", "'"+horseName+"'",
#                      "'"+horseSex+"'", "'"+horseCOB+"'",
#                      horseAge, "'"+horseDOB+"'", "'"+horseYOB+"'", "'"+horseColour+"'", "'"+horseSire+"'",
#                      "'"+horseSireYOB+"'",
#                      "'"+horseDam+"'", "'"+horseDamYOB+"'", "'"+horseDamSire+"'", "'"+horseDamSireYOB+"'", "'"+horseSireSire+"'",
#                      "'"+horseSireSireYOB+"'", "'"+horseSireDam+"'", "'"+horseSireDamYOB+"'", "'"+horseDamDam+"'", "'"+horseDamDamYOB+"'",
#                      "'"+horseDamDamSire+"'", "'"+horseDamDamSireYOB+"'", "'"+horseStartingPrice+"'", "'"+horsePosition+"'",
#                      "'"+horseMargin+"'",
#                      "'"+horsePriceMoneyWon+"'", "'"+horsePriceMoneyWonCurrency+"'", "'"+horseWeightCarried+"'", "'"+horseWeightCarriedUnit+"'",
#                      "'"+horseJocky+"'", "'"+horseTrainer+"'", "'"+horseOwner+"'", "'"+horseComment+"'", "'"+horseCreatedAt+"'",
#                      "'"+horseUpdatedAt+"'", "'"+horseWeight+"'", "'"+horseWeightUnit+"'", "'"+horseTime+"'", "'"+horseBarrierNumber+"'")
#
#         cursor.execute(sql)
#         conn.commit()
#
#         horseHistoryURL = horseurl
#         horseHistoryOwner = horseOwner
#         horseHistoryHorseName = horseName
#         horseHistoryYOB = horseYOB
#         horseHistoryDam = horseDam
#         horseHistoryCreatedAt = ''
#         horseHistoryUpdatedAt = ''
#         formtb = response.xpath('//table[@class="tbl_database tbl_horse_data mrgt_10"]//tr[@class="odd_row" or @class="even_row"]')
#         for t in formtb:
#             horseHistoryDate = t.xpath('.//td[1]//text()').extract_first()
#             hHYearF = horseHistoryDate.split('/')[0].strip()
#             hHYearL = horseHistoryDate.split('/')[-1].strip()
#             horseHistoryYear = ''
#             if len(hHYearL) == 4:
#                 horseHistoryYear = hHYearL
#             elif len(hHYearL) < 4:
#                 horseHistoryYear = hHYearF
#             horseHistoryPosition = t.xpath('.//td[12]//text()').extract_first()
#             horseHistoryTotalRun = t.xpath('.//td[7]//text()').extract_first()
#             horseHistoryWeight = t.xpath('.//td[14]//text()').extract_first()
#             horseHistoryWinner = t.xpath('.//td[22]//text()').extract_first()
#             horseHistoryJocky = t.xpath('.//td[15]//a//text()').extract_first()
#             horseHistoryTrainer = t.xpath('.//td[16]//a//text()').extract_first()
#             try:
#                 horseHistoryRaceDetail = t.xpath('.//td[4]//a/@href').extract_first()
#             except:
#                 horseHistoryRaceDetail = ''
#             horseHistoryBeatenBy = ''
#
#             # csvfilnm3 = 'Unamity_NAR_HorseFormData2012DB_ja_unicode_1.csv'
#             # csvRow3 = (
#             # horseid, horseHistoryURL, horseHistoryYear, horseHistoryDate, horseHistoryPosition, horseHistoryTotalRun,
#             # horseHistoryWeight, horseHistoryWinner, horseHistoryBeatenBy, horseHistoryOwner, horseHistoryTrainer,
#             # horseHistoryJocky, horseHistoryRaceDetail, horseHistoryHorseName, horseHistoryYOB, horseHistoryDam,
#             # horseHistoryCreatedAt, horseHistoryUpdatedAt
#             # )
#             # with open(csvfilnm3, "a", encoding="utf-8", newline='') as fp3:
#             #     wr = csv.writer(fp3, dialect='excel')
#             #     wr.writerow(csvRow3)
#
#             # with connection.cursor() as cursor:
#             sql = "INSERT INTO unamityNARHorseForm_from_april_2023 (horseid, horseHistoryURL, horseHistoryYear, horseHistoryDate, " \
#                   "horseHistoryPosition, horseHistoryTotalRun,horseHistoryWeight, horseHistoryWinner, " \
#                   "horseHistoryBeatenBy, horseHistoryOwner, horseHistoryTrainer,horseHistoryJocky, " \
#                   "horseHistoryRaceDetail, horseHistoryHorseName, horseHistoryYOB, horseHistoryDam," \
#                   "horseHistoryCreatedAt, horseHistoryUpdatedAt)" \
#                   "VALUES (N%s, N%s, N%s, N%s, N%s, N%s, N%s, N%s, N%s, N%s, N%s, N%s, N%s, N%s, %s, N%s, N%s, N%s)"
#
#             sql = sql % ("'"+horseid+"'", "'"+horseHistoryURL+"'", "'"+horseHistoryYear+"'", "'"+horseHistoryDate+"'",
#                          "'"+horseHistoryPosition+"'",
#                          "'"+horseHistoryTotalRun+"'", "'"+horseHistoryWeight+"'", "'"+horseHistoryWinner+"'",
#                          "'"+horseHistoryBeatenBy+"'",
#                          "'"+horseHistoryOwner+"'", "'"+horseHistoryTrainer+"'", "'"+horseHistoryJocky+"'",
#                          "'"+horseHistoryRaceDetail+"'",
#                          "'"+horseHistoryHorseName+"'", horseHistoryYOB, "'"+horseHistoryDam+"'",
#                          "'"+horseHistoryCreatedAt+"'",
#                          "'"+horseHistoryUpdatedAt+"'")
#
#             # print(sql)
#             cursor.execute(sql)
#             conn.commit()
