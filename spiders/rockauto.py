import scrapy
import csv
import pandas as pd
import requests
from csv import reader

class WorkSpider(scrapy.Spider):
    name = 'rockauttto'
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'Catagary Url','Url','Manufacture','Part Number','Info3','Info4','Image','Feature','Price','OEM','More Info'
        ]
    };

    def start_requests(self):
        file_in = 'D:\\Vishwapreviousprojects\\seliniumPython\\rockauto_other_rockauto_ford_2021_f59_7_3l_v8_new_5_9_23.csv'
        with open(file_in, 'r', encoding='unicode_escape') as read_obj:
            csv_reader = reader(read_obj)
            # next(csv_reader, None)#skip header row
            h=0
            for row in csv_reader:
                caturl = row[0]
                u = row[1]
                manufc = row[2]
                prtnum = row[4]
                info3 = row[5]
                info4 = row[6]
                yield scrapy.Request(url=u,callback=self.parse,
                                     headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                     meta={'caturl':caturl,'manufacture':manufc,'prtnum':prtnum,'info3':info3,'info4':info4})

    def parse(self, response):
        caturl = response.meta['caturl']
        manufacture = response.meta['manufacture']
        partnumber = response.meta['prtnum']
        info3 = response.meta['info3']
        info4 = response.meta['info4']
        try:
            image = 'https://www.rockauto.com'+response.xpath('//img[@id="inlineimg[1]"]//@src').extract_first()
        except:
            image = ''
        feature = ''.join([x.strip() for x in response.xpath("//*[contains(text(),'Features ')]//following-sibling::ul//text() | //*[contains(text(),'Features:')]//following-sibling::ol//text()").extract()])
        price = response.xpath('//span[@id="dprice[1][v]"]//text()').extract_first()
        oem = ''.join([x.strip() for x in response.xpath("//*[contains(text(),'OEM ')]//text()").extract()])

        moreinfo = ''.join([x.strip() for x in response.xpath('//section[contains(@aria-label, "More Information for ")]//text()').extract()]).split('Service Tip:')[0].split('{')[0]

        yield {
            'Catagary Url': caturl,
            'Url': response.url,
            'Manufacture': manufacture,
            'Part Number': partnumber,
            'Info3': info3,
            'Info4': info4,
            'Image': image,
            'Feature': feature,
            'Price': price,
            'OEM': oem,
            'More Info': moreinfo
        }
