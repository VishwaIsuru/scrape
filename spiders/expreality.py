import scrapy
import csv
import json
import requests

class WorkSpider(scrapy.Spider):
    name = 'expreality'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            "First Name","Last Name","Primary Email","Secondary Email", "Phone Number"

        ]
    };
    urls = [
        "https://experts.expcloud.com/api4/std?searchterms=Texas&size=12&from=0"

    ]
    i = 0
    while i < 4800:
        ur = "https://experts.expcloud.com/api4/std?searchterms=Texas&size=12&from=" + str(i)
        urls.append(ur)
        i += 12

    def start_requests(self):

        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u.strip(), callback=self.parse,
                                 headers={"User-Agent": "Mozilla Firefox 12.0","Connection":"keep-alive"}, meta={'Index': i})
            i += 1

    def parse(self, response):
        d=response.body
        # # dataset=d["data"]
        j=json.loads(d)
        dataset=j['hits']
        realdata=dataset['hits']
        # rldata=realdata['_score']
        for da in realdata:
            todata=da['_source']
            try:
                fnma=todata['firstName']
            except:
                pass
            try:
                lsnm=todata['lastName']
            except:
                pass
            try:
                primaryemail=todata['primaryEmail']
            except:
                primaryemail=todata['EmailAddress']
            try:
                secondryemail=todata['secondaryEmail']
            except:
                pass
            try:
                phone=todata['primaryPhone']
            except:
                pass


            yield {

                    "First Name":fnma,
                    "Last Name":lsnm,
                    "Primary Email":primaryemail,
                    "Secondary Email":secondryemail,
                    "Phone Number":phone

                }