import scrapy
import re
import json
import csv
import pymysql.cursors
# import parser
from tika import parser
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import time
#
# options = Options()
# options.headless = True
# browser = webdriver.Firefox(options=options, executable_path=r'.\geckodriver.exe')
# browser.get('https://www.bluenile.com/uk/diamond-search')
# time.sleep(8)
#
# bnuuid = browser.get_cookie('bn_uuid-ssn')['value']
# print(bnuuid)
# browser.close()

# import psycopg2
# csv.register_dialect('myDialect', delimiter='/', quoting=csv.QUOTE_NONE)
#
# connection = psycopg2.connect(
#     host="diamond-production.ctbdnd6gbcye.ap-southeast-1.rds.amazonaws.com",
#     database="diamond_production",
#     port=5432,
#     user="postgres",
#     password="hdfw934klPK")

class workspider(scrapy.Spider):
    name = "bluenile_recursive"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': ["detail_page_url","carat","price","price_per_carat","sku","id","shape","clarity","color",
                               "culet","cut","depth","fluorescence","polish","symmetry","measurements","image_url","sold","certificate"]
    };

    def start_requests(self):

        colors = ['K', 'J', 'I', 'H', 'G', 'F', 'E', 'D']
        claritys = ['SI2', 'SI1', 'VS1', 'VS2', 'VVS2', 'VVS1', 'IF', 'FL']

        url = "https://www.bluenile.com/api/public/diamond-search-grid/v2?startIndex=0&pageSize=50&_=1604484200853&unlimitedPaging=false&sortDirection=asc&sortColumn=default&shape=RD,PR,EC,AS,CU,MQ,RA,OV,PS,HS&minCarat=0.30&maxCarat=0.45&maxDateType=MANUFACTURING_REQUIRED&isQuickShip=false&hasVisualization=false&isFiltersExpanded=false&astorFilterActive=false&country=USA&language=en-us&currency=USD&productSet=BN"
        header = {
            "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"
        }
        total_count = 0
        cookies = {
            'bn_uuid-ssn': bnuuid,
            # 'bn_uuid': bnuuid,
            # 'browserCheck': 'ver~1&browserCheck~true',
        }
        for color in colors:
            count = 0
            for clarity in claritys:
                url = f"https://www.bluenile.com/api/public/diamond-search-grid/v2?startIndex=0&pageSize=50&unlimitedPaging=true&sortDirection=asc&sortColumn=default&shape=RD,PR,EC,AS,CU,MQ,RA,OV,PS,HS&minCarat={0.2}&maxCarat={21}&minColor={color}&maxColor={color}&minClarity={clarity}&maxClarity={clarity}&maxDateType=MANUFACTURING_REQUIRED&isQuickShip=false&hasVisualization=false&isFiltersExpanded=false&astorFilterActive=false&country=USA&language=en-us&currency=USD&productSet=BN"
                yield scrapy.Request(url, callback=self.recursive_find_partitions, headers=header,cookies =cookies,meta={'first':True,'color':color,'clarity':clarity,'min_carat':0.20,'max_carat':21},dont_filter=True)

    def recursive_find_partitions(self,response):
        header = {
            "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"
        }
        color = response.meta['color']
        clarity = response.meta['clarity']
        min_carat = response.meta['min_carat']
        max_carat = response.meta['max_carat']

        cookies = {
            'bn_uuid-ssn': bnuuid,
            'bn_uuid': bnuuid,
            'browserCheck': 'ver~1&browserCheck~true',
        }

        cont_count = int(json.loads(response.body)['count'].replace(',',''))
        print(cont_count)
        if cont_count>2000:
            max_carat_temp = (max_carat + min_carat) / 2.0
            max_carat_temp = int(max_carat_temp * 100) / 100.0
            if max_carat_temp!=min_carat and max_carat_temp!=min_carat:
                url_1 = f"https://www.bluenile.com/api/public/diamond-search-grid/v2?startIndex=0&pageSize=50&unlimitedPaging=true&sortDirection=asc&sortColumn=default&shape=RD,PR,EC,AS,CU,MQ,RA,OV,PS,HS&minCarat={min_carat}&maxCarat={max_carat_temp}&minColor={color}&maxColor={color}&minClarity={clarity}&maxClarity={clarity}&maxDateType=MANUFACTURING_REQUIRED&isQuickShip=false&hasVisualization=false&isFiltersExpanded=false&astorFilterActive=false&country=USA&language=en-us&currency=USD&productSet=BN"
                yield scrapy.Request(url_1, callback=self.recursive_find_partitions, headers=header,cookies=cookies,
                             meta={'first': False, 'color': color, 'clarity': clarity, 'min_carat': min_carat, 'max_carat': max_carat_temp},dont_filter=True)
                url_2 = f"https://www.bluenile.com/api/public/diamond-search-grid/v2?startIndex=0&pageSize=50&unlimitedPaging=true&sortDirection=asc&sortColumn=default&shape=RD,PR,EC,AS,CU,MQ,RA,OV,PS,HS&minCarat={max_carat_temp}&maxCarat={max_carat}&minColor={color}&maxColor={color}&minClarity={clarity}&maxClarity={clarity}&maxDateType=MANUFACTURING_REQUIRED&isQuickShip=false&hasVisualization=false&isFiltersExpanded=false&astorFilterActive=false&country=USA&language=en-us&currency=USD&productSet=BN"
                yield scrapy.Request(url_2, callback=self.recursive_find_partitions, headers=header,cookies=cookies,
                                     meta={'first': False, 'color': color, 'clarity': clarity, 'min_carat': max_carat_temp,
                                           'max_carat': max_carat},dont_filter=True)
            else:
                url_3 = f"https://www.bluenile.com/api/public/diamond-search-grid/v2?startIndex=0&pageSize=1000&unlimitedPaging=false&sortDirection=asc&sortColumn=default&shape=RD,PR,EC,AS,CU,MQ,RA,OV,PS,HS&minCarat={min_carat}&maxCarat={max_carat}&minColor={color}&maxColor={color}&minClarity={clarity}&maxClarity={clarity}&maxDateType=MANUFACTURING_REQUIRED&isQuickShip=false&hasVisualization=false&isFiltersExpanded=false&astorFilterActive=false&country=USA&language=en-us&currency=USD&productSet=BN"
                yield scrapy.Request(url_3, callback=self.parse, headers=header, dont_filter=True,cookies=cookies)
                if cont_count > 1000:
                    url_3 = f"https://www.bluenile.com/api/public/diamond-search-grid/v2?startIndex=1000&pageSize=1000&unlimitedPaging=false&sortDirection=asc&sortColumn=default&shape=RD,PR,EC,AS,CU,MQ,RA,OV,PS,HS&minCarat={min_carat}&maxCarat={max_carat}&minColor={color}&maxColor={color}&minClarity={clarity}&maxClarity={clarity}&maxDateType=MANUFACTURING_REQUIRED&isQuickShip=false&hasVisualization=false&isFiltersExpanded=false&astorFilterActive=false&country=USA&language=en-us&currency=USD&productSet=BN"
                    yield scrapy.Request(url_3, callback=self.parse, headers=header, dont_filter=True,cookies=cookies)
        else:
            if cont_count>0:
                # yield {
                #     'url': response.url, 'color': color, 'min_carat': min_carat, 'clarity': clarity,
                #     'max_carat': max_carat, 'count': cont_count
                # }
                url_3 = f"https://www.bluenile.com/api/public/diamond-search-grid/v2?startIndex=0&pageSize=1000&unlimitedPaging=false&sortDirection=asc&sortColumn=default&shape=RD,PR,EC,AS,CU,MQ,RA,OV,PS,HS&minCarat={min_carat}&maxCarat={max_carat}&minColor={color}&maxColor={color}&minClarity={clarity}&maxClarity={clarity}&maxDateType=MANUFACTURING_REQUIRED&isQuickShip=false&hasVisualization=false&isFiltersExpanded=false&astorFilterActive=false&country=USA&language=en-us&currency=USD&productSet=BN"
                yield scrapy.Request(url_3, callback=self.parse, headers=header, dont_filter=True,cookies=cookies)
                if cont_count>1000:
                    url_3 = f"https://www.bluenile.com/api/public/diamond-search-grid/v2?startIndex=1000&pageSize=1000&unlimitedPaging=false&sortDirection=asc&sortColumn=default&shape=RD,PR,EC,AS,CU,MQ,RA,OV,PS,HS&minCarat={min_carat}&maxCarat={max_carat}&minColor={color}&maxColor={color}&minClarity={clarity}&maxClarity={clarity}&maxDateType=MANUFACTURING_REQUIRED&isQuickShip=false&hasVisualization=false&isFiltersExpanded=false&astorFilterActive=false&country=USA&language=en-us&currency=USD&productSet=BN"
                    yield scrapy.Request(url_3, callback=self.parse, headers=header, dont_filter=True,cookies=cookies)

    def parse(self, response):
        # print(response.body)
        data = json.loads(response.body)

        for diamond in data['results']:
            detailsPageUrl = diamond['detailsPageUrl'].replace('.','')
            detail_page_url = 'https://www.bluenile.com'+detailsPageUrl
            carat = ''.join([x.strip() for x in diamond['carat']])
            price = ''.join([x.strip() for x in diamond['price']]).replace('$','').replace(',','')
            price_per_carat = ''.join([x.strip() for x in diamond['pricePerCarat']]).replace('$','').replace(',','')
            sku = diamond['skus'][0]
            blue_niles_id = diamond['id']
            shape = ''.join([x.strip() for x in diamond['shapeName']])
            clarity = ''.join([x.strip() for x in diamond['clarity']])
            color = ''.join([x.strip() for x in diamond['color']])
            culet = ''.join([x.strip() for x in diamond['culet']])
            cuts = diamond['cut']
            if cuts:
                cuts = cuts[0]['label']
            depth = ''.join([x.strip() for x in diamond['depth']])
            fluorescence = ''.join([x.strip() for x in diamond['fluorescence']])
            polish = ''.join([x.strip() for x in diamond['polish']])
            symmetry = ''.join([x.strip() for x in diamond['symmetry']])
            measurementss = diamond['measurements']
            if measurementss:
                measurementss = measurementss[0]['label']
            image_url = diamond['imageUrl']
            sold = diamond['sold']

            # pdfurl= 'https://bnsec.bluenile.com/bnsecure/certs/'+str(id)+'/GIA?country=USA&language=en-us'
            # pdfFile = parser.from_file(pdfurl)
            # # print(pdfFile["content"])
            # s=pdfFile["content"]
            # certificate = s.split('GIA ')[-1].split('\n')[0]
            certificate = None
            yield {
                "detail_page_url": detail_page_url,"carat": carat,"price": price,"price_per_carat": price_per_carat,
                "sku" : sku,"id": blue_niles_id,"shape": shape, "clarity": clarity, "color": color,"culet": culet,
                "cut": cuts,"depth": depth, "fluorescence": fluorescence,"polish": polish, "symmetry": symmetry,
                "measurement": measurementss, "image_url": image_url, "sold_or_inactive": sold , "certificate_number": certificate
            }

            # now = datetime.now()
            # dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
            # with connection.cursor() as cursor:
            #
            #     sql = "SELECT id,blue_niles_id,prices FROM blue_niles_diamonds WHERE blue_niles_id='%s'"
            #     # print(sql)
            #     # return
            #
            #     cursor.execute(sql % (blue_niles_id))
            #     result = cursor.fetchone()
            #     connection.commit()
            #     has_carat = certificate!=None
            #     if result is None:
            #         price_str = "'{\"" + str(price) + "\"}'"
            #         sql = "INSERT INTO blue_niles_diamonds (blue_niles_id,detail_page_url,sku,has_cert,certificate_number,carat," \
            #               "price_per_carat,shape,color,culet,clarity,fluorescence,polish,symmetry,cut,measurement" \
            #               ",depth,prices,sold_or_inactive,first_scraped_at,last_scraped_at)" \
            #               "VALUES ('%s','%s', '%s', %s, '%s', %s," \
            #               " %s, '%s', '%s', '%s' , '%s','%s', '%s', '%s', '%s', '%s'," \
            #               " %s, %s, %s, '%s','%s' )"%(blue_niles_id,detail_page_url,sku,has_carat,certificate,carat,
            #                         price_per_carat,shape,color,culet,clarity,fluorescence,polish,symmetry,cuts,measurementss
            #                         ,depth,price_str,sold,dt_string,dt_string)
            #
            #         # print(sql)
            #         cursor.execute(sql)
            #
            #         connection.commit()
            #
            #     else:
            #         exist_price = result[2]
            #         if exist_price[-1] != price:
            #             exist_price.append(price)
            #             exist_price = [str(ext_pr) for ext_pr in exist_price]
            #             price_str = "'{" + ','.join(exist_price) + "}'"
            #             sql = "UPDATE blue_niles_diamonds SET prices=%s,last_scraped_at='%s'" \
            #                   "WHERE blue_niles_id ='%s'"
            #
            #             cursor.execute(sql % (price_str, dt_string, blue_niles_id))
            #             connection.commit()
            #         else:
            #             sql = "UPDATE blue_niles_diamonds SET last_scraped_at='%s'" \
            #                   "WHERE blue_niles_id ='%s'"
            #
            #             cursor.execute(sql % (dt_string, blue_niles_id))
            #             connection.commit()
