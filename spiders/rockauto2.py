import scrapy
import pandas as pd
import csv
from openpyxl import load_workbook

class WorkSpider(scrapy.Spider):
    name = 'rockauto2'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'link','brand','part_number','category','title','price','image'
        ]
    };

    def start_requests(self):
        workbook = load_workbook("D:\Vishwapreviousprojects\seliniumPython\RockAutoBrands.xlsx")
        sheet = workbook.active
        row_count = sheet.max_row
        print(row_count)
        AURL_1_Links=[]
        for i in range(220,250): #starting at 2 and 336 because row one has headers
            hyprlink = sheet.cell(row=i, column=1).hyperlink.display
            # print('hyyyyyyyyyyyyyyy',hyprlink)

            yield scrapy.Request(url=hyprlink, callback=self.parseInside,
                                 # headers=headers
                                 )

    def parseInside(self, response):
        # brand = response.meta['brand']
        sublinks = response.xpath('//a[@class="navlabellink nvoffset nnormal"]//@href').extract()
        for sblink in sublinks:
            sublink1 = 'https://www.rockauto.com'+str(sblink)
            yield scrapy.Request(url=sublink1, callback=self.parsePartnum,
                                 # headers=header
                                 )
    def parsePartnum(self, response):
        prtlinks = response.xpath('//a[@class="navlabellink nvoffset nnormal"]//@href').extract()
        for prtlink in prtlinks:
            # print(prtlink)
            prtlink1 = 'https://www.rockauto.com'+str(prtlink)
            yield scrapy.Request(url=prtlink1, callback=self.parsePartdata,
                                 # headers=header
                                 )

    def parsePartdata(self, response):
        brand = ''.join([x.strip() for x in response.xpath('//span[@class="listing-final-manufacturer "]//text()').extract()])
        partnumber = ''.join([x.strip() for x in response.xpath('//span[@id="vew_partnumber[3]"]//text()').extract()])
        category = ''.join([x.strip() for x in response.xpath("//*[contains(text(), 'Category:')]//text()").extract()]).replace('Category:','').replace('  ','')
        title = ''.join([x.strip() for x in response.xpath('//span[@class="span-link-underline-remover"]//text()').extract()])
        price = ''.join([x.strip() for x in response.xpath('//span[@id="dprice[3][v]"]//text()').extract()])
        image = 'https://www.rockauto.com'+''.join([x.strip() for x in response.xpath('//img[@id="inlineimg_thumb[3]"]//@src').extract()])

        yield {
            'link': response.url,
            'brand': brand,
            'part_number': partnumber,
            'category': category,
            'title': title,
            'price': price,
            'image': image
        }
