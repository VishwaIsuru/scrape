# import scrapy
# import csv
#
# class WorkSpider(scrapy.Spider):
#     name = 'searchasp'
#
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': ['url','full_name','category','supreme_court_number','email',
#             'notification_directory','notarial_headquarters_address','office_telephone','first_name','last_name'
#
#         ]
#     };
#
#     urls = []
#     for p in range(1,238):
#         d= "https://unired.ramajudicial.pr/dirabogados/Search.aspx?&i="+str(p)
#         urls.append(d)
#     print(urls)
#     def start_requests(self):
#         i = 0
#         for u in self.urls:
#             yield scrapy.Request(url=u,callback=self.parse,
#                                  headers={'User-Agent': 'Mozilla Firefox 12.0'},
#                                  )
#             i += 1
#
#     def parse(self, response):
#
#         trs = response.xpath('//tr')
#         for tr in trs:
#             fullname = tr.xpath('.//td[1]//text()').extract_first()
#             catagery = tr.xpath('.//td[2]//text()').extract_first()
#             supcrtnum = tr.xpath('.//td[3]//text()').extract_first()
#             try:
#                 link = 'https://unired.ramajudicial.pr/dirabogados/'+tr.xpath('.//td[3]//@href').extract_first()
#             except:
#                 link = 'https://unired.ramajudicial.pr/'
#             yield scrapy.Request(url=link,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True,
#                                  meta={'fullname':fullname,'cat':catagery,'supcrtnum':supcrtnum})
#
#     def parseInside(self,response):
#         fullname = response.meta['fullname']
#         catagery = response.meta['cat']
#         supnum = response.meta['supcrtnum']
#         firstname = fullname.split(',')[-1]
#         lastname = fullname.split(',')[0]
#         email = response.xpath('//td[contains(text(), "Email")]/following-sibling::td/text()').extract_first()
#         notificationdirectory = ''.join([x.strip() for x in response.xpath('//td[contains(text(), "Dirección de Notificaciones")]/following-sibling::td/text()').extract()])
#         notarialheadqueartsaddress = ''.join([x.strip() for x in response.xpath('//td[contains(text(), "Dirección de Sede Notarial")]/following-sibling::td/text()').extract()])
#         officetelephone = response.xpath('//td[contains(text(), "Teléfono de la Oficina")]/following-sibling::td/text()').extract_first()
#
#         yield {
#             'url': response.url,
#             'full_name': fullname,
#             'category': catagery,
#             'supreme_court_number': supnum,
#             'email': email,
#             'notification_directory': notificationdirectory,
#             'notarial_headquarters_address': notarialheadqueartsaddress,
#             'office_telephone': officetelephone,
#             'first_name': firstname,
#             'last_name': lastname
#         }
