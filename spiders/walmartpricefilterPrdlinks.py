import scrapy
import csv
import codecs

class WorkSpider(scrapy.Spider):
    name = 'walmartpricefilter'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "prdlink","Product_Name",

        ]
    };

    urls = [
        # "https://www.walmart.com/search?q=motorcraft&affinityOverride=default&max_price=14&min_price=5&page=2",
    ]
    for p in range(1,26):
        for pr in range(1,86):
            s= "https://www.walmart.com/search?q=motorcraft&affinityOverride=default&max_price="+str(pr+1)+"&min_price="+str(pr)+"&page="+str(p)
            urls.append(s)
    def start_requests(self):

        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Gecko/20100'+str(i)+' Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):
        dd = response.xpath('//div[@class="mb0 ph1 pa0-xl bb b--near-white w-25"]')
        for d in dd:
            ff = 'https://www.walmart.com'+d.xpath('.//a//@href').extract_first()
            identi = d.xpath('.//a//@link-identifier').extract_first()
            yield {
                'prdlink': ff,
                'Product_Name': identi
            }