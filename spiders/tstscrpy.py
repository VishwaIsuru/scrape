import json
from copy import deepcopy
from scrapy import Spider, Request
from collections import OrderedDict


class Rococo(Spider):
    name = 'rococo'
    base_url = 'https://rococo.ie'

    custom_settings = {
        'FEED_EXPORTERS': {'xlsx': 'scrapy_xlsx.XlsxItemExporter'},
        'FEED_FORMAT': 'xlsx',
        'FEED_URI': 'Rococo.xlsx'
    }

    def start_requests(self):
        yield Request(self.base_url, self.parse_homepage)

    def parse_homepage(self, response):
        for level1 in response.css('ul.sf-menu > li'):
            label1 = level1.css('a span::text').get('').strip()
            url1 = level1.css('a::attr(href)').get()
            if label1 not in ['Shop All', 'Gift Vouchers']:
                continue

            if 'gift-voucher' in url1:
                meta = deepcopy(response.meta)
                meta['categories'] = [label1]
                yield Request(response.urljoin(url1), self.parse_products, meta=meta)

            for level2 in level1.css('.sub-menu > li'):
                label2 = level2.css('a span::text').get('').strip()
                url2 = level2.css('a::attr(href)').get()
                meta = deepcopy(response.meta)
                meta['categories'] = [label1, label2]
                yield Request(response.urljoin(url2), self.parse_products, meta=meta)

    def parse_products(self, response):
        product_urls = response.css('.product .product-meta > a::attr(href)').getall()
        if not product_urls:
            yield Request(response.url, self.parse_detail, meta=response.meta, dont_filter=True)
            return

        for url in product_urls:
            url = response.urljoin(url)
            yield Request(url, self.parse_detail, meta=response.meta)

        yield self.parse_pagination(response)

    def parse_pagination(self, response):
        next_page = response.css('a.next::attr(href)').get()
        if next_page:
            url = response.urljoin(next_page)
            return Request(url, self.parse_products, meta=response.meta)

    def parse_detail(self, response):
        item = OrderedDict()
        item['sku'] = response.css('.sku::text').get('').strip()
        if not item['sku'] or item['sku'] == 'N/A':
            item['sku'] = response.url.strip('/').split('/')[-1]
        item['sku'] = item['sku'][:32]
        item['name'] = response.css('.product_title::text').get('').strip()
        item['productClass'] = response.css('.product_title::text').get('').strip()
        item['price'], item['marketPrice'] = self.get_prices(response)
        categories = response.css('.posted_in a::text').getall()
        item['vender_categories'] = '&&'.join(categories)
        item['tags'] = response.css('.tagged_as a::text').get()
        images = response.css('.easyzoom a::attr(href)').getall()
        item['images'] = '&&'.join(images)
        item['description'] = '\n'.join(response.css('.woocommerce-product-details__short-description p::text').getall())
        item['Size (field:class)'] = ''
        item['Color (field:class)'] = ''
        item['Amount (field:variant)'] = ''
        item['stockLevel'] = int(response.css('.quantity input::attr(max)').get('0') or '0')
        item['variantID'] = ''
        item['variantQuantity'] = ''
        item['VariantPrice'] = ''
        item['vendor'] = 'info@rococo.ie'
        item['cat1'] = ''
        item['cat2'] = ''
        item['cat3'] = ''
        item['cat4'] = ''
        item['cat5'] = ''
        for index, cat in enumerate(categories):
            item['cat{}'.format(index + 1)] = cat
        item['breadcrumbs'] = ''
        item['brand'] = ''
        item['notes'] = 'women'

        sizes = self.get_sizes(response) or self.get_gift_sizes(response)
        if not sizes:
            if item['price']:
                yield item
            return

        is_voucher = bool(self.get_gift_sizes(response))

        for index, size in enumerate(sizes):
            size_item = deepcopy(item)
            size_item['price'] = size[3]
            if not is_voucher:
                size_item['Size (field:class)'] = size[0]
            else:
                size_item['Amount (field:variant)'] = size[0]
            size_item['Color (field:class)'] = size[4]
            size_item['stockLevel'] = size[1]
            size_item['variantID'] = '{}-{}'.format(size_item['sku'], size[2])
            size_item['variantQuantity'] = size[1]
            size_item['VariantPrice'] = size[3]

            if not index:
                if size_item['price']:
                    yield size_item
            else:
                if size_item['price']:
                    size_item['name'] = ''
                    size_item['productClass'] = ''
                    size_item['images'] = ''
                    yield size_item

    def get_sizes(self, response):
        final_sizes = []
        sizes = response.css('.variations_form::attr(data-product_variations)').get('[]')
        if not sizes:
            return final_sizes
        sizes = json.loads(sizes)
        for size in sizes:
            size_name = size['attributes'].get('attribute_pa_size', '')
            color_name = size['attributes'].get('attribute_pa_color', '')
            size_stock = size['max_qty']
            size_id = size['variation_id']
            size_price = size['display_price']
            final_sizes.append((size_name, size_stock, size_id, size_price, color_name))
        return final_sizes

    def get_gift_sizes(self, response):
        final_sizes = []
        for button in response.css('.gift-cards-list button'):
            size_name = button.css('::attr(value)').get()
            size_stock = 1
            size_id = button.css('::attr(value)').get()
            size_price = button.css('::attr(data-price)').get()
            color_name = ''
            final_sizes.append((size_name, size_stock, size_id, size_price, color_name))
        return final_sizes

    def get_prices(self, response):
        prices = response.css('.summary .price bdi::text').getall()
        if not prices:
            return '', ''
        prices = [price.strip() for price in prices if price.strip()]
        if len(prices) == 1:
            return prices[0], ''
        return prices[1], prices[0]
