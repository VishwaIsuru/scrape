import scrapy
import csv
import json
import requests

class WorkSpider(scrapy.Spider):
    name = 'fathom'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
             "First Name","Last Name","Phone","Email","Address"


        ]
    };
    urls = [
        "https://www.cbdallas.com/agents"

    ]

    def start_requests(self):

        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u.strip(), callback=self.parse,
                                 headers={"User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0","Host": "www.cbdallas.com","Content-Length": "1077",
                                          "Connection":"keep-alive","Accept-Encoding":"gzip, deflate, br","Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"})
            i += 1

    def parse(self, response):
        d=response.body
        # dataset=d["data"]
        j=json.loads(d)
        dataset=j['data']
        for da in dataset:
            fname=da['first_name']
            lname=da['last_name']
            phone=da['cell_phone']
            email=da['email']
            ads=da['offices']
            for ad in ads:
                address=ad['name']



                yield {

                    "Last Name":lname,
                    "First Name":fname,
                    "Phone":phone,
                    "Email":email,
                    "Address":address
                }