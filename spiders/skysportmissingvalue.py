# import scrapy
# import pymysql.cursors
# import csv
#
# csv.register_dialect('myDialect', delimiter='/', quoting=csv.QUOTE_NONE)
#
# connection = pymysql.connect(host='localhost',
#                              user='root',
#                              password='root',
#                              db='scrape', use_unicode=True, charset="utf8")
#
#
# class workspider(scrapy.Spider):
#     name = "skysportmissingdata"
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': ["ID",]
#     };
#
#     def start_requests(self):
#
#         with connection.cursor() as cursor:
#             sql = "SELECT horseurl,raceid FROM skysporthorsedata where horsename = %s"
#             cursor.execute(sql, (""))
#             notDoneItems = cursor.fetchall()
#             connection.commit()
#
#         for d in notDoneItems:
#
#             yield scrapy.Request(d[0], callback=self.parse,
#                              meta={'horseurl': d[0], 'raceid': d[1]})
#
#
#     def parse(self, response):
#         hoursurl = response.meta['horseurl']
#         raceid = response.meta['raceid']
#         horsenamenw = response.xpath('//h2[@class="sdc-site-racing-header__name"]//text()').extract_first().replace("'",
#                                                                                                                   "´")
#         try:
#             runnercob = horsenamenw.split('(')[1].replace(')', '').replace("'", "´")
#         except:
#             runnercob = '-'
#         headerdetails = response.xpath('//li[@class="sdc-site-racing-header__details-item"]//text()').extract()
#         agetx = ''
#         sex = ''
#         breading = ''
#         owner = ''
#         h = 0
#         for hddetail in headerdetails:
#             if 'Age:' in hddetail:
#                 agetx = headerdetails[h + 1].replace(' \n', '').replace('  ', '').replace("'", "´")
#             if 'Sex:' in hddetail:
#                 sex = headerdetails[h + 1].replace(' \n', '').replace('  ', '').replace('\n', '').replace("'", "´")
#             if 'Breeding:' in hddetail:
#                 breading = headerdetails[h + 1].replace(' \n', '').replace('  ', '').replace('\n', '').replace("'", "´")
#             if 'Owner:' in hddetail:
#                 owner = headerdetails[h + 1].replace(' \n', '').replace('  ', '').replace('\n', '').replace("'", "´")
#             h += 1
#         try:
#             age = agetx.split('(')[0]
#         except:
#             age = ''
#         try:
#             runneryob = agetx.split(',')[-1].replace(')', '').replace('\n', '').replace(' ', '').replace("'", "´")
#         except:
#             runneryob = ''
#         try:
#             sire = breading.split('(')[0].split('-')[0].replace("'", "´")
#         except:
#             sire = ''
#         try:
#             sirecob = breading.split('(')[1].split('-')[0].replace(')', '').strip().replace("'", "´")
#         except:
#             sirecob = ''
#         if len(sirecob) != 3:
#             sirecob = ''
#         try:
#             dam = breading.split('-')[1].split('(')[0].replace("'", "´")
#         except:
#             dam = ''
#         try:
#             damcob = breading.split('-')[1].split(')')[0].split('(')[1].strip().replace("'", "´")
#         except:
#             damcob = ''
#
#         try:
#             damsire = breading.split(') (')[1].split('(')[0].replace("'", "´")
#         except:
#             damsire = ''
#         try:
#             damsirecob = breading.split(') (')[1].split('(')[1].split(')')[0].strip().replace("'", "´")
#         except:
#             damsirecob = ''
#
#         if len(damcob) > 3:
#             damsire = damcob
#         if len(damcob) != 3:
#             damcob = ''
#
#         horsename = ''
#         with connection.cursor() as cursor:
#             sql = "UPDATE skysporthorsedata SET  horsename=%s ,runnercob = %s,runnerage = %s ,runneryob = %s," \
#                   "runnersex = %s," \
#                   "breading = %s,sire = %s,sirecob = %s," \
#                   "dam = %s,damcob = %s,damsire = %s,damsirecob = %s,owner = %s" \
#                   "WHERE hoursurl =%s and raceid =%s and horsename =%s"
#             # sql = sql % (horsenamenw, runnercob, age, runneryob, sex,
#             #              breading, sire, sirecob, dam, damcob, damsire, damsirecob, owner,hoursurl,raceid,horsename
#             #              )
#             print(sql)
#             cursor.execute(sql, (horsenamenw, runnercob, age, runneryob, sex,
#                          breading, sire, sirecob, dam, damcob, damsire, damsirecob, owner,hoursurl,raceid,horsename))
#
#             connection.commit()