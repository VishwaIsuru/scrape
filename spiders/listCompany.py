import scrapy
import csv
import re

class WorkSpider(scrapy.Spider):
    name = 'listcompany'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': ['Link','Company name', 'main products', 'contact person', 'Telephone','Mobilephone',
                               'address','email','emails']
    };

    urls = ["https://www.listcompany.org/A4_Paper_In_Thailand/p1.html","https://www.listcompany.org/A4_Paper_In_Thailand/p2.html",
            "https://www.listcompany.org/A4_Paper_In_Thailand/p3.html","https://www.listcompany.org/A4_Paper_In_Thailand/p4.html",
            ]

    def start_requests(self):
        i = 0
        for u in self.urls:

            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):
        links = response.xpath('//h4//a//@href').extract()
        for link in links:
            yield scrapy.Request(url=link,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True)

    def parseInside(self,response):
        companyName = response.xpath('//h1//text()').extract_first()
        mainproducts = ''.join([x.strip() for x in response.xpath('//strong[contains(text(),"Main Products: ")]/following-sibling::span//text()').extract()])
        contactPerson = ''.join([x.strip() for x in response.xpath('//strong[contains(text(),"Contact Person: ")]/following-sibling::span//text()').extract()])
        telephone = ''.join([x.strip() for x in response.xpath('//strong[contains(text(),"Telephone:")]/following-sibling::span//text()').extract()])
        mobiephone = ''.join([x.strip() for x in response.xpath('//strong[contains(text(),"Mobilephone: ")]/following-sibling::span//text()').extract()])
        address = ''.join([x.strip() for x in response.xpath('//strong[contains(text(),"Address: ")]/following-sibling::span//text()').extract()])
        email = ''.join([x.strip() for x in response.xpath(
            '//strong[contains(text(),"Email")]/following-sibling::span//text()').extract()])
        textall = ''.join([x.strip() for x in response.xpath('//text()').extract()])
        emails = re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", textall)

        yield {
            'Link':response.url,'Company name':companyName, 'main products':mainproducts, 'contact person':contactPerson, 'Telephone':telephone,'Mobilephone':mobiephone, 'address':address,'email':email,'emails':emails,
        }