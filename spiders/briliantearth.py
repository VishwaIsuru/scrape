# import scrapy
# import re
# import json
# import csv
# import pymysql.cursors
# from scrapy.http import FormRequest
# import random
# import string
# # encoding=utf8
# # import sys
# # from importlib import reload
# #
# # reload(sys)
# # sys.setdefaultencoding('utf8')
#
# import csv
# from datetime import datetime
#
# csv.register_dialect('myDialect', delimiter='/', quoting=csv.QUOTE_NONE)
#
# connection = pymysql.connect(host='localhost',
#                              user='root',
#                              password='root',
#                              db='scrape', use_unicode=True, charset="utf8")
#
# class workspider(scrapy.Spider):
#     name = "brilliant_earth"
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': ["id","upc","index_id","index_name","has_cert","certificate_number","rank_order","title","carat","shape",
#                                "color","clarity","fluorescence","polish","symmetry","cut","origin","measurement","depth","images"]
#     };
#
#     def start_requests(self):
#         colors = ['J','I','H','G','F','E','D']
#         colors = ['D']
#         clarities = ['SI2','SI1','VS2','VS1','VVS2','VVS1','IF','FL']
#
#         for col in colors:
#             for cal in clarities:
#                 for page in range(1, 5):
#                     header = {
#                         "user-agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(
#                             page) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"
#                     }
#                     url = "https://www.brilliantearth.com/loose-diamonds/list/?shapes=All&cuts=Fair,Good,Very Good,Ideal,Super Ideal&colors={col}&clarities={cal}&polishes=Good,Very Good,Excellent&symmetries=Good,Very Good,Excellent&fluorescences=Very Strong,Strong,Medium,Faint,None&min_carat=0.25&max_carat=12.88&min_table=48.00&max_table=87.00&min_depth=41.30&max_depth=89.10&min_price=370&max_price=1348720&stock_number=&row=0&page={page}&requestedDataSize=1000&order_by=price&order_method=asc&currency=$&has_v360_video=&dedicated=&min_ratio=1.00&max_ratio=2.75&shipping_day=&MIN_PRICE=370&MAX_PRICE=1348720&MIN_CARAT=0.25&MAX_CARAT=12.88&MIN_TABLE=45&MAX_TABLE=87&MIN_DEPTH=41.3&MAX_DEPTH=89.1"
#                     # url = f"https://www.brilliantearth.com/loose-diamonds/list/?page={page}&requestedDataSize=1000&order_by=price&order_method=asc"
#                     yield scrapy.Request(url, callback=self.parse, headers=header)
#
#     def parse(self, response):
#         # print(response.body)
#         data = json.loads(response.body)
#
#         for diamond in data['similar']:
#             id = diamond['id']
#             index_id = diamond['index_id']
#             upc = diamond['upc']
#             index_name = diamond['index_name']
#             has_cert = diamond['has_cert']
#             certificate_number = None
#             try:
#                 certificate_number = diamond['certificate_number']
#             except:
#                 pass
#             rank_order = diamond['rank_order']
#             title = diamond['title']
#             carat = diamond['carat']
#             shape = diamond['shape']
#             color = diamond['color']
#             clarity = diamond['clarity']
#             fluorescence = diamond['fluorescence']
#             polish = diamond['polish']
#             symmetry = diamond['symmetry']
#             cut = None
#             try:
#                 cut = diamond['cut']
#             except:
#                 pass
#             origin = None
#             try:
#                 origin = diamond['origin']
#             except:
#                 pass
#             measurement = None
#             try:
#                 measurement = diamond['measurements']
#             except:
#                 pass
#             depth = None
#             try:
#                 depth = diamond['depth']
#             except:
#                 pass
#             images = None
#             try:
#                 images = ','.join(['"https:'+image['src']+'"' for image in diamond['images']['images']])
#             except:
#                 pass
#
#             now = datetime.now()
#             dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
#
#             yield {
#                 "id":id, "upc":upc, "index_id":index_id, "index_name":index_name,
#                 "has_cert":has_cert, "certificate_number":certificate_number, "rank_order":rank_order,
#                 "title":title,"carat":carat, "shape":shape, "color":color, "clarity":clarity,
#                 "fluorescence":fluorescence, "polish":polish, "symmetry":symmetry, "cut":cut,
#                 "origin":origin,"measurement":measurement, "depth":depth, "images":images
#             }
#
#             with connection.cursor() as cursor:
#
#                 sql = "SELECT * FROM `BrilliantEarth` WHERE `id`=%s"
#                 cursor.execute(sql, (id))
#                 result = cursor.fetchone()
#                 print(result)
#                 # connection.commit()
#                 if result is None:
#                     sql = "INSERT INTO BrilliantEarth (id,upc,index_id,index_name,has_cert,certificate_number,rank_order," \
#                           "title,carat,shape,"\
#                           "color,clarity,fluorescence,polish,symmetry,cut,origin,measurement,depth,images,first_scraped_at)" \
#                           "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s , %s,%s, %s, %s, %s, %s, %s, %s, %s, %s , %s,%s)"
#
#                     cursor.execute(sql,
#                                    (id, upc, index_id, index_name,has_cert,certificate_number,rank_order,title,carat,shape,color,
#                                     clarity,fluorescence,polish,symmetry,cut,origin,measurement,depth,images,dt_string)
#                                    )
#
#                     connection.commit()
#
#                 else:
#                     sql = "UPDATE BrilliantEarth SET upc =%s ,index_id = %s,index_name = %s ,has_cert = %s,certificate_number = %s," \
#                           "rank_order = %s,title = %s,carat = %s," \
#                           "shape = %s,color = %s,clarity = %s,fluorescence = %s,polish = %s,symmetry = %s,cut = %s," \
#                           "origin = %s,measurement = %s,depth = %s,images = %s" \
#                           "WHERE id =%s"
#
#                     cursor.execute(sql, (upc,index_id,index_name,has_cert,certificate_number,rank_order,title,carat,shape,color,
#                                          clarity,fluorescence,polish,symmetry,cut,origin,measurement,depth,images,id))
#                     connection.commit()
