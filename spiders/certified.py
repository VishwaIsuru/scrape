import scrapy
import csv


class WorkSpider(scrapy.Spider):
    name = 'certified12'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Url", "Name","Adress","Email","Telephone"

        ]
    };

    urls = [

    ]

    # i = 0
    # while i < 9:
    #     ur = "http://www.trouverunsophrologue.fr/mots-acouphenes-m40-p"+str(i)+".html"
    #     urls.append(ur)
    #     i += 1
    # i = 0
    # while i < 10:
    #     ur = "http://www.trouverunsophrologue.fr/mots-adolescence-m41-p" + str(i) + ".html"
    #     urls.append(ur)
    #     i += 1
    # i = 0
    # while i < 6:
    #     ur = "http://www.trouverunsophrologue.fr/mots-cancer-m42-p" + str(i) + ".html"
    #     urls.append(ur)
    #     i += 1
    # i = 0
    # while i < 14:
    #     ur = "http://www.trouverunsophrologue.fr/mots-enfance-m43-p" + str(i) + ".html"
    #     urls.append(ur)
    #     i += 1
    # i = 0
    # while i < 11:
    #     ur = "http://www.trouverunsophrologue.fr/mots-entreprise-m44-p" + str(i) + ".html"
    #     urls.append(ur)
    #     i += 1
    # i = 0
    # while i < 13:
    #     ur = "http://www.trouverunsophrologue.fr/mots-perinatalite-m45-p" + str(i) + ".html"
    #     urls.append(ur)
    #     i += 1
    # i = 0
    # while i < 6:
    #     ur = "http://www.trouverunsophrologue.fr/mots-personnes-agees-m48-p" + str(i) + ".html"
    #     urls.append(ur)
    #     i += 1
    # i = 0
    # while i < 4:
    #     ur = "http://www.trouverunsophrologue.fr/mots-sexualite-m47-p"+str(i)+".html"
    #     urls.append(ur)
    #     i += 1
    # i = 0
    # while i < 14:
    #     ur = "http://www.trouverunsophrologue.fr/mots-sommeil-m46-p" + str(i) + ".html"
    #     urls.append(ur)
    #     i += 1
    i = 0
    while i < 5:
        ur = "http://www.trouverunsophrologue.fr/mots-sport-m49-p" + str(i) + ".html"
        urls.append(ur)
        i += 1

    # print (urls)

    def start_requests(self):

        i = 0
        for u in self.urls:
            print(u)
            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        details1 = response.xpath('//div[@class="column_in_grey"]/a')
        # details1 = response.xpath('//div[@class="column_in"]/a')

        links = details1.xpath('./@href').extract()

        # print links
        for link in links:
            l = 'http://www.trouverunsophrologue.fr' + link
            yield scrapy.Request(url=l,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'})
        # print l ,dont_filter=True


    def parseInside(self,response):

        name= response.xpath('//div[@class="title_h"]/h1/text()').extract()
        # specialization=response.xpath('//div[@class="column_in"][2]/div[@class="infos_details"]//text()').extract()
        try:
            q=response.xpath('//div[@class="column_in"][3]/div[@class="form_details"]//text()').extract()
            q = ''.join(q)
            address = q.split('Adresse postale')[1].split('Code postal')[0]
        except:
            address=" "
        try:
            e= response.xpath('//div[@class="column_in"][3]/div[@class="form_details"]//text()').extract()
            e = ''.join(e)
            email = e.split('Adresse email')[1].split('",')[0]
        except:
            email=" "

        try:
            t=response.xpath('//div[@class="column_in"][3]/div[@class="form_details"]//text()').extract()
            t = ''.join(t)
            teleph = t.split('phone 1')[1].split('Adresse email')[0]
        except:
            teleph=" "

        yield {
            "Url" : response.url,
            "Name":name,
            # "Specialization":specialization,

            "Adress":address,
            "Email":email,
            "Telephone":teleph

        }

