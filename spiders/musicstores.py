import scrapy
import csv
import codecs

class WorkSpider(scrapy.Spider):
    name = 'musicstores'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'url','Name','Phone','City Address',
            'Rating','Email','Website'
        ]
    };

    urls = ["https://music-stores.cmac.ws/states/"]

    def start_requests(self):
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parse(self, response):
        states =response.xpath('//a[@class="icon-right-circled"]//@href').extract()
        for li in states:
            yield scrapy.Request(url=li.strip(), callback=self.parseInside, headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parseInside(self, response):
        alpht = response.xpath('//ul[@class="pagination"]//li//a//@href').extract()
        for allink in range(1,len(alpht)):
            yield scrapy.Request(url=alpht[allink], callback=self.parseInsideAlpCity,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parseInsideAlpCity(self, response):
        citys = response.xpath('//a[@class="icon-right-circled"]//@href').extract()
        for lic in citys:
            yield scrapy.Request(url=lic.strip(), callback=self.parseInsideCityAll,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parseInsideCityAll(self, response):
        pagingUrls = []
        allshowcount = response.css('.main_title_3 > p:nth-child(3)').xpath('.//text()').extract_first()
        k = allshowcount.split(' ')[-1]
        prcount = int(k)
        if prcount > 20:
            l = int(prcount / 20)
            for k in range(1, l+2):
                li = response.url + str(k)+'/'
                pagingUrls.append(li)

        else:
            pagingUrls.append(response.url)

        for pagesofprdt in pagingUrls:
            yield scrapy.Request(url=pagesofprdt, callback=self.toAllMusicStores,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

    def toAllMusicStores(self, response):
        stlinks = response.xpath('//div[@class="wrapper"]//h3//a/@href').extract()
        for stlink in stlinks:
            yield scrapy.Request(url=stlink, callback=self.toAllStoreDetails,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

    def toAllStoreDetails(self, response):
        ur= response.url
        name = response.css('#description > h1').xpath('.//text()').extract_first()
        phone = response.xpath('//a[@class="tel"]//text()').extract_first()
        cityaddress = ''.join([x.strip() for x in response.css('#description > h2').xpath('.//text()').extract()])
        v = response.xpath('//div[@class="rating"]//i[@class="icon_star voted"]').extract()
        rating = len(v)
        wb =  response.xpath('//section[@id="description"]//p')
        email1 = ''
        website = ''
        for wbs in wb:
            wbel = ''.join([x.strip() for x in wbs.xpath('.//text()').extract()])
            if 'Email:' in wbel:
                email1 = wbel.replace('Email:','')
            if 'Website:' in wbel:
                website = wbel.replace('Website:','')

        yield {
            'url': ur,
            'Name': name,
            'Phone': phone,
            'City Address': cityaddress,
            'Rating': rating,
            'Email': email1,
            'Website': website
        }
