import scrapy
import csv
from scrapy_scrapingbee import ScrapingBeeRequest

class WorkSpider(scrapy.Spider):
    name = 'ricardocatageries'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': ['url','full_name',
        ]
    };

    urls = ["https://www.ricardo.ch/de/"]
    def start_requests(self):
        i = 0
        for u in self.urls:
            yield ScrapingBeeRequest(url=u, callback=self.parse, dont_filter=True,
                                     params={'render_js':True,'premium_proxy':False,'country_code':'us'}
                                     )
            i += 1

    def parse(self, response):
        allcats =response.xpath('//a[@class="MuiTypography-root MuiLink-root MuiLink-underlineNone style_categoryLink__3lflP MuiTypography-colorPrimary"]')
        for l in allcats:
            catlink = 'https://www.ricardo.ch'+l.xpath('.//@href').extract_first()
            cattext = ''.join([x.strip() for x in l.xpath('.//text()').extract()])
            yield ScrapingBeeRequest(url=catlink, callback=self.parseInsideSubcat1,dont_filter=True,
                                     params={'render_js':True,'premium_proxy':False,'country_code':'us'},meta={'maincat':cattext})

    #Get the catagery and subcategory
    def parseInsideSubcat1(self, response):
        maincattext = response.meta['maincat']
        subcat1divs = response.xpath('//a[@class="MuiButtonBase-root MuiListItem-root MuiListItem-gutters MuiListItem-button MuiListItem-secondaryAction"]')
        for subcat1 in subcat1divs:
            subcat1link = 'https://www.ricardo.ch'+subcat1.xpath('.//@href').extract_first()
            subcat1txt = ''.join([x.strip() for x in subcat1.xpath('.//text()').extract()])
            yield ScrapingBeeRequest(url=subcat1link, callback=self.parseInsideSubcat2, dont_filter=True,
                                     params={'render_js': True, 'premium_proxy': False, 'country_code': 'us'},
                                     meta={'maincat': maincattext,'subcat1':subcat1txt})

# Get max catagery level
    def parseInsideSubcat2(self, response):
        maincattext = response.meta['maincat']
        subcat1txt = response.meta['subcat1']
        subcat1link = response.url
        subctnum = subcat1link.split('/')[-2].split('-')[-1]
        subcat2divs = response.xpath('//li[@data-testid="breadcrumb-'+subctnum+'"]//li//a')
        if len(subcat2divs)==0:
            print('end subcat11111111111111111111111111111111111111111111111111111111111111111111111111111111')
            csvfilnm1 = 'ricadocatagery.csv'
            csvRow = (maincattext, subcat1txt, subcat1link)
            with open(csvfilnm1, "a", encoding="utf-8", newline='') as fp:
                wr = csv.writer(fp, dialect='excel')
                wr.writerow(csvRow)
        else:
            for subcat2 in subcat2divs:
                subcat2link = 'https://www.ricardo.ch'+subcat2.xpath('.//@href').extract_first()
                subcat2txt = ''.join([x.strip() for x in subcat2.xpath('.//text()').extract()])
                yield ScrapingBeeRequest(url=subcat2link, callback=self.parseInsideSubcat3, dont_filter=True,
                                         params={'render_js': True, 'premium_proxy': False, 'country_code': 'us'},
                                         meta={'maincat': maincattext, 'subcat1': subcat1txt,'subcat2':subcat2txt})

    def parseInsideSubcat3(self, response):
        maincattext = response.meta['maincat']
        subcat1txt = response.meta['subcat1']
        subcat2txt = response.meta['subcat2']
        subcat2link = response.url
        subctnum2 = subcat2link.split('/')[-2].split('-')[-1]
        subcat3divs = response.xpath('//li[@data-testid="breadcrumb-'+subctnum2+'"]//li//a')
        if len(subcat3divs)==0:
            print('end subcat222222222222222222222222222222222222222222222222222222222222222222222')
            csvfilnm1 = 'ricadocatagery.csv'
            csvRow = (maincattext, subcat1txt, subcat2txt,subcat2link)
            with open(csvfilnm1, "a", encoding="utf-8", newline='') as fp:
                wr = csv.writer(fp, dialect='excel')
                wr.writerow(csvRow)
        else:
            for subcat3 in subcat3divs:
                subcat3link = 'https://www.ricardo.ch' + subcat3.xpath('.//@href').extract_first()
                subcat3txt = ''.join([x.strip() for x in subcat3.xpath('.//text()').extract()])
                yield ScrapingBeeRequest(url=subcat3link, callback=self.parseInsideSubcat4, dont_filter=True,
                                         params={'render_js': True, 'premium_proxy': False, 'country_code': 'us'},
                                         meta={'maincat': maincattext, 'subcat1': subcat1txt,
                                               'subcat2': subcat2txt,'subcat3':subcat3txt})

    def parseInsideSubcat4(self, response):
        maincattext = response.meta['maincat']
        subcat1txt = response.meta['subcat1']
        subcat2txt = response.meta['subcat2']
        subcat3txt = response.meta['subcat3']
        subcat3link = response.url
        subctnum3 = subcat3link.split('/')[-2].split('-')[-1]
        subcat4divs = response.xpath('//li[@data-testid="breadcrumb-' + subctnum3 + '"]//li//a')
        if len(subcat4divs) == 0:
            print('end subcat33333333333333333333333333333333333333333333333333333333333333333333')
            csvfilnm1 = 'ricadocatagery.csv'
            csvRow = (maincattext, subcat1txt, subcat2txt, subcat3txt,subcat3link)
            with open(csvfilnm1, "a", encoding="utf-8", newline='') as fp:
                wr = csv.writer(fp, dialect='excel')
                wr.writerow(csvRow)
        else:
            for subcat4 in subcat4divs:
                subcat4link = 'https://www.ricardo.ch' + subcat4.xpath('.//@href').extract_first()
                subcat4txt = ''.join([x.strip() for x in subcat4.xpath('.//text()').extract()])
                yield ScrapingBeeRequest(url=subcat4link, callback=self.parseInsideSubcat5, dont_filter=True,
                                         params={'render_js': True, 'premium_proxy': False, 'country_code': 'us'},
                                         meta={'maincat': maincattext, 'subcat1': subcat1txt,
                                               'subcat2': subcat2txt, 'subcat3': subcat3txt,'subcat4':subcat4txt})
    def parseInsideSubcat5(self, response):
        maincattext = response.meta['maincat']
        subcat1txt = response.meta['subcat1']
        subcat2txt = response.meta['subcat2']
        subcat3txt = response.meta['subcat3']
        subcat4txt = response.meta['subcat4']
        subcat4link = response.url
        subctnum4 = subcat4link.split('/')[-2].split('-')[-1]
        subcat5divs = response.xpath('//li[@data-testid="breadcrumb-' + subctnum4 + '"]//li//a')
        if len(subcat5divs) == 0:
            print('end subcat4444444444444444444444444444444444444444444444444444444444444444444444444')
            csvfilnm1 = 'ricadocatagery.csv'
            csvRow = (maincattext, subcat1txt, subcat2txt, subcat3txt,subcat4txt,subcat4link)
            with open(csvfilnm1, "a", encoding="utf-8", newline='') as fp:
                wr = csv.writer(fp, dialect='excel')
                wr.writerow(csvRow)
        else:
            for subcat5 in subcat5divs:
                subcat5link = 'https://www.ricardo.ch' + subcat5.xpath('.//@href').extract_first()
                subcat5txt = ''.join([x.strip() for x in subcat5.xpath('.//text()').extract()])
                csvfilnm1 = 'ricadocatagery.csv'
                csvRow = (maincattext, subcat1txt, subcat2txt, subcat3txt, subcat4txt,subcat5txt, subcat5link)
                with open(csvfilnm1, "a", encoding="utf-8", newline='') as fp:
                    wr = csv.writer(fp, dialect='excel')
                    wr.writerow(csvRow)

