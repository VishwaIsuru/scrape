import scrapy
import csv

class workspider(scrapy.Spider):
    name = "resourcecoach"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Url","Area","Name","Type of coaching",
            "Place","Email Address","Contact Number","Website URL"

                ]
    };

    urls = ["https://reseaucoaching.com/region/auvergne-rhone-alpes/","https://reseaucoaching.com/region/auvergne-rhone-alpes/?wpv_view_count=27715&wpv_paged=2",
            "https://reseaucoaching.com/region/auvergne-rhone-alpes/?wpv_view_count=27715&wpv_paged=3","https://reseaucoaching.com/region/auvergne-rhone-alpes/?wpv_view_count=27715&wpv_paged=4",
            "https://reseaucoaching.com/region/auvergne-rhone-alpes/?wpv_view_count=27715&wpv_paged=5","https://reseaucoaching.com/region/auvergne-rhone-alpes/?wpv_view_count=27715&wpv_paged=6",
            "https://reseaucoaching.com/region/bretagne/","https://reseaucoaching.com/region/bretagne/?wpv_view_count=27752&wpv_paged=2",
            "https://reseaucoaching.com/region/corse/","https://reseaucoaching.com/region/hauts-de-france/","https://reseaucoaching.com/region/hauts-de-france/?wpv_view_count=27876&wpv_paged=2",
            "https://reseaucoaching.com/region/outre-mer/"]

    def start_requests(self):

        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )

    def parse(self, response):
        allcouchlinks = response.xpath('//h4/a/@href').extract()

        for coachlink in allcouchlinks:
            yield scrapy.Request(url=coachlink, callback=self.toAllCoachDetail,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

    def toAllCoachDetail(self, response):
        url = response.url
        area = response.css('.rc21-cch-loc').xpath('.//text()').extract_first()
        typecouch = response.css('div.wp-block-column:nth-child(2) > div:nth-child(1)').xpath('.//text()').extract_first()
        name = response.css('.page-header-title').xpath('.//text()').extract_first()
        place1 = response.css('div.wp-block-group:nth-child(3) > div:nth-child(1) > div:nth-child(2)').xpath('.//text()').extract_first()
        place = area + ' '+place1
        email = response.css('div.tb-field:nth-child(6) > a:nth-child(1)').xpath('.//text()').extract_first()
        contact = response.css('div.tb-field:nth-child(5)').xpath('.//text()').extract_first()
        weburl = response.css('div.tb-field:nth-child(7) > a:nth-child(1)').xpath('.//text()').extract_first()

        yield {
            "Url": url,
            "Area": area,
            "Name": name,
            "Type of coaching": typecouch,
            "Place": place,
            "Email Address": email,
            "Contact Number": contact,
            "Website URL": weburl
        }