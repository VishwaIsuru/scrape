import scrapy
from scrapy.http import FormRequest
import csv
import pandas as pd
import json
import requests


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:103.0) Gecko/20100101 Firefox/103.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1',
}

class workspider(scrapy.Spider):
    name = "fleetPrideScCopy"

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'ASIN','URL','Status','Seller','Price','Shipping Amount'
    ]
    };

    def start_requests(self):
        col_list = ['Part Number', 'Cross Reference','Product Link']
        df = pd.read_csv("D:\\Songs\\amazonWeekly\\Fleet_Pride_product_Links_15_5_2023.csv",
                         usecols=col_list)
        partnumrs = df["Part Number"]
        crossreffs = df["Cross Reference"]
        prdlinks = df["Product Link"]
        print(len(partnumrs))
        for i in range(0, len(partnumrs)):
        #     print(i, 'current csv point')
        # urls = [
        #     'https://www.fleetpride.com/parts/otr-brake-drum-otr1601b',
        #     'https://www.fleetpride.com/parts/power-products-electrical-tape-el60'
            prtnum = partnumrs[i]
            crosfer = crossreffs[i]
            yield scrapy.Request(url=prdlinks[i], callback=self.parse_inside, dont_filter=True,
                                 meta={'prtnumbr':prtnum,'crosfer': crosfer}
                                 )

    def parse_inside(self,response):
        prdurl = response.url
        prtnumfip = response.meta['prtnumbr']
        crosfer = response.meta['crosfer']
        b = response.body
        try:
            title = str(b).split('"shortDesc":')[1].split('"productSpecsS"')[0].replace('",','').replace('"','')
        except:
            title = ''
        try:
            breadcumb = ' > '.join(str(b).split('"CCFPProductCategory":')[1].split('"allowBO"')[0].replace('",','').replace('"','').split('|')[::-1])
        except:
            breadcumb = title
        try:
            brand = str(b).split('"FPBrandName":')[1].split('"FPBrandCode"')[0].replace('",', '').replace('"', '')
        except:
            brand = ''
        try:
            partnumber = str(b).split('"partNumber":')[1].split('"manufacturerPart"')[0].replace('",', '').replace('"', '')
        except:
            partnumber = ''
        k = str(b).split('"interchangePartNumber":')
        crossrefernces = []
        for l in k:
            if 'fleetprideCarried' in l:
                cr= l.split('fleetprideCarried')[0].replace('",', '').replace('"', '')
                crossrefernces.append(cr)
        crossrefernce = ' '.join(crossrefernces)

        q = str(b).split('"PKG&HAZMAT":')[-1].split(',"showNewSubscriptionSelection"')[0].replace('",', '').replace('"','')
        specifications = []
        for u in q.split(','):
            if 'Ship' not in u:
                speci = ': '.join(u.split('key')[0].replace('{value:','').replace('[','').split('name:')[::-1])
                specifications.append(speci)
        try:
            mainimage = str(b).split('"Product Image":')[1].split(',')[0].replace('",','').replace('"','').replace('\\','').split('uri:')[1]
        except:
            mainimage = ''
        try:
            altim1 = str(b).split('"mediaType":"Alternate Images"')[1].split('"uri":')[1].split(',')[0].replace('",', '').replace('"', '').replace('\\', '')
        except:
            altim1 = ''
        try:
            altim2 = str(b).split('"mediaType":"Alternate Images"')[2].split('"uri":')[1].split(',')[0].replace('",','').replace('"', '').replace('\\', '')
        except:
            altim2 = ''
        try:
            altim3 = str(b).split('"mediaType":"Alternate Images"')[3].split('"uri":')[1].split(',')[0].replace('",','').replace('"', '').replace('\\', '')
        except:
            altim3 = ''
        thumb_imagess = altim1+',',altim2+','+altim3
        # for price
        sku = str(b).split('"SKU":')[1].split(',"ownerId":')[0].replace('",', '').replace('"', '')
        poolnumber = str(b).split('"DSPPartNumber":')[1].split(',"partNumber":')[0].split(',"poolNumber":')[1].replace('"','')
        partNumber = str(b).split('"DSPPartNumber":')[1].split(',"manufacturerPart":')[0].split(',"partNumber":')[1].replace('"', '')
        csrf = str(b).split('"name":"getPrice",')[1].split('}')[0].split('"csrf":')[1].replace('"', '')
        vid = str(b).split('"vid":')[1].split(',')[0].replace('"', '')
        ver = str(b).split('"name":"getPrice",')[1].split('}')[0].split(',"csrf":')[0].split(',"ver":')[1].replace('"', '')

        headersprice = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:103.0) Gecko/20100101 Firefox/103.0',
            'Accept': '*/*',
            'Accept-Language': 'en-US,en;q=0.5',
            'X-User-Agent': 'Visualforce-Remoting',
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
            'Origin': 'https://www.fleetpride.com',
            'Connection': 'keep-alive',
            'Referer': prdurl,
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
        }

        data = {"action": "CCPDCProductDetailSimpleController", "method": "getPrice", "data": [
            {"storefront": "parts", "portalUserId": "", "effAccountId": "", "priceGroupId": "", "currentCartId": "",
             "userIsoCode": "USD", "userLocale": "en_US", "currentPageName": "ccrz__ProductDetails",
             "currentPageURL": prdurl,
             "queryParams": {"isPunchoutCart": False, "BranchLoc": "COM", "DCLoc": "COM,COI,LB,GAR,WCA,ANA,VS"}},
            ["{\"SKU\":\"'"+sku+"'\",\"poolNumber\":\""+str(poolnumber)+"\",\"partNumber\":\"'"+partNumber+"'\"}"], "COM"], "type": "rpc",
                "tid": 8, "ctx": {
                "csrf": csrf,
                "vid": vid, "ns": "", "ver": int(float(ver))}}

        print('verrrr',poolnumber)
        # csvfilnm = 'Fleet_Pride_Freightliner_Product_Data_2.csv'
        # csvRow = ([prtnumfip, crosfer, prdurl, breadcumb, title, brand, partNumber, 'price', crossrefernce, mainimage,
        #            thumb_imagess] + specifications)
        # with open(csvfilnm, "a", encoding="utf-8", newline='') as fp1:
        #     wr = csv.writer(fp1, dialect='excel')
        #     wr.writerow(csvRow)
        request_body = json.dumps(data)
        yield scrapy.Request(url='https://www.fleetpride.com/parts/apexremote', callback=self.parse_inside_price,
                             method="POST",dont_filter=True,
                             body=request_body,headers=headersprice,
                             meta= {'prtnumFip':prtnumfip,'crosrefFip':crosfer,'prdlink':prdurl,'breadcum':breadcumb,
                                    'prdname':title,'brand': brand,'prtnumbrfp':partNumber,'crossfrefp':crossrefernce,
                                    'mainimg': mainimage,'thumimgs':thumb_imagess,'specifications': specifications
                                    }
                             )

    def parse_inside_price(self, response):
        url = response.body
        try:
            price = str(response.body).split('BASEPRICE":')[1].split(',')[0]
        except:
            price = ''
        prtnumFip = response.meta['prtnumFip']
        crosrefFip = response.meta['crosrefFip']
        prdlink = response.meta['prdlink']
        breadcum = response.meta['breadcum']
        prd_name = response.meta['prdname']
        brand = response.meta['brand']
        part_num = response.meta['prtnumbrfp']
        crossrefernce = response.meta['crossfrefp']
        main_img = response.meta['mainimg']
        thumb_imagess = response.meta['thumimgs']
        specifictsarr = response.meta['specifications']

        csvfilnm = 'Fleet_Pride_16_05_2023_All.csv'
        csvRow = ([prtnumFip, crosrefFip, prdlink, breadcum, prd_name, brand, part_num, price, crossrefernce, main_img,
                   thumb_imagess] + specifictsarr)
        with open(csvfilnm, "a", encoding="utf-8", newline='') as fp1:
            wr = csv.writer(fp1, dialect='excel')
            wr.writerow(csvRow)