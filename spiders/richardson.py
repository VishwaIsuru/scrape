import scrapy
import csv
import re
import requests

class WorkSpider(scrapy.Spider):
    name = 'richardson'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
                # "Name","Phone","Email","Website","Address","Office Phone"
                "Url"

        ]
    };
    urls = [

        "file:///home/vishwa/Desktop/Goede%20doelen%20%E2%80%A2%20Geef.nl.html"
    ]

    def start_requests(self):

        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'})
            i += 1

    def parse(self, response):


        details = response.xpath('//div[@class="charityInformation charity charityCard"]/a[@class="button colouredButton white fullWidth"]')

        links = details.xpath('./@href').extract()

        for link in links:

            yield scrapy.Request(url=link, callback=self.parseInside, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 dont_filter=True)
        # print l ,dont_filter=True

    def parseInside(self, response):

        # dataurl=response.url.split('over-ons')[-1]
        datadetail = response.url+'/anbi'




        yield {
            # "Url":response.url,
            "Url":datadetail
        }