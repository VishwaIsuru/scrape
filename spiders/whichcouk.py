import scrapy
import csv
from csv import reader

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
}

class workspider(scrapy.Spider):
    name = "whichcouk"

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': ['url','trade','postalcode','name',
            'primary_phone','secondary_phone','email','website','address'
                               ]
    };

    def start_requests(self):
        trades = []
        postcodes = []
        with open('tradewhichsearch.csv', 'r', encoding="utf8",errors='ignore') as read_obj:
            csv_reader = reader(read_obj)
            for row in csv_reader:
                trade = row[1]
                trades.append(trade)
        with open('postcode.csv', 'r', encoding="utf8",errors='ignore') as read_obj1:
            csv_reader = reader(read_obj1)
            for row in csv_reader:
                postcode = row[1]
                postcodes.append(postcode)
        print('trade linkddddd',len(trades))
        print('post linkddddd', len(postcodes))
        for i in range(1, len(trades)):
            for j in range(1,len(postcodes)):
                ur = 'https://trustedtraders.which.co.uk/'+str(trades[i])+'-in-'+str(postcodes[j].replace(' ','-').lower())+'/'
                yield scrapy.Request(url=ur, callback=self.parse_inside, dont_filter=True,headers=headers,
                                     meta={'trade':trades[i],'postalcode':postcodes[j],}
                                     )

    def parse_inside(self,response):
        url = response.url
        trade = response.meta['trade']
        postalcode = response.meta['postalcode']
        tradescount = response.xpath('//div[@class="search-page__main-heading"]//b[2]//text()').extract_first()
        if tradescount is not None:
            pg = int(tradescount)/10+1
            for p in range(1,int(pg)+1):
                pglink = url+str(p)+'/'
                yield scrapy.Request(url=pglink, callback=self.parse_inside_pages, dont_filter=True, headers=headers,
                                     meta={'trade': trade, 'postalcode': postalcode}
                                     )

    def parse_inside_pages(self, response):
        tradeslinks = response.xpath('//div[@class="search-result__name"]//a//@href').extract()
        trade = response.meta['trade']
        postalcode = response.meta['postalcode']
        for tradelink in tradeslinks:
            trlink = 'https://trustedtraders.which.co.uk'+tradelink
            yield scrapy.Request(url=trlink, callback=self.parse_inside_trades, dont_filter=True, headers=headers,
                                 meta={'trade': trade, 'postalcode': postalcode}
                                 )

    def parse_inside_trades(self,response):
        url = response.url
        trade = response.meta['trade']
        postalcode = response.meta['postalcode']
        name = response.xpath('//h1//text()').extract_first()
        primaryphone = response.xpath('//a[@class="primary-phone-number"]//text()').extract_first()
        secondaryphone = response.xpath('//a[@class="secondary-phone-number"]//text()').extract_first()
        email = response.xpath('//a[@class="is-trackable"]//text()').extract_first()
        website = response.xpath('//a[@class="is-trackable url"]//text()').extract_first()
        address = ''.join([x.strip() for x in response.xpath('//div[@class="icon-location"]/following-sibling::div[@class="contact-info"]//text()').extract()])

        yield {
            'url':url,
            'trade': trade,
            'postalcode': postalcode,
            'name': name,
            'primary_phone': primaryphone,
            'secondary_phone': secondaryphone,
            'email': email,
            'website': website,
            'address': address
        }