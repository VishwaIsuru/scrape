import scrapy
import csv
import codecs

class WorkSpider(scrapy.Spider):
    name = 'usamls'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            "Name","Email","Phone"

        ]
    };

    urls = []
    for i in range(1,2):
        pgs = 'https://www.usamls.net/gdwcar/agents.asp?content=agents_roster&menu_id=0&the_letter=ALL&page='+str(i)
        urls.append(pgs)

    def start_requests(self):

        cookies = {
            'ASPSESSIONIDACDRRCSC': 'HMFPJIPDNFDCHIAGLEJPOLFN',
            'SERVERID': 'pn1|YAkgY|YAkSU',
            'ASPSESSIONIDACCSTASC': 'OEGJKIPDEJNPMAOAAAIJICAI',
            'ASPSESSIONIDACAQQCTC': 'FOEFJIPDKLFIJFDIMJIGMPGO',
            'ASPSESSIONIDAAAQQDTC': 'DMDPIIPDCFBIONLEOLJHMOMM',
        }

        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'Cache-Control': 'max-age=0',
        }
        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers=headers,cookies= cookies
                                 )
            i += 1

    def parse(self, response):
        agentdivs = response.xpath("//div[contains(@class,'agent_background')]//table//tr//td")
        name = ''
        for agent in agentdivs:
            name = agent.xpath('.//div[@class="agent_heading_inline"][1]//text()').extract_first()

        yield {
            "url":response.url,
            "Name": name

        }



