import scrapy
import csv
import codecs
import json

class WorkSpider(scrapy.Spider):
    name = 'rival'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            "url",
        ]
    };

    urls = [
        "https://n.rivals.com/api/v1/people",
    ]

    def start_requests(self):
        data = '{search:{member:Prospect,sport:Football,page_number:2,page_size:50,recruit_year:2013}}'
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0',
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-US,en;q=0.5',
            'Referer': 'https://n.rivals.com/search',
            'Content-Type': 'application/json;charset=utf-8',
            # 'X-XSRF-TOKEN': '6V0tmlyiRiUTFa3blwR3VMy8RX2OGlgZxaDik+CmnqgytBPb9FHkwL3ouQuK0npu2T0hzreFNwk25nY6CfXpjw==',
            'Origin': 'https://n.rivals.com',
            'Connection': 'keep-alive',
            'TE': 'Trailers',
        }
        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers= headers,method='POST', body=json.dumps(data),
                                 )
            i += 1

    def parse(self, response):

        details = response.xpath('//ul[@class="dl-menu"]//a')
        print(response.body)

