import scrapy
import csv
import codecs

class WorkSpider(scrapy.Spider):
    name = 'aptekaverano'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            "url","Product_Name","cat","Main_Image","Product_Price",
            "Producent","Kod_Producent","ID","EAN"

        ]
    };

    urls = [
        "https://aptekaverano.pl/",
    ]

    def start_requests(self):

        f = codecs.open("./aptekaVeranoError.xml", "w+", encoding='utf8')
        contents = "\n".join([
                                 '<?xml version="1.0" encoding="utf-8"?><offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1">',
                                 '</offers>'])
        f.write(contents)
        f.flush()
        f.close()

        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        print ('im here')
        details = response.xpath('//ul[@class="menu-content"]//a')

        links = details.xpath('.//@href').extract()
        lenarr = len(links)+1
        # print (links)

        for x in range(1,lenarr):

            yield scrapy.Request(url=links[x].strip(), callback=self.parseInside, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )

    def parseInside(self, response):

        pagingUrls = []

        try:
            paging = response.xpath('//div[@class="bottom-pagination-content clearfix"]//a/@href').extract_first()
            lastpgnum = response.xpath('//div[@class="bottom-pagination-content clearfix"]//a/span/text()').extract()
            valuelastpg = lastpgnum[-1]
            pagingother = paging.split('=')[0]

            i = 1
            while i < int(valuelastpg)+1:
                pgurl = "https://aptekaverano.pl" + pagingother + "=" + str(i)
                pagingUrls.append(pgurl)
                i += 1
        except:
            pagingUrls.append(response.url)

        for pagesofprdt in pagingUrls:
            yield scrapy.Request(url=pagesofprdt, callback=self.toAllPrdt,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

    def toAllPrdt(self, response):

        prducturl = response.xpath('//span[@class="product-name"]//a')
        prdlinks = prducturl.xpath('./@href').extract()

        for prdlink in prdlinks:
            linn = prdlink
            yield scrapy.Request(url=linn, callback=self.prdDetails, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 dont_filter=True)

    def prdDetails(self,response):

        print('I m herrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr')
        prdname = response.xpath('//h1/text()').extract_first()
        id = response.xpath('//span[@itemprop="sku"]//text()').extract_first()
        cat = response.url.split('/')[3].replace('-','')
        imageurl = response.xpath('//img[@id="bigpic"]/@src').extract_first()
        prdprice1 = response.xpath('//span[@id="our_price_display"]//text()').extract_first().replace(',','.')
        prdprice = prdprice1.split()[0]
        ean = ''
        discrdt = response.xpath('//table[@class="table descriptiontable"]//tr')
        for trdt in discrdt:
            td1 = trdt.xpath('.//td[1]/text()').extract_first()
            td2 = trdt.xpath('.//td[2]/text()').extract_first()
            if td1 == 'EAN':
                ean = td2

        producer = ''
        producerdtl = response.xpath('//div[@class="tab-content"]//text()').extract()
        z=0
        for prdcr in producerdtl:
            if prdcr == 'Producent:':
                producer = producerdtl[z+1]
            z+=1

        kodproducer = 'BBBB'
        # descri = '\n'.join([x.strip('') for x in
        #                     response.xpath('//div[@id="component_projector_longdescription"]//p//text()').extract() if
        #                     x.strip() != ''])
        # id = response.url.split('https://www.aptekaolmed.pl/product-pol-')[1].split('-')[0]

        line = '<o id="%s" price="%s" url="%s"><cat><![CDATA[%s]]></cat><name><![CDATA[%s]]></name><imgs><main url="%s"/></imgs><attrs><a name="Producent"><![CDATA[%s]]></a><a name="Kod_producenta"><![CDATA[%s]]></a><a name="EAN"><![CDATA[%s]]></a></attrs></o>' % (
            id, prdprice, response.url, cat, prdname, imageurl, producer, kodproducer,ean
        )

        yield {
            "url":response.url,
            "Product_Name":prdname,
            "cat":cat,
            "Main_Image":imageurl,
            "Product_Price":prdprice,
            "Producent":producer,
            "Kod_Producent":kodproducer,
            "ID": id,
            "EAN": ean
        }

        f = codecs.open("./aptekaVeranoError.xml", "r", encoding='utf8')
        contents = f.readlines()
        contents = [a for a in contents if a.strip() != '']
        f.close()

        contents.insert(len(contents) - 1, line.strip())

        f = codecs.open("./aptekaVeranoError.xml", "w", encoding='utf8')
        contents = "\n".join(contents)
        f.write(contents)
        f.flush()
        f.close()


