# import scrapy
# import re
# import json
# import csv
# import pymysql.cursors
# from datetime import datetime
# import numpy as np
# from selenium import webdriver
# from selenium.webdriver.firefox.options import Options
# import time
#
# options = Options()
# options.headless = True
#
# browser = webdriver.Firefox(options=options, executable_path=r'.\geckodriver.exe')
# browser.get('https://www.bluenile.com/uk/diamond-search')
# time.sleep(8)
#
# bnuuidssn = browser.get_cookie('bn_uuid-ssn')['value']
#
# print(bnuuidssn)
# browser.close()
#
# csv.register_dialect('myDialect', delimiter='/', quoting=csv.QUOTE_NONE)
#
# connection = pymysql.connect(host='localhost',
#                              user='root',
#                              password='root',
#                              db='scrape', use_unicode=True, charset="utf8")
#
# class workspider(scrapy.Spider):
#     name = "bluenile"
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': [
#             "detail_page_url","carat","price","price_per_carat",
#             "sku","id","shape", "clarity", "color","culet",
#             "cut","depth", "fluorescence","polish", "symmetry",
#             "measurements","image_url", "sold" , "certificate","dimdcount"
#
#             # "mincolor","maxColor","minClarity","maxClarity","minCarat","maxCarat","dimdcount"
#
#         ]
#     };
#     urls = []
#     color = ['K','J', 'I', 'H', 'G', 'F', 'E', 'D']
#
#     clarity = ['SI2','SI1','VS1','VS2','VVS2','VVS1','IF','FL']
#     for i in range(0,8):
#         for j in range(0,8):
#             colrclarity_url = 'https://www.bluenile.com/api/public/diamond-search-grid/v2?startIndex=0&pageSize=1000&unlimitedPaging=false&sortDirection=desc&minColor='+str(color[i])+'&maxColor='+str(color[i])+'&minClarity='+str(clarity[j])+'&maxClarity='+str(clarity[j])
#
#             for p1 in np.arange(0.22, 1.60, 0.01):
#                 p = round(p1, 2)
#                 carturl = colrclarity_url+'&sortColumn=cut&shape=RD,PR,EC,AS,CU,MQ,RA,OV,PS,HS&minCarat='+str(p)+'&maxCarat='+str(p)+'&maxDateType=MANUFACTURING_REQUIRED&isQuickShip=false&hasVisualization=false&isFiltersExpanded=false&astorFilterActive=false&country=USA&language=en-us&currency=USD&productSet=BN'
#                 urls.append(carturl)
#
#             for q1 in np.arange(1.50, 3.05, 0.05):
#                 q = round(q1, 2)
#                 carturl = colrclarity_url + '&sortColumn=default&shape=RD,PR,EC,AS,CU,MQ,RA,OV,PS,HS&minCarat=' + str(
#                     q) + '&maxCarat='+str(q)+'&maxDateType=MANUFACTURING_REQUIRED&isQuickShip=false&hasVisualization=false&isFiltersExpanded=false&astorFilterActive=false&country=USA&language=en-us&currency=USD&productSet=BN'
#                 urls.append(carturl)
#
#             for r1 in np.arange(3.00,5.20, 0.10):
#                 r = round(r1, 2)
#                 carturl = colrclarity_url + '&sortColumn=default&shape=RD,PR,EC,AS,CU,MQ,RA,OV,PS,HS&minCarat=' + str(
#                     r) + '&maxCarat='+str(r)+'&maxDateType=MANUFACTURING_REQUIRED&isQuickShip=false&hasVisualization=false&isFiltersExpanded=false&astorFilterActive=false&country=USA&language=en-us&currency=USD&productSet=BN'
#                 urls.append(carturl)
#
#             for s1 in np.arange(5.00,24.00, 0.50):
#                 s = round(s1, 2)
#                 carturl = colrclarity_url + '&sortColumn=default&shape=RD,PR,EC,AS,CU,MQ,RA,OV,PS,HS&minCarat=' + str(
#                     s) + '&maxCarat=' + str(s) + '&maxDateType=MANUFACTURING_REQUIRED&isQuickShip=false&hasVisualization=false&isFiltersExpanded=false&astorFilterActive=false&country=USA&language=en-us&currency=USD&productSet=BN'
#                 urls.append(carturl)
#
#     # url = 'https://www.bluenile.com/api/public/diamond-search-grid/v2?startIndex=1000&pageSize=1000&unlimitedPaging=false&sortDirection=asc&minColor=K&maxColor=D&minClarity=SI2&maxClarity=FL&sortColumn=default&shape=RD,PR,EC,AS,CU,MQ,RA,OV,PS,HS&minCarat=0.23&maxCarat=19.00&hasVisualization=true&country=USA&language=en-us&currency=USD&productSet=BN'
#
#     def start_requests(self):
#         print('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy')
#         u=1
#         for url in self.urls:
#             header = {
#                 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:93.0) Gecko/20100101 Firefox/93.0',
#                 'Accept': 'application/json, text/plain, */*',
#                 'Accept-Language': 'en-US,en;q=0.5',
#                 'x-bn-pageid': 'Diamond Search',
#                 'Connection': 'keep-alive',
#                 'Referer': 'https://www.bluenile.com/diamond-search',
#                 'Sec-Fetch-Dest': 'empty',
#                 'Sec-Fetch-Mode': 'cors',
#                 'Sec-Fetch-Site': 'same-origin',
#             }
#             cookies = {
#                 'browserCheck': 'ver~1&browserCheck~true',
#                 # 'pop': 'ver~3&b?S74RKV93V2RjYnpsJTJCUU5IcDdOaVc5dmdLeVE0NXVKb0RJazE5VVRZSW1XT3NUNHI2Q2hDJTJCJTJGSENOclhYJTJCNXk5OHVvZnlBb1BRaGU5OTZEVHlvM2UwS3hoVUNCUVdtN1VyVU9vaUwzbDBpa09FeWslMkZmWnJKMHdYRWIlMkZiU1VhWUNhWXZvM3VOU0JvTW0wUGdiY05jREtCWGRJb2clM0QlM0Q',
#                 # 'x_tm_a270366c96a911eaa1da0a797bbb2a41': f"EY+viewDiamondDetails*diamond-details*Vbluenile.com*Mbluenile.com*.catalogView*LD17163329*+DIAMONDS*round-cut*diamonds*refTab*.track*+true*track?{*B3*C*P3*5-1633340137911*4-www.bluenile.com/diamond-search*Y*Z*`{2-/`$1-/`$5-?`$9-*`$6-*`$10-*`$0-*`$4-*`$11-*Y*A1633321888770}*C{*`$3-/`$1-/`$5-?`$9-*`$6-*`$10-*`$0-*`$4-*`$11-*Y*Z*`$2-/`$1-/LD16925937?`$12-*+FCOM*.slot*+1*.type*+2*Y*A1633321905361}*C{*`$3-/uk/*Y*Z*`$2-/uk/`$8-/`$7-*Y*A1633340137911}]*C*E*Q*`$2-/uk/`$8-/`$7-*Y*Q__v*6-}",
#                 'bn_uuid-ssn': bnuuidssn,
#             }
#
#             u+=1
#
#
#
#             # url = "https://www.bluenile.com/api/public/diamond-search-grid/v2?startIndex="+str(page)+"&pageSize=50&country=USA&language=en-us&currency=USD&productSet=BN"
#             yield scrapy.Request(url, callback=self.parse,
#                                  headers=header,
#                                  cookies=cookies
#                                  )
#
#     def parse(self, response):
#         print('parser funtion.........................................................................')
#         print(response.body)
#         rurl = response.url
#         mincolor = rurl.split('minColor=')[-1].split('&')[0]
#         maxColor = rurl.split('maxColor=')[-1].split('&')[0]
#         minClarity = rurl.split('minClarity=')[-1].split('&')[0]
#         maxClarity = rurl.split('maxClarity=')[-1].split('&')[0]
#         minCarat = rurl.split('minCarat=')[-1].split('&')[0]
#         maxCarat = rurl.split('maxCarat=')[-1].split('&')[0]
#
#         data = json.loads(response.body)
#         dimdcount = data['countRaw']
#
#         if int(dimdcount)>1001:
#             pricedivurls = []
#             for b in (200, 1000, 10):
#                 c=b+10
#                 prcurl = response.url + '&minPrice=' + str(b) + '.00&maxPrice=' + str(c) + '.00'
#                 pricedivurls.append(prcurl)
#             for b1 in (1000, 3002875, 50000):
#                 d=b1+50000
#                 prcurl1 = response.url + '&minPrice=' + str(b1) + '.00&maxPrice=' + str(d) + '.00'
#                 pricedivurls.append(prcurl1)
#             y = 2
#             for prcurlget in pricedivurls:
#                 header = {
#                     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:93.0) Gecko/20100101 Firefox/93.0',
#                     'Accept': 'application/json, text/plain, */*',
#                     'Accept-Language': 'en-US,en;q=0.5',
#                     'x-bn-pageid': 'Diamond Search',
#                     'Connection': 'keep-alive',
#                     'Referer': 'https://www.bluenile.com/diamond-search',
#                     'Sec-Fetch-Dest': 'empty',
#                     'Sec-Fetch-Mode': 'cors',
#                     'Sec-Fetch-Site': 'same-origin',
#                 }
#                 cookies = {
#                     'browserCheck': 'ver~1&browserCheck~true',
#                     # 'pop': 'ver~3&b?S74RKV93V2RjYnpsJTJCUU5IcDdOaVc5dmdLeVE0NXVKb0RJazE5VVRZSW1XT3NUNHI2Q2hDJTJCJTJGSENOclhYJTJCNXk5OHVvZnlBb1BRaGU5OTZEVHlvM2UwS3hoVUNCUVdtN1VyVU9vaUwzbDBpa09FeWslMkZmWnJKMHdYRWIlMkZiU1VhWUNhWXZvM3VOU0JvTW0wUGdiY05jREtCWGRJb2clM0QlM0Q',
#                     # 'x_tm_a270366c96a911eaa1da0a797bbb2a41': f"EY+viewDiamondDetails*diamond-details*Vbluenile.com*Mbluenile.com*.catalogView*LD17163329*+DIAMONDS*round-cut*diamonds*refTab*.track*+true*track?{*B3*C*P3*5-1633340137911*4-www.bluenile.com/diamond-search*Y*Z*`{2-/`$1-/`$5-?`$9-*`$6-*`$10-*`$0-*`$4-*`$11-*Y*A1633321888770}*C{*`$3-/`$1-/`$5-?`$9-*`$6-*`$10-*`$0-*`$4-*`$11-*Y*Z*`$2-/`$1-/LD16925937?`$12-*+FCOM*.slot*+1*.type*+2*Y*A1633321905361}*C{*`$3-/uk/*Y*Z*`$2-/uk/`$8-/`$7-*Y*A1633340137911}]*C*E*Q*`$2-/uk/`$8-/`$7-*Y*Q__v*6-}",
#                     'bn_uuid-ssn': bnuuidssn,
#                 }
#                 y += 1
#                 yield scrapy.Request(prcurlget, callback=self.parse,
#                                      headers=header,
#                                      cookies=cookies
#                                      )
#
#         # yield {
#         #     "mincolor" : mincolor,
#         #     "maxColor" : maxColor,
#         #     "minClarity" : minClarity,
#         #     "maxClarity" : maxClarity,
#         #     "minCarat" : minCarat,
#         #     "maxCarat" : maxCarat,
#         #     "dimdcount" : dimdcount
#         #
#         # }
#
#         for diamond in data['results']:
#             detailsPageUrl = diamond['detailsPageUrl'].replace('.','')
#             detail_page_url = 'https://www.bluenile.com'+detailsPageUrl
#             carat = ''.join([x.strip() for x in diamond['carat']])
#             price = ''.join([x.strip() for x in diamond['price']])
#             price_per_carat = ''.join([x.strip() for x in diamond['pricePerCarat']])
#             sku = diamond['skus'][0]
#             id = diamond['id']
#             shape = ''.join([x.strip() for x in diamond['shapeCode']])
#             clarity = ''.join([x.strip() for x in diamond['clarity']])
#             color = ''.join([x.strip() for x in diamond['color']])
#             culet = ''.join([x.strip() for x in diamond['culet']])
#             cuts = diamond['cut']
#             cut = ''
#             for ct in cuts:
#                 cut = ct['label']
#             # cut = str(cuts).replace('[{u','').replace('}]','')
#             depth = ''.join([x.strip() for x in diamond['depth']])
#             fluorescence = ''.join([x.strip() for x in diamond['fluorescence']])
#             polish = ''.join([x.strip() for x in diamond['polish']])
#             symmetry = ''.join([x.strip() for x in diamond['symmetry']])
#             measurementss = diamond['measurements']
#             measurements = ''
#             for mv in measurementss:
#                 measurements = mv['label']
#             # measurements = str(measurementss).replace('[{u','').replace('}]','')
#             image_url = diamond['visualizationImageUrl']
#             sold = diamond['sold']
#
#             # pdfurl= 'https://bnsec.bluenile.com/bnsecure/certs/'+str(id)+'/GIA?country=USA&language=en-us'
#             # pdfFile = parser.from_file(pdfurl)
#             # # print(pdfFile["content"])
#             # s=pdfFile["content"]
#             # certificate = s.split('GIA ')[-1].split('\n')[0]
#             certificate = '-----'
#
#             print(id)
#             # if int(dimdcount)<1001:
#             yield {
#                 "detail_page_url": detail_page_url,"carat": carat,"price": price,"price_per_carat": price_per_carat,
#                 "sku" : sku,"id": id,"shape": shape, "clarity": clarity, "color": color,"culet": culet,
#                 "cut": cut,"depth": depth, "fluorescence": fluorescence,"polish": polish, "symmetry": symmetry,
#                 "measurements": measurements, "image_url": image_url, "sold": sold , "certificate": certificate,"dimdcount" : dimdcount
#             }
#
#             #
#             # with connection.cursor() as cursor:
#             #
#             #     sql = "SELECT * FROM `BlueNile` WHERE `id`=%s"
#             #     cursor.execute(sql, (id))
#             #     result = cursor.fetchone()
#             #     # connection.commit()
#             #     if result is None:
#             #         sql = "INSERT INTO BlueNile (detail_page_url,carat,price,price_per_carat,sku,id,shape,clarity,color," \
#             #               "culet,cut,depth,fluorescence,polish,symmetry,measurements,image_url,sold,certificate)" \
#             #               "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s , %s,%s, %s, %s, %s, %s, %s, %s, %s, %s )"
#             #
#             #         cursor.execute(sql,
#             #                        (detail_page_url,carat,price,price_per_carat,sku,id,shape,clarity,color,
#             #                         culet,cut,depth,fluorescence,polish,symmetry,measurements,image_url,sold,certificate)
#             #                        )
#             #
#             #         connection.commit()
#             #
#             #     else:
#             #         sql = "UPDATE BlueNile SET detail_page_url =%s ,carat = %s,price = %s ,price_per_carat = %s," \
#             #               "sku = %s,shape = %s,clarity = %s," \
#             #               "color = %s,culet = %s,cut = %s,depth = %s,fluorescence = %s,polish = %s,symmetry = %s," \
#             #               "measurements = %s,image_url = %s,sold = %s,certificate = %s" \
#             #               "WHERE id =%s"
#             #
#             #         cursor.execute(sql, (detail_page_url,carat,price,price_per_carat,sku,shape,clarity,color,
#             #                         culet,cut,depth,fluorescence,polish,symmetry,measurements,image_url,sold,certificate,id))
#             #         connection.commit()
#
#
#
#
#
#
#
#
#
#
