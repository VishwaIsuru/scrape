# import scrapy
# import csv
# import json
# import sys
#
# # reload(sys)
# # sys.setdefaultencoding('utf8')
#
# import csv
#
# csv.register_dialect('myDialect', delimiter='/', quoting=csv.QUOTE_NONE)
# import pymysql.cursors
#
# csv.register_dialect('myDialect', delimiter='/', quoting=csv.QUOTE_NONE)
# connection = pymysql.connect(host='localhost',
#                              user='root',
#                              password='root',port=3306,
#                              db='scrape', use_unicode=True, charset="utf8")
#
#
# class WorkSpider(scrapy.Spider):
#     name = 'yadavmess'
#
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': [
#             "ID","price","measure","flourence","ratio"
#
#         ]
#     };
#
#     def start_requests(self):
#
#         with connection.cursor() as cursor:
#             sql = "SELECT id,shape,carat FROM yadav"
#             cursor.execute(sql)
#             allItems = cursor.fetchall()
#             connection.commit()
#
#         for d in allItems:
#             header = {
#                 "Host": "www.yadavjewelry.com",
#                 "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0",
#                 "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
#                 "Accept-Language": "en-US,en;q=0.5",
#                 "Accept-Encoding": "gzip, deflate, br",
#                 "Referer": "https://www.yadavjewelry.com/",
#                 "Connection": "keep-alive",
#                 "Cookie": "__cfduid=dd64b11eac685f92654aa89fdcd0515dc1611304275; guest_token=Ii1MRWxYYng5aFBjeS1Obk51Y1lnWWcxNjExMzA0Mjc2NTY5Ig%3D%3D--25f6a1735b300156ff017594779103f903f6d339; _yadav_session=WGdyVWZVMDZLTktXazBuaFA3ZjZkeGlQRHpsSEVqaVFOTGF6elBVKzVYd1d0cnVSNFlQMDdJamJMSjdsK0hDd2pGOWhwRHZsUmhjUTZTMEM2RlBxM3VkbHk0TnU1RUpsNnhFMVJONjcwWWEyM3JYNEV5c0cxSkVHcFRDNitOK2ZJOXhYRHNRN0dZWWIxY293b01od1RRPT0tLXNkejU5d2J0QjAwZjRiOUFpemNXckE9PQ%3D%3D--acc4ed6b0b7e5697a0b78b7d8cb1b62c9ffb233d; ds-single-natural=%7B%22utf8%22%3A%22%E2%9C%93%22%2C%22labcreated%22%3A%22false%22%2C%22supplier_shipping_date%22%3A%22%22%2C%22shape-checkbox%22%3A%22%22%2C%22shape%22%3A%22CUS%22%2C%22search_waiting%22%3A%221%22%2C%22cut_min%22%3A%221%22%2C%22cut_max%22%3A%225%22%2C%22cut_ui_min%22%3A%221%22%2C%22cut_ui_max%22%3A%226%22%2C%22color_min%22%3A%2214%22%2C%22color_max%22%3A%2223%22%2C%22weight_min%22%3A%220.2%22%2C%22weight_max%22%3A%2220%22%2C%22color_colored%22%3A%22%22%2C%22clarity_min%22%3A%222%22%2C%22clarity_max%22%3A%2211%22%2C%22price_min%22%3A%22100%22%2C%22price_ui_min%22%3A%22%24100%22%2C%22price_max%22%3A%222000000%22%2C%22price_ui_max%22%3A%22%242000000%22%2C%22instore%22%3A%22all%22%2C%22colored_intensity%22%3A%22%22%2C%22polish_min%22%3A%220%22%2C%22polish_max%22%3A%225%22%2C%22depth_min%22%3A%2243%22%2C%22depth_ui_min%22%3A%2243%22%2C%22depth_max%22%3A%2288%22%2C%22depth_ui_max%22%3A%2288%22%2C%22symmetry_min%22%3A%220%22%2C%22symmetry_max%22%3A%225%22%2C%22tablep_min%22%3A%2248%22%2C%22tablep_ui_min%22%3A%2248%22%2C%22tablep_max%22%3A%2285%22%2C%22tablep_ui_max%22%3A%2285%22%2C%22fluor_min%22%3A%22-1%22%2C%22fluor_max%22%3A%226%22%2C%22width_min%22%3A%223%22%2C%22width_ui_min%22%3A%223%22%2C%22width_max%22%3A%2220%22%2C%22width_ui_max%22%3A%2220%22%2C%22with_image%22%3A%22false%22%7D; __kla_id=eyIkcmVmZXJyZXIiOnsidHMiOjE2MTEzMDQyODAsInZhbHVlIjoiIiwiZmlyc3RfcGFnZSI6Imh0dHBzOi8vd3d3LnlhZGF2amV3ZWxyeS5jb20vbmF0dXJhbC1sb29zZS1kaWFtb25kcy9yb3VuZC1kaWFtb25kcy9nb29kLWV4Y2VsbGVudC1jdXQifSwiJGxhc3RfcmVmZXJyZXIiOnsidHMiOjE2MTE3NzA5MDUsInZhbHVlIjoiIiwiZmlyc3RfcGFnZSI6Imh0dHBzOi8vd3d3LnlhZGF2amV3ZWxyeS5jb20vZGlhbW9uZC9yb3VuZC1kaWFtb25kLTAuMzItY2FyYXQtai1zaTIteWQxMDgxMzM4NCJ9fQ==; tracker_device=37146619-70ef-4440-8af4-6f827466d610; _ga=GA1.2.143006353.1611304282; popupSignedUp=no; intercom-id-orpdg3jb=12bf2975-3c72-46a1-99c8-7d0e3162072d; intercom-session-orpdg3jb=; _gcl_au=1.1.887953374.1611304766; _hjid=80e232e8-75a1-4dd8-bfcc-18be38002580; _fbp=fb.1.1611304768464.1202038824; _pin_unauth=dWlkPU5tUXdOakkzT0dZdE1qWmlNUzAwTVRNMkxUbGhaREV0WkRjd1ltVmpNR05qTkdZNQ; undefined=%7B%22utf8%22%3A%22%E2%9C%93%22%2C%22per_page%22%3A%22%22%2C%22page%22%3A%22%22%2C%22taxon_id%22%3A%2230%22%2C%22search%5Bmetal_any%5D%5B%5D%22%3A%22all%22%7D; cto_bundle=vq1vMV82TExPU001U1EzNCUyQnZmbjgwSHhmTiUyRlFzc255MGhWUExtUXR1STVpVzRZaHRXVDlQTUNuazZCQlZONG5lM2VLNUMlMkZTT0d2WmpXNWRTMWRIJTJGdWpwM3olMkJiaG9oUjMlMkZYSiUyRmh2clJlUG5pVSUyRjB3WlNWaTU3VG8xSDNLWklYWlJpZHhZVnA4UkxzM1Rma1NPJTJCRFU3bHlBV1ElM0QlM0Q; _gid=GA1.2.444974734.1611744793; _hjTLDTest=1; _hjIncludedInPageviewSample=1; Purchase=%5B%7B%22started_from%22%3A%22from_diamond%22%2C%22diamond%22%3Anull%2C%22setting%22%3Anull%2C%22yesp%22%3Afalse%2C%22type%22%3A%22engagement%20ring%22%2C%22status%22%3A%22initial%22%7D%5D; __cf_bm=de0864fe3b9c5d5b6a86a9d4c4f5639c6c813f78-1611770906-1800-ATLmygXEMETafoPRQySzC8zjTZcuudx/FMdUQ7shfhv33EQUcbMb/1c9CP0vRQR6f/NB+2td+/HUYcRZBjzaBzAJ2Do6rCwMFLK7TQoHqE2dnvvRnfTx3+6p1FS89QhL1w==; _uetvid=55f16c205c8d11eba8f1af656b2bbe17; _uetsid=21635c30608e11eb8ce453911e1bde77; _hjAbsoluteSessionInProgress=1",
#                 "Upgrade-Insecure-Requests": 1
#             }
#             if d[1] is not None:
#                 ndurl = 'https://www.yadavjewelry.com/diamond/'+str(d[0])
#                 yield scrapy.Request(ndurl, callback=self.parse,headers=header,
#                                  meta={'ID': d[0]})
#
#     def parse(self, response):
#
#         id = int(response.meta['ID'])
#         price = ''.join([x.strip() for x in response.css('.style-price').xpath('.//text()').extract()])
#         try:
#             gia = \
#             response.xpath('//a[@title="Open GIA certification"]/@href').extract_first().split('/')[-1].split('.')[
#                 0].replace('report-check?reportno=', '')
#         except:
#             gia = ''
#         j = 0
#         measurements = ''
#         fluorescence = ''
#         ratio = ''
#         msn = response.xpath('//div[@class="col-xs-12 col-sm-6"]//dt//text()').extract()
#         for s in msn:
#             if 'Measurements' == s:
#                 measurements = msn[j + 1]
#             if 'Fluorescence' == s:
#                 fluorescence = msn[j + 1]
#             if 'L/W Ratio' == s:
#                 ratio = msn[j + 1]
#             j += 1
#
#         yield {
#                 "ID": id,
#                 "price": price,
#                 "measure": measurements,
#                 "flourence": fluorescence,
#                 "ratio": ratio
#             }
#
#         # with connection.cursor() as cursor:
#         #     # print("DONE")
#         #     sql = "UPDATE RareDiamond SET measurements=%s,depth=%s,certificateId=%s WHERE id=%s"
#         #     cursor.execute(sql,(measurements,depth,certificateId,id))
#         #     connection.commit()
#
