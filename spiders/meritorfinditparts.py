import scrapy
import csv
import re
import requests
import pandas as pd

csvRow = ['Link','Asin','Name','Part Number','Manufacture','Description','MSRP','Price','Merito Description','Feature Benifits',
           'Cross Reference','Part Description'
          ]
with open('meritorFindpartScrapeData20.csv', "w", encoding="utf-8", newline='') as fp:
    wr = csv.writer(fp, dialect='excel')
    wr.writerow(csvRow)

class WorkSpider(scrapy.Spider):
    name = 'meritorfinditparts'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Name","Email","Number"

        ]
    };

    def start_requests(self):
        col_list = ["prdlink"]
        df = pd.read_csv("finditpartslinksFilter20.csv", usecols=col_list)
        urls = df["prdlink"]

        for i in range(0, 13000):
            u = urls[i]
            yield scrapy.Request(url=u, callback=self.parseProductdata,
                                 headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/201001'},
                                 meta = {'csvlink':u})

    # def parse(self, response):
    #     productlinks = response.xpath('//div[@class="product_search_result_tile_direction"]//a[1]//@href').extract()
    #     i = 0
    #     for prli in productlinks:
    #         prdlink = 'https://www.finditparts.com'+prli
    #         yield scrapy.Request(url=prdlink, callback=self.parseProductdata,
    #                              headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/201001'+str(i)+' Firefox/94.0','Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8','Accept-Language': 'en-US,en;q=0.5','Connection': 'keep-alive','Upgrade-Insecure-Requests': '1','Sec-Fetch-Dest': 'document','Sec-Fetch-Mode': 'navigate','Sec-Fetch-Site': 'none','Sec-Fetch-User': '?1','TE': 'trailers',},
    #                              # meta={'Index': i}
    #                              )
    #         i += 1

    def parseProductdata(self,response):
        linkcsv = response.meta['csvlink']
        producturl = response.url
        name = response.xpath('//div[@id="stellar_phase_2"]//h1//text()').extract_first().replace('  ','').replace('\n','')
        print(name)
        partnumber = response.xpath("//dt[contains(.,'Part Number')]/following-sibling::dd/text()").extract_first()
        print(partnumber)
        manufacture = response.xpath("//dt[contains(.,'Manufacturer')]/following-sibling::dd/text()").extract_first()
        print(manufacture)
        discription = response.xpath("//dt[contains(.,'Description')]/following-sibling::dd/h2/text()").extract_first()
        print(discription)
        try:
            msrp = response.xpath('//p[@class="product_info__msrp"]//text()').extract_first()
        except:
            msrp = ''
        print(msrp)
        try:
            pricedolr = response.xpath('//span[@class="price_tag__dollars"]//text()').extract_first()
        except:
            pricedolr = ''
        try:
            prcicecent = response.xpath('//span[@class="price_tag__cents"]//text()').extract_first()
        except:
            prcicecent = ''
        try:
            price = '$ ' + pricedolr + '.' + prcicecent
        except:
            price = '-'
        print(price)
        try:
            meritodescri = response.xpath('//div[@class="product_seo_content__row"]//p//text()').extract_first()
        except:
            meritodescri = ''
        print(meritodescri)
        featurebnfts = []
        try:
            featurebenifits = response.xpath(
                ('//div[@class="product_seo_content__row"]/following-sibling::ul//li'))
            for f in featurebenifits:
                fbnt = f.xpath('.//text()').extract_first()
                featurebnfts.append(fbnt)
                # print('feature benifits: ', fbnt)
        except:
            featurebenifits = ''
        feature_benifits = '\n'.join(featurebnfts)
        crosrefecss = []
        try:
            crosref = response.xpath(
                ('//ul[@class="list-unstyled cross-reference-list"]//li'))
            for cr in crosref:
                crossref = cr.xpath('.//text()').extract_first()
                crosrefecss.append(crossref)
        except:
            crossref = ''
        cros_reference = ','.join(crosrefecss)

        partdescripss = []
        pardescrs = response.xpath('//div[@class="product_description__row"]//tr')
        for prtdisc in pardescrs:
            try:
                deskey = prtdisc.xpath('.//td[1]//text()').extract_first()
            except:
                deskey = ''
            try:
                desval = prtdisc.xpath('.//td[2]//text()').extract_first()
            except:
                desval = ''
            try:
                descriptionpart = deskey + ' : ' + desval
            except:
                descriptionpart = '-'
            partdescripss.append(descriptionpart)
        # print('partdescriptss', partdescripss)
        asin = producturl.split('/')[4]
        csvfilnm1 = 'meritorFindpartScrapeData20.csv'
        csvRow = ([linkcsv,asin, name, partnumber, manufacture, discription, msrp, price, meritodescri, feature_benifits,
                   cros_reference] + partdescripss
                  )

        with open(csvfilnm1, "a", encoding="utf-8", newline='') as fp:
            wr = csv.writer(fp, dialect='excel')
            wr.writerow(csvRow)

        # images = []
        # nomalimg = response.xpath('//picture[@class="product_image_picture"]//img//@src').extract_first()
        # images.append(nomalimg)
        # thumb = response.xpath('//div[@class="product_page__thumbnail"]//a')
        # for th in thumb:
        #     thmimg = th.xpath('.//@href').extract_first()
        #     images.append(thmimg)
        #
        # imgfol = partnumber.replace('/', '').replace(':', '').replace(' ', '').replace('"', '')
        # for i in range(0, len(images)):
        #     r = requests.get(images[i])
        #     folder = "./finditpartsImages/" +imgfol
        #     imgName = folder + "_" + str(i) + ".jpg"
        #     if r.status_code == 200:
        #         with open(imgName, 'wb') as f:
        #             f.write(r.content)
