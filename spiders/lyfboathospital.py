import scrapy
import csv

class WorkSpider(scrapy.Spider):
    name = 'lyfboathospital'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Hospital Url","Hospital Name","Hospital Location","Rating","Recommended Count","Overviews",
            "Infrastructure","Services for International patients","Facilities for International patients",
            "At A Glance","Awards and recognition","Accreditations","Procedures","Facilities","Gallery","Dr listwb"
        ]
    };

    urls = []
    for p in range(1,5):
        l = 'https://www.lyfboat.com/hospitals/hospitals-and-costs-in-india/?pageview='+str(p)+'&show=10'
        urls.append(l)

    def start_requests(self):
        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        hslinks = response.xpath('//h2//a//@href').extract()
        for hslink in hslinks:
            l = hslink
            yield scrapy.Request(url=l,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parseInside(self,response):
        hsLink = response.url
        hsName = ''.join([x.strip() for x in response.xpath('//h1[@id="ga_hospital_name"]//text()').extract()])
        hsLocation = ''.join([x.strip() for x in response.xpath('//div[@class="rating-location"]//text()').extract()])
        rating = response.xpath('//span[@class="rating-value"]//text()').extract_first()
        recommendCount = response.xpath('//span[@id="recommendation_count"]//text()').extract_first()
        a=''.join([x.strip() for x in response.xpath('//div[@id="Overview"]//text()').extract()])
        try:
            overview = a.split('Overview')[1].split('Infrastructure')[0]
        except:
            overview = ''
        try:
            infrastructure = a.split('Infrastructure')[1].split('Services for International patients')[0]
        except:
            infrastructure = ''
        try:
            serviceForInterPat = a.split('Services for International patients')[1].split('Facilities for International patients')[0]
        except:
            serviceForInterPat = ''
        try:
            faciForIntenPat = a.split('Facilities for International patients')[1].split('At A Glance')[0]
        except:
            faciForIntenPat = ''
        atAGlance = a.split('At A Glance')[1].split('Awards and recognition')[0]
        try:
            awardAndRecogni = a.split('Awards and recognition')[1].split('Accreditations')[0]
        except:
            awardAndRecogni = ''
        try:
            accreditation = a.split('Accreditations')[1]
        except:
            accreditation = ''

        procedes = []
        p = response.xpath('//div[@class="proceduresdiv"]//li//a')
        for pr in p:
            spn = pr.xpath('.//span//text()').extract_first()
            lb = pr.xpath('.//label//text()').extract_first()
            prds = str(spn)+" - "+str(lb)
            procedes.append(prds)
        procedures = '\n'.join(procedes)
        facilities = '\n'.join([x.strip() for x in response.xpath('//div[@id="Facilities"]//text()').extract()]).replace('\n\n','')
        gallery = '\n'.join([x.strip() for x in response.xpath('//div[@id="Gallery"]//@href').extract()]).replace('\n\n','')
        drlistweb = response.xpath('//div[@class="doctors_hospital_directory"]//a//text()').extract()

        yield {
            "Hospital Url": hsLink,
            "Hospital Name": hsName,
            "Hospital Location": hsLocation,
            "Rating": rating,
            "Recommended Count": recommendCount,
            "Overviews": overview,
            "Infrastructure": infrastructure,
            "Services for International patients": serviceForInterPat,
            "Facilities for International patients": faciForIntenPat,
            "At A Glance": atAGlance,
            "Awards and recognition": awardAndRecogni,
            "Accreditations": accreditation,
            "Procedures": procedures,
            "Facilities": facilities,
            "Gallery": gallery,
            "Dr listwb": drlistweb
        }


