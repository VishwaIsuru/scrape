import scrapy
import csv
import pymssql

conn = pymssql.connect(server="data-sp.ch54mk5vyxni.ap-southeast-2.rds.amazonaws.com",
                       user="admin",
                       password="8eC484Z2dLnxUFg3",
                       database="skyhorse")

cursor = conn.cursor()
cursor.execute("SELECT @@VERSION")
row = cursor.fetchone()
print(f"\n\nSERVER VERSION:\n\n{row[0]}")

# csvRow11 = ['Race_URL','Race_ID','Race_Year','Date','Race_Name','Time','Venue','Total_Rns','Distance','Track_cond',
#             'Track_type','Prizemoney','Status'
#           ]
# with open('SkySportRaceDetailData2019.csv', "w", encoding="utf-8", newline='') as fp11:
#     wr = csv.writer(fp11, dialect='excel')
#     wr.writerow(csvRow11)
#
# csvRow12 = ['hourseurl','raceurl','raceid','horseid','horsename','runnersp','runnercob','runnerage',
#             'runneryob','runnersex','breeding','sire','sirecob','dam','damcob','damsire','damsirecob',
#             'owner','runnerpositn','runnertrainer','runnerjocky','runnercomment'
#           ]
# with open('SkySportHorseData2019.csv', "w", encoding="utf-8", newline='') as fp12:
#     wr = csv.writer(fp12, dialect='excel')
#     wr.writerow(csvRow12)
#
# csvRow13 = ['horseurl','horseid','formyear','formdate','formposition','formtotalran','formweight',
#             'formwinner','formbeatenby','formor','formtrainer','formjockey','formracedetial','horsename',
#             'yearofbirth','dam'
#           ]
# with open('SkySportHorseFormData2019.csv', "w", encoding="utf-8", newline='') as fp13:
#     wr = csv.writer(fp13, dialect='excel')
#     wr.writerow(csvRow13)

class workspider(scrapy.Spider):
    name = "skyhorse2012db"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'Race_URL',
            'Race_ID',
            'Race_Year',
            'Date',
            'Race_Name',
            'Time',
            'Venue',
            'Total_Rns',
            'Distance',
            'Track_cond',
            'Track_type',
            'Prizemoney',
            'Status'
                ]
    };
    urls = []
    M = ['01','02','03','04','05','06','07','08','09','10','11','12']
    for d1 in M:
        for m in M:
            for y in range(2012,2013):
                ur1 = 'https://www.skysports.com/racing/results/'+str(d1)+'-'+str(m)+'-'+str(y)
                urls.append(ur1)

    for d in range(13,32):
        for m in M:
            for y in range(2012,2013):
                ur = 'https://www.skysports.com/racing/results/'+str(d)+'-'+str(m)+'-'+str(y)
                urls.append(ur)


    def start_requests(self):
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 # headers= headers,cookies= cookies
                                 )

    def parse(self,response):
        allraces = response.xpath('//a[@class="sdc-site-racing-meetings__event-link"]//@href').extract()
        for race in allraces:
            racelink = 'https://www.skysports.com'+race
            yield scrapy.Request(url=racelink, callback=self.toAllRace,
                                 # headers= headers, cookies = cookies,dont_filter=True
                                 )

    def toAllRace(self,response):
        urlracdtail = response.url
        raceid = urlracdtail.split('/')[6]
        raceyear = urlracdtail.split('/')[-2].split('-')[-1]
        date = response.xpath('//button[@aria-controls="sdc-select-1"]//text()').extract_first().strip()
        racename = ''.join([x for x in response.xpath('//h3[@class="sdc-site-racing-header__description"]//text()').extract()]).strip().replace('\n',' ').replace('  ','').replace("'","´")
        time = response.xpath('//h2[@class="sdc-site-racing-header__name"]//text()').extract_first().split(' ')[0]+' '
        venue = response.xpath('//h2[@class="sdc-site-racing-header__name"]//text()').extract_first().replace(time,'')
        rcdetails = response.xpath('//li[@class="sdc-site-racing-header__details-item"]')
        totalrunrs = ''
        distance = ''
        trckcon = ''
        trctype = ''
        pricemoney = ''
        for rcdetail in rcdetails:
            rcdettxt = ''.join([x for x in rcdetail.xpath('.//text()').extract()]).strip().replace('\n',' ').replace('  ','')
            # print(rcdettxt)
            if 'Runners' in rcdettxt:
                totalrunrs = rcdettxt
            if 'Distance' in rcdettxt:
                distance = rcdettxt.replace('Distance:','')
            if 'Going' in rcdettxt:
                trckcon = rcdettxt.replace('Going:','')
            if 'Surface' in rcdettxt:
                trctype = rcdettxt.replace('Surface:','')
            if 'added' in rcdettxt:
                pricemoney = rcdettxt.replace('added','')
        ststus = response.xpath('//div[@class="sdc-site-racing-status__status"]//text()').extract_first()

        # yield {
        #     'Race_URL': urlracdtail,
        #     'Race_ID': raceid,
        #     'Race_Year': raceyear,
        #     'Date': date,
        #     'Race_Name': racename,
        #     'Time': time,
        #     'Venue': venue,
        #     'Total_Rns': totalrunrs,
        #     'Distance': distance,
        #     'Track_cond': trckcon,
        #     'Track_type': trctype,
        #     'Prizemoney': pricemoney,
        #     'Status': ststus
        # }
        # csvfilnm1 = 'SkySportRaceDetailData2019.csv'
        # csvRow1 = (urlracdtail, raceid, raceyear, date,racename,time,venue,totalrunrs,distance,trckcon,trctype,
        #            pricemoney,ststus)
        # with open(csvfilnm1, "a", encoding="utf-8", newline='') as fp1:
        #     wr = csv.writer(fp1, dialect='excel')
        #     wr.writerow(csvRow1)

        # sql = "SELECT * FROM horseracedetail WHERE raceid='%s'"
        # cursor.execute(sql %raceid)
        # result = cursor.fetchone()
        # print(result)
        # # connection.commit()
        # if result is None:
        sql = "INSERT INTO horseracedetail(urlracdtail, raceid, raceyear, date,racename,time,venue,totalrunrs," \
              "distance,trckcon,trctype,pricemoney,ststus)" \
              "VALUES ('%s', %s, %s, '%s', '%s'," \
              " '%s'," \
              " '%s', '%s', '%s' , '%s','%s', '%s'" \
              ",'%s')"

        sql = sql % (urlracdtail, raceid, raceyear, date,racename,time,venue,totalrunrs,distance,trckcon,trctype,
               pricemoney,ststus)
        print(sql)
        cursor.execute(sql)
        conn.commit()

############## horse second part ############################################
        horsedtls = response.xpath('//div[@class="sdc-site-racing-card__item"]')
        for horsedt in horsedtls:
            hourseurl = horsedt.xpath('.//h4[@class="sdc-site-racing-card__name"]//a//@href').extract_first()
            try:
                horseid = hourseurl.split('/')[4]
            except:
                horseid = ''
            horsename = horsedt.xpath('.//h4[@class="sdc-site-racing-card__name"]//a//text()').extract_first()
            runnerposition = horsedt.xpath('.//span[@class="sdc-site-racing-card__position"]//text()').extract_first().replace('\n','').replace('  ','').replace("'","´")
            runnercomment = horsedt.xpath('.//p[@class="sdc-site-racing-card__summary"]//text()').extract_first()
            runll = horsedt.xpath('.//a[@class="sdc-site-racing-card__details-item-link"]')
            runnsp = horsedt.xpath('.//span[@class="sdc-site-racing-card__betting-odds"]//text()').extract_first()
            trainer = ''
            jockey = ''
            for rulin in runll:
                linkboth = rulin.xpath('.//@href').extract_first()
                textboth = rulin.xpath('.//text()').extract_first()
                if '/trainer/' in linkboth:
                    trainer = textboth
                if '/jockey/' in linkboth:
                    jockey = textboth

            # yield {
            #     'raceurl': urlracdtail,
            #     'raceid': raceid,
            #     'horseid': horseid,
            #     'horsename': horsename,
            #     'possis': runnerposition,
            #     'trianer': trainer,
            #     'jockey': jockey,
            #     'comment': runnercomment,
            #     'runnersp': runnsp
            # }
            horselink = 'https://www.skysports.com'+hourseurl
            yield scrapy.Request(url=horselink, callback=self.parse_inside_horse, dont_filter=True,
                                 meta={'raceurl': urlracdtail, 'raceid': raceid,'runnerposition': runnerposition,
                                       'trainer': trainer,'jocky': jockey,'comment':runnercomment,'runsp': runnsp,
                                       'horseid': horseid}
                                 )

    def parse_inside_horse(self,response):
        hoursurl = response.url
        raceurl = "'"+response.meta['raceurl']+"'"
        raceid = response.meta['raceid']
        horseid = response.meta['horseid']
        runnerposition = response.meta['runnerposition']
        runnertrainer = response.meta['trainer'].replace("'","´")
        runnerjocky = response.meta['jocky']
        runnercomment = response.meta['comment']
        runnersp = response.meta['runsp']

        horsename = response.xpath('//h2[@class="sdc-site-racing-header__name"]//text()').extract_first().replace("'","´")
        try:
            runnercob = horsename.split('(')[1].replace(')','').replace("'","´")
        except:
            runnercob = '-'
        headerdetails = response.xpath('//li[@class="sdc-site-racing-header__details-item"]//text()').extract()
        agetx = ''
        sex = ''
        breading = ''
        owner = ''
        h = 0
        for hddetail in headerdetails:
            if 'Age:' in hddetail:
                agetx = headerdetails[h+1].replace(' \n','').replace('  ','').replace("'","´")
            if 'Sex:' in hddetail:
                sex = headerdetails[h+1].replace(' \n','').replace('  ','').replace('\n','').replace("'","´")
            if 'Breeding:' in hddetail:
                breading = headerdetails[h+1].replace(' \n','').replace('  ','').replace('\n','').replace("'","´")
            if 'Owner:' in hddetail:
                owner = headerdetails[h+1].replace(' \n','').replace('  ','').replace('\n','').replace("'","´")
            h+=1
        try:
            age = agetx.split('(')[0]
        except:
            age = ''
        try:
            runneryob = agetx.split(',')[-1].replace(')','').replace('\n','').replace(' ','').replace("'","´")
        except:
            runneryob = ''
        try:
            sire = breading.split('(')[0].split('-')[0].replace("'","´")
        except:
            sire = ''
        try:
            sirecob = breading.split('(')[1].split('-')[0].replace(')','').strip().replace("'","´")
        except:
            sirecob = ''
        if len(sirecob) != 3:
            sirecob = ''
        try:
            dam = breading.split('-')[1].split('(')[0].replace("'","´")
        except:
            dam = ''
        try:
            damcob = breading.split('-')[1].split(')')[0].split('(')[1].strip().replace("'","´")
        except:
            damcob = ''

        try:
            damsire = breading.split(') (')[1].split('(')[0].replace("'","´")
        except:
            damsire = ''
        try:
            damsirecob = breading.split(') (')[1].split('(')[1].split(')')[0].strip().replace("'","´")
        except:
            damsirecob = ''

        if len(damcob)>3:
            damsire = damcob
        if len(damcob) != 3:
            damcob = ''

        # yield {
        #     'hourseurl': hoursurl,
        #     'raceurl': raceurl,
        #     'raceid': raceid,
        #     'horseid': horseid,
        #     'horsename': horsename,
        #     'runnersp': runnersp,
        #     'runnercob': runnercob,
        #     'runnerage': age,
        #     'runneryob': runneryob,
        #     'runnersex': sex,
        #     'breeding': breading,
        #     'sire': sire,
        #     'sirecob': sirecob,
        #     'dam': dam,
        #     'damcob': damcob,
        #     'damsire': damsire,
        #     'damsirecob': damsirecob,
        #     'owner': owner,
        #     'runnerpositn': runnerposition,
        #     'runnertrainer': runnertrainer,
        #     'runnerjocky': runnerjocky,
        #     'runnercomment': runnercomment,
        # }
        # csvfilnm2 = 'SkySportHorseData2019.csv'
        # csvRow2 = (
        # hoursurl, raceurl, raceid, horseid, horsename, runnersp, runnercob, age, runneryob, sex, breading,
        # sire,sirecob,dam,damcob,damsire,damsirecob,owner,runnerposition,runnertrainer,runnerjocky,runnercomment
        # )
        # with open(csvfilnm2, "a", encoding="utf-8", newline='') as fp2:
        #     wr = csv.writer(fp2, dialect='excel')
        #     wr.writerow(csvRow2)

        sql = "INSERT INTO horsedata(hoursurl, raceurl, raceid, horseid, horsename, runnersp, runnercob, age, " \
              "runneryob, sex, breading,sire,sirecob,dam,damcob,damsire,damsirecob,owner,runnerposition," \
              "runnertrainer,runnerjocky,runnercomment)" \
              "VALUES ('%s', %s, %s, '%s', '%s'," \
              " '%s'," \
              " '%s', '%s', '%s' , '%s','%s', '%s'" \
              ",'%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"

        sql = sql % (hoursurl, raceurl, raceid, horseid, horsename, runnersp, runnercob, age, runneryob, sex,
                     breading,sire,sirecob,dam,damcob,damsire,damsirecob,owner,runnerposition,runnertrainer,
                     runnerjocky,runnercomment)
        print(sql)
        cursor.execute(sql)
        conn.commit()

        #########3rd form horse################################
        items = response.xpath('//div[@class="sdc-site-racing-profile__item"]')

        tempformyr = ''
        for item in items:
            # formyear = response.xpath('//h4[@class="sdc-site-racing-profile__header"]//text()').extract_first()
            formdate = item.xpath('.//div[@class="sdc-site-racing-profile__cell sdc-site-racing-profile__cell--1"]//text()').extract_first()
            formposition = item.xpath('.//div[@class="sdc-site-racing-profile__cell sdc-site-racing-profile__cell--2"]//strong[@class="sdc-site-racing-profile__pos"]//text()').extract_first()
            formtotalrun = item.xpath('.//div[@class="sdc-site-racing-profile__cell sdc-site-racing-profile__cell--2"]//small//text()').extract_first()
            cel3itemslbl = item.xpath('.//div[@class="sdc-site-racing-profile__cell sdc-site-racing-profile__cell--3"]//li//span//text()').extract()
            cel3itemstext = item.xpath('.//div[@class="sdc-site-racing-profile__cell sdc-site-racing-profile__cell--3"]//li//strong//text()').extract()
            cel3alink = item.xpath('.//div[@class="sdc-site-racing-profile__cell sdc-site-racing-profile__cell--3"]//li//a//@href').extract()
            yrlink = ''
            for cel3link in cel3alink:
                if '/racing/results/full-result/' in cel3link:
                    yrlink = cel3alink
            try:
                formyear = yrlink[-1].split('/')[6].split('-')[-1]
            except:
                formyear = ''
            if formyear != '':
                tempformyr = formyear
            if formyear == '':
                formyear = response.xpath('//h4[@class="sdc-site-racing-profile__header"]//text()').extract_first()

            l=0
            formweight = ''
            formwinner = ''
            formbeatenby = ''
            formor = ''
            formtrainer = ''
            formjockey = ''
            formracedetails = ''

            for cel3item in cel3itemslbl:
                if 'Weight' in cel3item:
                    formweight = cel3itemstext[l]
                if 'Winner' in cel3item:
                    formwinner = cel3itemstext[l].replace("'","´")
                if 'Beaten by' in cel3item:
                    formbeatenby = cel3itemstext[l].replace("'","´")
                if 'OR' in cel3item:
                    formor = cel3itemstext[l].replace("'","´")
                if 'Trainer' in cel3item:
                    formtrainer = cel3itemstext[l].replace("'","´")
                if 'Jockey' in cel3item:
                    formjockey = cel3itemstext[l].replace("'","´")
                if 'Race details' in cel3item:
                    try:
                        formracedetails = cel3itemstext[l].replace("'","´")
                    except:
                        formracedetails = ''
                l+=1

            yield {
                'horseurl': hoursurl,
                'horseid': horseid,
                'formyear': formyear,
                'formdate': formdate,
                'formposition': formposition,
                'formtotalran': formtotalrun,
                'formweight': formweight,
                'formwinner': formwinner,
                'formbeatenby': formbeatenby,
                'formor': formor,
                'formtrainer': formtrainer,
                'formjockey': formjockey,
                'formracedetial': formracedetails,
                'horsename': horsename,
                'yearofbirth': runneryob,
            }
            # csvfilnm3 = 'SkySportHorseFormData2019.csv'
            # csvRow3 = (
            #         hoursurl, horseid, formyear,formdate,formposition,formtotalrun,formweight,formwinner,
            #     formbeatenby,formor,formtrainer,formjockey,formracedetails,horsename,runneryob,dam
            # )
            # with open(csvfilnm3, "a", encoding="utf-8", newline='') as fp3:
            #     wr = csv.writer(fp3, dialect='excel')
            #     wr.writerow(csvRow3)

            sql = "INSERT INTO horsefromdata(hoursurl, horseid, formyear,formdate,formposition,formtotalrun," \
                  "formweight,formwinner,formbeatenby,formor,formtrainer,formjockey,formracedetails,horsename," \
                  "runneryob,dam)" \
                  "VALUES ('%s', %s, %s, '%s', '%s'," \
                  " '%s'," \
                  " '%s', '%s', '%s' , '%s','%s', '%s'" \
                  ",'%s', '%s', '%s', '%s')"

            sql = sql % (
                hoursurl, horseid, formyear, formdate, formposition, formtotalrun, formweight, formwinner,
                formbeatenby, formor, formtrainer, formjockey, formracedetails, horsename, runneryob, dam)
            print(sql)
            cursor.execute(sql)
            conn.commit()
