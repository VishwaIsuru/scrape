import scrapy
import csv
import pandas as pd
import requests

class WorkSpider(scrapy.Spider):
    name = 'finditpartssearchterms'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Search list","search link","Product link","Part Number",
            "Manufacture","Price","MSRP"
        ]
    };

    def start_requests(self):
        col_list = ["Search List 1"]
        df = pd.read_csv("FIp Scrape File 12_29_2021.csv", usecols=col_list)
        urls = df["Search List 1"]

        for i in range(0,5):
            u = 'https://www.finditparts.com/search/v3?s_s='+urls[i]+'&search_active=1'
            yield scrapy.Request(url=u,callback=self.parse,meta={'searhterm':urls[i]},
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parse(self, response):
        searchterm = response.meta['searhterm']
        prdlink = response.xpath('//div[@class="product_search_result_tile_direction"]//a//@href').extract()
        partnumber = response.xpath(
            '//dt[contains(text(),"Part Number")]/following-sibling::dd[1]/text()').extract_first()
        manufacture = response.xpath(
            '//dt[contains(text(),"Manufacturer")]/following-sibling::dd[1]/text()').extract_first()
        try:
            msrp = response.xpath('//p[@class="product_info__msrp"]//text()').extract_first()
        except:
            msrp = ''
        try:
            pricedolr = response.xpath('//span[@class="price_tag__dollars"]//text()').extract_first()
        except:
            pricedolr = ''
        try:
            prcicecent = response.xpath('//span[@class="price_tag__cents"]//text()').extract_first()
        except:
            prcicecent = ''
        try:
            price = '$ ' + pricedolr + '.' + prcicecent
        except:
            price = '-'

        yield {
            "Search list": searchterm,
            "search link": response.url,
            "Product link": prdlink,
            "Part Number": partnumber,
            "Manufacture": manufacture,
            "Price": price,
            "MSRP": msrp
            }