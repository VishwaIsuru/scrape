import scrapy
import csv

class WorkSpider(scrapy.Spider):
    name = 'amazonautoprdlink'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Main Url","Pagein","Product url"
        ]
    };

    def start_requests(self):
        urls = []
        lspg = []
        with open('amazonAutomativePageCountAllData.csv', 'r', encoding='utf-8') as file:
            reader = csv.DictReader(file)
            for row in reader:
                urls.append(row['fstcsvlink'])
                lspg.append(row['lastpage'])

        alllinks = []
        d = len(urls)
        for i in range(2022, 2285):
            url = urls[i]
            lstpg = lspg[i]
            for k in range(1,int(lstpg)+3):
                urlnw = url+'&page='+str(k)
                alllinks.append(urlnw)
        # print(alllinks)
        for ur in alllinks:
            header = {
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(alllinks.index(ur)) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
                "Referer": "https://www.amazon.com",
                "Origin": "https://www.amazon.com",
                "Connection": "keep-alive",
            }
            yield scrapy.Request(url=ur, headers=header, callback=self.parse, dont_filter=True,
                                 # meta={'page':lstpg}
                                 )

    def parse(self,response):
        # next_page = int(response.meta['page']) + 1
        pageget = response.url.split('&page=')[-1]
        prdlinks = response.xpath('//a[@class= "a-link-normal a-text-normal"]')
        for prdlink in prdlinks.xpath('./@href').extract():
            productlink = 'https://www.amazon.com' + prdlink
            if '/gp/slredirect/' not in productlink:
                productlink = productlink
                yield {"Main Url": response.url,"Pagein":pageget,"Product url": productlink}

        # pages = response.css('.a-pagination').xpath('.//li')
        # print (next_page)
        # # print(pages)
        # for page in pages:
        #     print("Next Page ........................................................................")
        #     text = page.css('a').xpath('./text()').extract_first()
        #     if text is None or '←' in text or 'Next' in text or 'Previous' in text or '→' in text:
        #         text = '401'
        #     # print(text)
        #     pageNum = int(text)
        #     if pageNum == next_page:
        #         header = {
        #             "User-Agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(
        #                 pageNum) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
        #             "Referer": "https://www.amazon.com",
        #             "Origin": "https://www.amazon.com",
        #             "Connection": "keep-alive",
        #         }
        #         l='https://www.amazon.com' +page.xpath('./a/@href').extract_first()
        #         yield scrapy.Request(url=l, callback=self.parse, headers=header, meta={'page': next_page})
        #
