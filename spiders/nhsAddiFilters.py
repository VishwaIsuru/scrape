import time
import re
import scrapy
import csv

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    # 'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',

}

class workspider(scrapy.Spider):
    name = "nhsAddiFiltrs"

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'url','Staff Group','AfC pay band','Other pay band','location','ocupation','Contract','Hours','Job ref',
            'Employer','Employer type','Site','Town','Salary','Salary period','Closing','Contact Name',
            'Job title','Email','Phone','Alltels','Email in description1','Email in description2',
            'Email in description3','Email in description4','Email in description5'
    ]
    };

    def start_requests(self):
        # urls = ['https://www.otodom.pl/pl/oferty/sprzedaz/mieszkanie/cala-polska?market=ALL&viewType=listing&lang=pl&searchingCriteria=sprzedaz&searchingCriteria=mieszkanie&page=2',
        #         # 'https://soundcloud.com/northwaycc/sets/resist'
        #         ]
        afcs = ['4',#Band 4
                # 2,7,'8a'-d, 9
                ]
        for afc in afcs:
            form_data = {
                        'page_view_status': '{"tw_AdditionalFilters":"open"}',
                        'action': 'search',
                        'search_form': '1',
                        'keyword': '',
                        'location': '',
                        'field': '',
                        'location_in': 'all',
                        'min_salary': 'ANY',
                        'max_salary': 'ANY',
                        # 'pay_band': afc,
                        # 'pay_scheme': 'CN',
                        'pay_band': [
                            afc,
                            'CN',#Consultant
                        ],
                        'staff_group': 'SG20',#Additional Clinical Services
                        'client_name': '',
                        'daysback': '',
                        'daysback_supercede': '',
                        'exclude': '',
                        'exclude_field': '',
                        'searchBtn3': '',
                    }
            # for ur in urls:
                # ur = urls[i]
            yield scrapy.FormRequest(url='https://www.jobs.nhs.uk/xi/search_vacancy/',formdata=form_data, callback=self.parse_inside, dont_filter=True,headers=headers,
                                 # cookies=cookies
                                 )

    def parse_inside(self,response):
        url = response.url
        ll = response.xpath('//div[@class="total"]//text()').extract_first().strip().split(' ')[-1]
        print('xxxxxxxxxxxxxxxxx',ll)
        for k in range(1,int(ll)+1):
            pgli = 'https://www.jobs.nhs.uk/xi/search_vacancy?action=page&page='+str(k)
            yield scrapy.Request(url=pgli, callback=self.parse_inside_page, dont_filter=True, headers=headers,
                                 # cookies=cookies
                                 )

    def parse_inside_page(self, response):
        url = response.url
        title = response.xpath('//span[@class="jobCount"]//text()').extract()
        print('urllllll',url)
        print('tttt',title)
        vacancys = response.xpath('//div[@class="vacancy" or  @class="vacancy last"]')
        for vcn in vacancys:
            vurl = 'https://www.jobs.nhs.uk/xi/direct_apply/?action=redirect;vac_ref='+vcn.xpath('.//h2//a//@href').extract_first().split('/')[-1]
            ocup = vcn.xpath('.//h3//text()').extract_first()
            yield scrapy.Request(url=vurl, callback=self.parse_inside_vacancy, dont_filter=True, headers=headers,
                                 meta={'ocup': ocup}
                                 )
    def parse_inside_vacancy(self,response):
        vcnurl = response.url
        staffgroup = 'Additional Clinical Services'
        afcpaybrand = 'Band 4'
        otherpayband = 'Consultant'
        ocupation = response.meta['ocup']
        jobtitle = response.xpath('//h1[@id="heading"]//text()').extract_first()
        # 'https://www.jobs.nhs.uk/xi/vacancy/917143041' None jobtitle not beta
        if "beta.jobs" not in vcnurl:
            vacnyUrlnBe = 'https://www.jobs.nhs.uk/xi/vacancy/'+vcnurl.split('=')[-1]
            yield scrapy.Request(url=vacnyUrlnBe, callback=self.parse_inside_vacancy_Normal, dont_filter=True, headers=headers,
                                 meta={'ocup':ocupation}
                                 )
        else:
            try:
                loc1 = response.xpath('//p[@id="employer_address_line_1"]//text()').extract_first()
            except:
                loc1 = ''
            try:
                loc2 = response.xpath('//p[@id="employer_address_line_2"]//text()').extract_first()
            except:
                loc2 = ''
            try:
                loc3 = response.xpath('//p[@id="employer_town"]//text()').extract_first()
            except:
                loc3 = ''
            try:
                loc4 = response.xpath('//p[@id="employer_county"]//text()').extract_first()
            except:
                loc4 = ''
            try:
                loc5 = response.xpath('//p[@id="employer_postcode"]//text()').extract_first()
            except:
                loc5 = ''

            location = str(loc1)+" ,"+str(loc2)+" ,"+str(loc3)+" ,"+str(loc4)+" ,"+str(loc5)
            contract = response.xpath('//p[@id="contract_type"]//text()').extract_first()
            try:
                hours = response.xpath("//h3[contains(text(), 'Working pattern')]/following-sibling::p[1]/text()").extract_first().strip().replace('\n','').replace('  ','')
            except:
                hours = ''
            try:
                referenceNumber = response.xpath("//h3[contains(text(), 'Reference number')]/following-sibling::p[1]/text()").extract_first().strip()
            except:
                referenceNumber = ''
            employer = response.css('#employer_name').xpath('./text()').extract_first()
            employertype = ''
            ety = response.xpath('//img[@id="employer_logo"]//@class').extract_first()
            try:
                if 'nhs' in ety:
                    employertype = 'NHS'
            except:
                employertype = ''
            employerWebSite = response.xpath('//a[@id="employer_website_url_link"]//@href').extract_first()
            try:
                salaryall = response.xpath("//h3[contains(text(), 'Salary')]/following-sibling::p[1]/text()").extract_first().strip()
            except:
                salaryall = ''
            salary = salaryall.split('a')[0]
            salaryPeriod = salaryall.replace(salary,'').replace('\n','').replace('  ','')
            try:
                closingDate = response.xpath('//h2[@id="closing_date"]//text()').extract_first().replace('The closing date is','')
            except:
                closingDate = ''
            contact_details_name = response.xpath('//p[@id="contact_details_name"]//text()').extract_first()
            email = response.xpath('//p[@id="contact_details_email"]//text()').extract()
            phone = response.xpath('//p[@id="contact_details_number"]//text()').extract_first()
            emailadress = ''
            des = ' '.join(response.xpath('//text()').extract())
            emails = re.findall(r"[a-zA-Z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", des)
            emails = [email for email in emails if email not in emailadress]
            try:
                email1 = emails[0]
            except:
                email1 = ''
            try:
                email2 = emails[1]
            except:
                email2 = ''
            try:
                email3 = emails[2]
            except:
                email3 = ''
            try:
                email4 = emails[3]
            except:
                email4 = ''
            try:
                email5 = emails[4]
            except:
                email5 = ''

            yield {
                'url': vcnurl,
                'Staff Group': staffgroup,
                'AfC pay band': afcpaybrand,
                'Other pay band': otherpayband,
                'location': location,
                'ocupation': ocupation,
                'Contract': contract,
                'Hours': hours,
                'Job ref': referenceNumber,
                'Employer': employer,
                'Employer type': employertype,
                'Site': employerWebSite,
                'Town': loc3,
                'Salary': salary,
                'Salary period': salaryPeriod,
                'Closing': closingDate,
                'Contact Name': contact_details_name,
                'Job title': jobtitle,
                'Email': email,
                'Phone': f'"{phone}"',
                'Alltels': f'"{phone}"',
                'Email in description1': email1,
                'Email in description2': email2,
                'Email in description3': email3,
                'Email in description4': email4,
                'Email in description5': email5
            }

    def parse_inside_vacancy_Normal(self,response):
        normalurl = response.url
        staffgroup = 'Additional Clinical Services'
        afcpaybrand = 'Band 4'
        otherpayband = 'Consultant'
        ocupation = response.meta['ocup']
        contact_names = [''] * 5
        contact_emails = [''] * 5
        contact_tels = [''] * 5
        contact_name_index = 0
        contact_email_index = 0
        contact_tel_index = 0
        job_name = ''.join(response.css('.vacancyContainer h1')[0].xpath('./text()').extract()).strip()
        job_reference = ''.join(
            response.css('.vacancyContainer h2.h2Strong strong').xpath('./text()').extract()).strip()
        left_datas = response.css('.vacancyContainer .info .pairedData').xpath('./*//text()').extract()
        employer = ''
        department = ''
        location = ''
        salary = ''
        paidata = response.xpath('//dl[@class="pairedData"]//dt//text()').extract()
        paidatatext = response.xpath('//dl[@class="pairedData"]//dd//text()').extract()
        k = 0
        for pdt in paidata:
            if pdt == 'Employer:':
                employer = paidatatext[k]
            if pdt == 'Location:':
                location = paidatatext[k]
            if pdt == 'Salary:':
                salary = paidatatext[k]
            if pdt == 'Department:':
                department = paidatatext[k]
            k += 1

        employertype = ''
        if "NHS" in employer:
            employertype = 'NHS'
        right_datas = response.css('.vacancyContainer .pairedData').xpath('./*//text()').extract()

        job_type = ''
        working_pattern = ''
        pay_scheme = ''
        pay_band = ''
        staff = ''
        speciality = ''
        closing_date = ''
        contact_name = ''
        contact_email = ''
        contact_phone = ''

        i = 0
        for data in right_datas:
            if data.strip().replace(':', '') == 'Job Type':
                job_type = right_datas[i + 1].strip().strip()
            elif data.strip().replace(':', '') == 'Working pattern':
                p = 1
                while True:
                    try:
                        if right_datas[i + p].replace(':', '').strip() in ['Pay Scheme', 'Pay Band', 'Staff Group',
                                                                           'Specialty/Function', 'Closing Date',
                                                                           'Email Address', 'Telephone']:
                            break
                        working_pattern += right_datas[i + p].strip() + '\n'
                        p += 1
                    except:
                        break
            elif data.strip().replace(':', '') == 'Pay Scheme':
                pay_scheme = right_datas[i + 1].strip().strip()
            elif data.strip().replace(':', '') == 'Pay Band':
                pay_band = right_datas[i + 1].strip().strip()
            elif data.strip().replace(':', '') == 'Staff Group':
                staff = right_datas[i + 1].strip().strip()
            elif data.strip().replace(':', '') == 'Specialty/Function':
                speciality = right_datas[i + 1].strip().strip()
            elif data.strip().replace(':', '') == 'Closing Date':
                closing_date = right_datas[i + 1].strip().strip()
            elif data.strip().replace(':', '') == 'Contact Name':
                contact_name = right_datas[i + 1].strip().strip()
            elif data.strip().replace(':', '') == 'Email Address':
                contact_email = right_datas[i + 1].strip().strip()
            elif data.strip().replace(':', '') == 'Telephone':
                contact_phone = right_datas[i + 1].strip().strip()
                contact_phone = re.sub('[^0-9/]*', '', contact_phone)
            i += 1
        if contact_name != '':
            names = contact_name.split('/')
            for i in range(len(names)):
                contact_names[i] = names[i].strip()
            contact_name_index = len(names)
        if contact_email != '':
            mails = contact_email.split('/')
            for i in range(len(mails)):
                contact_emails[i] = mails[i]
            contact_email_index = len(mails)
        if contact_phone != '':
            tels = contact_phone.split('/')
            for i in range(len(tels)):
                contact_tels[i] = tels[i]
            contact_tel_index = len(tels)
        job_specification = ""
        person_specification = ""
        links = response.css('.vacancyContainer .mainDocs.panel').xpath('.//li/a')
        for link in links:
            text = link.xpath('./text()').extract_first()
            l = 'https://www.jobs.nhs.uk' + link.xpath('./@href').extract_first()

            if 'Job Description' in text:
                job_specification = l
            elif 'Person' in text:
                person_specification = l

        des = ' '.join(response.css('.description').xpath('.//text()').extract())
        emails = re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", des)
        emails = [email for email in emails if email not in contact_email]
        nname = ''
        if emails:
            nname = emails[0].split('@')[0].split('.')[0]
        contact_email_index = contact_email_index if contact_email_index > contact_name_index else contact_name_index
        if emails:
            for i in range(len(emails)):
                contact_emails[contact_email_index] = emails[i]
                nname = emails[i].split('@')[0].split('.')[0]
                if '.' in emails[i].split('@')[0]:
                    last_name = emails[i].split('@')[0].split('.')[1]
                    last_name = re.sub("[^a-zA-Z]+", "", last_name)
                    nname += ' ' + last_name
                contact_names[contact_email_index] = nname.title()
                contact_email_index += 1

        a = re.sub('[^0-9 -]', '', des)
        b = re.sub('  +', '@@@', a)
        b = b.replace(' ', '').replace('-', '')
        numbers = re.findall('[0-9]+', b)
        numbers = [num for num in numbers if len(num) == 10 or len(num) == 11]
        numbers = [num for num in numbers if num not in contact_phone]
        for number in numbers:
            contact_tels[contact_tel_index] = number
            contact_tel_index += 1
        numbers = '\n'.join(numbers)

        yield {
            'url': normalurl,
            'Staff Group': staffgroup,
            'AfC pay band': afcpaybrand,
            'Other pay band': otherpayband,
            'location': location,
            'ocupation': ocupation,
            'Contract': '',
            'Hours': working_pattern.strip(),
            'Job ref': job_reference,
            'Employer': employer,
            'Employer type': employertype,
            'Site': '',
            'Town': location,
            'Salary': salary,
            'Salary period': salary,
            'Closing': closing_date,
            'Contact Name': contact_names[0],
            'Job title': job_name,
            'Email': contact_emails[0],
            'Phone': f'"{contact_tels[0]}"',
            'Alltels': f'"{contact_tels}"',
            'Email in description1': contact_emails[0],
            'Email in description2': contact_emails[1],
            'Email in description3': contact_emails[2],
            'Email in description4': contact_emails[3],
            'Email in description5': contact_emails[4]
        }
