import scrapy
import csv
import pandas as pd
import requests
import json
import csv

class workspider(scrapy.Spider):
    name = "sport247jsonply"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Url",
                ]
    };

    def start_requests(self):
        col_list = ["plyKey"]
        df = pd.read_csv("D:\\repullplayerkeyNoDATA.csv", usecols=col_list)
        urls = df["plyKey"]

        hist1 = []
        for i in range(0, len(urls)):
            plyrurl = 'https://247sports.com/player/' + str(urls[i]) + '/.json'
            # print (plyrurl)
            # plyrurl = urls[i]
            headers = {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.5',
                'Connection': 'keep-alive',
                'Host': '247sports.com',
                'Referer': 'https://www.google.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100102 Firefox/87.0'
            }

        # for u in self.urls:
            yield scrapy.Request(url=plyrurl, callback=self.parse,headers=headers,dont_filter=True,meta= {'page':i})

    def parse(self, response):
        pgnum = response.meta['page']
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.5',
            'Connection': 'keep-alive',
            'Host': '247sports.com',
            'Referer': 'https://www.google.com/',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100102 Firefox/87.0'
        }

        resurl = response.url

        responseply = requests.get(resurl, headers=headers)
        print(responseply)
        d= responseply.status_code
        print(d)
        if (d== 200):
            plyjsont = json.loads(responseply.content)
            print(plyjsont)

            # for j in range(0,179630,10000):
                # print(j)
                # if (pgnum>j and pgnum<j+10000):
            jsonfile = 'plyrangejsonrepullNODATA.json'
            with open(jsonfile, 'a') as f:
                f.write(json.dumps(plyjsont))
                f.write(",")
                f.close()

        if (d != 200):
            print('jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj')

            csvRow = (str(resurl), str(d))
            csvfile = "plyjsonupdate.csv"
            with open(csvfile, "a+") as fp:
                wr = csv.writer(fp, dialect='excel', lineterminator='\n')
                wr.writerow(csvRow)
