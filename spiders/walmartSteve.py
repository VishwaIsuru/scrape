import scrapy
import csv
import pandas as pd
import requests

class WorkSpider(scrapy.Spider):
    name = 'walmartsteve'
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'Url','Title','Price','Cross of Price','Date of Arrival','Sold By','Product Detail','Brand','Manufacturer',
            'Manufacturer Part Number','Assembled Product Weight','Model','Assembled Product Dimensions',
            'Warranty Information','Warranty Length','Features','Image1','Image2','Image3','Image4','Image5'
        ]
    };

    def start_requests(self):
        col_list = ["prdlink","Product_Name"]
        df = pd.read_csv("walmartPriceFilterLink_9_5_2023.csv", usecols=col_list)
        urls = df["prdlink"]
        prtnum = df["Product_Name"]
        for i in range(0,len(urls)):
            u = urls[i]
            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},meta={'partnumber':prtnum[i]})

    def parse(self, response):
        title = response.xpath('//h1//text()').extract_first()
        price = response.xpath('//span[@itemprop="price"]//text()').extract_first()
        crosssofprice = response.xpath('//span[@class="mr2 f6 gray strike"]//text()').extract_first()
        try:
            freeshipping = ' '.join([x for x in response.xpath('//div[@data-testid="fulfillmentLabel-1P-Free shipping" or @data-testid="fulfillmentLabel-1P-Shipping"]//text()').extract()]).replace('\n','').replace('  ','').split(' Want it')[0]
        except:
            freeshipping = ''
        soldBy = ''.join([x for x in response.xpath('//div[@class="f6 mr3 inline-flex mid-gray"]//text()').extract()]).replace('\n','').replace('  ','').replace('Sold and shipped by ',' ')
        alltext = ''.join([x for x in response.xpath('//text()').extract()])
        try:
            productDetail = alltext.split(',"shortDescription":"')[1].split('","')[0]+' '+alltext.split(',"longDescription":"')[1].split('","')[0].replace('\\u003c/li\\u003e  \\u003cli\\u003e',', ').replace('\\u003cul\\u003e  \\u003cli\\u003e',' ').replace('\\u003c/li\\u003e \\u003c/ul\\u003e',' ').replace('\n','').replace('\\n','').replace('\\u003cbr/\\u003e','').replace('\\u003cli\\u003e','').replace('\\u003c','').replace('\\u003e','')
        except:
            productDetail = ''
        try:
            brand = alltext.split('"Brand","value":"')[1].split('"},{"')[0].split('"')[0]
        except:
            brand = ''
        try:
            manufacture = alltext.split('"Manufacturer","value":"')[1].split('"},{"')[0].split('"')[0]
        except:
            manufacture = ''
        try:
            manufacturePrtNum = alltext.split('"Manufacturer Part Number","value":"')[1].split('"},{"')[0].split('"')[0]
        except:
            manufacturePrtNum = ''
        try:
            assembledProductWeight = alltext.split('"Assembled Product Weight","value":"')[1].split('"},{"')[0].split('"')[0]
        except:
            assembledProductWeight = ''
        try:
            model = alltext.split('"Model","value":"')[1].split('"},{"')[0].split('"')[0]
        except:
            model = ''
        try:
            feature = alltext.split('"Features","value":"')[1].split('"},{"')[0].split('"')[0]
        except:
            feature = ''
        try:
            assembledProductDimensions = alltext.split('"Assembled Product Dimensions (L x W x H)","value":"')[1].split('"')[0]
        except:
            assembledProductDimensions = ''
        try:
            warrentyINformation = alltext.split('{"information":"')[1].split('","')[0].split('"')[0]
        except:
            warrentyINformation = ''
        try:
            warrentyLength = alltext.split('"length":"')[1].split('"')[0]
        except:
            warrentyLength = ''
        images = response.xpath('//div[@class="container overflow-y-hidden mv3"]//img[@loading="lazy"]//@src').extract()
        try:
            img1 = images[0]
        except:
            img1 = ''
        try:
            img2 = images[1]
        except:
            img2 = ''
        try:
            img3 = images[2]
        except:
            img3 = ''
        try:
            img4 = images[3]
        except:
            img4 = ''
        try:
            img5 = images[4]
        except:
            img5 = ''

        yield {
            'Url': response.url,
            'Title': title,
            'Price': price,
            'Cross of Price': crosssofprice,
            'Date of Arrival': freeshipping,
            'Sold By': soldBy,
            'Product Detail': productDetail,
            'Brand': brand,
            'Manufacturer': manufacture,
            'Manufacturer Part Number': manufacturePrtNum,
            'Assembled Product Weight': assembledProductWeight,
            'Model': model,
            'Assembled Product Dimensions': assembledProductDimensions,
            'Warranty Information': warrentyINformation,
            'Warranty Length': warrentyLength,
            'Features': feature,
            'Image1': img1,
            'Image2': img2,
            'Image3': img3,
            'Image4': img4,
            'Image5': img5

        }
