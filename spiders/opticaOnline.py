import scrapy
import csv
from csv import reader

class workspider(scrapy.Spider):
    name = "opticaOnline"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'LC','PN','link','title','brand','manufacture_website','part','part_type','year','make','model'
        ]
    };

    def start_requests(self):
        lc = []
        pn = []
        with open('partFitment_sample.csv', 'r', encoding="utf8", errors='ignore') as read_obj:
            csv_reader = reader(read_obj)
            for row in csv_reader:
                lc1 = row[0]
                lc.append(lc1)
                pn1 = row[1]
                pn.append(pn1)
        for i in range(1, len(lc)+1):
            yield scrapy.Request(url='https://www.opticatonline.com/search?q='+pn[i], callback=self.parse,
                                 meta ={'lc':lc[i],'pn':pn[i]}
                                 )

    def parse(self,response):
        lc = response.meta['lc']
        pn = response.meta['pn']
        allraces = 'https://www.opticatonline.com'+response.xpath('//tbody//tr[1]//h4//@href').extract_first()

        yield scrapy.Request(url= allraces,
                             callback=self.parse_inside_allpages,meta ={'lc':lc,'pn':pn}
                             )

    def parse_inside_allpages(self, response):
        lc = response.meta['lc']
        pn = response.meta['pn']
        title = ''.join([x.strip() for x in response.xpath('//h1[@class="media-heading"]//text()').extract()])
        brand = ''.join([x.strip() for x in response.xpath('//div[@class="brand_name"]//span//text()').extract()])
        manwebsite = response.xpath('//div[@class="brand_link"]//a//@href').extract_first()
        prtnum = ''.join([x.strip() for x in response.xpath('//div[@class="part_number"]//span//text()').extract()])
        prttype = ''.join([x.strip() for x in response.xpath('//div[@class="part_type"]//text()').extract()]).replace('Part Type:','')
        ff = response.xpath("//h2[contains(text(),' Vehicle Linkages')]//following-sibling::ul//h3//text()").extract()
        for i in range(1,len(ff)):
            make = ff[i]
            kk = response.xpath("//h3[contains(text(),'"+ff[i]+"')]//following-sibling::ul//li")
            # print(kk)
            for t in kk:
                hhhh = ''.join([x.strip() for x in t.xpath('.//text()').extract()])
                # print(hhhh)
                model = hhhh.split(':')[0]
                print(model)
                fl = hhhh.split(':')[1].split(',')
                for jjj in fl:
                    p1 = jjj.split('-')[0].strip()
                    try:
                        p2 = int(jjj.split('-')[1].strip()) + 1
                    except:
                        p2 = int(p1)+1
                    for m1 in range(int(p1), int(p2)):
                        print('year',m1)
                        if m1>2023:
                            break

                        yield {
                            'LC': lc,
                            'PN': pn,
                            'link': response.url,
                            'title': title,
                            'brand': brand,
                            'manufacture_website': manwebsite,
                            'part': prtnum,
                            'part_type': prttype,
                            'year':m1,
                            'make': make,
                            'model': model
                        }
