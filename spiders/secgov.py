import scrapy
import csv

class WorkSpider(scrapy.Spider):
    name = 'secgov'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            'Name','Current Age','State','Enforcement Action','Date Filed','Doc & Releases 1',
            'Date 1','Link 1','Doc & Releases 2','Date 2','Link 2','Doc & Releases 3',
            'Date 3','Link 3','Doc & Releases 4','Date 4','Link 4','Doc & Releases 5','Date 5','Link 5',
            'Doc & Releases 6','Date 6','Link 6','Doc & Releases 7','Date 7','Link 7'


        ]
    };

    urls = []
    alp = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
    for a in alp:
        for b in alp:
            link = 'https://www.sec.gov/litigations/sec-action-look-up?aId=&last_name='+str(a+b)+'&first_name='
            urls.append(link)

    def start_requests(self):
        i = 0
        for u in self.urls:

            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        details = response.url
        print(details)

        l = response.xpath('//div[@class="view--below-footer--count"]//text()').extract_first()
        pgs = l.split(' ')[-2]
        pg = int(int(pgs)/25)+1
        for link in range(0,pg):
            pglink = details + '&items_per_page=25&page='+str(link)
            yield scrapy.Request(url=pglink,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parseInside(self,response):

        divs = response.xpath("//div[contains(@class,'card border-divide views-row view-row-count-')]")
        for div in divs:
            try:
                name = div.xpath('.//h2[@class="field-content card-title"]//text()').extract_first()
            except:
                name = ''
            try:
                curAge = div.xpath('.//div[@class="views-field views-field-field-age-in-document"]//span[2]//text()').extract_first()
            except:
                curAge = ''
            try:
                state = div.xpath('.//div[@class="views-field views-field-field-state-idd"]//span[2]//text()').extract_first()
            except:
                state = ''
            try:
                enforceaction = div.xpath('.//div[@class="views-field views-field-field-action-name-in-document"]//span[2]//text()').extract_first().replace('\t','')
            except:
                enforceaction = ''
            try:
                datefield = ''.join([x.strip() for x in div.xpath('.//div[@class="views-field views-field-field-date-filed"]//span[2]//text()').extract()])
            except:
                datefield = ''

            relese = div.xpath('.//div[@class="views-field views-field-field-related-documents"]//span[2]//li')
            docReleases = []
            docdate = []
            doclink = []
            for re in relese:
                relink = re.xpath('.//div[@class="field-link-title-url"]//a//@href').extract_first()
                doclink.append(relink)
                retitle = re.xpath('.//div[@class="field-link-title-url"]//a//text()').extract_first()
                docReleases.append(retitle)
                try:
                    redate = re.xpath('.//div[@class="article-date field_document_date"]//text()').extract_first()
                    docdate.append(redate)
                except:
                    redate = ''
                    docdate.append(redate)

            try:
                docrelese1 = docReleases[0]
            except:
                docrelese1 = ''
            try:
                docdate1 = docdate[0]
            except:
                docdate1 = ''
            try:
                doclink1 = doclink[0]
            except:
                doclink1 = ''

            try:
                docrelese2 = docReleases[1]
            except:
                docrelese2 = ''
            try:
                docdate2 = docdate[1]
            except:
                docdate2 = ''
            try:
                doclink2= doclink[1]
            except:
                doclink2 = ''

            try:
                docrelese3 = docReleases[2]
            except:
                docrelese3 = ''
            try:
                docdate3 = docdate[2]
            except:
                docdate3 = ''
            try:
                doclink3 = doclink[2]
            except:
                doclink3 = ''

            try:
                docrelese4 = docReleases[3]
            except:
                docrelese4 = ''
            try:
                docdate4 = docdate[3]
            except:
                docdate4 = ''
            try:
                doclink4 = doclink[3]
            except:
                doclink4 = ''

            try:
                docrelese5 = docReleases[4]
            except:
                docrelese5 = ''
            try:
                docdate5 = docdate[4]
            except:
                docdate5 = ''
            try:
                doclink5 = doclink[4]
            except:
                doclink5 = ''

            try:
                docrelese6 = docReleases[5]
            except:
                docrelese6 = ''
            try:
                docdate6 = docdate[5]
            except:
                docdate6 = ''
            try:
                doclink6 = doclink[5]
            except:
                doclink6 = ''

            try:
                docrelese7 = docReleases[6]
            except:
                docrelese7 = ''
            try:
                docdate7 = docdate[6]
            except:
                docdate7 = ''
            try:
                doclink7 = doclink[6]
            except:
                doclink7 = ''


            yield {

                'Name': name,
                'Current Age': curAge,
                'State': state,
                'Enforcement Action': enforceaction,
                'Date Filed': datefield,
                'Doc & Releases 1': docrelese1,
                'Date 1': docdate1,
                'Link 1': doclink1,
                'Doc & Releases 2': docrelese2,
                'Date 2': docdate2,
                'Link 2': doclink2,
                'Doc & Releases 3': docrelese3,
                'Date 3': docdate3,
                'Link 3': doclink3,
                'Doc & Releases 4': docrelese4,
                'Date 4': docdate4,
                'Link 4': doclink4,
                'Doc & Releases 5': docrelese5,
                'Date 5': docdate5,
                'Link 5': doclink5,
                'Doc & Releases 6': docrelese6,
                'Date 6': docdate6,
                'Link 6': doclink6,
                'Doc & Releases 7': docrelese7,
                'Date 7': docdate7,
                'Link 7': doclink7,
            }