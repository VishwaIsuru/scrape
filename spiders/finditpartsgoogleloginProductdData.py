import scrapy
import csv
import pandas as pd
import requests

class WorkSpider(scrapy.Spider):
    name = 'finditpartsGoogleloginPrdData'
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Link", "title", "part numer", "manufacturer", "description", "price", "cut off price(msrp)", "description1",
            "product description", "application summary", "attributes", "part interchanges", "cross references"
        ]
    };

    def start_requests(self):
        col_list = ["linkp","page"]
        df = pd.read_csv("D:\\Vishwapreviousprojects\\seliniumPython\\not_in_cols_findits_googlesin_21_6.csv", usecols=col_list)
        urls = df["linkp"]
        prtnum = df["page"]
        for i in range(0,len(urls)):
            u = urls[i]
            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},meta={'partnumber':prtnum[i]})

    def parse(self, response):
        searchnumber = response.meta['partnumber']
        prdTitle = response.css('#stellar_phase_2').xpath('.//h1//text()').extract_first().replace('\n','').replace('  ','')
        catagery = ' / '.join([x.strip() for x in response.xpath('//li[@itemprop="itemListElement"]//span//text()').extract()])
        partnumber = response.xpath('//dt[contains(text(),"Part Number")]/following-sibling::dd[1]/text()').extract_first()
        manufacture = response.xpath('//dt[contains(text(),"Manufacturer")]/following-sibling::dd[1]/text()').extract_first()
        description = response.xpath('//dt[contains(text(),"Description")]/following-sibling::dd//h2//text()').extract_first()
        vmrscode = ''.join([x.strip() for x in response.xpath("//td[contains(.,'VMRS Code')]/following-sibling::td//text()").extract()])

        partdescrip = ''.join([x.strip() for x in response.xpath('//div[@class="product_seo_content__row"]//p//text()').extract()])
        if partdescrip == '':
            partdescrip = response.xpath('//div[@class="product_description__row"]//p[1]//text()').extract_first()
        featurebnfts = []
        try:
            featurebenifits = response.xpath('//h4[contains(text(),"Features & Benefits")]/following-sibling::ul//li') or response.xpath('//h5[contains(text(),"Features & Benefits")]/following-sibling::ul//li')
            for f in featurebenifits:
                fbnt = f.xpath('.//text()').extract_first()
                featurebnfts.append(fbnt)
                # print('feature benifits: ', fbnt)
        except:
            featurebenifits = ''
        feature_benifits = ' , '.join(featurebnfts)
        try:
            d1 = response.xpath('//text()[contains(., "Cross Reference")]').extract_first()
        except:
            d1 = 'Cross Reference'
        crossreferense = response.xpath('//h5[contains(text(),"'+str(d1)+'")]/following-sibling::p[2]//text()').extract_first()
        crossreferenselist = ','.join([x.strip() for x in response.xpath('//ul[@class="list-unstyled cross-reference-list"]//a//text()').extract()])
        try:
            msrp = response.xpath('//p[@class="product_info__msrp"]//text()').extract_first().replace('MSRP:','')
        except:
            msrp = ''
        try:
            price = '$'+response.xpath('//span[@class="price_tag__dollars"]//text()').extract_first()+'.'+response.xpath('//span[@class="price_tag__cents"]//text()').extract_first()
        except:
            price = ''
        infoaddition = ''.join([x.strip() for x in response.xpath('//p[contains(.,"®")]//text()').extract()])
        productDescription = response.xpath(
            '//h4[contains(text(),"Product Description")]/following-sibling::p[1]/text()').extract_first()
        applicationSummery = response.xpath(
            '//h4[contains(text(),"Application Summary")]/following-sibling::p[1]/text()').extract_first()

        productatributes = []
        prdatri = response.xpath(
            '//h4[contains(text(),"Product Attributes")]/following-sibling::div[1]//tr')
        for atritr in prdatri:
            tratri = atritr.xpath('.//td[1]//text()').extract_first()+' :- '+atritr.xpath('.//td[2]//text()').extract_first()
            productatributes.append(tratri)
        productAtribute = ' , '.join([x.strip() for x in productatributes])

        partInterchanges = ' , '.join([x.strip() for x in response.xpath(
            '//h4[contains(text(),"Part Interchanges")]/following-sibling::div[1]//li//text()').extract()])
        images = []
        nomalimg = response.xpath('//div[@id="product_page__main_image"]//img//@src').extract_first()
        images.append(nomalimg)
        thumb = response.xpath('//div[@class="product_page__thumbnail"]//a')
        for th in thumb:
            thmimg = th.xpath('.//@href').extract_first()
            images.append(thmimg)

        # imgfol = partnumber.replace('/', '').replace(':', '').replace(' ', '').replace('"', '')
        # for i in range(0, len(images)):
        #     r = requests.get(images[i])
        #     folder = "./finditpartsImages/" +imgfol
        #     imgName = folder + "_" + str(i) + ".jpg"
        #     if r.status_code == 200:
        #         with open(imgName, 'wb') as f:
        #             f.write(r.content)

        yield {
            "Link": response.url,
            "title": prdTitle,
            "part numer": partnumber,
            "manufacturer": manufacture,
            "description": description,
            "price" :price,
            "cut off price(msrp)": msrp,
            "description1": partdescrip,
            "product description": productDescription,
            "application summary": applicationSummery,
            "attributes": productAtribute,
            "part interchanges": partInterchanges,
            "cross references": crossreferenselist
        }