import scrapy
import csv
import pandas as pd
import requests
from csv import reader
import json

class WorkSpider(scrapy.Spider):
    name = 'civicids'
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'Address','Yard Waste pick-up day','Garbage pick-up day','Blue Box pick-up day','Green Bin pick-up day'
        ]
    };

    def start_requests(self):
        #'Civic_Addresses.csv'
        with open('Whitby Recycling _ Green Bin.csv', 'r', encoding="utf8", errors='ignore') as read_obj1:
            csv_reader = reader(read_obj1)
            for row in csv_reader:
                #not roade type
                # addres = row[4] + ',' + row[10] + ',' + row[16]
                # row[1] + ',' + row[2] + ',' + row[3] + ',' + row[4]
                addres = row[0]
                if 'Whitby' in addres:
                    u = 'https://api.recollect.net/api/areas/Whitby/services/260/address-suggest?q=' + str(
                        addres) + '&locale=en'
                else:
                    u='https://api.recollect.net/api/areas/Durham/services/257/address-suggest?q='+str(addres)+'&locale=en'
                # 'https://api.recollect.net/api/areas/Whitby/services/260/address-suggest?q=14,Strandmore,Circle,Whitby&locale=en&_=1668771464696'
                # response = requests.get('https://api.recollect.net/api/areas/Durham/services/257/address-suggest',
                #                         params=params,
                #                         # cookies=cookies, headers=headers
                #                         )

                yield scrapy.Request(url=u,callback=self.parse,dont_filter=True,
                                     headers={'User-Agent': 'Mozilla Firefox 12.0'},meta={'addrss': addres})

    def parse(self, response):
        addres = response.meta['addrss']
        d = response.body
        j = json.loads(d)

        # print(j)curl 'https://oss-services.dbc.dk/oai?verb=ListRecords&metadataPrefix=marcx' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Cache-Control: max-age=0'
        # dataset = j['Data']
        for dt in j:
            place_id = dt['place_id']
            name = dt['name']
            # print(place_id) 260
            if 'Whitby' in addres:
                uu = 'https://api.recollect.net/api/places/' + str(
                    place_id) + '/services/260/events?nomerge=1&hide=reminder_only&after=2022-10-30&before=2022-12-12&locale=en'
            else:
                uu = 'https://api.recollect.net/api/places/'+str(place_id)+'/services/257/events?nomerge=1&hide=reminder_only&after=2022-10-30&before=2022-12-12&locale=en'
            yield scrapy.Request(url=uu, callback=self.parseDatacollect,dont_filter=True,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},meta={'addrss': addres})
            break

    def parseDatacollect(self,response):
        address = response.meta['addrss']
        d = response.body
        try:
            j = json.loads(d)
            dataset = j['events']
            yardpickdays = []
            garbagepickdays = []
            blueboxpickdays = []
            greenboxpickdays = []
            for dt in dataset:
                days = dt['day']
                flgs = dt['flags']
                # d = ''
                for f in flgs:
                    try:
                        d = f['icon_uri_fragment']
                    except:
                        d = ''
                    # print(d)
                    if 'yard' in d:
                        yardpickdays.append(days)
                    if 'garbage' in d:
                        garbagepickdays.append(days)
                    if 'blue-box' in d:
                        blueboxpickdays.append(days)
                    if 'green-bin' in d:
                        greenboxpickdays.append(days)
            # print(yardpickdays, 'yardspicksdays')
            # print(garbagepickdays, 'garbagebin')

            yield {
                'Address': address,
                'Yard Waste pick-up day': yardpickdays,
                'Garbage pick-up day': garbagepickdays,
                'Blue Box pick-up day': blueboxpickdays,
                'Green Bin pick-up day': greenboxpickdays
            }

        except:
            print('ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt')
            csvfilnm1 = 'civicnotids.csv'
            csvRow1 = (address,'not in')
            with open(csvfilnm1, "a", encoding="utf-8", newline='') as fp1:
                wr = csv.writer(fp1, dialect='excel')
                wr.writerow(csvRow1)