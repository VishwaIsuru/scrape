import scrapy
import csv
from datetime import datetime
import pandas as pd
import json
from operator import itemgetter

class workspider(scrapy.Spider):
    name = "rivalsovst"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Url","ID","FirstName","LastName","Position","Height","Weight","HighSchool","HomeTown","State",
            "GraduationClass","CommittedCollage","CommittedClass","RivalsRatting","RivalStars","RivalsNationalRanking",
            "RivalsPositionalRanking","RivalsStateRanking","RivalsAllTimeRanking","Reqdicton"
        ]
    };
    urls = ["https://n.rivals.com/content/prospects/2018/trevor-lawrence-1731",
            "https://n.rivals.com/content/prospects/2021/camar-wheaton-194402"
            ]

    def start_requests(self):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            # 'If-None-Match': 'W/959b5fd88d06c50387fa44d5d43a32dc',
            'Cache-Control': 'max-age=0',
            'TE': 'Trailers',
        }
        col_list = ["Prolink", "Year"]
        df = pd.read_csv("D:\\project_ubuntu_akva\\seliniumPython\\rivalslinksData21to25.csv", usecols=col_list)
        inturls = df["Prolink"]
        # idurl = df['url']
        l = len(inturls)
        for i in range(0, l):
            # id = idurl[i].split('/')[4]
        # for url in self.urls:
            yield scrapy.Request(inturls[i], callback=self.parse,headers= headers)

    def parse(self, response):

        id = response.url.split('/')[6]
        fsnm = response.css('.first-name').xpath('.//text()').extract_first()
        lsnm = response.css('div.name:nth-child(2)').xpath('.//text()').extract_first()
        position = response.xpath('//div[@class="vital-line desktop-tablet"]//text()').extract_first()
        measurments = response.xpath('//div[@class="vital-line measurements"]//text()').extract()
        try:
            height = measurments[0]
        except:
            height = ''
        try:
            weight = measurments[2]
        except:
            weight = ''
        highsch = response.css('.map-info > a:nth-child(1) > div:nth-child(1) > div:nth-child(2)').xpath('.//text()').extract_first()
        locatinons = response.css('.map-info > a:nth-child(1) > div:nth-child(1) > div:nth-child(1)').xpath('.//text()').extract_first()
        hmtwn = locatinons.split(',')[0]
        state = locatinons.split(',')[-1]
        grdclss = response.css('div.full-vital-line:nth-child(3) > div:nth-child(1) > div:nth-child(4)').xpath('.//text()').extract_first()
        graduclass = grdclss.split(' ')[-1]
        comtcolg = response.css('.college').xpath('.//text()').extract_first()
        comclass = ''.join([x.strip('') for x in response.xpath('//div[@class="date"]//text()').extract()]).split('/')[-1]
        rivalsrating = response.css('div.rank-box-small:nth-child(1) > div:nth-child(1)').xpath('.//text()').extract_first().replace('\n','')
        rivalstars = response.css('rv-stars.stars').xpath('.//@num-stars').extract_first()
        rivalsnatrank = response.css('#national-rank').xpath('.//a/text()').extract_first()
        rivalposrank = response.css('#position-rank').xpath('.//a/text()').extract_first()
        rivstateranking = response.css('#state-rank').xpath('.//a/text()').extract_first()
        rivalltimerank = ""


        j= response.xpath('//div[@class="profile-columns right-column"]//rv-prospect-school-interests//@data').extract()
        data = json.dumps(j)
        data1= json.loads(data)
        # for dd in data1:
            # for f in dd:
                # try:
        # for key, value in j.iteritems():
        #     print(key, value)
                # except:
                #     ids = ''

        ids = ''.join(data1)
        # for ij in ids:
            # idss= ij[-1]
        # print(ids)


        yield {
            "Url": response.url,
            "ID": id,
            "FirstName": fsnm,
            "LastName": lsnm,
            "Position": position,
            "Height": height,
            "Weight": weight,
            "HighSchool": highsch,
            "HomeTown": hmtwn,
            "State": state,
            "GraduationClass": graduclass,
            "CommittedCollage": comtcolg,
            "CommittedClass": comclass,
            "RivalsRatting": rivalsrating,
            "RivalStars": rivalstars,
            "RivalsNationalRanking": rivalsnatrank,
            "RivalsPositionalRanking": rivalposrank,
            "RivalsStateRanking": rivstateranking,
            "RivalsAllTimeRanking": rivalltimerank,
            "Reqdicton": ids
        }