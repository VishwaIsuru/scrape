import scrapy
import csv
import os

class workspider(scrapy.Spider):
    name = "shopireview"
    fieldnames=[
            "Url", "ThemeTitle",
            "NameOfReviewer","Date","imgsm","ReviewText"
                ]
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': fieldnames
    };
    urls = ["https://themes.shopify.com/themes?page=1","https://themes.shopify.com/themes?page=2",
            "https://themes.shopify.com/themes?page=3","https://themes.shopify.com/themes?page=4"]

    def start_requests(self):
        # with open('out/shopifyreview.csv', 'w', newline='') as csvfile:
        #     writer = csv.DictWriter(csvfile, fieldnames=self.fieldnames)
        #     writer.writeheader()

        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )

    def parse(self, response):
        allthemes = response.xpath('//a[@class="theme__link"]/@href').extract()
        page = 2
        for themes in allthemes:
            themelink = 'https://themes.shopify.com'+themes
            yield scrapy.Request(url=themelink, callback=self.toAllThemes,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

    def toAllThemes(self, response):
        # next_page = response.meta['page'] + 1

        allpagespaging = []
        u = response.url
        allpagespaging.append(u)
        pagins = response.xpath('//div[@class="pagination pagination--reviews clearfix"]//a//text()').extract()
        pgtx = response.xpath('//div[@class="pagination pagination--reviews clearfix"]//a//@href').extract()
        try:
            pgall = pagins[-2]
        except:
            pgall = 1
        try:
            pgtxx = pgtx[-2]
        except:
            pgtxx = ''
        print(pgall)
        ll = pgtxx.split('page=')[0]

        for j in range(1,int(pgall)+1):
            pglinkget = 'https://themes.shopify.com'+ll+'page='+str(j)+'#Reviews'
            allpagespaging.append(pglinkget)

        for rvpg in allpagespaging:
            yield scrapy.Request(url=rvpg, callback=self.toAllPages,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def toAllPages(self,response):
        themetitle = response.xpath(
            '//h1[@class="section-heading__heading theme-meta__theme-name"]//text()').extract_first()
        reviews = response.xpath('//figure[@class="review"]')
        for review in reviews:
            reviewrnm = review.xpath(
                './/span[@class="review-title__author heading--4 gutter-bottom--reset"]//text()').extract_first()
            daterevi = review.xpath('.//span[@class="review-title__date text-minor"]//text()').extract_first()
            reviewtxt = review.xpath('.//p[@class="review__body"]//text()').extract_first()
            reimg = review.xpath('.//svg/@class').extract_first()
            ratting = ''
            if 'positive' in reimg:
                ratting = 'Happy'
            if 'neutral' in reimg:
                ratting = 'Neutral'
            if 'negative' in reimg:
                ratting = 'Negative'

            yield {
                "Url": response.url, "ThemeTitle": themetitle,
                "NameOfReviewer":reviewrnm,"Date":daterevi,"imgsm": ratting,"ReviewText": reviewtxt
            }
            # yield data
            #
            # with open('out/shopifyreview.csv', 'a+', newline='') as csvfile:
            #     writer = csv.DictWriter(csvfile, fieldnames=self.fieldnames)
            #     writer.writerow(data)
