import scrapy
import csv
from csv import reader

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
}

class workspider(scrapy.Spider):
    name = "trusttrades"

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': ['url','trade','postalcode','name',
            'primary_phone','secondary_phone','email','website','address'
                               ]
    };

    def start_requests(self):
        trades = []
        postcodes = []
        with open('tradewhichsearch.csv', 'r', encoding="utf8",errors='ignore') as read_obj:
            csv_reader = reader(read_obj)
            for row in csv_reader:
                trade = row[1]
                trades.append(trade)
        with open('postcode.csv', 'r', encoding="utf8",errors='ignore') as read_obj1:
            csv_reader = reader(read_obj1)
            for row in csv_reader:
                postcode = row[0]
                postcodes.append(postcode)
        print('trade linkddddd',len(trades))
        print('post linkddddd', len(postcodes))
        for i in range(1, len(trades)):
            for j in range(1,len(postcodes)):
                ur = 'https://www.trustatrader.com/search?trade_name='+str(trades[i].replace('&','%26').replace(' ','+'))+'&search_trade_id=&location_str='+str(postcodes[j].replace('&','%26').replace(' ','+'))+'&lat=&lon=&trader=&search_trader_id='
                yield scrapy.Request(url=ur, callback=self.parse_inside, dont_filter=True,headers=headers,
                                     meta={'trade':trades[i],'postalcode':postcodes[j],}
                                     )

    def parse_inside(self,response):
        url = response.url
        trade = response.meta['trade']
        postalcode = response.meta['postalcode']
        try:
            pgs = response.xpath('//p[@class="pagination__page-count"]//text()').extract_first().replace('\n','').replace('  ','').replace('Page 1 of ','')
        except:
            pgs = '2'

        pg = int(pgs)
        for i in range(1,pg):
            pgurl = response.url+'&page='+str(i)

            yield scrapy.Request(url=pgurl, callback=self.parse_inside_pages, dont_filter=True, headers=headers,
                                 meta={'trade': trade, 'postalcode': postalcode}
                                 )

    def parse_inside_pages(self, response):
        tradeslinks = response.xpath('//a[@class="profile-card__heading-link"]//@href').extract()
        trade = response.meta['trade']
        postalcode = response.meta['postalcode']
        for tradelink in tradeslinks:
            trlink = 'https://www.trustatrader.com'+tradelink
            yield scrapy.Request(url=trlink, callback=self.parse_inside_trades, dont_filter=True, headers=headers,
                                 meta={'trade': trade, 'postalcode': postalcode}
                                 )

    def parse_inside_trades(self,response):
        url = response.url
        trade = response.meta['trade']
        postalcode = response.meta['postalcode']
        name = response.xpath('//h1[@class="profile-opening__heading"]//text()').extract_first().replace('\n','').replace('  ','')
        phones = response.xpath('//span[@itemprop="telephone"]//text()').extract()
        try:
            primaryphone = phones[0]
        except:
            primaryphone = '-'
        try:
            secondaryphone = phones[1]
        except:
            secondaryphone = ''
        email = response.xpath('//a[@class="is-trackable"]//text()').extract_first()
        website = response.xpath('//a[@class="profile-opening__cta-link  profile-opening__cta-link--website"]//@href').extract_first()
        address = '\n'.join([x.strip() for x in response.xpath('//address[@class="profile-location__address"]//text()').extract()])

        yield {
            'url':url,
            'trade': trade,
            'postalcode': postalcode,
            'name': name,
            'primary_phone': primaryphone,
            'secondary_phone': secondaryphone,
            'email': email,
            'website': website,
            'address': address
        }