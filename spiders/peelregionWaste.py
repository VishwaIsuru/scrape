import scrapy
import csv
import pandas as pd
import requests
from csv import reader
import json

class WorkSpider(scrapy.Spider):
    name = 'peelregionwaste'
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'Identifier',
            'Address',
            'Waste pick-up day',
            'Garbage pick-up day',
            'Recycling pick-up day',
            'Organics pick-up day','Yard waste pick-up day'
        ]
    };

    def start_requests(self):
        #'Civic_Addresses.csv'
        with open('peelregionwaste.csv', 'r', encoding="utf8", errors='ignore') as read_obj1:
            csv_reader = reader(read_obj1)
            for row in csv_reader:
                addres = row[8]
                u='https://api.recollect.net/api/areas/PeelRegionON/services/1063/address-suggest?q='+str(addres)+'&locale=en'

                yield scrapy.Request(url=u,callback=self.parse,dont_filter=True,
                                     headers={'User-Agent': 'Mozilla Firefox 12.0'},meta={'identifier':row[0],'addrss': addres})

    def parse(self, response):
        addres = response.meta['addrss']
        identifier = response.meta['identifier']
        d = response.body
        j = json.loads(d)

        # print(j)curl 'https://oss-services.dbc.dk/oai?verb=ListRecords&metadataPrefix=marcx' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Cache-Control: max-age=0'
        # dataset = j['Data']
        for dt in j:
            place_id = dt['place_id']
            name = dt['name']
            uu = 'https://api.recollect.net/api/places/' + str(
                    place_id) + '/services/1063/events?nomerge=1&hide=reminder_only&after=2023-03-26&before=2023-05-08&locale=en'
            yield scrapy.Request(url=uu, callback=self.parseDatacollect,dont_filter=True,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},meta={'addrss': addres,'identifier':identifier})
            break

    def parseDatacollect(self,response):
        address = response.meta['addrss']
        d = response.body
        identifier = response.meta['identifier']
        j = json.loads(d)
        dataset = j['events']
        wastedays = []
        garbagepickdays = []
        recyclingpickdays = []
        organicspickdays = []
        yardwastpickupdays = []
        for dt in dataset:
            days = dt['day']
            flgs = dt['flags']
            # d = ''
            for f in flgs:
                try:
                    d = f['subject']
                except:
                    d = ''
                try:
                    d2 = f['service_name']
                except:
                    d2 = ''
                if 'waste' in d2:
                    wastedays.append(days)
                if 'Garbage' in d:
                    garbagepickdays.append(days)
                if 'Recycling' in d:
                    recyclingpickdays.append(days)
                if 'Organics' in d:
                    organicspickdays.append(days)
                if 'Yard Waste' in d:
                    yardwastpickupdays.append(days)
        # print(yardpickdays, 'yardspicksdays')
        # print(garbagepickdays, 'garbagebin')

        yield {
            'Identifier':identifier,
            'Address': address,
            'Waste pick-up day': wastedays,
            'Garbage pick-up day': garbagepickdays,
            'Recycling pick-up day': recyclingpickdays,
            'Organics pick-up day': organicspickdays,
            'Yard waste pick-up day': yardwastpickupdays
        }

        # except:
        #     print('ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt')
        #     csvfilnm1 = 'civicnotids.csv'
        #     csvRow1 = (address,'not in')
        #     with open(csvfilnm1, "a", encoding="utf-8", newline='') as fp1:
        #         wr = csv.writer(fp1, dialect='excel')
        #         wr.writerow(csvRow1)