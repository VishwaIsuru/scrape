import scrapy
import csv
import csv
import requests
import pandas as pd


class WorkSpider(scrapy.Spider):
    name = 'medical'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Url","Registration Number","Full Name","Title","First Name","Intial","Last Name","Address","Sex","Email","Registration Date","Registration Type",
            "Primary Abbreviation","Primary Description","Primary University","Primary CONFERRAL  Date",
            "Ad Qulifi 1 Abbreviation", "Ad Qulifi 1 Description", "Ad Qulifi 1 University", "Ad Qulifi 1 CONFERRAL  Date",
            "Ad Qulifi 2 Absudo crontab -ebreviation", "Ad Qulifi 2 Description", "Ad Qulifi 2 University","Ad Qulifi 2 CONFERRAL  Date",
            "Ad Qulifi 3 Abbreviation", "Ad Qulifi 3 Description", "Ad Qulifi 3 University","Ad Qulifi 3 CONFERRAL  Date",
            "Ad Qulifi 4 Abbreviation", "Ad Qulifi 4 Description", "Ad Qulifi 4 University","Ad Qulifi 4 CONFERRAL  Date",
            "Ad Qulifi 5 Abbreviation", "Ad Qulifi 5 Description", "Ad Qulifi 5 University","Ad Qulifi 5 CONFERRAL  Date",
            "Ad Qulifi 6 Abbreviation", "Ad Qulifi 6 Description", "Ad Qulifi 6 University","Ad Qulifi 6 CONFERRAL  Date",
            "Ad Qulifi 7 Abbreviation", "Ad Qulifi 7 Description", "Ad Qulifi 7 University","Ad Qulifi 7 CONFERRAL  Date",
            "Specialty 1","Division 1","From Date 1","Specialty 2","Division 2","From Date 2",
            "Specialty 3", "Division 3", "From Date 3","Specialty 4","Division 4","From Date 4",
            "Hospital 1","Hospital Speciality 1","Hospital Start Date 1","Hospital End Date 1",
            "Hospital 2", "Hospital Speciality 2", "Hospital Start Date 2", "Hospital End Date 2",
            "Hospital 3", "Hospital Speciality 3", "Hospital Start Date 3", "Hospital End Date 3",
            "Hospital 4", "Hospital Speciality 4", "Hospital Start Date 4", "Hospital End Date 4",
            "Post Title","conditions attached"

        ]
    };

    # urls = [
    #     # "https://www.medicalcouncil.ie/public-information/check-the-register/search-results/?regno=008904&doctorid=21158210",
    #     # "https://www.medicalcouncil.ie/public-information/check-the-register/search-results/?regno=000003&doctorid=21158210",
    #     # "https://www.medicalcouncil.ie/public-information/check-the-register/search-results/?regno=327784&doctorid=21158210",
    #     # "https://www.medicalcouncil.ie/public-information/check-the-register/search-results/?regno=006078&doctorid=21158210",
    #     "https://www.medicalcouncil.ie/public-information/check-the-register/search-results/?regno=001300&doctorid=21158210",
    #     "https://www.medicalcouncil.ie/public-information/check-the-register/search-results/?regno=000003&doctorid=21158210",
    #     "https://www.medicalcouncil.ie/public-information/check-the-register/search-results/?regno=327784&doctorid=21158210"
    # ]
    # #
    col_list = ["link"]
    df = pd.read_csv("Registrationdateismissingcsvf.csv", usecols=col_list)
    urls = df["link"]

    # i = 1
    # while i < 10:
    #     ur = "https://www.medicalcouncil.ie/public-information/check-the-register/search-results/?regno=00000"+str(i)+"&doctorid=21158210"
    #     urls.append(ur)
    #     i += 1
    #
    #
    # i = 10
    # while i < 100:
    #     ur = "https://www.medicalcouncil.ie/public-information/check-the-register/search-results/?regno=0000"+str(i)+"&doctorid=21158210"
    #     urls.append(ur)
    #     i += 1
    #
    # i = 100
    # while i < 1000:
    #     ur = "https://www.medicalcouncil.ie/public-information/check-the-register/search-results/?regno=000"+str(i)+"&doctorid=21158210"
    #     urls.append(ur)
    #     i += 1
    #
    # i = 1000
    # while i < 10000:
    #     ur = "https://www.medicalcouncil.ie/public-information/check-the-register/search-results/?regno=00"+str(i)+"&doctorid=21158210"
    #     urls.append(ur)
    #     i += 1
    #
    # i = 10000
    # while i < 100000:
    #     ur = "https://www.medicalcouncil.ie/public-information/check-the-register/search-results/?regno=0"+str(i)+"&doctorid=21158210"
    #     urls.append(ur)
    #     i += 1
    #
    # i = 100000
    # while i < 1000000:
    #     ur = "https://www.medicalcouncil.ie/public-information/check-the-register/search-results/?regno="+str(i)+"&doctorid=21158210"
    #     urls.append(ur)
    #     i += 1

    def start_requests(self):

        for u in self.urls:
            yield scrapy.Request(url=u.strip(), callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parse(self, response):

        s = response.xpath('//table[@class="checkdetails"]//tr//td/text()').extract()
        regnum = s[0]
        fulname = s[1]
        title = fulname.split('.')[0]
        nwtit = fulname.split('.')[-1]
        names = nwtit.split()
        ndcount = len(names)-1

        fnames = []
        i=0
        for x in range(0,ndcount):
            fnames.append(names[x])
            i+=1

        fname = ' '.join([x.strip() for x in fnames])
        lsname = fulname.split( )[-1]

        efnm = ''.join([x.strip() for x in fnames])
        email = efnm+'.'+lsname+'@hse.ie'

        intials= []
        for x in names:
            intials.append(x[0])

        initial = ''.join([x.strip() for x in intials])

        address = ''
        sex = ''
        regdate = ''
        regtype = ''
        # hhhhhh
        specilityall = []
        divisiall = []
        fromdatall = []


        hospital = []
        hosspeciality = []
        hosstrdate = []
        hosenddate = []

        d = response.xpath('//table[@class="checkdetails"]//tr')
        for dh in d:
            dth = dh.xpath('.//th/text()').extract_first()
            dtd = dh.xpath('.//td/text()').extract()
            dtd = ''.join([x.strip() for x in dtd])

            dtdul = dh.xpath('.//td/ul/li/text()').extract()

            if dth == 'Address:':
                address = dtd
            if dth == 'Sex:':
                sex = dtd
            if dth == 'Registration Type:':
                regtype = dtd
            if dth == 'Registration Date:':
                regdate = dtd

            # specilllity data

            if dth == 'Specialist Division of the Register:':
                priqulist = len(dtdul)
                priqulist = int(priqulist)
                for uli in range(0,priqulist):
                    textbr = dtdul[uli].split(':')[0]
                    textaf = dtdul[uli].split(':')[-1]

                    if textbr == 'Specialty':
                        specilityall.append(textaf)
                    if textbr == 'Division':
                        divisiall.append(textaf)
                    if textbr == 'From Date':
                        fromdatall.append(textaf)

            if dth == 'Registration Details:':
                regdehoslist = len(dtdul)
                regdehoslist = int(regdehoslist)
                for uli in range(0,regdehoslist):
                    textbr = dtdul[uli].split(':')[0]
                    textaf = dtdul[uli].split(':')[-1]

                    if textbr == 'Hospital':
                        hospital.append(textaf)
                    if textbr == 'Speciality':
                        hosspeciality.append(textaf)
                    if textbr == 'Start Date':
                        hosstrdate.append(textaf)
                    if textbr == 'End Date':
                        hosenddate.append(textaf)




        try:
            conditnatt = response.xpath('//li[@class="MsoNormal"]//text()').extract()
        except:
            conditnatt = ''

        allqlif = response.xpath('//table[@class="checkdetails"]//tr/td/ul/li/text()').extract()
        #
        pabri = allqlif[0].split(':')[-1]
        pdisc = allqlif[1].split(':')[-1]
        puni = allqlif[2].split(':')[-1]
        pcndate = allqlif[3].split(':')[-1]

        abriviall = []
        dicsriall = []
        uniall = []
        confdateall = []


        cqli = len(allqlif)

        for x in range(4,cqli):
            textone = allqlif[x].split(':')[0]
            texttwo = allqlif[x].split(':')[-1]
            if textone == 'Abbreviation':
                abriviall.append(texttwo)
            elif textone == 'Description':
                dicsriall.append(texttwo)
            elif textone == 'University':
                uniall.append(texttwo)
            elif textone == 'CONFERRAL  Date':
                confdateall.append(texttwo)

        try:
            adabri1 = abriviall[0]
        except:
            adabri1 = ''
        try:
            addis1 = dicsriall[0]
        except:
            addis1 = ''
        try:
            aduni1 = uniall[0]
        except:
            aduni1 = ''
        try:
            adconfdate1 = confdateall[0]
        except:
            adconfdate1 = ''
        try:
            adabri2 = abriviall[1]
        except:
            adabri2 = ''
        try:
            addis2 = dicsriall[1]
        except:
            addis2 = ''
        try:
            aduni2 = uniall[1]
        except:
            aduni2 = ''
        try:
            adconfdate2 = confdateall[1]
        except:
            adconfdate2 = ''
        try:
            adabri3 = abriviall[2]
        except:
            adabri3 = ''
        try:
            addis3 = dicsriall[2]
        except:
            addis3 = ''
        try:
            aduni3 = uniall[2]
        except:
            aduni3 = ''
        try:
            adconfdate3 = confdateall[2]
        except:
            adconfdate3 = ''
        try:
            adabri4 = abriviall[3]
        except:
            adabri4 = ''
        try:
            addis4 = dicsriall[3]
        except:
            addis4 = ''
        try:
            aduni4 = uniall[3]
        except:
            aduni4 = ''
        try:
            adconfdate4 = confdateall[4]
        except:
            adconfdate4 =''
        try:
            adabri5 = abriviall[4]
        except:
            adabri5 = ''
        try:
            addis5 = dicsriall[4]
        except:
            addis5 =''

        try:
            aduni5 = uniall[4]
        except:
            aduni5 = ''
        try:
            adconfdate5 = confdateall[5]
        except:
            adconfdate5 = ''

        try:
            adabri6 = abriviall[5]
        except:
            adabri6 = ''
        try:
            addis6 = dicsriall[5]
        except:
            addis6 = ''
        try:
            aduni6 = uniall[5]
        except:
            aduni6 = ''
        try:
            adconfdate6 = confdateall[6]
        except:
            adconfdate6 = ''
        try:
            adabri7 = abriviall[6]
        except:
            adabri7 = ''
        try:
            addis7 = dicsriall[6]
        except:
            addis7 = ''
        try:
            aduni7 = uniall[6]
        except:
            aduni7 = ''
        try:
            adconfdate7 = confdateall[6]
        except:
            adconfdate7 =''


        try:
            specilityall1 = specilityall[0]
        except:
            specilityall1 = ''
        try:
            divisiall1 = divisiall[0]
        except:
            divisiall1 = ''
        try:
            fromdatall1 = fromdatall[0]
        except:
            fromdatall1 = ''
        try:
            specilityall2 = specilityall[1]
        except:
            specilityall2 = ''
        try:
            divisiall2 = divisiall[1]
        except:
            divisiall2 = ''
        try:
            fromdatall2 = fromdatall[1]
        except:
            fromdatall2 = ''
        try:
            specilityall3 = specilityall[2]
        except:
            specilityall3 = ''
        try:
            divisiall3 = divisiall[2]
        except:
            divisiall3 = ''
        try:
            fromdatall3 = fromdatall[2]
        except:
            fromdatall3 = ''

        try:
            specilityall4 = specilityall[3]
        except:
            specilityall4 = ''
        try:
            divisiall4 = divisiall[3]
        except:
            divisiall4 = ''
        try:
            fromdatall4 = fromdatall[3]
        except:
            fromdatall4 = ''


        try:
            hospital1 = hospital[0]
        except:
            hospital1 = ''
        try:
            hosspeciality1 = hosspeciality[0]
        except:
            hosspeciality1 = ''
        try:
            hosstrdate1 = hosstrdate[0]
        except:
            hosstrdate1 = ''
        try:
            hosenddate1 = hosenddate[0]
        except:
            hosenddate1 = ''
        try:
            hospital2 = hospital[1]
        except:
            hospital2 = ''
        try:
            hosspeciality2 = hosspeciality[1]
        except:
            hosspeciality2 = ''
        try:
            hosstrdate2 = hosstrdate[1]
        except:
            hosstrdate2 = ''
        try:
            hosenddate2 = hosenddate[1]
        except:
            hosenddate2 =''
        try:
            hospital3 = hospital[2]
        except:
            hospital3 = ''
        try:
            hosspeciality3 = hosspeciality[2]
        except:
            hosspeciality3 = ''
        try:
            hosstrdate3 = hosstrdate[2]
        except:
            hosstrdate3 = ''
        try:
            hosenddate3 = hosenddate[2]
        except:
            hosenddate3 = ''
        try:
            hospital4 = hospital[3]
        except:
            hospital4 = ''
        try:
            hosspeciality4 = hosspeciality[3]
        except:
            hosspeciality4 = ''
        try:
            hosstrdate4 = hosstrdate[3]
        except:
            hosstrdate4 = ''
        try:
            hosenddate4 = hosenddate[3]
        except:
            hosenddate4 =''

        if regdate == '':
            regdate = hosstrdate1

        pt = response.xpath('//table[@class="checkdetails"]//tr//text()').extract()
        posttitle = ''
        for x in range(len(pt)):
            pfpt = pt[x]
            s=x+2

            if pfpt == 'Post Title:':
                posttitle = pt[s]



        yield {
            "Url" : response.url,
            "Registration Number" : regnum,
            "Full Name" : fulname,
            "Title" : title,
            "First Name" : fname,
            "Intial" : initial,
            "Last Name" : lsname,
            "Address" : address,
            "Sex" : sex,
            "Email" : email,
            "Registration Date" : regdate,
            "Registration Type" : regtype,
            "Primary Abbreviation" : pabri,
            "Primary Description": pdisc,
            "Primary University" : puni,
            "Primary CONFERRAL  Date" : pcndate,


            "Ad Qulifi 1 Abbreviation":adabri1, "Ad Qulifi 1 Description":addis1, "Ad Qulifi 1 University":aduni1, "Ad Qulifi 1 CONFERRAL  Date":adconfdate1,
            "Ad Qulifi 2 Abbreviation":adabri2, "Ad Qulifi 2 Description":addis2, "Ad Qulifi 2 University":aduni2,"Ad Qulifi 2 CONFERRAL  Date":adconfdate2,
            "Ad Qulifi 3 Abbreviation":adabri3, "Ad Qulifi 3 Description":addis3, "Ad Qulifi 3 University":aduni3,"Ad Qulifi 3 CONFERRAL  Date":adconfdate3,
            "Ad Qulifi 4 Abbreviation":adabri4, "Ad Qulifi 4 Description":addis4, "Ad Qulifi 4 University":aduni4,"Ad Qulifi 4 CONFERRAL  Date":adconfdate4,
            "Ad Qulifi 5 Abbreviation":adabri5, "Ad Qulifi 5 Description":addis5, "Ad Qulifi 5 University":aduni5,"Ad Qulifi 5 CONFERRAL  Date":adconfdate5,
            "Ad Qulifi 6 Abbreviation":adabri6, "Ad Qulifi 6 Description":addis6, "Ad Qulifi 6 University":aduni6,"Ad Qulifi 6 CONFERRAL  Date":adconfdate6,
            "Ad Qulifi 7 Abbreviation":adabri7, "Ad Qulifi 7 Description":addis7, "Ad Qulifi 7 University":aduni7,"Ad Qulifi 7 CONFERRAL  Date":adconfdate7,

            "Specialty 1":specilityall1, "Division 1":divisiall1, "From Date 1":fromdatall1, "Specialty 2":specilityall2, "Division 2":divisiall2, "From Date 2":fromdatall2,
            "Specialty 3":specilityall3, "Division 3":divisiall3, "From Date 3":fromdatall3, "Specialty 4":specilityall4, "Division 4":divisiall4, "From Date 4":fromdatall4,

            "Hospital 1":hospital1, "Hospital Speciality 1":hosspeciality1, "Hospital Start Date 1":hosstrdate1, "Hospital End Date 1":hosenddate1,
            "Hospital 2":hospital2, "Hospital Speciality 2":hosspeciality2, "Hospital Start Date 2":hosstrdate2, "Hospital End Date 2":hosenddate2,
            "Hospital 3":hospital3, "Hospital Speciality 3":hosspeciality3, "Hospital Start Date 3":hosstrdate3, "Hospital End Date 3":hosenddate3,
            "Hospital 4":hospital4, "Hospital Speciality 4":hosspeciality4, "Hospital Start Date 4":hosstrdate4, "Hospital End Date 4":hosenddate4,

            "conditions attached": conditnatt,
            "Post Title" : posttitle
        }

