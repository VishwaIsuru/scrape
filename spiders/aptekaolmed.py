import scrapy
import csv
import codecs




class WorkSpider(scrapy.Spider):
    name = 'aptekaolmed'


    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            "url", "Product_Name", "cat", "Main_Image", "Product_Price", "Producent", "Kod_Producent",
            "Description", "id"

        ]
    };

    urls = [
        # "https://www.aptekaolmed.pl",
        "https://www.aptekaolmed.pl/categories.php"

    ]

    def start_requests(self):

        f = codecs.open("/home/aptekolmed.xml", "w+", encoding='utf8')
        contents = "\n".join([
                                 '<?xml version="1.0" encoding="utf-8"?><offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1">',
                                 '</offers>'])
        f.write(contents)
        f.flush()
        f.close()

        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        # print ('im here')
        details = response.xpath('//div[@class="categories-list_wrapper"]//a')

        links = details.xpath('./@href').extract()
        print (len(links))
        # print (links)

        for link in links:
            l = 'https://www.aptekaolmed.pl' + link
            # print (l)
            yield scrapy.Request(url=l, callback=self.parseInside, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 dont_filter=True)
            # break

    def parseInside(self, response):

        pagingUrls = []

        try:
            paging = response.xpath('//div[@id="search_paging"]//a/@href').extract_first()

            lastpgnum = response.xpath('//div[@id="search_paging"]//a/text()').extract()
            valuelastpg = lastpgnum[-2]
            pagingother = paging.split('=')[0]

            i = 0
            while i < int(valuelastpg):
                pgurl = "https://www.aptekaolmed.pl" + pagingother + "=" + str(i)
                pagingUrls.append(pgurl)
                i += 1
        except:
            pagingUrls.append(response.url)

        for pagesofprdt in pagingUrls:
            yield scrapy.Request(url=pagesofprdt, callback=self.toAllPrdt,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

    def toAllPrdt(self, response):

        prducturl = response.xpath('//a[@class="product-icon align_row"]')
        prdlinks = prducturl.xpath('./@href').extract()

        for prdlink in prdlinks:
            linn = 'https://www.aptekaolmed.pl' + prdlink
            yield scrapy.Request(url=linn, callback=self.toStyleUrls, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 dont_filter=True)

    def toStyleUrls(self,response):
        styles = response.xpath('//div[@class="carousel-nav mobile-carousel-nav gutter-bottom--half"]//button//span[@class="style-selector__label"]//text()').extract()
        lsp = response.url.split('?')[-1]
        them = response.url.split('?')[0].split('/')[4]
        for style in styles:
            stylesurl = 'https://themes.shopify.com/themes/'+them+'/styles/'+style.lower()+'?'+lsp
            yield scrapy.Request(url=stylesurl, callback=self.prdDetails, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 dont_filter=True)

    def prdDetails(self, response):
        prdname = response.xpath('//h1/text()').extract_first()
        cat = response.xpath('//div[@id="breadcrumbs_sub"]/ol/li//text()').extract()
        cat = cat[-2]
        image = response.xpath('//a[@class="projector_medium_image"]/@href').extract_first()
        imageurl = 'https://www.aptekaolmed.pl' + image
        prdprice = response.xpath('//strong[@id="projector_price_value"]//text()').extract_first()
        producer = ''.join(response.xpath('//div[@class="producer"]/a/text()').extract())
        kodproducer = ''.join(response.xpath('//div[@class="code"]/strong/text()').extract())
        descri = '\n'.join([x.strip('') for x in
                            response.xpath('//div[@id="component_projector_longdescription"]//p//text()').extract() if
                            x.strip() != ''])
        id = response.url.split('https://www.aptekaolmed.pl/product-pol-')[1].split('-')[0]

        line = '<o id="%s" price="%s" url="%s"><cat><![CDATA[%s]]></cat><name><![CDATA[%s]]></name><imgs><main url="%s"/></imgs><attrs><a name="Producent"><![CDATA[%s]]></a><a name="Kod_producenta"><![CDATA[%s]]></a><a name="EAN"><![CDATA[%s]]></a></attrs></o>' % (
            id, prdprice, response.url, cat, prdname, imageurl, producer, "BBBB", kodproducer
        )

        yield {
            "url":response.url,
            "Product_Name":prdname,
            "cat":cat,
            "Main_Image":imageurl,
            "Product_Price":prdprice,
            "Producent":producer,
            "Kod_Producent":kodproducer,
            "Description":descri,
            "id" : id
        }
        #
        f = codecs.open("/home/aptekolmed.xml", "r", encoding='utf8')
        contents = f.readlines()
        contents = [a for a in contents if a.strip() != '']
        f.close()

        contents.insert(len(contents) - 1, line.strip())

        f = codecs.open("/home/aptekolmed.xml", "w", encoding='utf8')
        contents = "\n".join(contents)
        f.write(contents)
        f.flush()
        f.close()


