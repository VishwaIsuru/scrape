import scrapy
from scrapy.http import FormRequest
import csv
import pandas as pd
import json
import requests


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:103.0) Gecko/20100101 Firefox/103.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1',
}

class workspider(scrapy.Spider):
    name = "gelbeseiten2"

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'url','seller','address','telephone','email','website'
    ]
    };

    def start_requests(self):
        col_list = ['prdlink', 'l2']
        df = pd.read_csv("D:\Vishwapreviousprojects\seliniumPython\gelbeseiten_juweliere_Freiburg_link.csv",
                         usecols=col_list)

        crossreffs = df["l2"]
        prdlinks = df["prdlink"]
        # print(len(partnumrs))
        for i in range(0, len(prdlinks)):
            prtnum = prdlinks[i]
            crosfer = crossreffs[i]
            yield scrapy.Request(url=prdlinks[i], callback=self.parse_inside, dont_filter=True,
                                 )

    def parse_inside(self,response):
        prdurl = response.url
        seller = ''.join([x for x in response.xpath('//h1//text()').extract()]).replace('\n','').replace('\t','')
        address = ''.join([x for x in response.xpath('//address[@class="mod-TeilnehmerKopf__adresse"]//text()').extract()]).replace('\n','').replace('\t','')
        # branch = ''.join([x for x in response.xpath('//div[@class="mod-TeilnehmerKopf__branchen"]//text()').extract()]).replace('\n','').replace('\t','')
        # opensat = ''.join([x for x in response.xpath('//div[@class="mod-TeilnehmerKopf__oeffnungszeiten"]//text()').extract()]).replace('\n','').replace('\t','')
        tele = ''.join([x for x in response.xpath('//span[@data-role="telefonnummer"]//text()').extract()]).replace('\n','').replace('\t','').replace('  ','')
        email = ''.join([x for x in response.xpath('//div[@id="email_versenden"]//@data-link').extract()]).replace('\n','').replace('\t','').replace('mailto:','').replace('?subject=Anfrage über Gelbe Seiten','')
        if email == '#':
            email = str(response.body).split('contact_email&quot;:&quot;')[1].split('&quot')[0]
        website = response.xpath('//a[@data-wipe-realview="detailseite_webadresse"]//@href').extract_first()
        # otherinfo = ''.join([x for x in response.xpath('//section[@id="beschreibung"]//text()').extract()]).replace('\n','').replace('\t','')
        # branchas = ', '.join([x for x in response.xpath('//section[@id="branchen_und_stichworte"]//text()').extract()]).replace('\n','').replace('\t','')

        yield {
            'url' : prdurl,
            'seller' : seller,
            'address' : address,
            'telephone' : tele,
            'email' : email,
            'website' : website,
        }
