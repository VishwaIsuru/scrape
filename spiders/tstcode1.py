import scrapy
import pandas as pd
import csv
import json

class WorkSpider(scrapy.Spider):
    name = 'tstcode1'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'url','title','factorynew','minimalWear','fieldTested','wellWorn','battleScarred'
        ]
    };

    def start_requests(self):
        u = 'https://wiki.cs.money/'
        yield scrapy.Request(url=u, callback=self.parseInside,
                             # headers=header,meta={'page':1}, dont_filter=True
                             )

    def parseInside(self, response):
        skinurls = response.xpath('//div[@class="brzpbogxsgrlikcnlwpafrzdyt"]//a[@class="itixalfiylvsvmdssbpcdzfawb"]//@href').extract()
        for skiurl in skinurls:
            yield scrapy.Request(url='https://wiki.cs.money'+skiurl, callback=self.parseInsidePost,
                                 # headers=header,meta={'page':1}, dont_filter=True
                                 )
    def parseInsidePost(self, response):
        prdurls = response.xpath('//div[@class="kxmatkcipwonxvwweiqqdoumxg"]//a[@class="blzuifkxmlnzwzwpwjzrrtwcse"]//@href').extract()
        for prdurl in prdurls:
            yield scrapy.Request(url='https://wiki.cs.money'+prdurl, callback=self.parseInsidePrduct,
                                 # method="POST",body=request_body,headers=headers,
                                 # meta={'page':1}, dont_filter=True
                                 )

    def parseInsidePrduct(self,response):
        factorynew = 0
        minimalWear = 0
        fieldTested = 0
        wellWorn = 0
        battleScarred = 0

        k = str(response.body).split('"name_ids":[')[1].split('],"price":')[0]
        # .split('"name_id":')[1].split("}")[0]
        for t in k.split('},{'):
            vlu = t.split('"name_id":')[1].replace('}','')
            if 'Factory New' in t and 'StatTrak' not in t:
                factorynew = vlu
            if 'Minimal Wear' in t and 'StatTrak' not in t:
                minimalWear = vlu
            if 'Field-Tested' in t and 'StatTrak' not in t:
                fieldTested = vlu
            if 'Well-Worn' in t and 'StatTrak' not in t:
                wellWorn = vlu
            if 'Battle-Scarred' in t and 'StatTrak' not in t:
                battleScarred = vlu
        # except:
        #     nameids = ''
        title = ' '.join([x.strip() for x in response.xpath('//h1[@class="rdmwocwwwyeqwxiiwtdwuwgwkh"]//text()').extract()])
        yield {
            'url': response.url,
            'title': title,
            # 'k':k
            'factorynew': factorynew,
            'minimalWear': minimalWear,
            'fieldTested': fieldTested,
            'wellWorn': wellWorn,
            'battleScarred': battleScarred
        }
        # headers = {
        #     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/118.0',
        #     'Accept': '*/*',
        #     'Accept-Language': 'en-US,en;q=0.5',
        #     'Referer': response.url,
        #     'content-type': 'application/json',
        #     'Origin': 'https://wiki.cs.money',
        #     'Alt-Used': 'wiki.cs.money',
        #     'Connection': 'keep-alive',
        #     'Sec-Fetch-Dest': 'empty',
        #     'Sec-Fetch-Mode': 'cors',
        #     'Sec-Fetch-Site': 'same-origin',
        # }
        # data = {"operationName":"price_trader_log",
        # "variables":{"name_ids":[int(nameids)]},
        # "query":"query price_trader_log($name_ids: [Int!]!) {\n  price_trader_log(input: {name_ids: $name_ids}) {\n    name_id\n    values {\n      price_trader_new\n      time\n    }\n  }\n}"
        # }

        # yield {
        #     'data':data,
        #     'header': headers
        # }
        # request_body = json.dumps(data)
    #     yield scrapy.Request(url=response.url,method="POST",body=request_body,headers=headers,
    #                          callback=self.parseInsidePostData,
    #                          )
    #
    # def parseInsidePostData(self,response):
    #     yield {
    #         'dddddddddddddddddddd': json.loads(response.text)
    #     }
