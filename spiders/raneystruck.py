import scrapy
import csv
import requests

class WorkSpider(scrapy.Spider):
    name = 'raneystruck'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Product Link","Title","SKU","Category Path","Price","Description","Images"
        ]
    };

    urls = ["https://www.raneystruckparts.com/fl-50-60-70-80-112/","https://www.raneystruckparts.com/freightliner-cascadia/",
            "https://www.raneystruckparts.com/freightliner-century/","https://www.raneystruckparts.com/freightliner-classic/",
            "https://www.raneystruckparts.com/freightliner-columbia/","https://www.raneystruckparts.com/freightliner-coronado/",
            "https://www.raneystruckparts.com/freightliner-fld/","https://www.raneystruckparts.com/freightliner-m2-business-class/",
            "https://www.raneystruckparts.com/freightliner-flb/"]

    def start_requests(self):
        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u,callback=self.parseSubOne,headers={'User-Agent': 'Mozilla Firefox 12.0'},)
            i += 1

    def parseSubOne(self, response):
        sublinkOnes = response.xpath('//ul[@id="categories-navList"]//li//a//@href').extract()
        for link in sublinkOnes:
            l = link
            yield scrapy.Request(url=l,callback=self.parseInsideSubTwo,headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True)

    def parseInsideSubTwo(self,response):
        sublinksTwo = response.xpath('//ul[@id="categories-navList"]//li//a//@href').extract()
        for link in sublinksTwo:
            l = link
            yield scrapy.Request(url=l, callback=self.parseInsideProductPagin, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 dont_filter=True)

    def parseInsideProductPagin(self,response):
        try:
            s = response.xpath('//div[@class="pagination-info"]//text()').extract_first().split('of')[-1].replace('total','').strip()
        except:
            s = '40'

        if int(s)> 40:
            pgcount = int(s)/40+2
        else:
            pgcount = 2
        for i in range(1,int(pgcount)):
            pgurl = response.url+'?sort=featured&page='+str(i)
            yield scrapy.Request(url=pgurl, callback=self.parseInsideProduct,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 dont_filter=True)

    def parseInsideProduct(self,response):
        productslinks = response.xpath('//h4[@class="card-title"]//a//@href').extract()
        for prli in productslinks:
            yield scrapy.Request(url=prli, callback=self.parseInsideProductData,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 dont_filter=True)

    def parseInsideProductData(self,response):
        productlink = response.url
        title = response.xpath('//h1[@class="productView-title"]//text()').extract_first()
        catagerypath = '/'.join([x.strip() for x in response.xpath('//li[@itemprop="itemListElement"]//text()').extract()]).replace('///','/')
        price = response.xpath('//h5[@class="productView-brand"]/following-sibling::div//span[@class="price price--withoutTax price--main"]//text()').extract_first()
        description = ''.join([x.strip() for x in response.xpath('//div[@class="productView-description-tabContent emthemesModez-mobile-collapse-content"]//text()').extract()]).split('var sa_products_count')[0]
        sku = response.xpath('//dd[@data-product-sku=""]//text()').extract_first()
        a = response.xpath('//div[@class="productView-detailsWrapper"]//img//@src').extract()
        images = list(set(a))
        for i in range(0, len(images)):
            r = requests.get(images[i].replace('100x100','608x608'))
            folder = "./raneysImage/"+str(sku)
            imgName = folder + "_img" + str(i) + ".jpg"
            if r.status_code == 200:
                with open(imgName, 'wb') as f:
                    f.write(r.content)

        yield {
            "Product Link": productlink,
            "Title": title,
            "SKU": sku,
            "Category Path": catagerypath,
            "Price": price,
            "Description": description,
            "Images": images
        }