import scrapy
import csv
import requests

class WorkSpider(scrapy.Spider):
    name = 'tstindustries'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'link','sku','options name','option','price','type','name',
            'short_Description','long_Description',
            'image 1','image 2','image 3','image 4','image 5',
            'image 6','image 7','image 8','image 9','image 10',
            'image 11','image 12','image 13','image 14','image 15',
            'image 16','image 17','image 18','image 19','image 20'

        ]
    };

    urls = ["https://tstindustries.com/tst-industries-llc/?page=1",
            "https://tstindustries.com/tst-industries-llc/?page=2","https://tstindustries.com/tst-industries-llc/?page=3",
            "https://tstindustries.com/tst-industries-llc/?page=4","https://tstindustries.com/tst-industries-llc/?page=5","https://tstindustries.com/yamaha-yzf-r1-2020/","https://tstindustries.com/yamaha-yzf-r1-2015/","https://tstindustries.com/yamaha-yzf-r1-2009-2014/",
            "https://tstindustries.com/yamaha-yzf-r1-2007-2008/","https://tstindustries.com/yamaha-yzf-r1-2004-2006/","https://tstindustries.com/yamaha-yzf-r1-2002-2003/",
            "https://tstindustries.com/yamaha-yzf-r6-2017-2020/","https://tstindustries.com/yamaha-yzf-r6-2008-2016/","https://tstindustries.com/yamaha-yzf-r6-2006-2007/",
            "https://tstindustries.com/yamaha-yzf-r6-2003-2005/","https://tstindustries.com/yamaha-yzf-r6-2001-2002/","https://tstindustries.com/yamaha-yzf-r7-2022/",
            "https://tstindustries.com/yamaha-yzf-r3-2019/","https://tstindustries.com/yamaha-yzf-r3-2015-2018/","https://tstindustries.com/yamaha-fz-10-mt-10-2022/",
            "https://tstindustries.com/yamaha-fz-10-mt-10-2016-2021/","https://tstindustries.com/yamaha-mt-09-2021/","https://tstindustries.com/yamaha-fz-09-mt-09-2017-2020/",
            "https://tstindustries.com/yamaha-fz-09-mt-09-2014-2016/","https://tstindustries.com/yamaha-mt-07-2021/","https://tstindustries.com/yamaha-fz-07-mt-07-2018-2020/",
            "https://tstindustries.com/yamaha-fz-07-mt-07-2015-2017/","https://tstindustries.com/yamaha-mt-03-2020/","https://tstindustries.com/yamaha-xsr900-2022/",
            "https://tstindustries.com/yamaha-xsr900-2016/","https://tstindustries.com/yamaha-xsr700-2016-2021/","https://tstindustries.com/yamaha-tracer-900-900-gt-2018-2020/",
            "https://tstindustries.com/yamaha-tenere-700-2021/","https://tstindustries.com/yamaha-wr250r-2008-2020/","https://tstindustries.com/yamaha-tw200-1987/",
            "https://tstindustries.com/yamaha-yzf-r6s-2006-2009/","https://tstindustries.com/yamaha-fz6r-all-years/","https://tstindustries.com/yamaha-fz8-2011-2013/",
            "https://tstindustries.com/yamaha-niken-2018/","https://tstindustries.com/yamaha-raider-2008-2017/",
            "https://tstindustries.com/honda-cbr-1000rr-2017/","https://tstindustries.com/honda-cbr-1000rr-2012-2016/","https://tstindustries.com/honda-cbr-1000rr-2008-2011/",
            "https://tstindustries.com/honda-cbr-1000rr-2004-2007/","https://tstindustries.com/honda-cbr-600rr-2013-/","https://tstindustries.com/honda-cbr-600rr-2007-2012/",
            "https://tstindustries.com/honda-cbr-600rr-2003-2006/","https://tstindustries.com/honda-msx-125-grom-2022/","https://tstindustries.com/honda-grom-2017-2020/",
            "https://tstindustries.com/honda-grom-2013-2016/","https://tstindustries.com/honda-monkey-2019/","https://tstindustries.com/honda-crf250l-2017-/",
            "https://tstindustries.com/honda-crf250l-2012-2016/","https://tstindustries.com/honda-crf300l-2021/","https://tstindustries.com/honda-crf450l-2019/","https://tstindustries.com/honda-cbr650r-2019/",
            "https://tstindustries.com/honda-cbr650f-cb650f-2014-/","https://tstindustries.com/honda-cbr-500r-cb-500f-2019/","https://tstindustries.com/honda-cbr-500r-cb-500f-2013-2018/",
            "https://tstindustries.com/honda-cbr300r-cbr250r-all-years/","https://tstindustries.com/honda-cb650r-2019/","https://tstindustries.com/honda-cb-1000r-2008-2017/",
            "https://tstindustries.com/honda-cbr-900-1992-1999/","https://tstindustries.com/honda-f4-f4i-all-years/","https://tstindustries.com/honda-hornet-600-900-all-years/",
            "https://tstindustries.com/honda-rc51-all-years/","https://tstindustries.com/honda-954-all-years/","https://tstindustries.com/honda-929-all-years/",
            "https://tstindustries.com/kawasaki-ninja-zx10r-2021/","https://tstindustries.com/kawasaki-ninja-zx10r-2016-2020/","https://tstindustries.com/kawasaki-ninja-zx10r-2011-2015/",
            "https://tstindustries.com/kawasaki-ninja-zx10r-2008-2010/","https://tstindustries.com/kawasaki-ninja-zx10r-2006-2007/","https://tstindustries.com/kawasaki-ninja-636-zx6r-2024/",
            "https://tstindustries.com/kawasaki-ninja-636-zx6r-2019-2023/","https://tstindustries.com/kawasaki-ninja-636-zx6r-2013-2018/","https://tstindustries.com/kawasaki-ninja-636-zx6r-2009-2012/",
            "https://tstindustries.com/kawasaki-ninja-636-zx6r-2007-2008/","https://tstindustries.com/kawasaki-ninja-636-zx6r-2005-2006/","https://tstindustries.com/kawasaki-ninja-636-zx6r-2003-2004/",
            "https://tstindustries.com/kawasaki-ninja-zx4rr-2023-/","https://tstindustries.com/kawasaki-ninja-650-2020/","https://tstindustries.com/kawasaki-ninja-650-2017-2019/",
            "https://tstindustries.com/kawasaki-ninja-650r-2012-2016/","https://tstindustries.com/kawasaki-ninja-400-2018-/","https://tstindustries.com/kawasaki-ninja-300-2013-2017/",
            "https://tstindustries.com/kawasaki-ninja-250r-2008-2012/","https://tstindustries.com/2017-/","https://tstindustries.com/kawasaki-zh2-2020/",
            "https://tstindustries.com/kawasaki-z900-2020/","https://tstindustries.com/kawasaki-z900-2017-2019/","https://tstindustries.com/kawasaki-z650-2020/",
            "https://tstindustries.com/kawasaki-z650-2017-2019/","https://tstindustries.com/kawasaki-z400-2019/","https://tstindustries.com/kawasaki-z125-2017/",
            "https://tstindustries.com/kawasaki-z1000-2014-2016/","https://tstindustries.com/kawasaki-z1000-2009-2013/","https://tstindustries.com/kawasaki-z1000-2007-2008/",
            "https://tstindustries.com/kawasaki-z800-2013-2016/","https://tstindustries.com/kawasaki-z750-2007-2011/","https://tstindustries.com/kawasaki-z300-2015/",
            "https://tstindustries.com/kawasaki-klx400sr-2003-2004/","https://tstindustries.com/kawasaki-klx300-kxl300sm-2021-2023/","https://tstindustries.com/kawasaki-klx-230-s-2020/",
            "https://tstindustries.com/kawasaki-klx-230-sm-2023/","https://tstindustries.com/kawasaki-klx250-2009/",
            "https://tstindustries.com/suzuki-gsx-r1000-2017/","https://tstindustries.com/suzuki-gsxr-1000-2009-2016/","https://tstindustries.com/suzuki-gsxr-1000-2007-2008/",
            "https://tstindustries.com/suzuki-gsxr-1000-2006/","https://tstindustries.com/suzuki-gsxr-1000-2005/","https://tstindustries.com/suzuki-gsxr-750-2011/",
            "https://tstindustries.com/suzuki-gsxr-750-2008-2009/","https://tstindustries.com/suzuki-gsxr-750-2006-2007/","https://tstindustries.com/suzuki-gsxr-750-2004-2005/",
            "https://tstindustries.com/suzuki-gsxr-600-2011/","https://tstindustries.com/suzuki-gsxr-600-2008-2010/","https://tstindustries.com/suzuki/gsxr-600/2006-2007/",
            "https://tstindustries.com/suzuki-gsxr-600-2004-2005/","https://tstindustries.com/suzuki-gsx-8s-2023/","https://tstindustries.com/suzuki-dr-z400s-2000/",
            "https://tstindustries.com/suzuki-dr-z400sm-2005/","https://tstindustries.com/suzuki-dr650-se-s-2001-/","https://tstindustries.com/suzuki-2017-gsxs1000f/",
            "https://tstindustries.com/suzuki-gsx-s750-2017-/","https://tstindustries.com/suzuki-sv650-2017-/","https://tstindustries.com/suzuki-sv650x-2019/",
            "https://tstindustries.com/suzuki-boulevard-m109r-2006/",
            "https://tstindustries.com/bmw-s1000rr-2023/","https://tstindustries.com/bmw-s1000rr-2020-2022/","https://tstindustries.com/bmw-s1000rr-2009-2019/",
            "https://tstindustries.com/bmw-s1000r-2014-2020/",
            "https://tstindustries.com/ducati-848-1098-1198-all-years/","https://tstindustries.com/ducati-other-models-all-years/",
            "https://tstindustries.com/aprilia-rs660-2021/","https://tstindustries.com/aprilia-tuono-660-2021/","https://tstindustries.com/aprilia-rsv4-2017-2020/",
            "https://tstindustries.com/aprilia-rsv4-2011-2016/","https://tstindustries.com/aprilia-tuono-1000-2017-2020/","https://tstindustries.com/aprilia-tuono-1000-2011-2016/",
            "https://tstindustries.com/aprilia-caponord-1200-2014-2017/",
            "https://tstindustries.com/ktm-rc390-2015/","https://tstindustries.com/690-smc-r-enduro-2019/",
            "https://tstindustries.com/triumph-daytona-675-675r-2006-2016/","https://tstindustries.com/triumph-street-triple-r-2007-2018/",
            ]

    def start_requests(self):
        i = 0
        for u in self.urls:

            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        # bikemake = response.xpath('//a[@class="bread-crumb"][2]//text()').extract_first()
        # bikemodel = response.xpath('//a[@class="bread-crumb"][3]//text()').extract_first()
        # bikeyear = response.xpath('//span[@class="bread-crumb last-bread-crumb"]//text()').extract_first()

        links = response.xpath('//a[@class="tst_prod_single"]//@href').extract()
        for link in links:
            if 'hyperpack' not in link:
                l = link
                # print(l)
                yield scrapy.Request(url=l,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True,
                                     # meta={'bikemake':bikemake,'bikemodel':bikemodel,'bikeyear':bikeyear},
                                     )
        # print l ,dont_filter=True

    def parseInside(self,response):
        # bikemake = response.meta['bikemake']
        # bikemodel = response.meta['bikemodel']
        # bikeyear = response.meta['bikeyear']
        producttitle = response.xpath('//h2[@class="tstProdImgTitle"]//text()').extract_first()
        sku = []
        price = response.xpath('//span[@id="product_price"]//text()').extract_first()
        d=response.text.split('var skus = [];')[-1].split('*')[0].split(';')
        for sd in d:
            if '.push' in sd:
                sku.append(sd.split('.push("')[1].split('"')[0])
        if len(sku) == 0:
            sku = response.xpath('//td[@itemprop="sku"]//text()').extract()
        shortdescription = ''.join([x.strip() for x in response.xpath('//div[@class="tst_product_short_desc"]//p//text()').extract()])
        images = response.xpath('//ul[@class="slides"]//picture//img//@src').extract()
        imgall = []
        for im in images:
            if 'http' not in im:
                imgall.append(im.replace('./','https://tstindustries.com/'))

        # for immm in imgall:
        #     r = requests.get(immm)
        #     imagename = immm.split('/')[-1]
        #     imgName = "./tstindustry_images/" + imagename
        #     if r.status_code == 200:
        #         with open(imgName, 'wb') as f:
        #             # for chunk in r:
        #             f.write(r.content)

        longdescri = ''
        prdDetail = response.xpath('//li[@class="tst-tab-link current"]//text()').extract_first()
        if 'Product Details' in prdDetail:
            longdescri = ''.join([x.strip() for x in response.xpath('//div[@class="tst-tab-content current"]//text()').extract()])
        skuval = response.xpath('//*[@class="product_options"]//@name').extract()
        options = []
        d1 = response.text
        l= response.xpath('//td[@class="property-name product-input"]//text()').extract()
        type11 = 'parent'
        try:
            imgall1 = imgall[0]
        except:
            imgall1 = ''
        try:
            imgall2 = imgall[1]
        except:
            imgall2 = ''
        try:
            imgall3 = imgall[2]
        except:
            imgall3 = ''
        try:
            imgall4 = imgall[3]
        except:
            imgall4 = ''
        try:
            imgall5 = imgall[4]
        except:
            imgall5 = ''
        try:
            imgall6 = imgall[5]
        except:
            imgall6 = ''
        try:
            imgall7 = imgall[6]
        except:
            imgall7 = ''
        try:
            imgall8 = imgall[7]
        except:
            imgall8 = ''
        try:
            imgall9 = imgall[8]
        except:
            imgall9 = ''
        try:
            imgall10 = imgall[9]
        except:
            imgall10 = ''
        try:
            imgall11 = imgall[10]
        except:
            imgall11 = ''
        try:
            imgall12 = imgall[11]
        except:
            imgall12 = ''
        try:
            imgall13 = imgall[12]
        except:
            imgall13 = ''
        try:
            imgall14 = imgall[13]
        except:
            imgall14 = ''
        try:
            imgall15 = imgall[14]
        except:
            imgall15 = ''
        try:
            imgall16 = imgall[15]
        except:
            imgall16 = ''
        try:
            imgall17 = imgall[16]
        except:
            imgall17 = ''
        try:
            imgall18 = imgall[17]
        except:
            imgall18 = ''
        try:
            imgall19 = imgall[18]
        except:
            imgall19 = ''
        try:
            imgall20 = imgall[19]
        except:
            imgall20 = ''

        yield {
            'link': response.url,
            'sku':  ','.join([x.strip() for x in list(set(sku))]).replace('\n','').replace('  ','').replace('\t',''),
            'options name': '',
            'option': ' ',
            'price': price,
            'type': type11,
            'name': producttitle,
            'short_description': shortdescription,
            'long_Description': longdescri,
            'image 1': imgall1,'image 2': imgall2,'image 3': imgall3,'image 4': imgall4,'image 5': imgall5,
            'image 6': imgall6,'image 7': imgall7,'image 8': imgall8,'image 9': imgall9,'image 10': imgall10,
            'image 11': imgall11,'image 12': imgall12,'image 13': imgall13,'image 14': imgall14,'image 15': imgall15,
            'image 16': imgall16,'image 17': imgall17,'image 18': imgall18,'image 19': imgall19,'image 20': imgall20,

        }

        if len(l)>1:
            type11 = 'child'
            for l1 in l:
                if 'Quantity' not in l1:
                    options.append(l1.strip())
            for opt in options:
                try:
                    opt1 = response.xpath('//td[contains(text(),"'+opt+'")]//following-sibling::td//select//@name').extract_first()
                except:
                    opt1 = ''
                try:
                    opt1nm = response.xpath('//td[contains(text(),"'+opt+'")]//following-sibling::td//select//option//text()').extract()
                except:
                    opt1nm = []
                opt1vle = response.xpath('//td[contains(text(),"'+opt+'")]//following-sibling::td//select//option//@value').extract()

                for r in range(len(opt1vle)):
                    pr = d1.split("modifiers["+opt1.replace("product_options[","").replace("]","")+"]["+opt1vle[r]+"]")
                    skunw = []
                    price2 = ''
                    for pr1 in pr:
                        pr2= pr1.split(";")[0]
                        if len(pr2)<100 and '$' in pr2:
                            price2 = pr2.replace(' = [','').replace(",'$',{}]","")
                            # print(price2)
                    k = d1.split("skus["+opt1.replace("product_options[","").replace("]","")+"]['options']["+opt1vle[r]+"].push(")
                    for kk in k:
                        p= kk.split(");")[0]
                        if len(p)<100:
                            skunw.append(p.replace('"',''))

                    yield {
                        'link':response.url,
                        'sku': skunw,
                        'options name': opt,
                        'option': opt1nm[r].strip(),
                        'price': price2,
                        'type': type11,
                        'name': producttitle,
                        'short_Description': shortdescription,
                        'long_Description': longdescri,
                        'image 1': '','image 2': '','image 3': '','image 4': '','image 5': '',
                        'image 6': '','image 7': '','image 8': '','image 9': '','image 10': '',
                        'image 11': '','image 12': '','image 13': '','image 14': '','image 15': '',
                        'image 16': '','image 17': '','image 18': '','image 19': '','image 20': ''
                    }
