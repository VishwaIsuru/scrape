# import scrapy
# import csv
# import requests
# import pandas as pd
#
# class WorkSpider(scrapy.Spider):
#     name = 'charityvalue'
#
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': [
#             "link","Name","Category","EIN",
#             "WebUrl","OverallScore","OverallRating"
#         ]
#     };
#
#     def start_requests(self):
#         col_list = ["urls"]
#         df = pd.read_csv("C:\Users\Vishwa\ScrapyProject\ScrapyProject\Charitynavigator.csv", usecols=col_list)
#         urls = df["urls"]
#         lena = len(urls)+1
#
#         for i in range(0, lena):
#             yield scrapy.Request(url=urls[i], callback=self.parse, dont_filter=True,
#                                  headers={
#                                      'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0',
#                                      'Accept-Encoding': 'gzip, deflate, br', 'Connection': 'keep-alive'})
#
#     def parse(self, response):
#
#         name = response.xpath('//h1[@class="charityname"]/text()').extract_first().strip()
#         category = response.xpath('//p[@class="crumbs"]/text()').extract_first().strip()
#         rat = response.xpath('//div[@class="rating"]//p//text()').extract()
#         j=0
#         ein = ''
#         for eis in rat:
#             if eis == 'EIN':
#                 ein = rat[j+1].strip()
#             j+=1
#         url = response.xpath('//a[@class="orgSiteLink"]/@href').extract_first()
#         ratting = response.xpath('//strong//svg[@class="stars"]/title/text()').extract_first()
#         ovrlrat = ''
#         if ratting == 'four stars':
#             ovrlrat = 4
#         if ratting == 'three stars':
#             ovrlrat = 3
#         if ratting == 'two stars':
#             ovrlrat = 2
#         if ratting == 'one star':
#             ovrlrat = 1
#
#         tables = response.xpath('//div[@class="shadedtable"][1]//tr[2]//text()').extract()
#         scrrat = tables[3]
#         # for tbrw in tables:
#         #     tdstng = tbrw.xpath('.//td//strong//text()').extract_first()
#         yield {
#             "link":response.url,
#             "Name": name,
#             "Category": category,
#             "EIN": ein,
#             "WebUrl": url,
#             "OverallScore": ovrlrat,
#             "OverallRating": scrrat
#         }
