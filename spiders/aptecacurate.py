import scrapy
import csv
import codecs

class WorkSpider(scrapy.Spider):
    name = 'aptekacurate'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            "url","Product_Name","cat","Main_Image","Product_Price",
            "Producent","Kod_Producent","ID","EAN"

        ]
    };

    urls = [
        "https://aptekacurate.pl/",
    ]

    def start_requests(self):

        f = codecs.open("./aptekaCurate.xml", "w+", encoding='utf8')
        contents = "\n".join([
                                 '<?xml version="1.0" encoding="utf-8"?><offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1">',
                                 '</offers>'])
        f.write(contents)
        f.flush()
        f.close()

        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        details = response.xpath('//li[contains(@id,"item_")]//a//@href').extract()
        for link in details:
            l= 'https://aptekacurate.pl'+link
            yield scrapy.Request(url=l.strip(), callback=self.parseInside, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )

    def parseInside(self, response):

        pagingUrls = []
        allprdcount = response.css('.navigation_total > b:nth-child(1)').xpath('.//text()').extract_first()
        prcount = int(allprdcount)
        if prcount>120:
            l = int(prcount/120)
            for k in range(0,l):
                li = response.url+'?counter='+str(k)
                pagingUrls.append(li)

        else:
            pagingUrls.append(response.url)

        for pagesofprdt in pagingUrls:
            yield scrapy.Request(url=pagesofprdt, callback=self.toAllPrdt,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

    def toAllPrdt(self, response):

        prdlinks = response.xpath('//a[@class="product-name"]//@href').extract()

        for prdlink in prdlinks:
            linn = 'https://aptekacurate.pl'+prdlink
            yield scrapy.Request(url=linn, callback=self.prdDetails, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 dont_filter=True)

    def prdDetails(self,response):

        prdname = response.xpath('//h1/text()').extract_first()
        id = response.url.split('/')[-1].split('-')[2]
        r = response.xpath('//a[@class="category"]//text()').extract()
        cat = r[-1]
        try:
            imageurl = 'https://aptekacurate.pl'+ response.xpath('//img[@class="photo"]//@src').extract_first()
        except:
            imageurl = ''
        prdprice1 = response.xpath('//strong[@id="projector_price_value"]//text()').extract_first().replace(',','.')
        prdprice = prdprice1.split()[0]

        producer = response.css('.brand').xpath('.//text()').extract_first()
        kodproducer = response.css('.n56177_p_code').xpath('.//text()').extract_first()
        ean = kodproducer

        line = '<o id="%s" price="%s" url="%s"><cat><![CDATA[%s]]></cat><name><![CDATA[%s]]></name><imgs><main url="%s"/></imgs><attrs><a name="Producent"><![CDATA[%s]]></a><a name="Kod_producenta"><![CDATA[%s]]></a><a name="EAN"><![CDATA[%s]]></a></attrs></o>' % (
            id, prdprice, response.url, cat, prdname, imageurl, producer, kodproducer,ean
        )

        yield {
            "url":response.url,
            "Product_Name":prdname,
            "cat":cat,
            "Main_Image":imageurl,
            "Product_Price":prdprice,
            "Producent":producer,
            "Kod_Producent":kodproducer,
            "ID": id,
            "EAN": ean
        }

        f = codecs.open("./aptekaCurate.xml", "r", encoding='utf8')
        contents = f.readlines()
        contents = [a for a in contents if a.strip() != '']
        f.close()

        contents.insert(len(contents) - 1, line.strip())

        f = codecs.open("./aptekaCurate.xml", "w", encoding='utf8')
        contents = "\n".join(contents)
        f.write(contents)
        f.flush()
        f.close()


