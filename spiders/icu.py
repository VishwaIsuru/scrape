import scrapy
import csv
import re

class WorkSpider(scrapy.Spider):
    name = 'icu'
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Link","Name", "street address", "city", "state/prov", "zip/postal", "country",
            "student enrollment", "FB page", "Twitter", "Instagram",

        ]
    };

    urls = [
        "https://www.4icu.org/us/","https://www.4icu.org/ca/",
    ]

    def start_requests(self):

        for u in self.urls:
            yield scrapy.Request(url=u.strip(), callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parse(self, response):
        universities = response.xpath('//table[@class="table table-hover"]//tbody//tr//td[2]//a')
        links = []
        for university in universities:
            link = university.xpath('./@href').extract_first()
            links.append(link)

        for link in links:
            l = 'https://www.4icu.org'+link
            yield scrapy.Request(url=l.strip(), callback=self.parseInside,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parseInside(self,response):

        names = response.xpath('//ol[@class="breadcrumb small"]//li//a//text()').extract()
        name = names[-1]
        staddress = response.xpath('//span[@itemprop="streetAddress"]//text()').extract_first()
        city = response.xpath('//span[@itemprop="addressLocality"]//text()').extract_first()
        state = response.xpath('//span[@itemprop="addressRegion"]//text()').extract_first()
        postlcode = response.xpath('//span[@itemprop="postalCode"]//text()').extract_first()
        postlcode = re.sub('  +', '', postlcode)
        postlcode = re.sub('\n+', '\n', postlcode).strip()
        country = names[-2]

        alltbles = response.xpath('//table[@class="table borderless"]//tr')
        stuenrol = ''
        for tbdata in alltbles:
            thtext = tbdata.xpath('.//th/text()').extract_first()
            tdtext = ''.join([x.strip() for x in tbdata.xpath('.//td//text()').extract()])

            if thtext == "Student Enrollment":
                stuenrol = tdtext

        socialmedias = response.xpath('//div[@id="social-media"]//a/@href').extract()
        fblink = ''
        twiter = ''
        inster = ''
        for socimdia in socialmedias:
            if 'facebook.com' in socimdia:
                fblink = socimdia
            if 'twitter.com' in socimdia:
                twiter = socimdia
            if 'instagram.com' in socimdia:
                inster = socimdia

        yield {
            "Link": response.url,
            "Name":name, "street address":staddress, "city":city,
            "state/prov":state, "zip/postal":postlcode, "country":country,
            "student enrollment":stuenrol, "FB page":fblink,
            "Twitter":twiter, "Instagram":inster,

        }
