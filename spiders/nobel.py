import scrapy
import csv
import requests
import pandas as pd


class WorkSpider(scrapy.Spider):
    name = 'nobel'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            "url","Name","Title","nobel #","Manufacture","MPN",
            "Contry of origin","Ability One","Image url","Product Description","Contry Of Origin",
            "Grainger Number","Manufacturer","SDS","Package Quantity","UOM",
            "Weight","Contracts","Earth Friendly","GSA Approved",
            "MPN1","NSN","PSN","UPC"

        ]
    };

    # urls = [
    #
    #     "https://shop.noble.com/n283b93.html","https://shop.noble.com/n344a29.html"
    # ]


    def start_requests(self):

        col_list = ["linku"]

        df = pd.read_csv("dataQ5.csv", usecols=col_list)
        urls = df["linku"]

        for i in range(0,136000):
            yield scrapy.Request(url=urls[i], callback=self.parse,dont_filter=True,
                                 headers={
                                     'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0',
                                         'Accept-Encoding': 'gzip, deflate, br', 'Connection': 'keep-alive'})

        # f = open("dataQ4.csv", "r")
        # contents = f.read().split('\n')
        # i = 0
        # # contents=['','https://www.cameo.com/c/actors']
        # for c in contents:
        #     i += 1
        #     if i == 1:
        #         continue
        #     yield scrapy.Request(url=c.strip(), callback=self.parse,
        #                          headers={'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0',
        #                              'Accept-Encoding': 'gzip, deflate, br', 'Connection': 'keep-alive'})

        # i=0
        # for u in self.urls:
        #     yield scrapy.Request(url=u, callback=self.parse,headers={'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0','Accept-Encoding':'gzip, deflate, br','Connection':'keep-alive'})
        #     i+=1

    def parse(self, response):
        try:
            name=response.css('.h1').xpath('./text()').extract_first()
        except:
            name=''
        try:
            nobelnum=response.css('.quick-overview > ul:nth-child(2) > li:nth-child(1) > span:nth-child(2)').xpath('./text()').extract_first()
        except:
            nobelnum=''
        try:
            manufacture = response.css('.quick-overview > ul:nth-child(2) > li:nth-child(2) > span:nth-child(2)').xpath('./text()').extract_first()
        except:
            manufacture=''
        try:
            mpn = response.css('.quick-overview > ul:nth-child(2) > li:nth-child(3) > span:nth-child(2)').xpath('./text()').extract_first()
        except:
            mpn=''
        try:
            contyori = response.css('.quick-overview > ul:nth-child(2) > li:nth-child(4) > span:nth-child(2)').xpath('./text()').extract_first()
        except:
            contyori=''
        try:
            imageur=response.css('#image-main').xpath('./@src').extract_first()
        except:
            imageur=''
        try:
            descri=response.css('.std').xpath('./text()').extract()
        except:
            descri=''
        try:
            abilityone=response.css('.product-specs-container > span:nth-child(2)').xpath('./text()').extract_first()
        except:
            abilityone=''
        try:
            countryof=response.css('.product-specs-container > span:nth-child(6)').xpath('./text()').extract_first()
        except:
            countryof=''
        try:
            grainigrnum = response.css('.product-specs-container > span:nth-child(10)').xpath('./text()').extract_first()
        except:
            grainigrnum=''
        try:
            manufact = response.css('.product-specs-container > span:nth-child(14)').xpath('./text()').extract_first()
        except:
            manufact=''
        try:
            sds = response.css('.product-specs-container > span:nth-child(18)').xpath('./text()').extract_first()
        except:
            sds=''
        try:
            pccon = response.css('.product-specs-container > span:nth-child(22)').xpath('./text()').extract_first()
        except:
            pccon=''
        try:
            umo = response.css('.product-specs-container > span:nth-child(26)').xpath('./text()').extract_first()
        except:
            umo=''
        try:
            weight = response.css('.product-specs-container > span:nth-child(30)').xpath('./text()').extract_first()
        except:
            weight=''
        try:
            contract = response.css('.product-specs-container > span:nth-child(4)').xpath('./text()').extract_first()
        except:
            contract=''
        try:
            erthfri = response.css('.product-specs-container > span:nth-child(8)').xpath('./text()').extract_first()
        except:
            erthfri=''
        try:
            gsaapp = response.css('.product-specs-container > span:nth-child(12)').xpath('./text()').extract_first()
        except:
            gsaapp=''
        try:
            mmp = response.css('.product-specs-container > span:nth-child(16)').xpath('./text()').extract_first()
        except:
            mmp=''
        try:
            psn = response.css('.product-specs-container > span:nth-child(24)').xpath('./text()').extract_first()
        except:
            psn=''
        try:
            upc = response.css('.product-specs-container > span:nth-child(28)').xpath('./text()').extract_first()
        except:
            upc=''
        try:
            nsn = response.css('.product-specs-container > span:nth-child(20)').xpath('./text()').extract_first()
        except:
            nsn=''
        try:
            title='Home/'+name
        except:
            title=''


        yield {
            "url":response.url,
            "Name":name,
            "Title":title,
            "nobel #":nobelnum,
            "Manufacture":manufacture,
            "MPN":mpn,
            "Contry of origin":contyori,
            "Ability One":abilityone,
            "Image url":imageur,
            "Product Description":descri,
            "Contry Of Origin":countryof,
            "Grainger Number":grainigrnum,
            "Manufacturer":manufact,
            "SDS":sds,
            "Package Quantity":pccon,
            "UOM":umo,
            "Weight":weight,
            "Contracts":contract,
            "Earth Friendly":erthfri,
            "GSA Approved":gsaapp,
            "MPN1":mmp,
            "NSN":nsn,
            "PSN":psn,
            "UPC":upc
        }

