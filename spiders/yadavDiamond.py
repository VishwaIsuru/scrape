import scrapy
import pymysql.cursors
import csv

csv.register_dialect('myDialect', delimiter='/', quoting=csv.QUOTE_NONE)

connection = pymysql.connect(host='localhost',
                             user='root',
                             password='root',
                             db='scrape', use_unicode=True, charset="utf8")

class WorkSpider(scrapy.Spider):
    name = 'yadav'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "id","image","shape","carat","color","clarity","cut","lab",
            "polish","symmetry","depth","tablevalue",
            "price","gia","Measurements","Fluorescence","ratio"
        ]
    };

    urls = [
        "https://www.yadavjewelry.com/diamond?clarity_max=11&clarity_min=2&color_colored=&color_max=23&color_min=14&colored_intensity=&cut_max=4&cut_min=2&cut_ui_max=4&cut_ui_min=1&depth_max=88&depth_min=43&depth_ui_max=88&depth_ui_min=43&fluor_max=6&fluor_min=-1&instore=all&labcreated=false&page=1&polish_max=5&polish_min=0&price_max=2000000&price_min=100&price_ui_max=%242000000&price_ui_min=%24100&search_waiting=0&shape-checkbox=&shape=RB&supplier_shipping_date=&symmetry_max=5&symmetry_min=0&tablep_max=85&tablep_min=48&tablep_ui_max=85&tablep_ui_min=48&utf8=%E2%9C%93&weight_max=20&weight_min=0.2&width_max=20&width_min=3&width_ui_max=20&width_ui_min=3&with_image=false",
        # "https://www.yadavjewelry.com/diamond?clarity_max=11&clarity_min=2&color_colored=&color_max=23&color_min=14&colored_intensity=&cut_max=5&cut_min=2&cut_ui_max=4&cut_ui_min=1&depth_max=88&depth_min=43&depth_ui_max=88&depth_ui_min=43&fluor_max=6&fluor_min=-1&instore=all&labcreated=false&page=1&polish_max=5&polish_min=0&price_max=2000000&price_min=100&price_ui_max=%242000000&price_ui_min=%24100&search_waiting=1&shape-checkbox=&shape=CUS&supplier_shipping_date=&symmetry_max=5&symmetry_min=0&tablep_max=85&tablep_min=48&tablep_ui_max=85&tablep_ui_min=48&utf8=%E2%9C%93&weight_max=20&weight_min=0.2&width_max=20&width_min=3&width_ui_max=20&width_ui_min=3&with_image=false",
        # "https://www.yadavjewelry.com/diamond?clarity_max=11&clarity_min=2&color_colored=&color_max=23&color_min=14&colored_intensity=&cut_max=5&cut_min=2&cut_ui_max=4&cut_ui_min=1&depth_max=88&depth_min=43&depth_ui_max=88&depth_ui_min=43&fluor_max=6&fluor_min=-1&instore=all&labcreated=false&page=1&polish_max=5&polish_min=0&price_max=2000000&price_min=100&price_ui_max=%242000000&price_ui_min=%24100&search_waiting=1&shape-checkbox=&shape=OV&supplier_shipping_date=&symmetry_max=5&symmetry_min=0&tablep_max=85&tablep_min=48&tablep_ui_max=85&tablep_ui_min=48&utf8=%E2%9C%93&weight_max=20&weight_min=0.2&width_max=20&width_min=3&width_ui_max=20&width_ui_min=3&with_image=false",
        # "https://www.yadavjewelry.com/diamond?clarity_max=11&clarity_min=2&color_colored=&color_max=23&color_min=14&colored_intensity=&cut_max=5&cut_min=2&cut_ui_max=4&cut_ui_min=1&depth_max=88&depth_min=43&depth_ui_max=88&depth_ui_min=43&fluor_max=6&fluor_min=-1&instore=all&labcreated=false&page=1&polish_max=5&polish_min=0&price_max=2000000&price_min=100&price_ui_max=%242000000&price_ui_min=%24100&search_waiting=1&shape-checkbox=&shape=PS&supplier_shipping_date=&symmetry_max=5&symmetry_min=0&tablep_max=85&tablep_min=48&tablep_ui_max=85&tablep_ui_min=48&utf8=%E2%9C%93&weight_max=20&weight_min=0.2&width_max=20&width_min=3&width_ui_max=20&width_ui_min=3&with_image=false",
        # "https://www.yadavjewelry.com/diamond?clarity_max=11&clarity_min=2&color_colored=&color_max=23&color_min=14&colored_intensity=&cut_max=5&cut_min=2&cut_ui_max=4&cut_ui_min=1&depth_max=88&depth_min=43&depth_ui_max=88&depth_ui_min=43&fluor_max=6&fluor_min=-1&instore=all&labcreated=false&page=1&polish_max=5&polish_min=0&price_max=2000000&price_min=100&price_ui_max=%242000000&price_ui_min=%24100&search_waiting=1&shape-checkbox=&shape=PR&supplier_shipping_date=&symmetry_max=5&symmetry_min=0&tablep_max=85&tablep_min=48&tablep_ui_max=85&tablep_ui_min=48&utf8=%E2%9C%93&weight_max=20&weight_min=0.2&width_max=20&width_min=3&width_ui_max=20&width_ui_min=3&with_image=false",
        # "https://www.yadavjewelry.com/diamond?clarity_max=11&clarity_min=2&color_colored=&color_max=23&color_min=14&colored_intensity=&cut_max=5&cut_min=2&cut_ui_max=4&cut_ui_min=1&depth_max=88&depth_min=43&depth_ui_max=88&depth_ui_min=43&fluor_max=6&fluor_min=-1&instore=all&labcreated=false&page=1&polish_max=5&polish_min=0&price_max=2000000&price_min=100&price_ui_max=%242000000&price_ui_min=%24100&search_waiting=1&shape-checkbox=&shape=RAD&supplier_shipping_date=&symmetry_max=5&symmetry_min=0&tablep_max=85&tablep_min=48&tablep_ui_max=85&tablep_ui_min=48&utf8=%E2%9C%93&weight_max=20&weight_min=0.2&width_max=20&width_min=3&width_ui_max=20&width_ui_min=3&with_image=false",
        # "https://www.yadavjewelry.com/diamond?clarity_max=11&clarity_min=2&color_colored=&color_max=23&color_min=14&colored_intensity=&cut_max=5&cut_min=2&cut_ui_max=4&cut_ui_min=1&depth_max=88&depth_min=43&depth_ui_max=88&depth_ui_min=43&fluor_max=6&fluor_min=-1&instore=all&labcreated=false&page=1&polish_max=5&polish_min=0&price_max=2000000&price_min=100&price_ui_max=%242000000&price_ui_min=%24100&search_waiting=1&shape-checkbox=&shape=SQR&supplier_shipping_date=&symmetry_max=5&symmetry_min=0&tablep_max=85&tablep_min=48&tablep_ui_max=85&tablep_ui_min=48&utf8=%E2%9C%93&weight_max=20&weight_min=0.2&width_max=20&width_min=3&width_ui_max=20&width_ui_min=3&with_image=false",
        # "https://www.yadavjewelry.com/diamonds?utf8=%E2%9C%93&labcreated=false&supplier_shipping_date=&shape-checkbox=&shape=SQR&search_waiting=1&cut_min=2&cut_max=5&cut_ui_min=1&cut_ui_max=4&color_min=14&color_max=23&weight_min=0.2&weight_max=20&color_colored=&clarity_min=2&clarity_max=11&price_min=100&price_ui_min=%24100&price_max=2000000&price_ui_max=%242000000&instore=all&colored_intensity=&polish_min=0&polish_max=5&depth_min=43&depth_ui_min=43&depth_max=88&depth_ui_max=88&symmetry_min=0&symmetry_max=5&tablep_min=48&tablep_ui_min=48&tablep_max=85&tablep_ui_max=85&fluor_min=-1&fluor_max=6&width_min=3&width_ui_min=3&width_max=20&width_ui_max=20&with_image=false",
        # "https://www.yadavjewelry.com/diamond?clarity_max=11&clarity_min=2&color_colored=&color_max=23&color_min=14&colored_intensity=&cut_max=5&cut_min=2&cut_ui_max=4&cut_ui_min=1&depth_max=88&depth_min=43&depth_ui_max=88&depth_ui_min=43&fluor_max=6&fluor_min=-1&instore=all&labcreated=false&page=1&polish_max=5&polish_min=0&price_max=2000000&price_min=100&price_ui_max=%242000000&price_ui_min=%24100&search_waiting=1&shape-checkbox=&shape=EC&supplier_shipping_date=&symmetry_max=5&symmetry_min=0&tablep_max=85&tablep_min=48&tablep_ui_max=85&tablep_ui_min=48&utf8=%E2%9C%93&weight_max=20&weight_min=0.2&width_max=20&width_min=3&width_ui_max=20&width_ui_min=3&with_image=false",
        # "https://www.yadavjewelry.com/diamond?clarity_max=11&clarity_min=2&color_colored=&color_max=23&color_min=14&colored_intensity=&cut_max=5&cut_min=2&cut_ui_max=4&cut_ui_min=1&depth_max=88&depth_min=43&depth_ui_max=88&depth_ui_min=43&fluor_max=6&fluor_min=-1&instore=all&labcreated=false&page=1&polish_max=5&polish_min=0&price_max=2000000&price_min=100&price_ui_max=%242000000&price_ui_min=%24100&search_waiting=1&shape-checkbox=&shape=AS&supplier_shipping_date=&symmetry_max=5&symmetry_min=0&tablep_max=85&tablep_min=48&tablep_ui_max=85&tablep_ui_min=48&utf8=%E2%9C%93&weight_max=20&weight_min=0.2&width_max=20&width_min=3&width_ui_max=20&width_ui_min=3&with_image=false",
        # "https://www.yadavjewelry.com/diamonds?utf8=%E2%9C%93&labcreated=false&supplier_shipping_date=&shape-checkbox=&shape=HS&search_waiting=1&cut_min=2&cut_max=5&cut_ui_min=1&cut_ui_max=4&color_min=14&color_max=23&weight_min=0.2&weight_max=20&color_colored=&clarity_min=2&clarity_max=11&price_min=100&price_ui_min=%24100&price_max=2000000&price_ui_max=%242000000&instore=all&colored_intensity=&polish_min=0&polish_max=5&depth_min=43&depth_ui_min=43&depth_max=88&depth_ui_max=88&symmetry_min=0&symmetry_max=5&tablep_min=48&tablep_ui_min=48&tablep_max=85&tablep_ui_max=85&fluor_min=-1&fluor_max=6&width_min=3&width_ui_min=3&width_max=20&width_ui_max=20&with_image=false",
        # "https://www.yadavjewelry.com/diamond?clarity_max=11&clarity_min=2&color_colored=&color_max=23&color_min=14&colored_intensity=&cut_max=5&cut_min=2&cut_ui_max=4&cut_ui_min=1&depth_max=88&depth_min=43&depth_ui_max=88&depth_ui_min=43&fluor_max=6&fluor_min=-1&instore=all&labcreated=false&page=1&polish_max=5&polish_min=0&price_max=2000000&price_min=100&price_ui_max=%242000000&price_ui_min=%24100&search_waiting=1&shape-checkbox=&shape=MQ&supplier_shipping_date=&symmetry_max=5&symmetry_min=0&tablep_max=85&tablep_min=48&tablep_ui_max=85&tablep_ui_min=48&utf8=%E2%9C%93&weight_max=20&weight_min=0.2&width_max=20&width_min=3&width_ui_max=20&width_ui_min=3&with_image=false"

    ]

    def start_requests(self):
        header = {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(
                31111111111111) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
            "Host": "www.yadavjewelry.com",
            "Connection": "keep-alive",
        }

        for url in self.urls:
            yield scrapy.Request(url=url, headers=header, callback=self.parseInside, dont_filter=True,meta={'page':1})

    def parseInside(self, response):
        header = {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(
                31111111111111) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
            "Host": "www.yadavjewelry.com",
            "Connection": "keep-alive",
        }

        next_page = response.meta['page'] + 1
        table = response.xpath('//tbody[@class="list-view-container"]//tr')
        for tr in table:
            img = tr.xpath('.//td[1]//img/@src').extract_first()
            shape = tr.xpath('.//td[2]//text()').extract_first()
            carat = tr.xpath('.//td[3]//text()').extract_first()
            color = tr.xpath('.//td[4]//text()').extract_first()
            clarity = tr.xpath('.//td[5]//text()').extract_first()
            cut = tr.xpath('.//td[6]//text()').extract_first()
            lab = tr.xpath('.//td[7]//text()').extract_first()
            polish = tr.xpath('.//td[8]//text()').extract_first()
            symmetry = tr.xpath('.//td[9]//text()').extract_first()
            depth = tr.xpath('.//td[10]//text()').extract_first()
            tablevl = tr.xpath('.//td[11]//text()').extract_first()
            id = tr.xpath('.//td[14]//@data-id').extract_first()

            if id is not None:
                tomeasurementurl = 'https://www.yadavjewelry.com/diamond/yd'+str(id)
                yield scrapy.Request(url=tomeasurementurl,headers= header, callback=self.parseInsideMeasure, dont_filter=True,
                                     meta={'id':id,'image': img,'shape': shape,'carat': carat,'color': color,'clarity':clarity,'cut':cut,
                                           'lab': lab,'polish': polish,'symmetry':symmetry,'depth':depth,'tablevel': tablevl})

        pages = response.xpath('//ul[@class="pagination pagination-sm"][1]//li[@class=""]')
        print(next_page)
        # print(pages)
        for page in pages:
            print("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW")
            text = page.xpath('.//text()').extract()
            text = text[1]
            links = page.xpath('./a/@href').extract_first()

            print(text)
            pageNum = int(text)
            if pageNum == next_page:
                header = {
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(
                        pageNum) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
                    "Connection": "keep-alive",
                }

                l = 'https://www.yadavjewelry.com' + page.xpath('./a/@href').extract_first()
                yield scrapy.Request(url=l, callback=self.parseInside, headers=header, meta={'page': next_page})
            # break

    def parseInsideMeasure(self,response):

        id = response.meta['id']
        image = response.meta['image']
        shape = response.meta['shape']
        carat = response.meta['carat']
        color = response.meta['color']
        clarity = response.meta['clarity']
        cut = response.meta['cut']
        lab = response.meta['lab']
        polish = response.meta['polish']
        symmetry = response.meta['symmetry']
        depth = response.meta['depth']
        tablelevel = response.meta['tablevel']
        print("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv")
        price = ''.join([x.strip() for x in response.css('.style-price').xpath('.//text()').extract()])
        try:
            gia = response.xpath('//a[@title="Open GIA certification"]/@href').extract_first().split('/')[-1].split('.')[0].replace('report-check?reportno=','')
        except:
            gia = ''
        j=0
        measurements = ''
        fluorescence = ''
        ratio = ''
        msn = response.xpath('//div[@class="col-xs-12 col-sm-6"]//dt//text()').extract()
        for s in msn:
            if 'Measurements' == s:
                measurements = msn[j+1]
            if 'Fluorescence' == s:
                fluorescence = msn[j+1]
            if 'L/W Ratio' == s:
                ratio = msn[j+1]
            j+=1

        yield {
            "id": id,
            "image": image,
            "shape": shape,
            "carat": carat,
            "color": color,
            "clarity": clarity,
            "cut": cut,
            "lab": lab,
            "polish": polish,
            "symmetry": symmetry,
            "depth": depth,
            "tablevalue": tablelevel,
            "price": price,
            "gia": gia,
            "Measurements": measurements,
            "Fluorescence": fluorescence,
            "ratio": ratio
        }

        # with connection.cursor() as cursor:
        #
        #     sql = "SELECT * FROM `yadav` WHERE `id`=%s"
        #     cursor.execute(sql, (id))
        #     result = cursor.fetchone()
        #     print(result)
        #     # connection.commit()
        #     if result is None:
        #         sql = "INSERT INTO yadav (id,image,shape,carat,color,clarity,cut," \
        #               "lab,polish,symmetry,depth,tablevalue,price)" \
        #               "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s , %s,%s, %s, %s)"
        #
        #         cursor.execute(sql,
        #                        (id,image,shape,carat,color,clarity,cut,lab,polish,symmetry,depth,tablelevel,price)
        #                        )
        #
        #         connection.commit()
        #
        #     else:
        #         sql = "UPDATE yadav SET  image=%s ,shape = %s,carat = %s ,color = %s,clarity = %s," \
        #               "cut = %s,lab = %s,polish = %s," \
        #               "symmetry = %s,depth = %s,tablevalue = %s,price = %s" \
        #               "WHERE id =%s"
        #
        #         cursor.execute(sql, (image,shape,carat,color,clarity,cut,lab,polish,symmetry,depth,tablelevel,price,id))
        #         connection.commit()

