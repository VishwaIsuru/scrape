import scrapy
import csv
import requests

class WorkSpider(scrapy.Spider):
    name = 'posterimg'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            'Name',
        ]
    };

    urls = [
        'https://posterfoundry.com/wall-art/posters/?sort=bestselling&page=86',
        # 'https://posterfoundry.com/wall-art/',
            # 'https://posterfoundry.com/2021-calendars/','https://posterfoundry.com/stationery/',
            # 'https://posterfoundry.com/mugs/','https://posterfoundry.com/art/',
            # 'https://posterfoundry.com/bob-ross/','https://posterfoundry.com/educational/',
            # 'https://posterfoundry.com/funny/','https://posterfoundry.com/gaming/',
            # 'https://posterfoundry.com/lost-in-space/','https://posterfoundry.com/movies/',
            # 'https://posterfoundry.com/motivational/','https://posterfoundry.com/music/',
            # 'https://posterfoundry.com/photography/','https://posterfoundry.com/sports/',
            # 'https://posterfoundry.com/tom-wood/','https://posterfoundry.com/tv/'
    ]

    def start_requests(self):
        i = 0
        for u in self.urls:

            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},meta={'page':86}
                                 )
            i += 1

    def parse(self, response):
        next_page = response.meta['page'] + 1

        postrimgs= response.xpath('//li[@class="product"]')
        for postrimg in postrimgs:
            imgurl = postrimg.xpath('.//@data-src').extract_first().replace('350x350','2200x2200')
            print(imgurl)
            r = requests.get(imgurl)
            imgalt = postrimg.xpath('.//h4//a//text()').extract_first().replace(' ','_').replace('/','').replace('"','').replace('\t','').replace('\n','').replace('*','_')
            print(imgalt)
            imgName = "./posterimges/" + imgalt+ ".jpg"
            if r.status_code == 200:
                with open(imgName, 'wb') as f:
                    f.write(r.content)

        pages = response.xpath('//li[@class="pagination-item"]')
        print(next_page)

        for page in pages:
            text = page.xpath('.//a/text()').extract_first()
            pageNum = int(text)
            if pageNum == next_page:
                header = {
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(
                        pageNum) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
                    "Referer": "https://posterfoundry.com/search.php?page="+str(pageNum)+"&section=product&search_query=posters",
                    "Connection": "keep-alive",
                    "Host": "posterfoundry.com"
                }

                l = 'https://posterfoundry.com' + page.xpath('.//a/@href').extract_first()
                yield scrapy.Request(url=l, callback=self.parse, headers=header, meta={'page': next_page})


