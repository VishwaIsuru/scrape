# from datetime import datetime
#
# import scrapy
# import re
# import json
# import csv
# import pymysql.cursors
# from scrapy.http import FormRequest
# import random
# import string
# # encoding=utf8
# # import sys
# # from importlib import reload
# # reload(sys)
# # sys.setdefaultencoding('utf8')
#
# import csv
# import psycopg2
# csv.register_dialect('myDialect', delimiter='/', quoting=csv.QUOTE_NONE)
#
# connection = psycopg2.connect(
#     host="diamond-production.ctbdnd6gbcye.ap-southeast-1.rds.amazonaws.com",
#     database="diamond_production",
#     port=5432,
#     user="postgres",
#     password="hdfw934klPK")
#
#
# class workspider(scrapy.Spider):
#     name = "brilliant_earth_lab"
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': ["id","upc","index_id","index_name","has_cert","certificate_number","rank_order","title","carat","shape","color","clarity","fluorescence","polish","symmetry","cut","origin","measurement","depth","price","images"]
#     };
#
#     def start_requests(self):
#         colors = ['J','I','H','G','F','E','D']
#         clarities = ['SI2','SI1','VS2','VS1','VVS2','VVS1','IF','FL']
#
#         for col in colors:
#             for cal in clarities:
#                 for page in range(1, 5):
#                     header = {
#                         "user-agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(
#                             page) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"
#                     }
#                     # url = f"https://www.brilliantearth.com/loose-diamonds/list/?shapes=All&cuts=Fair,Good,Very Good,Ideal,Super Ideal&colors={col}&clarities={cal}&polishes=Good,Very Good,Excellent&symmetries=Good,Very Good,Excellent&fluorescences=Very Strong,Strong,Medium,Faint,None&min_carat=0.25&max_carat=12.88&min_table=48.00&max_table=87.00&min_depth=41.30&max_depth=89.10&min_price=370&max_price=1348720&stock_number=&row=0&page={page}&requestedDataSize=1000&order_by=price&order_method=asc&currency=$&has_v360_video=&dedicated=&min_ratio=1.00&max_ratio=2.75&shipping_day=&MIN_PRICE=370&MAX_PRICE=1348720&MIN_CARAT=0.25&MAX_CARAT=12.88&MIN_TABLE=45&MAX_TABLE=87&MIN_DEPTH=41.3&MAX_DEPTH=89.1"
#                     url = "https://www.brilliantearth.com/lab-diamonds/list/?shapes=Round%2COval%2CCushion%2CPear%2CPrincess%2CEmerald%2CMarquise%2CAsscher%2CRadiant%2CHeart&cuts=Fair%2CGood%2CVery%20Good%2CIdeal%2CSuper%20Ideal&colors="+str(col)+"&clarities="+str(cal)+"&polishes=Good%2CVery%20Good%2CExcellent&symmetries=Good%2CVery%20Good%2CExcellent&fluorescences=Very%20Strong%2CStrong%2CMedium%2CFaint%2CNone&min_carat=0.29&max_carat=11.01&min_table=45.00&max_table=91.00&min_depth=5.00&max_depth=85.70&min_price=320&max_price=145210&stock_number=&row=0&page="+str(page)+"&requestedDataSize=1000&order_by=price&order_method=asc&currency=%24&has_v360_video=&dedicated=&min_ratio=1.00&max_ratio=2.75&exclude_quick_ship_suppliers=&MIN_PRICE=270&MAX_PRICE=145210&MIN_CARAT=0.27&MAX_CARAT=11.05&MIN_TABLE=45&MAX_TABLE=91&MIN_DEPTH=5&MAX_DEPTH=85.7"
#                     yield scrapy.Request(url, callback=self.parse, headers=header)
#                     # break
#
#     def parse(self, response):
#         data = json.loads(response.body)
#
#         for diamond in data['diamonds']:
#             id = diamond['id']
#             index_id = diamond['index_id']
#             upc = diamond['upc']
#             index_name = diamond['index_name']
#             has_cert = diamond['has_cert']
#             certificate_number = None
#             try:
#                 certificate_number = diamond['certificate_number']
#             except:
#                 pass
#
#             rank_order = diamond['rank_order']
#             title = diamond['title']
#             carat = diamond['carat']
#             shape = diamond['shape']
#             color = diamond['color']
#             price = diamond['price']
#             clarity = diamond['clarity']
#             fluorescence = diamond['fluorescence']
#             polish = diamond['polish']
#             symmetry = diamond['symmetry']
#             cut = None
#             try:
#                 cut = diamond['cut']
#             except:
#                 pass
#             origin = None
#             try:
#                 origin = diamond['origin']
#             except:
#                 pass
#             measurement = None
#             try:
#                 measurement = diamond['measurements']
#             except:
#                 pass
#             depth = None
#             try:
#                 depth = diamond['depth']
#             except:
#                 pass
#             images = None
#             try:
#                 images = "'{"+",".join(['\"https:'+image['src']+'\"' for image in diamond['images']['images']])+"}'"
#             except:
#                 pass
#
#             yield {
#                 "id":id, "upc":upc, "index_id":index_id, "index_name":index_name,
#                 "has_cert":has_cert, "certificate_number":certificate_number, "rank_order":rank_order,
#                 "title":title,"carat":carat, "shape":shape, "color":color, "clarity":clarity,
#                 "fluorescence":fluorescence, "polish":polish, "symmetry":symmetry, "cut":cut,
#                 "origin":origin,"measurement":measurement, "depth":depth,"price":price, "images":images
#             }
#
#             # now = datetime.now()
#             # dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
#             # with connection.cursor() as cursor:
#             #     sql = "SELECT id,prices FROM brilliant_earth_diamonds WHERE id=%s"
#             #     cursor.execute(sql%(id))
#             #     result = cursor.fetchone()
#             #     connection.commit()
#             #     if result is None:
#             #         price_str = "'{\""+str(price)+"\"}'"
#             #         sql = "INSERT INTO brilliant_earth_diamonds (id,upc,index_id,index_name,has_cert,certificate_number,rank_order," \
#             #               "title,carat,shape," \
#             #               "color,clarity,fluorescence,polish,symmetry,cut,origin,measurement,depth,prices,images,first_scraped_at,last_scraped_at)" \
#             #               "VALUES (%s, '%s', '%s', '%s', %s, '%s', %s," \
#             #               " '%s', %s , '%s'," \
#             #               "'%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s , %s, %s, '%s', '%s')"
#             #
#             #         cursor.execute(sql%
#             #                        (id, upc, index_id, index_name, has_cert, certificate_number, rank_order,
#             #                         title, carat,shape,
#             #                         color,clarity, fluorescence, polish, symmetry, cut, origin, measurement, depth,price_str, images,dt_string,dt_string)
#             #                        )
#             #
#             #         connection.commit()
#             #
#             #     else:
#             #         exist_price = result[1]
#             #         if exist_price[-1]!=price:
#             #             exist_price.append(price)
#             #             exist_price = [str(ext_pr) for ext_pr in exist_price]
#             #             price_str = "'{"+','.join(exist_price)+"}'"
#             #             sql = "UPDATE brilliant_earth_diamonds SET prices=%s,images = %s,last_scraped_at='%s'" \
#             #                   "WHERE id =%s"
#             #
#             #             cursor.execute(sql% (price_str,images,dt_string,id))
#             #             connection.commit()
#             #         else:
#             #             sql = "UPDATE brilliant_earth_diamonds SET last_scraped_at='%s',images = %s " \
#             #                   "WHERE id =%s"
#             #
#             #             cursor.execute(sql % (dt_string,images, id))
#             #             connection.commit()
