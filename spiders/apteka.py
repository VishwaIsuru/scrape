import scrapy
import csv

class WorkSpider(scrapy.Spider):
    name = 'apteka'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            "url","cat","name","main_image","Price","producent","Kod_producenta","id",#"EAN"

        ]
    };

    urls = ["https://aptekacurate.pl",]

    def start_requests(self):
        i = 0
        for u in self.urls:

            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        details = response.xpath('//ul[@class="dl-menu"]/li/a')

        links = details.xpath('./@href').extract()
        for link in links:
            l = 'https://aptekacurate.pl' + link
            yield scrapy.Request(url=l,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True)
        # print l ,dont_filter=True


    def parseInside(self,response):

        pages = response.xpath('//div[@id="content"]/div[@class="search_categoriesdescription clearfix"]/span/b/text()').extract()
        allpages = []
        for page in pages:
            pagenum= int(page)/120


            i = 0
            while i < pagenum+1:
                pgs = response.url+"?counter="+str(i)
                allpages.append(pgs)
                i += 1

            # print pagenum
        for pagedetl in allpages:
            yield scrapy.Request(url=pagedetl, callback=self.productdtl, headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True)

    def productdtl(self,response):


        producturls=response.xpath('//div[@id="content"]/div[@id="search"]//a[@class="product-name"]/@href').extract()

        for producturl in producturls:
            prdurl = 'https://aptekacurate.pl' + producturl
            yield scrapy.Request(url=prdurl, callback=self.productdetails, headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def productdetails(self,response):
        cat= response.xpath('//div[@id="breadcrumbs_sub"]/ol/li//text()').extract()
        cat=cat[-2]
        name= ''.join([x.strip() for x in response.xpath('//div[@id="content"]//h1/text()').extract()])
        # description=''.join([x.strip() for x in response.xpath('//div[@id="content"]//div[@class="projector_description description"]/ul/li/text()').extract()])
        image=response.xpath('//div[@id="content"]//div[@class="photos col-md-4 col-xs-12 "]/ul/li/a/@href').extract_first()
        imageurl= 'https://aptekacurate.pl'+image
        price=''.join([x.strip() for x in response.xpath('//div[@id="content"]//div[@id="projector_price_value_wrapper"]/div/strong/text()').extract()])
        producer=response.xpath('//div[@id="content"]//div[@class="product_info_sub col-lg-6 col-md-12"]//div[@class="producer"]/a/text()').extract()
        ean=''.join([x.strip() for x in response.xpath('//table[@class="n54117_producer_code"]/tr/td[@class="n56177_p_code"]/text()').extract()])

        id=response.url.split('https://aptekacurate.pl/product-pol-')[1].split('-')[0]

        yield {
            "url":response.url,
            "cat": cat,
            "name":name,

            "main_image": imageurl,
            "Price": price,
            "producent":producer,
            "Kod_producenta":ean,
            "id":id


        }
