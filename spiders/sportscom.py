import scrapy
import csv
from datetime import datetime
import pandas as pd


class workspider(scrapy.Spider):
    name = "sportscom"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "url","FirstName","LastName","Position","Height","Weight","HighSchool","JuniorSchool","HomeTown","State",
            "GraduationClass","CommittedCollege","CommittedCollageClass","CompositeRatting","CompositeStars","CompositeNationalRanking",
            "CompositePositionalRanking","CompositeStateRanking","CompositeAllTimeRanking","SportsRating","SportsStars",
            "SportsNationalRanking","SportsPositionalRanking","SportsStateRanking","SportsAllTimeRanking","ViewComplittemlistUrl"

        ]
    };
    # urls = []
    # for i in range(2021,2025):
    #     u = "https://247sports.com/Site/247Sports-33/Sitemap.Xml?Year="+str(i)
    #     urls.append(u)

    def start_requests(self):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'Cache-Control': 'max-age=0',
        }
        col_list = ["urlsno"]
        df = pd.read_csv("sporstcommisview21to25.csv", usecols=col_list)
        inturls = df["urlsno"]
        # idurl = df['url']
        l = len(inturls)
        for i in range(0, l):
            # id = idurl[i].split('/')[4]
            yield scrapy.Request(inturls[i], callback=self.parseInside, headers=headers)
        # for url in self.urls:
        #     yield scrapy.Request(url, callback=self.parse,headers = headers)

    # def parse(self, response):
    #     headers = {
    #         'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0',
    #         'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    #         'Accept-Language': 'en-US,en;q=0.5',
    #         'Connection': 'keep-alive',
    #         'Upgrade-Insecure-Requests': '1',
    #         'Cache-Control': 'max-age=0',
    #     }
    #
    #
    #     allurl = response.xpath('//loc//text()').extract()
    #     for u in allurl:
    #         if 'https://247sports.com/Player/' in u:
    #             yield scrapy.Request(u, callback=self.parseInsideProfile, headers=headers)

    def parseInside(self,response):

        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'Cache-Control': 'max-age=0',
        }
        # viewrequrl = ''
        # try:
        viewrequrl1 = response.css('.view-profile-link').xpath('./@href').extract_first()
        if viewrequrl1 is not None:
            # next_page = response.urljoin(viewrequrl1)
            # yield scrapy.Request(next_page, callback=self.parse)
            yield scrapy.Request(viewrequrl1, callback=self.parseInsideProfile, headers=headers)

    #     except:
    #         viewrequrl1 = response.url
    #         yield scrapy.Request(viewrequrl1, callback=self.parseInsideProfile, headers=headers)
    #     # else:
    #     #     viewrequrl = viewrequrl1


    def parseInsideProfile(self,response):

        # urls = response.url
        # yield {
        #     "urlall": urls
        # }
        name = response.xpath('//h1[@class="name"]//text()').extract_first()
        fsnm = name.split()[0]
        lsnm = name.split()[-1]
        pos = response.css('.metrics-list > li:nth-child(1) > span:nth-child(2)').xpath('.//text()').extract_first()
        height = response.css('.metrics-list > li:nth-child(2) > span:nth-child(2)').xpath('.//text()').extract_first()
        weight = response.css('.metrics-list > li:nth-child(3) > span:nth-child(2)').xpath('.//text()').extract_first()
        highscool = ''.join([x.strip('') for x in response.css('.details > li:nth-child(1) > span:nth-child(2)').xpath('.//text()').extract()])
        junschool = ''.join([x.strip('') for x in response.css('.juco > div:nth-child(1) > span:nth-child(2)').xpath('.//text()').extract()])
        hometoendtl = response.css('.details > li:nth-child(2) > span:nth-child(2)').xpath('.//text()').extract_first()
        hometown = hometoendtl.split(',')[0]
        state = hometoendtl.split(',')[-1]
        graclass = response.css('.details > li:nth-child(3) > span:nth-child(2)').xpath('.//text()').extract_first()
        commitedcolg = response.css('.commit-banner-list > li:nth-child(2) > span:nth-child(1)').xpath('.//text()').extract_first()
        commitedcolgclass = response.css('.commit-banner-list > li:nth-child(2) > div:nth-child(2) > span:nth-child(2)').xpath('.//text()').extract_first()
        try:
            comiteclgclass = commitedcolgclass.split('/')[-1]
        except:
            comiteclgclass = ''
        compositeratting = response.css('section.rankings-section:nth-child(2) > div:nth-child(2) > div:nth-child(2)').xpath('.//text()').extract_first()

        plyrtingylow = response.css('section.rankings-section:nth-child(2) > div:nth-child(2)').xpath('.//span[@class="icon-starsolid yellow"]').extract()
        composistars = len(plyrtingylow)
        comNatinRank = response.css('section.rankings-section:nth-child(2) > ul:nth-child(3) > li:nth-child(1) > a:nth-child(2) > strong:nth-child(1)').xpath('.//text()').extract()
        composiranking = response.css('section.rankings-section:nth-child(2) > ul:nth-child(3) > li:nth-child(2) > a:nth-child(2) > strong:nth-child(1)').xpath('.//text()').extract()
        comstateranking = response.css('section.rankings-section:nth-child(2) > ul:nth-child(3) > li:nth-child(3) > a:nth-child(2) > strong:nth-child(1)').xpath('.//text()').extract()
        comalltimranking = response.css('section.rankings-section:nth-child(2) > ul:nth-child(3) > li:nth-child(4) > a:nth-child(2) > strong:nth-child(1)').xpath('.//text()').extract()
        sportsrating = response.css('section.rankings-section:nth-child(3) > div:nth-child(2) > div:nth-child(2)').xpath('.//text()').extract()
        sportstrs = response.css('section.rankings-section:nth-child(3)').xpath('.//span[@class="icon-starsolid yellow"]').extract()
        sportsStars = len(sportstrs)
        sportsnatioranking = response.css('section.rankings-section:nth-child(3) > ul:nth-child(3) > li:nth-child(1) > a:nth-child(2) > strong:nth-child(1)').xpath('.//text()').extract()
        sportposiranking = response.css('section.rankings-section:nth-child(3) > ul:nth-child(3) > li:nth-child(2) > a:nth-child(2) > strong:nth-child(1)').xpath('.//text()').extract()
        sportstateranking = response.css('section.rankings-section:nth-child(3) > ul:nth-child(3) > li:nth-child(3) > a:nth-child(2) > strong:nth-child(1)').xpath('.//text()').extract()
        sportsalltimrankig = ' '
        viewcomplittemlisturl = response.css('.college-comp__view-all').xpath('.//@href').extract_first()

        yield {
            "url": response.url,
            "FirstName": fsnm,
            "LastName": lsnm,
            "Position": pos,
            "Height": height,
            "Weight": weight,
            "HighSchool": highscool,
            "JuniorSchool": junschool,
            "HomeTown": hometown,
            "State": state,
            "GraduationClass": graclass,
            "CommittedCollege": commitedcolg,
            "CommittedCollageClass": comiteclgclass,
            "CompositeRatting": compositeratting,
            "CompositeStars": composistars,
            "CompositeNationalRanking": comNatinRank,
            "CompositePositionalRanking": composiranking,
            "CompositeStateRanking": comstateranking,
            "CompositeAllTimeRanking": comalltimranking,
            "SportsRating": sportsrating,
            "SportsStars": sportsStars,
            "SportsNationalRanking": sportsnatioranking,
            "SportsPositionalRanking": sportposiranking,
            "SportsStateRanking": sportstateranking,
            "SportsAllTimeRanking": sportsalltimrankig,
            "ViewComplittemlistUrl": viewcomplittemlisturl
        }