import scrapy
import csv

class WorkSpider(scrapy.Spider):
    name = 'olmed'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Url","Phone","Price","Name","Ubicacion"


        ]
    };

    urls = [
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne-1297.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Dezynfekcja-1350.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna-1347.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-powiek-2283.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-rak-1351.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-uszu-1348.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Kosmetyczno-higieniczne-1346.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Kosmetyczno-higieniczne-1346.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Pielegnacja-nosa-1352.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki-1349.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Srodki-czystosci-2014.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Chusteczki-do-higieny-intymnej-1603.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Emulsje-do-higieny-intymnej-2183.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Kubeczki-menstruacyjne-2259.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Pianki-do-higieny-intymnej-2050.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Plyny-do-higieny-intymnej-1604.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Plyny-do-higieny-intymnej-1604.html?counter=1","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Plyny-do-higieny-intymnej-1604.html?counter=2",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Podpaski-1605.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Tampony-1606.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Wkladki-1607.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Wkladki-1607.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Majtki-chlonne-2016.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Majtki-chlonne-2016.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Majtki-podtrzymujace-1600.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Pieluchy-dla-doroslych-1598.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna_Pieluchy-dla-doroslych-1598.html?counter=1","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-uszu_Patyczki-higieniczne-1597.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-uszu_Pozostale-7069.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-uszu_Spraye-i-krople-7070.html",
        "https://www.aptekaolmed.pl/product-pol-58934-MoliCare-Skin-Chusteczki-oczyszczajace-50-sztuk.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Kosmetyczno-higieniczne_Chusteczki-nawilzone-2032.html",
        "https://www.aptekaolmed.pl/product-pol-80494-CLEANIC-Intimate-Papier-toaletowy-nawilzany-60-szt.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Kosmetyczno-higieniczne_Platki-i-waciki-higieniczne-1984.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Kosmetyczno-higieniczne_Pozostale-2015.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Kosmetyczno-higieniczne_Pudry-zasypki-1601.html",
        "https://www.aptekaolmed.pl/product-pol-19542-TEMBLAK-Rozmiar-M-1-sztuka.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Gazy-1614.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Halluksy-1620.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Pozostale-2017.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Kompresy-1613.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Odciski-i-pecherze-1619.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Opaski-dziane-1612.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Opaski-elastyczne-1609.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Opaski-elastyczne-1609.html?counter=1","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Opaski-elastyczne-1609.html?counter=2",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Opaski-uciskowe-2262.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Opatrunki-1616.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Opatrunki-1616.html?counter=1","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Opatrunki-1616.html?counter=2",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Opryszczka-1617.html","https://www.aptekaolmed.pl/product-pol-80615-GEHWOL-Poduszka-ochronna-pod-piete-srednia-2-sztuki-KROTKA-DATA-31-03-2020.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Pasy-brzuszne-2261.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Plastry-1610.html",
        "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Plastry-1610.html?counter=1","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Plastry-1610.html?counter=2",
        "https://www.aptekaolmed.pl/product-pol-80615-GEHWOL-Poduszka-ochronna-pod-piete-srednia-2-sztuki-KROTKA-DATA-31-03-2020.html","https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki_Przylepce-1615.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty-1294.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Alergia-i-katar-sienny-1309.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Alergia-i-katar-sienny-1309.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-diabetykow-1310.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-diabetykow-1310.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-kobiet-1311.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-kobiet-1311.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-kobiet-1311.html?counter=2","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-kobiet-1311.html?counter=3",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-mezczyzn-1312.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-mezczyzn-1312.html?counter=1","https://www.aptekaolmed.pl/pol_n_Bez-recepty_Dla-planujacych-rodzine-1957.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Drogi-moczowo-plciowe-1314.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Drogi-moczowo-plciowe-1314.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Drogi-moczowo-plciowe-1314.html?counter=2",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Drogi-moczowo-plciowe-1314.html?counter=3","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia-1329.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia-1329.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia-1329.html?counter=2","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Oczyszczanie-organizmu-1334.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Oczyszczanie-organizmu-1334.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Pamiec-i-koncentracja-1319.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Pamiec-i-koncentracja-1319.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Pamiec-i-koncentracja-1319.html?counter=2",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Pamiec-i-koncentracja-1319.html?counter=3","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Preparaty-przeciwbolowe-1316.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne-1320.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa-1321.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Rzuc-palenie-1322.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Seks-i-antykoncepcja-1323.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Seks-i-antykoncepcja-1323.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Seks-i-antykoncepcja-1323.html?counter=2","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Skora-wlosy-paznokcie-1331.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Stawy-miesnie-kosci-1324.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Uklad-krazenia-1325.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_uklad-nerwowy-1313.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_uklad-oddechowy-1333.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Uklad-pokarmowy-1326.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Witaminy-i-mikroelementy-1327.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Zdrowa-zywnosc-1330.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Zdrowe-oczy-1328.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Zdrowe-uszy-2037.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Ziola-1315.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Zywienie-medyczne-1332.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Zywienie-medyczne-1332.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Miody-2072.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Alergia-i-katar-sienny_Krople-aerozol-sztyft-1386.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Alergia-i-katar-sienny_Syrop-1389.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Alergia-i-katar-sienny_Tabletki-1387.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Alergia-i-katar-sienny_Wapno-1388.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-diabetykow_Glukoza-i-fruktoza-1394.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-diabetykow_Preparaty-regulujace-poziom-cukru-1390.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-diabetykow_Slodziki-1395.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-diabetykow_Witaminy-mikroelementy-1392.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-kobiet_Aktywnosc-i-prawidlowa-masa-ciala-2006.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-kobiet_Kobiece-infekcje-1400.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-kobiet_Menopauza-1396.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-kobiet_Menopauza-1396.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-kobiet_Napiecie-przedmiesiaczkowe-1399.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-kobiet_Pozostale-1398.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-kobiet_Probiotyki-1397.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-mezczyzn_Pozostale-1403.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-mezczyzn_Prostata-1401.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-mezczyzn_Wspomaganie-libido-1402.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-planujacych-rodzine_Dla-niej-1958.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Dla-planujacych-rodzine_Dla-niego-1959.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Drogi-moczowo-plciowe_Dolegliwosci-kobiece-1412.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Drogi-moczowo-plciowe_Dolegliwosci-kobiece-1412.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Drogi-moczowo-plciowe_Infekcje-drog-moczowych-1411.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Drogi-moczowo-plciowe_Infekcje-drog-moczowych-1411.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Drogi-moczowo-plciowe_Kamica-1413.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Drogi-moczowo-plciowe_Nietrzymanie-moczu-1414.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia_Bol-gardla-1530.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia_Hemoroidy-1532.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia_Infekcje-drog-moczowych-kamica-1533.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia_Kaszel-1531.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia_Katar-1534.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia_Problemy-kobiece-1535.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia_Przeziebienie-i-grypa-1536.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia_Przeziebienie-i-grypa-1536.html?counter=1",
        "https://www.aptekaolmed.pl/product-pol-76230-BOIRON-Hepar-Sulfur-9-CH-granulki-4-g-ok-80-granulek.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia_Uspokajajace-1538.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia_Zawroty-glowy-1539.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia_Zdrowa-watroba-1529.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Homeopatia_Zdrowe-oczy-1540.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Oczyszczanie-organizmu_Plastry-1546.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Oczyszczanie-organizmu_Plyny-zawiesiny-syropy-2282.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Oczyszczanie-organizmu_Tabletki-kapsulki-2038.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Odchudzanie_Dieta-1430.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Odchudzanie_Oczyszczanie-1431.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Odchudzanie_Usuwanie-wody-2023.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Odchudzanie_Wspomaganie-odchudzania-1429.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Odpornosc-organizmu_Echinacea-czosnek-aloes-1435.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Odpornosc-organizmu_Kwasy-omega-trany-oleje-1433.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Odpornosc-organizmu_Pozostale-1434.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Odpornosc-organizmu_Propolis-miod-1436.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Odpornosc-organizmu_Witamina-C-1432.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Pamiec-i-koncentracja_Preparaty-wspomagajace-nauke-1438.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Pamiec-i-koncentracja_Preparaty-wspomagajace-nauke-1438.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Pamiec-i-koncentracja_Z-lecytyna-milorzebem-1437.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Pamiec-i-koncentracja_Z-lecytyna-milorzebem-1437.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Preparaty-przeciwbolowe_Czopki-globulki-1425.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Preparaty-przeciwbolowe_Masci-kremy-i-zele-1423.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Preparaty-przeciwbolowe_Masci-kremy-i-zele-1423.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Preparaty-przeciwbolowe_Masci-kremy-i-zele-1423.html?counter=2",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Preparaty-przeciwbolowe_Spraye-1421.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Preparaty-przeciwbolowe_Syropy-1427.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Preparaty-przeciwbolowe_Tabletki-proszki-1419.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Preparaty-przeciwbolowe_Zespoly-bolowe-kregoslupa-nerwobole-1428.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Preparaty-przeciwbolowe_Plastry-przeciwbolowe-7071.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Atopowe-zapalenie-skory-1456.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Atopowe-zapalenie-skory-1456.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Blizny-1452.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Brodawki-kurzajki-1461.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Swierzb-i-grzybica-1440.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Swierzb-i-grzybica-1440.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Lupiez-1446.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Luszczyca-1443.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Lagodzenie-i-ochrona-przed-owadami-1449.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Lagodzenie-i-ochrona-przed-owadami-1449.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Odciski-1448.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Odlezyny-1445.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Odparzenia-1458.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Oparzenia-powierzchniowe-1459.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Oparzenia-sloneczne-1442.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Opryszczka-1454.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Owrzodzenia-1455.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Pozostale-1453.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Rany-i-podraznienia-1441.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Rany-i-podraznienia-1441.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Rany-i-podraznienia-1441.html?counter=2","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Regulatory-potliwosci-1457.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Skora-sucha-1460.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Tradzik-1439.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Tradzik-1439.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Wszawica-1444.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Wszawica-1444.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Wypadanie-wlosow-1451.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne_Zajady-1447.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Bol-gardla-1463.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Bol-gardla-1463.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Bol-gardla-1463.html?counter=2","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Bol-gardla-1463.html?counter=3",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Kaszel-1466.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Kaszel-1466.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Kaszel-1466.html?counter=2",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Katar-zatoki-1465.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Katar-zatoki-1465.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Katar-zatoki-1465.html?counter=2","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Preparaty-wspomagajace-1467.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Preparaty-wspomagajace-1467.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Preparaty-wspomagajace-1467.html?counter=2","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Przeziebienie-goraczka-1462.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Przeziebienie-goraczka-1462.html?counter=1","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Przeziebienie-goraczka-1462.html?counter=2","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa_Zapalenie-ucha-1464.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Rzuc-palenie_Gumy-do-zucia-1468.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Rzuc-palenie_Plastry-1469.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Rzuc-palenie_Spraye-1471.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Rzuc-palenie_Tabletki-1470.html",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Seks-i-antykoncepcja_Pozostale-1476.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Seks-i-antykoncepcja_Preparaty-nawilzajace-1474.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Seks-i-antykoncepcja_Prezerwatywy-1473.html","https://www.aptekaolmed.pl/pol_m_Bez-recepty_Seks-i-antykoncepcja_Prezerwatywy-1473.html?counter=1",
        "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Seks-i-antykoncepcja_Gadzety-2007.html",""





    ]

    i = 1
    while i < 25:
        ur = "https://www.aptekaolmed.pl/pol_m_Art-higieniczne-1297.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 8:
        ur = "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Higiena-intymna-1347.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 12:
        ur = "https://www.aptekaolmed.pl/pol_m_Art-higieniczne_Plastry-opatrunki-1349.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 197:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty-1294.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 9:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Preparaty-przeciwbolowe-1316.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 15:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Problemy-skorne-1320.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 14:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Przeziebienie-i-grypa-1321.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 7:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Skora-wlosy-paznokcie-1331.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 14:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Stawy-miesnie-kosci-1324.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 11:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Uklad-krazenia-1325.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 10:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_uklad-nerwowy-1313.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 4:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_uklad-oddechowy-1333.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 22:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Uklad-pokarmowy-1326.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 30:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Witaminy-i-mikroelementy-1327.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 11:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Zdrowa-zywnosc-1330.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 6:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Zdrowe-oczy-1328.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 11:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Ziola-1315.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 3:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Odchudzanie_Wspomaganie-odchudzania-1429.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 3:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Odpornosc-organizmu_Kwasy-omega-trany-oleje-1433.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 9:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Odpornosc-organizmu_Pozostale-1434.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 5:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Odpornosc-organizmu_Witamina-C-1432.html?counter=" + str(i)
        urls.append(ur)
        i += 1
    i = 1
    while i < 5:
        ur = "https://www.aptekaolmed.pl/pol_m_Bez-recepty_Preparaty-przeciwbolowe_Tabletki-proszki-1419.html?counter=" + str(i)
        urls.append(ur)
        i += 1


    def start_requests(self):


        i=0
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,meta={'Index':i}, dont_filter=True)
            i+=1

    def parse(self, response):

        # pagedetailurl = response.xpath('')
        # response.xpath(
        #     '//div[@class="left-column central"]/div[@id="tileRedesign"]//div[@class="tile-desc one-liner"]/a[@class="href-link tile-title-text"]/@href').extract()

        details = response.xpath('//div[@class="left-column central"]/div[@id="tileRedesign"]//div[@class="tile-desc one-liner"]/a[@class="href-link tile-title-text"]')
        links = details.xpath('.//@href').extract()

        # print(links)
        # print links
        for link in links:
            l = 'https://www.vivanuncios.com.mx' + link
            yield scrapy.Request(url=l, callback=self.parseInside, headers={'User-Agent': 'Mozilla Firefox 12.0'})
        # print l ,dont_filter=True
    def parseInside(self,response):

        # drugname=  response.xpath('//div[@class="col-md-12 title"]/h1/text()').extract()
        t=response.xpath('//script[@type="application/ld+json"]').extract()
        t = ''.join(t)
        phone=t.split('"telephone":')[1].split('"}')[0]

        price=''.join([x.strip() for x in response.xpath('//h3/span/span[@class="value"]/span[@class="ad-price"]/text()').extract()]).split()[0]

        q = response.xpath('//script').extract()
        q = ''.join(q)
        name=q.split('"sellerName":"')[1].split('",')[0]
        ubicacion=q.split('"geoLocDisplayName":"')[1].split('",')[0]


        yield {
            "Url" : response.url,


        }