import pandas as pd
import scrapy
import json
import csv
import re
from csv import reader

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
    # 'Upgrade-Insecure-Requests': '1',
}

class workspider(scrapy.Spider):
    name = "nhssearch"

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': ['url','location','occupation','sector','Main area','Grade','Contract',
            'Hours','Job ref','Employer','Employer type','Site','Town','Salary','Salary period',
            'Closing','Contact Name','Job title','Email','Phone','Alltels','Ext contact','Email in description1',
            'Email in description2','Email in description3','Email in description4','Email in description5'
                               ]
    };

    def start_requests(self):
        locvals = []
        loctxts = []
        ocuvals = []
        ocutxts = []
        sectors = []
        with open('nhssearchlocation.csv', 'r', encoding="utf8",errors='ignore') as read_obj:
            csv_reader = reader(read_obj)
            for row in csv_reader:
                locval = row[0]
                loctxt = row[1]
                locvals.append(locval)
                loctxts.append(loctxt)
        with open('nhssearchvalues.csv', 'r', encoding="utf8",errors='ignore') as read_obj1:
            csv_reader = reader(read_obj1)
            for row in csv_reader:
                ocuval = row[0]
                ocutxt = row[1]
                sector = row[2]
                ocuvals.append(ocuval)
                ocutxts.append(ocutxt)
                sectors.append(sector)

        for i in range(1, len(locvals)):
            for j in range(1,len(ocuvals)):
                ur = 'https://www.nhsjobs.com/job_list?JobSearch_q=&JobSearch_d='+str(ocuvals[j])+'&JobSearch_g=&JobSearch_re=_POST&JobSearch_re_0=1&JobSearch_re_1='+str(locvals[i])+'&JobSearch_re_2='+str(locvals[i])+'-_-_-&JobSearch_Submit=Search&_tr=JobSearch'
                yield scrapy.Request(url=ur, callback=self.parse_inside, dont_filter=True,headers=headers,
                                     meta={'loctxt':loctxts[i],'occutxt':ocutxts[j],'sector':sectors[j]}
                                     )

    def parse_inside(self,response):
        url = response.url
        loctxt = response.meta['loctxt']
        ocutxt = response.meta['occutxt']
        sector = response.meta['sector']

        joblinks = response.xpath('//a[@class="clearfix"]//@href').extract()
        for jb in joblinks:
            jblink = 'https://www.nhsjobs.com'+jb
            yield scrapy.Request(url=jblink, callback=self.parse_inside_job, dont_filter=True, headers=headers,
                                 meta={'loctxt': loctxt, 'occutxt': ocutxt,'sector':sector}
                                 )

    def parse_inside_job(self, response):
        url = response.url
        loctxt = response.meta['loctxt']
        ocutxt = response.meta['occutxt']
        sector = response.meta['sector']

        jbsumlb = response.xpath('//dl[@class="hj-dl-float front-end-job-summary"]//dt//text()').extract()
        jbsumtxt = response.xpath('//dl[@class="hj-dl-float front-end-job-summary"]//dd//text()').extract()

        p=0
        mainarea = ''
        grade = ''
        contract = ''
        hours = ''
        jobref = ''
        employer = ''
        employertype = ''
        sitec = ''
        town = ''
        salary = ''
        salaryperiod = ''
        closing = ''
        interviewdate = ''
        for jblb in jbsumlb:
            if 'Main area' in jblb:
                mainarea = jbsumtxt[p]
            if 'Grade' in jblb:
                grade = jbsumtxt[p]
            if 'Contract' in jblb:
                contract = jbsumtxt[p]
            if 'Hours' in jblb:
                hours = jbsumtxt[p]
            if 'Job ref' in jblb:
                jobref = jbsumtxt[p]
            if jblb == 'Employer':
                employer = jbsumtxt[p]
            if jblb == 'Employer type':
                employertype = jbsumtxt[p]
            if 'Site' in jblb:
                sitec = jbsumtxt[p]
            if 'Town' in jblb:
                town = jbsumtxt[p]
            if jblb == 'Salary':
                salary = jbsumtxt[p]
            if jblb == 'Salary period':
                salaryperiod = jbsumtxt[p]
            if 'Closing' in jblb:
                closing = jbsumtxt[p]
            if 'Interview date' in jblb:
                interviewdate = jbsumtxt[p]
            p+=1

        furinfolb = response.xpath('//section[@id="hj-enquiry-html"]//dt//text()').extract()
        furinfotxt = response.xpath('//section[@id="hj-enquiry-html"]//dd//text()').extract()
        name = ''
        jobtitle = ''
        emailadress = ''
        telephone = ''
        k = 0
        for finf in furinfolb:
            if 'Name' in finf:
                name = furinfotxt[k]
            if 'Job title' in finf:
                jobtitle = furinfotxt[k]
            if 'Email address' in finf:
                emailadress = furinfotxt[k]
            if 'Telephone number' in finf:
                telephone = '0'+furinfotxt[k]
            k+=1
        exttele = ''
        if 'x' in telephone:
            telephone = telephone.split('x')[0].replace('E','').replace('e','')
            exttele = telephone.split('x')[-1].replace('tension ','').replace('t','')
        des = ' '.join(response.xpath('//text()').extract())
        emails = re.findall(r"[a-zA-Z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", des)
        emails = [email for email in emails if email not in emailadress]

        destel = ' '.join(response.xpath('//section[@id="hj-enquiry-html"]//text()').extract())
        try:
            tels = re.findall(r"\d{5}\s?\d{6}", destel)
        except:
            tels = re.findall(r"\d{11}\s", destel.strip())

        # alltels = [tells for tells in tels if tells not in telephone]
        try:
            email1 = emails[0]
        except:
            email1 = ''
        try:
            email2 = emails[1]
        except:
            email2 = ''
        try:
            email3 = emails[2]
        except:
            email3 = ''
        try:
            email4 = emails[3]
        except:
            email4 = ''
        try:
            email5 = emails[4]
        except:
            email5 = ''
        # sector = response.css('ol.breadcrumbs:nth-child(1) > li:nth-child(2) > a').xpath('.//text').extract_first()
        yield {
            'url' : url,
            'location': loctxt,
            'occupation': ocutxt,
            'sector':sector,
            'Main area': mainarea,
            'Grade': grade,
            'Contract': contract,
            'Hours': hours,
            'Job ref': jobref,
            'Employer': employer,
            'Employer type'	:employertype,
            'Site': sitec,
            'Town' : town,
            'Salary': salary,
            'Salary period': salaryperiod,
            'Closing' : closing,
            'Contact Name': name,
            'Job title': jobtitle,
            'Email'	: emailadress,
            'Phone': telephone,
            'Alltels':tels,
            'Ext contact': exttele,
            'Email in description1': email1,
            'Email in description2': email2,
            'Email in description3': email3,
            'Email in description4': email4,
            'Email in description5': email5
        }