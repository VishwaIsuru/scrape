import scrapy
import csv
import pandas as pd
import requests
import json
import csv

csvRow = ['Link','Name','Bio','Areas of Expertise','Ballot Measures','Campaign Management','Campaign Technology/Digital & Data','Communications',
         'Corporate & Trade Association Management','Database and File Management','Direct Mail',
         'Fundraising','General Consulting','Grassroots Organizing','Legal','Media Buying and Placement',
         'Media Consulting','Polling/Research','Press, Public and Media Relations','Public Affairs',
         'Speech Writing','Telephone Voter Contact','Party Affiliation','States of Operation','Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia',
          'Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','International','Iowa','Kansas','Kentucky',
          'Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana',
          'Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota',
          'Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee',
          'Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming','Client list','Contact Name',
          'Contact email','LinkedIn','Facebook','Instagram','Twitter','Youtube']

with open('TheaapcData.csv', "w") as fp:
    wr = csv.writer(fp, dialect='excel')
    wr.writerow(csvRow)

class workspider(scrapy.Spider):
    name = "theaapa"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Url",
                ]
    };

    def start_requests(self):
        col_list = ["biolink"]
        df = pd.read_csv("Theaapalinks.csv", usecols=col_list)
        urls = df["biolink"]
        print(len(urls))

        for i in range(0, len(urls)):

            biourl = urls[i]

            yield scrapy.Request(url=biourl, callback=self.parse,dont_filter=True)

    def parse(self, response):
        name =  response.xpath('//span[@id="MainCopy_ctl26_lblName"]//text()').extract_first()
        bio = response.xpath('//span[@id="MainCopy_ctl36_Label1"]//text()').extract_first()
        reexprt = ','.join([x.strip('') for x in response.xpath('//div[@id="MainCopy_ctl41_pnlCommaSeparated"]//li//text()').extract()])

        experties = ['Ballot Measures','Campaign Management','Campaign Technology/Digital & Data','Communications',
                     'Corporate & Trade Association Management','Database and File Management','Direct Mail',
                     'Fundraising','General Consulting','Grassroots Organizing','Legal','Media Buying and Placement',
                     'Media Consulting','Polling/Research','Press, Public and Media Relations','Public Affairs',
                     'Speech Writing','Telephone Voter Contact']

        exprt_data = [""]*18
        for exp in reexprt.split(','):
            i=0
            for ex in experties:
                if ex == exp:
                    exprt_data[i] = 'Yes'
                i+=1

        partyaflition = response.xpath('//ul[@id="MainCopy_ctl43_ulDemographicItems"]//li//text()').extract_first()
        stofopert = response.css('#MainCopy_ctl45_pnlCommaSeparated > div:nth-child(1)').xpath('.//text()').extract_first()
        stofopertion = ''
        if stofopert is not None:
            stofopertion = stofopert.replace('\r\n                                    ','').replace('\r\n                                    \r\n                                ','').replace('\r\n                                ','')

        states = ['Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia',
                  'Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','International','Iowa','Kansas','Kentucky',
                  'Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana',
                  'Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota',
                  'Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee',
                  'Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming']

        state_data = [""] * 52
        for st in stofopertion.split(','):
            i = 0
            for stt in states:
                if stt == st:
                    state_data[i] = 'Yes'
                i += 1
        clientlistNn = response.css('#MainCopy_ctl47_pnlCommaSeparated > div:nth-child(1)').xpath('.//text()').extract_first()
        if clientlistNn is not None:
            clientlist = clientlistNn.replace('\r','').replace('\n','').replace('  ','')
        if clientlistNn is None:
            clientlist = ''
        contatnameNn = response.css('#MainCopy_ctl15_presentJob_CompanyNamePanel').xpath('.//text()').extract_first()
        if contatnameNn is not None:
            contatname = contatnameNn.replace('\r','').replace('\n','').replace('\t','').replace('  ','')
        if contatnameNn is None:
            contatname = ''
        email = response.css('#MainCopy_ctl15_presentJob_EmailAddress').xpath('.//text()').extract_first()
        sclmde = response.xpath('//div[@id="SocialMediaList"]//a/@href').extract()
        linkin = []
        fb = []
        inster = []
        twitr = []
        ytube = []
        for scl in sclmde:
            if 'https://www.linkedin.com/' in scl:
                linkin.append(scl)
            if 'https://www.facebook.com/' in scl:
                fb.append(scl)
            if 'https://www.instagram.com/' in scl:
                inster.append(scl)
            if 'https://twitter.com/' in scl:
                twitr.append(scl)
            if 'https://www.youtube.com/' in scl:
                ytube.append(scl)
        d = ''
        if bio is not None:
            d= bio.split(' ')
        for txtbio in d:
            if 'https://www.linkedin.com/' in txtbio:
                linkin.append(txtbio)
            if 'https://www.facebook.com/' in txtbio:
                fb.append(txtbio)
            if 'https://www.instagram.com/' in txtbio:
                inster.append(txtbio)
            if 'https://twitter.com/' in txtbio:
                twitr.append(txtbio)
            if 'https://www.youtube.com/' in txtbio:
                ytube.append(txtbio)

        linkedin = '\n'.join(linkin)
        facebook = '\n'.join(fb)
        instagram = '\n'.join(inster)
        twitter = '\n'.join(twitr)
        youtube = '\n'.join(ytube)

        csvfilnm1 = 'TheaapcData.csv'
        # 'Title', 'Breadcrumb', 'Image', 'Power', 'Light Colour', 'Downloadable file link'
        csvRow = ([response.url, name, bio, reexprt] + exprt_data+[partyaflition,stofopertion]+state_data+[clientlist,contatname,email,linkedin,facebook,instagram,twitter,youtube])
        with open(csvfilnm1, "a") as fp:
            wr = csv.writer(fp, dialect='excel')
            wr.writerow(csvRow)

        yield {
            'name': name,
            'reexp': reexprt,
            'paryafli': partyaflition,
            'stateopert': stofopertion,
            'clientlist': clientlist,
            'contactName': contatname
        }