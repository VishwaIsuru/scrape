# import scrapy
# import csv
# import json
# import sys
#
# # reload(sys)
# # sys.setdefaultencoding('utf8')
#
# import csv
#
# csv.register_dialect('myDialect', delimiter='/', quoting=csv.QUOTE_NONE)
# import pymysql.cursors
#
# csv.register_dialect('myDialect', delimiter='/', quoting=csv.QUOTE_NONE)
# connection = pymysql.connect(host='localhost',
#                              user='root',
#                              password='root',port=3306,
#                              db='scrape', use_unicode=True, charset="utf8")
#
#
# class WorkSpider(scrapy.Spider):
#     name = 'raredepth'
#
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': [
#             "ID","depth","measurement","certificateId"
#
#         ]
#     };
#
#     def start_requests(self):
#
#         with connection.cursor() as cursor:
#             sql = "SELECT id,depthurl,measurements,depth FROM RareDiamond"
#             cursor.execute(sql)
#             allItems = cursor.fetchall()
#             connection.commit()
#
#         for d in allItems:
#             headers = {'Connection': 'keep-alive', 'Host': 'webapi.rarecarat.com',
#                        'TE': 'Trailers',
#                        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0'}
#             if d[1] is not None:
#                 yield scrapy.Request(d[1], callback=self.parse,headers=headers,
#                                  meta={'ID': d[0]})
#
#     def parse(self, response):
#
#         id = int(response.meta['ID'])
#         d = response.body
#         j = json.loads(d)
#         measurements = j['measurements']
#         depth = j['depthPercentage']
#         try:
#             certificateId = j['certificateId']
#         except:
#             certificateId = ''
#
#         yield {
#             "ID": id,
#             "depth": depth,
#             "measurement": measurements,
#             "certificateId": certificateId
#         }
#
#         with connection.cursor() as cursor:
#             # print("DONE")
#             sql = "UPDATE RareDiamond SET measurements=%s,depth=%s,certificateId=%s WHERE id=%s"
#             cursor.execute(sql,(measurements,depth,certificateId,id))
#             connection.commit()
#
