import scrapy
import csv

class workspider(scrapy.Spider):
    name = "realtor"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'Url','Person Name','Broker Name','Office Phone Number','Personal Phone Number','Fax Number',
            'Address','Website 1','Website 2','For sale','Sold','Experience'

            # 'url','state','realtors','pages'
                ]
    };
    urls = [
        #################  set 11111111111111111 #####################
        # 'https://www.realtor.com/realestateagents/new-york_ny/',
        'https://www.realtor.com/realestateagents/los-angeles_ca/',
        # 'https://www.realtor.com/realestateagents/chicago_il/','https://www.realtor.com/realestateagents/houston_tx/',
        # 'https://www.realtor.com/realestateagents/phoenix_az/','https://www.realtor.com/realestateagents/philadelphia_pa/',
        # 'https://www.realtor.com/realestateagents/san-antonio_tx/','https://www.realtor.com/realestateagents/san-diego_ca/',
        # 'https://www.realtor.com/realestateagents/dallas_tx/','https://www.realtor.com/realestateagents/san-jose_ca/',
        # 'https://www.realtor.com/realestateagents/austin_tx/','https://www.realtor.com/realestateagents/jacksonville_fl/',
        # 'https://www.realtor.com/realestateagents/fort-worth_tx/','https://www.realtor.com/realestateagents/columbus_oh/',
        # 'https://www.realtor.com/realestateagents/indianapolis_in/','https://www.realtor.com/realestateagents/carolina_pr/',
        # 'https://www.realtor.com/realestateagents/san-francisco_ca/','https://www.realtor.com/realestateagents/seattle_wa/',
        # 'https://www.realtor.com/realestateagents/denver_co/','https://www.realtor.com/realestateagents/oklahoma-city_ok/',
        # 'https://www.realtor.com/realestateagents/nashville_tn/','https://www.realtor.com/realestateagents/el-paso_tx/',
        # 'https://www.realtor.com/realestateagents/washington_dc/','https://www.realtor.com/realestateagents/boston_ma/',
        # 'https://www.realtor.com/realestateagents/las-vegas_nv/','https://www.realtor.com/realestateagents/portland_or/',
        # 'https://www.realtor.com/realestateagents/detroit_mi/','https://www.realtor.com/realestateagents/louisville_ky/',
        # 'https://www.realtor.com/realestateagents/memphis_tn/','https://www.realtor.com/realestateagents/baltimore_md/',
        # 'https://www.realtor.com/realestateagents/milwaukee_wi/','https://www.realtor.com/realestateagents/albuquerque_nm/',
        # 'https://www.realtor.com/realestateagents/fresno_ca/','https://www.realtor.com/realestateagents/tucson_az/',
        # 'https://www.realtor.com/realestateagents/sacramento_ca/','https://www.realtor.com/realestateagents/mesa_az/',
        # 'https://www.realtor.com/realestateagents/kansas-city_mo/','https://www.realtor.com/realestateagents/atlanta_ga/',
        # 'https://www.realtor.com/realestateagents/omaha_ne/'
        #
        # ############ set 222222222222 ######
        # 'https://www.realtor.com/realestateagents/colorado-springs_co/','https://www.realtor.com/realestateagents/raleigh_nc/',
        # 'https://www.realtor.com/realestateagents/virginia-beach_va/','https://www.realtor.com/realestateagents/long-beach_ca/',
        # 'https://www.realtor.com/realestateagents/miami_fl/','https://www.realtor.com/realestateagents/oakland_ca/',
        # 'https://www.realtor.com/realestateagents/minneapolis_mn/','https://www.realtor.com/realestateagents/tulsa_ok/',
        # 'https://www.realtor.com/realestateagents/bakersfield_ca/','https://www.realtor.com/realestateagents/wichita_ks/',
        # 'https://www.realtor.com/realestateagents/arlington_tx/','https://www.realtor.com/realestateagents/aurora_co/',
        # 'https://www.realtor.com/realestateagents/tampa_fl/','https://www.realtor.com/realestateagents/new-orleans_la/',
        # 'https://www.realtor.com/realestateagents/cleveland_oh/','https://www.realtor.com/realestateagents/anaheim_ca/',
        # 'https://www.realtor.com/realestateagents/honolulu_hi/','https://www.realtor.com/realestateagents/henderson_nv/',
        # 'https://www.realtor.com/realestateagents/stockton_ca/','https://www.realtor.com/realestateagents/lexington_ky/',
        # 'https://www.realtor.com/realestateagents/corpus-christi_tx/','https://www.realtor.com/realestateagents/riverside_ca/',
        # 'https://www.realtor.com/realestateagents/santa-ana_ca/','https://www.realtor.com/realestateagents/orlando_fl/',
        # 'https://www.realtor.com/realestateagents/irvine_ca/','https://www.realtor.com/realestateagents/cincinnati_oh/',
        # 'https://www.realtor.com/realestateagents/newark_nj/','https://www.realtor.com/realestateagents/st.-paul_mn/',
        # 'https://www.realtor.com/realestateagents/pittsburgh_pa/','https://www.realtor.com/realestateagents/greensboro_nc/',
        # 'https://www.realtor.com/realestateagents/st.-louis_mo/','https://www.realtor.com/realestateagents/lincoln_ne/',
        # 'https://www.realtor.com/realestateagents/plano_tx/','https://www.realtor.com/realestateagents/anchorage_ak/',
        # 'https://www.realtor.com/realestateagents/durham_nc/','https://www.realtor.com/realestateagents/jersey-city_nj/',
        # 'https://www.realtor.com/realestateagents/chandler_az/','https://www.realtor.com/realestateagents/chula-vista_ca/',
        # 'https://www.realtor.com/realestateagents/buffalo_ny/','https://www.realtor.com/realestateagents/north-las-vegas_nv/',
        # 'https://www.realtor.com/realestateagents/gilbert_az/','https://www.realtor.com/realestateagents/madison_wi/',
        # 'https://www.realtor.com/realestateagents/reno_nv/','https://www.realtor.com/realestateagents/toledo_oh/',
        # 'https://www.realtor.com/realestateagents/fort-wayne_in/','https://www.realtor.com/realestateagents/lubbock_tx/',
        # 'https://www.realtor.com/realestateagents/st.-petersburg_fl/','https://www.realtor.com/realestateagents/laredo_tx/',
        # 'https://www.realtor.com/realestateagents/irving_tx/','https://www.realtor.com/realestateagents/chesapeake_va/',
        # 'https://www.realtor.com/realestateagents/winston-salem_nc/','https://www.realtor.com/realestateagents/glendale_az/',
        # 'https://www.realtor.com/realestateagents/scottsdale_az/','https://www.realtor.com/realestateagents/garland_tx/',
        # 'https://www.realtor.com/realestateagents/boise_id/','https://www.realtor.com/realestateagents/norfolk_va/',
        # 'https://www.realtor.com/realestateagents/spokane_wa/','https://www.realtor.com/realestateagents/fremont_ca/',
        # 'https://www.realtor.com/realestateagents/richmond_va/','https://www.realtor.com/realestateagents/santa-clarita_ca/',
        # 'https://www.realtor.com/realestateagents/san-bernardino_ca/','https://www.realtor.com/realestateagents/baton-rouge_la/',
        # 'https://www.realtor.com/realestateagents/hialeah_fl/','https://www.realtor.com/realestateagents/tacoma_wa/',
        # 'https://www.realtor.com/realestateagents/modesto_ca/','https://www.realtor.com/realestateagents/port-st.-lucie_fl/',
        # 'https://www.realtor.com/realestateagents/huntsville_al/','https://www.realtor.com/realestateagents/des-moines_ia/',
        # 'https://www.realtor.com/realestateagents/moreno-valley_ca/','https://www.realtor.com/realestateagents/fontana_ca/',
        # 'https://www.realtor.com/realestateagents/frisco_tx/','https://www.realtor.com/realestateagents/rochester_ny/',
        # 'https://www.realtor.com/realestateagents/yonkers_ny/','https://www.realtor.com/realestateagents/fayetteville_nc/',
        # 'https://www.realtor.com/realestateagents/worcester_ma/','https://www.realtor.com/realestateagents/columbus_ga/',
        # 'https://www.realtor.com/realestateagents/cape-coral_fl/','https://www.realtor.com/realestateagents/mckinney_tx/',
        # 'https://www.realtor.com/realestateagents/little-rock_ar/' ##119 csv point
        #
        # ################ 3 rd set #####################
        # 'https://www.realtor.com/realestateagents/oxnard_ca/','https://www.realtor.com/realestateagents/amarillo_tx/',
        # 'https://www.realtor.com/realestateagents/augusta_ga/','https://www.realtor.com/realestateagents/salt-lake-city_ut/',
        # 'https://www.realtor.com/realestateagents/montgomery_al/','https://www.realtor.com/realestateagents/birmingham_al/',
        # 'https://www.realtor.com/realestateagents/grand-rapids_mi/','https://www.realtor.com/realestateagents/grand-prairie_tx/',
        # 'https://www.realtor.com/realestateagents/overland-park_ks/','https://www.realtor.com/realestateagents/tallahassee_fl/',
        # 'https://www.realtor.com/realestateagents/huntington-beach_ca/','https://www.realtor.com/realestateagents/sioux-falls_sd/',
        # 'https://www.realtor.com/realestateagents/peoria_az/','https://www.realtor.com/realestateagents/knoxville_tn/',
        # 'https://www.realtor.com/realestateagents/glendale_ca/','https://www.realtor.com/realestateagents/vancouver_wa/',
        # 'https://www.realtor.com/realestateagents/providence_ri/','https://www.realtor.com/realestateagents/akron_oh/',
        # 'https://www.realtor.com/realestateagents/brownsville_tx/','https://www.realtor.com/realestateagents/mobile_al/',
        # 'https://www.realtor.com/realestateagents/newport-news_va/','https://www.realtor.com/realestateagents/tempe_az/',
        # 'https://www.realtor.com/realestateagents/shreveport_la/','https://www.realtor.com/realestateagents/chattanooga_tn/',
        # 'https://www.realtor.com/realestateagents/fort-lauderdale_fl/','https://www.realtor.com/realestateagents/aurora_il/',
        # 'https://www.realtor.com/realestateagents/elk-grove_ca/','https://www.realtor.com/realestateagents/ontario_ca/',
        # 'https://www.realtor.com/realestateagents/salem_or/','https://www.realtor.com/realestateagents/cary_nc/',
        # 'https://www.realtor.com/realestateagents/santa-rosa_ca/','https://www.realtor.com/realestateagents/rancho-cucamonga_ca/',
        # 'https://www.realtor.com/realestateagents/eugene_or/','https://www.realtor.com/realestateagents/oceanside_ca/',
        # 'https://www.realtor.com/realestateagents/clarksville_tn/','https://www.realtor.com/realestateagents/garden-grove_ca/',
        # 'https://www.realtor.com/realestateagents/lancaster_ca/','https://www.realtor.com/realestateagents/springfield_mo/',
        # 'https://www.realtor.com/realestateagents/pembroke-pines_fl/','https://www.realtor.com/realestateagents/fort-collins_co/',
        # 'https://www.realtor.com/realestateagents/palmdale_ca/','https://www.realtor.com/realestateagents/salinas_ca/',
        # 'https://www.realtor.com/realestateagents/hayward_ca/','https://www.realtor.com/realestateagents/corona_ca/',
        # 'https://www.realtor.com/realestateagents/paterson_nj/','https://www.realtor.com/realestateagents/murfreesboro_tn/',
        # 'https://www.realtor.com/realestateagents/macon_ga/','https://www.realtor.com/realestateagents/lakewood_co/',
        # 'https://www.realtor.com/realestateagents/killeen_tx/','https://www.realtor.com/realestateagents/springfield_ma/',
        # 'https://www.realtor.com/realestateagents/alexandria_va/','https://www.realtor.com/realestateagents/kansas-city_ks/',
        # 'https://www.realtor.com/realestateagents/sunnyvale_ca/','https://www.realtor.com/realestateagents/hollywood_fl/',
        # 'https://www.realtor.com/realestateagents/roseville_ca/','https://www.realtor.com/realestateagents/charleston_sc/',
        # 'https://www.realtor.com/realestateagents/escondido_ca/','https://www.realtor.com/realestateagents/joliet_il/',
        # 'https://www.realtor.com/realestateagents/jackson_ms/','https://www.realtor.com/realestateagents/bellevue_wa/',
        # 'https://www.realtor.com/realestateagents/surprise_az/','https://www.realtor.com/realestateagents/naperville_il/',
        # 'https://www.realtor.com/realestateagents/pasadena_tx/','https://www.realtor.com/realestateagents/pomona_ca/',
        # 'https://www.realtor.com/realestateagents/bridgeport_ct/','https://www.realtor.com/realestateagents/denton_tx/',
        # 'https://www.realtor.com/realestateagents/rockford_il/','https://www.realtor.com/realestateagents/mesquite_dallas-county_tx/',
        # 'https://www.realtor.com/realestateagents/savannah_ga/','https://www.realtor.com/realestateagents/syracuse_ny/',
        # 'https://www.realtor.com/realestateagents/mcallen_tx/','https://www.realtor.com/realestateagents/torrance_ca/',
        # 'https://www.realtor.com/realestateagents/olathe_ks/','https://www.realtor.com/realestateagents/visalia_ca/',
        # 'https://www.realtor.com/realestateagents/thornton_co/','https://www.realtor.com/realestateagents/fullerton_ca/',
        # 'https://www.realtor.com/realestateagents/gainesville_fl/','https://www.realtor.com/realestateagents/waco_tx/',
        # 'https://www.realtor.com/realestateagents/west-valley-city_ut/','https://www.realtor.com/realestateagents/warren_mi/',
        # 'https://www.realtor.com/realestateagents/hampton_va/','https://www.realtor.com/realestateagents/dayton_oh/',
        # 'https://www.realtor.com/realestateagents/columbia_sc/','https://www.realtor.com/realestateagents/orange_ca/',
        # 'https://www.realtor.com/realestateagents/cedar-rapids_ia/','https://www.realtor.com/realestateagents/stamford_ct/',
        # 'https://www.realtor.com/realestateagents/victorville_ca/','https://www.realtor.com/realestateagents/pasadena_ca/',
        # 'https://www.realtor.com/realestateagents/elizabeth_nj/','https://www.realtor.com/realestateagents/new-haven_ct/',
        # 'https://www.realtor.com/realestateagents/miramar_fl/','https://www.realtor.com/realestateagents/kent_wa/',
        # 'https://www.realtor.com/realestateagents/sterling-heights_mi/','https://www.realtor.com/realestateagents/carrollton_tx/',
        # 'https://www.realtor.com/realestateagents/coral-springs_fl/','https://www.realtor.com/realestateagents/midland_tx/',
        # 'https://www.realtor.com/realestateagents/norman_ok/','https://www.realtor.com/realestateagents/georgia_vt/',
        # 'https://www.realtor.com/realestateagents/santa-clara_ca/','https://www.realtor.com/realestateagents/columbia_mo/',
        # 'https://www.realtor.com/realestateagents/fargo_nd/','https://www.realtor.com/realestateagents/pearland_tx/',
        # 'https://www.realtor.com/realestateagents/simi-valley_ca/','https://www.realtor.com/realestateagents/topeka_ks/',
        # 'https://www.realtor.com/realestateagents/meridian_id/','https://www.realtor.com/realestateagents/allentown_pa/',
        # 'https://www.realtor.com/realestateagents/thousand-oaks_ca/','https://www.realtor.com/realestateagents/abilene_tx/',
        # 'https://www.realtor.com/realestateagents/vallejo_ca/','https://www.realtor.com/realestateagents/concord_ca/',
        # 'https://www.realtor.com/realestateagents/round-rock_tx/','https://www.realtor.com/realestateagents/arvada_co/',
        # 'https://www.realtor.com/realestateagents/clovis_ca/','https://www.realtor.com/realestateagents/palm-bay_fl/',
        # 'https://www.realtor.com/realestateagents/independence_mo/','https://www.realtor.com/realestateagents/lafayette_la/',
        # 'https://www.realtor.com/realestateagents/ann-arbor_mi/','https://www.realtor.com/realestateagents/rochester_mn/',
        # 'https://www.realtor.com/realestateagents/hartford_ct/','https://www.realtor.com/realestateagents/college-station_tx/',
        # 'https://www.realtor.com/realestateagents/fairfield_ca/','https://www.realtor.com/realestateagents/wilmington_nc/',
        # 'https://www.realtor.com/realestateagents/north-charleston_sc/','https://www.realtor.com/realestateagents/billings_mt/',
        # 'https://www.realtor.com/realestateagents/west-palm-beach_fl/','https://www.realtor.com/realestateagents/berkeley_ca/',
        # 'https://www.realtor.com/realestateagents/cambridge_ma/','https://www.realtor.com/realestateagents/clearwater_fl/',
        # 'https://www.realtor.com/realestateagents/west-jordan_ut/','https://www.realtor.com/realestateagents/evansville_in/',
        # 'https://www.realtor.com/realestateagents/richardson_tx/','https://www.realtor.com/realestateagents/broken-arrow_ok/',
        # 'https://www.realtor.com/realestateagents/richmond_ca/','https://www.realtor.com/realestateagents/league-city_tx/',
        # 'https://www.realtor.com/realestateagents/manchester_nh/','https://www.realtor.com/realestateagents/lakeland_fl/',
        # 'https://www.realtor.com/realestateagents/carlsbad_ca/','https://www.realtor.com/realestateagents/antioch_ca/',
        # 'https://www.realtor.com/realestateagents/westminster_co/','https://www.realtor.com/realestateagents/high-point_nc/',
        # 'https://www.realtor.com/realestateagents/provo_ut/','https://www.realtor.com/realestateagents/lowell_ma/',
        # 'https://www.realtor.com/realestateagents/elgin_il/','https://www.realtor.com/realestateagents/waterbury_ct/',
        # 'https://www.realtor.com/realestateagents/springfield_il/','https://www.realtor.com/realestateagents/gresham_or/',
        # 'https://www.realtor.com/realestateagents/murrieta_ca/','https://www.realtor.com/realestateagents/lewisville_tx/',
        # 'https://www.realtor.com/realestateagents/las-cruces_nm/','https://www.realtor.com/realestateagents/lansing_mi/',
        # 'https://www.realtor.com/realestateagents/beaumont_tx/','https://www.realtor.com/realestateagents/odessa_tx/',
        # 'https://www.realtor.com/realestateagents/pueblo_co/','https://www.realtor.com/realestateagents/peoria_il/',
        # 'https://www.realtor.com/realestateagents/downey_ca/','https://www.realtor.com/realestateagents/pompano-beach_fl/',
        # 'https://www.realtor.com/realestateagents/miami-gardens_fl/','https://www.realtor.com/realestateagents/temecula_ca/',
        # 'https://www.realtor.com/realestateagents/everett_wa/','https://www.realtor.com/realestateagents/costa-mesa_ca/',
        # 'https://www.realtor.com/realestateagents/ventura_ca/','https://www.realtor.com/realestateagents/sparks_nv/',
        # 'https://www.realtor.com/realestateagents/santa-maria_ca/','https://www.realtor.com/realestateagents/sugar-land_tx/',
        # 'https://www.realtor.com/realestateagents/greeley_co/','https://www.realtor.com/realestateagents/south-fulton_ga/',
        # 'https://www.realtor.com/realestateagents/dearborn_mi/','https://www.realtor.com/realestateagents/concord_nc/',
        # 'https://www.realtor.com/realestateagents/tyler_tx/','https://www.realtor.com/realestateagents/sandy-springs_ga/',
        # 'https://www.realtor.com/realestateagents/west-covina_ca/','https://www.realtor.com/realestateagents/green-bay_wi/',
        # 'https://www.realtor.com/realestateagents/centennial_co/','https://www.realtor.com/realestateagents/jurupa-valley_ca/',
        # 'https://www.realtor.com/realestateagents/el-monte_ca/','https://www.realtor.com/realestateagents/allen_tx/',
        # 'https://www.realtor.com/realestateagents/hillsboro_or/','https://www.realtor.com/realestateagents/menifee_ca/',
        # 'https://www.realtor.com/realestateagents/nampa_id/','https://www.realtor.com/realestateagents/spokane-valley_wa/',
        # 'https://www.realtor.com/realestateagents/rio-rancho_nm/','https://www.realtor.com/realestateagents/brockton_ma/'


    ]

    def start_requests(self):
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 # headers= headers,cookies= cookies
                                 )

    def parse(self,response):
        allraces = response.xpath('//a[@class="item btn "]//@href').extract()
        pg = allraces[-2].split('-')[1]
        realtors = response.css('span.jsx-2957100293:nth-child(2) > span:nth-child(1)').xpath('.//text()').extract()[1].replace(' REALTORS','').replace(',','')
        state = response.url.split('/')[-2]
        # yield {
        #     'url': response.url,
        #     'state': state,
        #     'realtors': realtors,
        #     'pages': pg
        #
        # }

        print('pgggg',pg)
        for p in range(1,int(pg)+1):#
            pgurl = response.url+'pg-'+str(p)
            yield scrapy.Request(url=pgurl, callback=self.toAllRealtors,
                                 # headers= headers, cookies = cookies,dont_filter=True
                                 )

    def toAllRealtors(self,response):
        realtrdivs = response.xpath("//div[contains(@class,'agent-list-card clearfix')]")
        for rdiv in realtrdivs:
            rehref = 'https://www.realtor.com'+rdiv.xpath('.//div[contains(@class,"agent-list-card-img-wrapper col-lg-2 col-sm-3 col-xxs-4")]//a/@href').extract_first()
            # print(rehref)
            try:
                forsale = rdiv.xpath('.//*[contains(text(),"For Sale")]//text()').extract()[3]
            except:
                forsale = ''
            try:
                sold = rdiv.xpath('.//*[contains(text(),"Sold")]//text()').extract()[3]
            except:
                sold = ''
            try:
                expirense = rdiv.xpath('.//*[contains(text(),"Experience")]//text()').extract()[3]
            except:
                expirense = ''

            yield scrapy.Request(url=rehref, callback=self.toAllRealtorsData,
                                 meta= {'forsale':forsale,'sold':sold,'experiance':expirense}
                                 )

    def toAllRealtorsData(self,response):
        forsale = response.meta['forsale']
        sold = response.meta['sold']
        experiance = response.meta['experiance']
        personname = ''.join([x.strip() for x in response.xpath('//p[contains(@class,"profile-Tiltle-main")]//text()').extract()])
        try:
            brokername = response.xpath('//p[contains(@class,"profile-Tiltle-sub")]//text()').extract()[-1]
        except:
            brokername = ''
        d = response.xpath('//text()').extract()
        mb = str(d).split(',"phones":[')[2].split(']')[0]
        k = mb.split('},{')
        officeph = ''
        mobilph = ''
        fax = ''
        for p1 in k:
            if 'Office' in p1:
                officeph = p1.split('"number":"')[1].split('"')[0]
            if 'Mobile' in p1:
                mobilph = p1.split('"number":"')[1].split('"')[0]
            if 'Fax' in p1:
                fax = p1.split('"number":"')[1].split('"')[0]

        st1 = str(d).split('"streetAddress"')[1].split(',')[0].replace('"','').replace(':','')
        st2 = str(d).split('"addressLocality"')[1].split(',')[0].replace('"','').replace(':','')
        st3 = str(d).split('"addressRegion"')[1].split(',')[0].replace('"','').replace(':','')
        st4 = str(d).split('"postalCode"')[1].split('}')[0].replace('"','').replace(':','')
        address = st1+','+st2+','+st3+','+st4
        web1 = str(d).split('"website"')[-2].split(',')[0].replace('"','').replace(':','')
        web2 = str(d).split('"website"')[-1].split(',')[0].replace('"','').replace(':','')

        yield {
            'Url': response.url,
            'Person Name': personname,
            'Broker Name': brokername,
            'Office Phone Number': officeph,
            'Personal Phone Number': mobilph,
            'Fax Number': fax,
            'Address': address,
            'Website 1': web1,
            'Website 2': web2,
            'For sale': forsale,
            'Sold': sold,
            'Experience': experiance
        }