import scrapy
import csv
import pandas as pd
import requests
import csv

class workspider(scrapy.Spider):
    name = "thearcho"

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'Url','Title','PropertyReference','Size',
            'Location','Rent','Type',
            'Available From','Local Authority','Deposit'
        ]
    };

    def start_requests(self):
        col_list = ["itemlink"]
        df = pd.read_csv("D:\\project_ubuntu_akva\\seliniumPython\\thercholink.csv", usecols=col_list)
        urls = df["itemlink"]

        # hist1 = []24560
        for i in range(0, len(urls)):

            yield scrapy.Request(url=urls[i], callback=self.parse,)

    def parse(self, response):
        prlink = response.url
        title1 = ''.join(response.xpath('/html/body/div[2]/div[2]/div/div/div[1]/div[1]/div[1]/h1//text()[1]').extract()).replace(' ','').replace('\n','')
        prreference = prlink.split('/')[-2]
        dlist = response.xpath('//ul[@class="icon-list"]//li')
        size = ''
        location = ''
        rent = ''
        type = ''
        avaifrm = ''
        loautho = ''
        deposit = ''
        for dl in dlist:
            texall = dl.xpath('./p//text()').extract_first().replace(' ','').replace('\n','')
            if 'Size:' in texall:
                size = texall
            if 'Location:' in texall:
                location = texall
            if 'Rent:' in texall:
                rent = texall
            if 'Type:' in texall:
                type = texall
            if 'AvailableFrom:' in texall:
                avaifrm = texall
            if 'LocalAuthority:' in texall:
                loautho = texall
            if 'Deposit:' in texall:
                deposit = texall
        sizetext = size.replace('Size:','')
        locationtext = location.replace('Location:','')
        renttext = rent.replace('Rent:','')
        typetext = type.replace('Type:','')
        availbfr = avaifrm.replace('AvailableFrom:','')
        localauth = loautho.replace('LocalAuthority:','')
        depositetxt = deposit.replace('Deposit:','')

        yield {
            'Url': prlink,
            'Title': title1,
            'PropertyReference': prreference,
            'Size': sizetext,
            'Location': locationtext,'Rent': renttext,'Type': typetext,
            'Available From': availbfr,'Local Authority': localauth,'Deposit': depositetxt
        }