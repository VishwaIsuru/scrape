import scrapy
import csv

class WorkSpider(scrapy.Spider):
    name = 'lawyerstribu'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Full Name","Category","Supreme Court Number","Email","Notification Directory",
            "Notarial Headquarters Address","Office telephone","First Name","Last Name"

        ]
    };
    # 237
    urls = []
    for i in range(1,237):
        lwurl = 'https://tribunalelectronico.ramajudicial.pr/dirabogados/Search.aspx?&i='+str(i)
        urls.append(lwurl)

    def start_requests(self):

        for u in self.urls:
            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parse(self, response):

        links = response.xpath('//td[@style="text-align:right"]//@href').extract()
        trs = response.xpath('//tr')
        for tr in trs:
            fnm = tr.xpath('.//td[1]//text()').extract_first()
            cat = tr.xpath('.//td[2]//text()').extract_first()
            numb = tr.xpath('.//td[3]//text()').extract_first()
            link = tr.xpath('.//td[3]//a//@href').extract_first()
            if link is not None:
                l = 'https://tribunalelectronico.ramajudicial.pr/dirabogados/' + link
            else:
                l = 'https://tribunalelectronico.ramajudicial.pr/dirabogados/Search.aspx?&i=1'
            yield scrapy.Request(url=l,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True,
                                 meta={'fnm':fnm,'catag':cat,'numb':numb})

    def parseInside(self,response):
        fulnm = response.meta['fnm']
        catg = response.meta['catag']
        numbr = response.meta['numb']
        dtrs = response.xpath('//tr')
        email = ''
        notifdirec = ''
        headdirec = ''
        tele = ''
        firstnm = fulnm.split(',')[0]
        lastnm = fulnm.split(',')[-1]
        for dtd in dtrs:
            td1 = dtd.xpath('.//td[1]//text()').extract_first()
            td2 = dtd.xpath('.//td[2]//text()').extract()
            if 'Email' in td1:
                email = td2
            if 'Dirección de Notificaciones' in td1:
                notifdirec = td2
            if 'Dirección de Sede Notarial' in td1:
                headdirec = td2
            if 'Teléfono de la Oficina' in td1:
                tele = td2

        yield {
            "Full Name": fulnm,
            "Category": catg,
            "Supreme Court Number": numbr,
            "Email": email,
            "Notification Directory": notifdirec,
            "Notarial Headquarters Address": headdirec,
            "Office telephone": tele,
            "First Name": firstnm,
            "Last Name": lastnm
        }