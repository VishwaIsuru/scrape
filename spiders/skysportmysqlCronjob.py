import scrapy
import csv
# from sshtunnel import SSHTunnelForwarder
import pymysql
from datetime import datetime, timedelta
import requests
from datetime import datetime
import pytz
import json
import numpy as np
import re

# server = SSHTunnelForwarder(
#     '158.220.104.93',
#     ssh_username='root',
#     ssh_password='wnw5UHBq75Yb4fB',
#     remote_bind_address=('127.0.0.1', 3306)
# )
# server.start()

conn = pymysql.connect(
    host='localhost',
    # port=server.local_bind_port,
    user='root',
    password='root',
    db='scrape'
)

cursor = conn.cursor()

headers = {'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.2.0 Safari/537.36",
    'Accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
    'Accept-Language': "en-US,en;q=0.5",
    'Accept-Encoding': "gzip, deflate, br",
    'Connection': "keep-alive",
    #'Upgrade-Insecure-Requests': "1",
    #'Sec-Fetch-Dest': "document",
    #'Sec-Fetch-Mode': "navigate",
    #'Sec-Fetch-Site': "none",
    #'Sec-Fetch-User': "?1",
    }

class workspider(scrapy.Spider):
    name = "skysport_mysql_cronjob"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'Race_URL','Race_ID','Race_Year','Date','Race_Name','Time','Venue','Total_Rns',
            'Distance','Track_cond','Track_type','Prizemoney','Status'
                ]
    };
    urls = []
    # M = ['07'] #'02','03','04','05','06','07','08','09','10','11','12'
    # D = ['11']
    yesterday = datetime.now() - timedelta(1)
    previausdate = datetime.strftime(yesterday, '%d-%m-%Y')
    ur1= 'https://www.skysports.com/racing/results/'+str(previausdate)
    urls.append(ur1)
    # for d1 in D:
    #     for m in M:
    #         for y in range(2024,2025):
    #             ur1 = 'https://www.skysports.com/racing/results/'+str(d1)+'-'+str(m)+'-'+str(y)
    #             urls.append(ur1)

    # from datetime import date
    # from datetime import timedelta
    # today = date(2014, 10, 10)  # or you can do today = date.today() for today's date
    #
    # for i in range(0, 7):
    #     print(today - timedelta(days=i))

    def start_requests(self):
        print('INFO::startScrape...................')
        aest = pytz.timezone('Australia/Sydney')
        current_time_aest = datetime.now(aest)
        formatted_time = current_time_aest.strftime('%Y-%m-%d %H:%M:%S (%Z)')
        headers = {'Content-type': 'application/json',}
        json_data = {
            # 'text': 'Hello, World!Text 1111111111111111111111',
            'text':formatted_time+' Sky Sports Scrape Initiated.'
        }
        response = requests.post('https://hooks.slack.com/services/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', headers=headers, json=json_data)

        try:
            for u in self.urls:
                yield scrapy.Request(url=u, callback=self.parse,
                                     #headers=headers,
                                     #cookies= cookies
                                     )
        except Exception as e:
            self.logger.error(f"Error during start_requests: {e}")
            headers = {'Content-type': 'application/json',}
            json_data = {
                # 'text': 'Hello, World!Text 1111111111111111111111',
                'text': e+' Sky Sports Scrape get Error.'
            }
            response = requests.post('https://hooks.slack.com/services/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', headers=headers, json=json_data)


    def parse(self,response):
        allraces = response.xpath('//a[@class="sdc-site-racing-meetings__event-link"]//@href').extract()
        for race in allraces:
            racelink = 'https://www.skysports.com'+race
            yield scrapy.Request(url=racelink, callback=self.toAllRace,
                    #headers=headers,
                                # cookies = cookies,dont_filter=True
                                 )

    def toAllRace(self,response):
        urlracdtail = response.url
        raceid = urlracdtail.split('/')[6]
        raceyear = urlracdtail.split('/')[-2].split('-')[-1]
        date = response.xpath('//button[@aria-controls="sdc-select-1"]//text()').extract_first().strip()
        racename = ''.join([x for x in response.xpath('//h3[@class="sdc-site-racing-header__description"]//text()').extract()]).strip().replace('\n',' ').replace('  ','').replace("'","´")
        time = response.xpath('//h2[@class="sdc-site-racing-header__name"]//text()').extract_first().split(' ')[0]+' '
        venue = response.xpath('//h2[@class="sdc-site-racing-header__name"]//text()').extract_first().replace(time,'').replace('  ','').replace("'","´").replace(';',',')
        rcdetails = response.xpath('//li[@class="sdc-site-racing-header__details-item"]')
        totalrunrs = ''
        distance = ''
        trckcon = ''
        trctype = ''
        pricemoney = ''
        for rcdetail in rcdetails:
            rcdettxt = ''.join([x for x in rcdetail.xpath('.//text()').extract()]).strip().replace('\n',' ').replace('  ','')
            # print(rcdettxt)
            if 'Runners' in rcdettxt:
                totalrunrs = rcdettxt
            if 'Distance' in rcdettxt:
                distance = rcdettxt.replace('Distance:','')
            if 'Going' in rcdettxt:
                trckcon = rcdettxt.replace('Going:','')
            if 'Surface' in rcdettxt:
                trctype = rcdettxt.replace('Surface:','')
            if 'added' in rcdettxt:
                pricemoney = rcdettxt.replace('added','')
        ststus = response.xpath('//div[@class="sdc-site-racing-status__status"]//text()').extract_first()

        # yield {
        #     'Race_URL': urlracdtail,
        #     'Race_ID': raceid,
        #     'Race_Year': raceyear,
        #     'Date': date,
        #     'Race_Name': racename,
        #     'Time': time,
        #     'Venue': venue,
        #     'Total_Rns': totalrunrs,
        #     'Distance': distance,
        #     'Track_cond': trckcon,
        #     'Track_type': trctype,
        #     'Prizemoney': pricemoney,
        #     'Status': ststus
        # }
        # csvfilnm1 = 'SkySportRaceDetailData2023_1.csv'
        # csvRow1 = (urlracdtail, raceid, raceyear, date,racename,time,venue,totalrunrs,distance,trckcon,trctype,
        #            pricemoney,ststus)
        # with open(csvfilnm1, "a", encoding="utf-8", newline='') as fp1:
        #     wr = csv.writer(fp1, dialect='excel')
        #     wr.writerow(csvRow1)

        # sql = "SELECT * FROM horseracedetail WHERE raceid='%s'"
        # cursor.execute(sql %raceid)
        # result = cursor.fetchone()
        # print(result)
        # # connection.commit()
        # if result is None:
        #tim = datetime.now()
        now = datetime.now()
        tim = now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        sql = "INSERT INTO skysport_race_from_april_2023(raceurl, raceid, raceyear, date,racename,time,venue,totalrns," \
              "distance,trackcond,tracktype,prizemoney,status,created,updated)" \
              " VALUES ('%s', %s, %s, '%s', '%s'," \
              " '%s'," \
              " '%s', '%s', '%s' , '%s', '%s', '%s'" \
              ", '%s', '%s', '%s')"

        sql = sql % (urlracdtail, raceid, raceyear, date,racename,time,venue,totalrunrs,distance,trckcon,trctype,
               pricemoney,ststus,tim,tim)
        print(sql)
        cursor.execute(sql)
        conn.commit()

############## horse second part ############################################
        horsedtls = response.xpath('//div[@class="sdc-site-racing-card__item"]')
        for horsedt in horsedtls:
            hourseurl = horsedt.xpath('.//h4[@class="sdc-site-racing-card__name"]//a//@href').extract_first()
            try:
                horseid = hourseurl.split('/')[4]
            except:
                horseid = ''
            horsename = horsedt.xpath('.//h4[@class="sdc-site-racing-card__name"]//a//text()').extract_first()
            runnerposition = horsedt.xpath('.//span[@class="sdc-site-racing-card__position"]//text()').extract_first().replace('\n','').replace('  ','').replace("'","´")
            runnercomment = horsedt.xpath('.//p[@class="sdc-site-racing-card__summary"]//text()').extract_first()
            runll = horsedt.xpath('.//a[@class="sdc-site-racing-card__details-item-link"]')
            runnsp = horsedt.xpath('.//span[@class="sdc-site-racing-card__betting-odds"]//text()').extract_first()
            nonrunner = ''.join([x.strip() for x in horsedt.xpath('.//@data-status').extract()])
            if nonrunner == 'non-runner':
                runnsp = ''
            tbbarried = ''.join([x.strip() for x in horsedt.xpath('.//div[@class="sdc-site-racing-card__number"]//text()').extract()])
            trainer = ''
            jockey = ''
            try:
                tabnumber = tbbarried.split('(')[0]
            except:
                tabnumber = ''
            try:
                barriernumber = tbbarried.split('(')[1].replace(')','')
            except:
                barriernumber = ''
            for rulin in runll:
                linkboth = rulin.xpath('.//@href').extract_first()
                textboth = rulin.xpath('.//text()').extract_first()
                if '/trainer/' in linkboth:
                    trainer = textboth
                if '/jockey/' in linkboth:
                    jockey = textboth

            # yield {
            #     'raceurl': urlracdtail,
            #     'raceid': raceid,
            #     'horseid': horseid,
            #     'horsename': horsename,
            #     'possis': runnerposition,
            #     'trianer': trainer,
            #     'jockey': jockey,
            #     'comment': runnercomment,
            #     'runnersp': runnsp
            # }
            horselink = 'https://www.skysports.com'+hourseurl
            yield scrapy.Request(url=horselink, callback=self.parse_inside_horse, dont_filter=True,
                    #headers=headers,
                                 meta={'raceurl': urlracdtail, 'raceid': raceid,'runnerposition': runnerposition,
                                       'trainer': trainer,'jocky': jockey,'comment':runnercomment,'runsp': runnsp,
                                       'horseid': horseid,'tabnum':tabnumber,'barriernum':barriernumber}
                                 )

    def parse_inside_horse(self,response):
        hoursurl = response.url
        raceurl = "'"+response.meta['raceurl']+"'"
        raceid = response.meta['raceid']
        horseid = response.meta['horseid']
        runnerposition = response.meta['runnerposition']
        runnertrainer = response.meta['trainer'].replace("'","´")
        runnerjocky = response.meta['jocky'].replace("'","´")
        runnercomment = response.meta['comment']
        runnersp = response.meta['runsp']
        tabnumber = response.meta['tabnum']
        barriernumber = response.meta['barriernum']

        horsename = response.xpath('//h2[@class="sdc-site-racing-header__name"]//text()').extract_first().replace("'","´")
        try:
            runnercob = horsename.split('(')[1].replace(')','').replace("'","´")
        except:
            runnercob = '-'
        headerdetails = response.xpath('//li[@class="sdc-site-racing-header__details-item"]//text()').extract()
        agetx = ''
        sex = ''
        breading = ''
        owner = ''
        h = 0
        for hddetail in headerdetails:
            if 'Age:' in hddetail:
                agetx = headerdetails[h+1].replace(' \n','').replace('  ','').replace("'","´")
            if 'Sex:' in hddetail:
                sex = headerdetails[h+1].replace(' \n','').replace('  ','').replace('\n','').replace("'","´")
            if 'Breeding:' in hddetail:
                breading = headerdetails[h+1].replace(' \n','').replace('  ','').replace('\n','').replace("'","´")
            if 'Owner:' in hddetail:
                owner = headerdetails[h+1].replace(' \n','').replace('  ','').replace('\n','').replace("'","´")
            h+=1
        try:
            age = agetx.split('(')[0]
        except:
            age = ''
        try:
            runneryob = agetx.split(',')[-1].replace(')','').replace('\n','').replace(' ','').replace("'","´")
        except:
            runneryob = ''
        try:
            sire = breading.split('(')[0].split('-')[0].replace("'","´")
        except:
            sire = ''
        try:
            sirecob = breading.split('(')[1].split('-')[0].replace(')','').strip().replace("'","´")
        except:
            sirecob = ''
        if len(sirecob) != 3:
            sirecob = ''
        try:
            dam = breading.split('-')[1].split('(')[0].replace("'","´")
        except:
            dam = ''
        try:
            damcob = breading.split('-')[1].split(')')[0].split('(')[1].strip().replace("'","´")
        except:
            damcob = ''

        try:
            damsire = breading.split(') (')[1].split('(')[0].replace("'","´")
        except:
            damsire = ''
        try:
            damsirecob = breading.split(') (')[1].split('(')[1].split(')')[0].strip().replace("'","´")
        except:
            damsirecob = ''

        if len(damcob)>3:
            damsire = damcob
        if len(damcob) != 3:
            damcob = ''

        # yield {
        #     'hourseurl': hoursurl,
        #     'raceurl': raceurl,
        #     'raceid': raceid,
        #     'horseid': horseid,
        #     'horsename': horsename,
        #     'runnersp': runnersp,
        #     'runnercob': runnercob,
        #     'runnerage': age,
        #     'runneryob': runneryob,
        #     'runnersex': sex,
        #     'breeding': breading,
        #     'sire': sire,
        #     'sirecob': sirecob,
        #     'dam': dam,
        #     'damcob': damcob,
        #     'damsire': damsire,
        #     'damsirecob': damsirecob,
        #     'owner': owner,
        #     'runnerpositn': runnerposition,
        #     'runnertrainer': runnertrainer,
        #     'runnerjocky': runnerjocky,
        #     'runnercomment': runnercomment,
        # }
        # csvfilnm2 = 'SkySportHorseData2023_1.csv'
        # csvRow2 = (
        # hoursurl, raceurl, raceid, horseid, horsename, runnersp, runnercob, age, runneryob, sex, breading,
        # sire,sirecob,dam,damcob,damsire,damsirecob,owner,runnerposition,runnertrainer,runnerjocky,runnercomment
        # )
        # with open(csvfilnm2, "a", encoding="utf-8", newline='') as fp2:
        #     wr = csv.writer(fp2, dialect='excel')
        #     wr.writerow(csvRow2)

        sql = "INSERT INTO skysport_horse_from_april_2023(horseUrl, raceUrl, raceId, horseId, horseName, runnersp, runnercob, runnerage, " \
              "runneryob, runnersex, breeding,sire,sirecob,dam,damcob,damsire,damsirecob,owner,runnerposition," \
              "runnertrainer,runnerjocky,runnercomment,tabnumber,barriernumber)" \
              "VALUES ('%s', %s, %s, '%s', '%s'," \
              " '%s'," \
              " '%s', '%s', '%s' , '%s', '%s', '%s'" \
              ", '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"

        sql = sql % (hoursurl, raceurl, raceid, horseid, horsename, runnersp, runnercob, age, runneryob, sex,
                     breading,sire,sirecob,dam,damcob,damsire,damsirecob,owner,runnerposition,runnertrainer,
                     runnerjocky,runnercomment,tabnumber,barriernumber)
        print(sql)
        cursor.execute(sql)
        conn.commit()

        #########3rd form horse################################
        items = response.xpath('//div[@class="sdc-site-racing-profile__item"]')

        tempformyr = ''
        for item in items:
            # formyear = response.xpath('//h4[@class="sdc-site-racing-profile__header"]//text()').extract_first()
            formdate = item.xpath('.//div[@class="sdc-site-racing-profile__cell sdc-site-racing-profile__cell--1"]//text()').extract_first()
            formposition = item.xpath('.//div[@class="sdc-site-racing-profile__cell sdc-site-racing-profile__cell--2"]//strong[@class="sdc-site-racing-profile__pos"]//text()').extract_first()
            formtotalrun = item.xpath('.//div[@class="sdc-site-racing-profile__cell sdc-site-racing-profile__cell--2"]//small//text()').extract_first()
            cel3itemslbl = item.xpath('.//div[@class="sdc-site-racing-profile__cell sdc-site-racing-profile__cell--3"]//li//span//text()').extract()
            cel3itemstext = item.xpath('.//div[@class="sdc-site-racing-profile__cell sdc-site-racing-profile__cell--3"]//li//strong//text()').extract()
            cel3alink = item.xpath('.//div[@class="sdc-site-racing-profile__cell sdc-site-racing-profile__cell--3"]//li//a//@href').extract()
            yrlink = ''
            for cel3link in cel3alink:
                if '/racing/results/full-result/' in cel3link:
                    yrlink = cel3alink
            try:
                formyear = yrlink[-1].split('/')[6].split('-')[-1]
            except:
                formyear = ''
            if formyear != '':
                tempformyr = formyear
            if formyear == '':
                formyear = response.xpath('//h4[@class="sdc-site-racing-profile__header"]//text()').extract_first()

            l=0
            formweight = ''
            formwinner = ''
            formbeatenby = ''
            formor = ''
            formtrainer = ''
            formjockey = ''
            formracedetails = ''

            for cel3item in cel3itemslbl:
                if 'Weight' in cel3item:
                    formweight = cel3itemstext[l]
                if 'Winner' in cel3item:
                    formwinner = cel3itemstext[l].replace("'","´")
                if 'Beaten by' in cel3item:
                    formbeatenby = cel3itemstext[l].replace("'","´")
                if 'OR' in cel3item:
                    formor = cel3itemstext[l].replace("'","´")
                if 'Trainer' in cel3item:
                    formtrainer = cel3itemstext[l].replace("'","´")
                if 'Jockey' in cel3item:
                    formjockey = cel3itemstext[l].replace("'","´")
                if 'Race details' in cel3item:
                    try:
                        formracedetails = cel3itemstext[l].replace("'","´")
                    except:
                        formracedetails = ''
                l+=1
        #
        #     yield {
        #         'horseurl': hoursurl,
        #         'horseid': horseid,
        #         'formyear': formyear,
        #         'formdate': formdate,
        #         'formposition': formposition,
        #         'formtotalran': formtotalrun,
        #         'formweight': formweight,
        #         'formwinner': formwinner,
        #         'formbeatenby': formbeatenby,
        #         'formor': formor,
        #         'formtrainer': formtrainer,
        #         'formjockey': formjockey,
        #         'formracedetial': formracedetails,
        #         'horsename': horsename,
        #         'yearofbirth': runneryob,
        #     }
        #     csvfilnm3 = 'SkySportHorseFormData2023_1.csv'
        #     csvRow3 = (
        #             hoursurl, horseid, formyear,formdate,formposition,formtotalrun,formweight,formwinner,
        #         formbeatenby,formor,formtrainer,formjockey,formracedetails,horsename,runneryob,dam
        #     )
        #     with open(csvfilnm3, "a", encoding="utf-8", newline='') as fp3:
        #         wr = csv.writer(fp3, dialect='excel')
        #         wr.writerow(csvRow3)

            sql = "INSERT INTO skysport_horse_form_data_from_april_2023(horseurl, horseid, formyear,formdate,formposition," \
                  "formtotalran," \
                  "formweight,formwinner,formbeatenby,formor,formtrainer,formjockey,formracedetail,horsename," \
                  "yearofbirth,dam)" \
                  "VALUES ('%s', %s, %s, '%s', '%s'," \
                  " '%s'," \
                  " '%s', '%s', '%s' , '%s', '%s', '%s'" \
                  ", '%s', '%s', '%s', '%s')"

            sql = sql % (
                hoursurl, horseid, formyear, formdate, formposition, formtotalrun, formweight, formwinner,
                formbeatenby, formor, formtrainer, formjockey, formracedetails, horsename, runneryob, dam)
            print(sql)
            cursor.execute(sql)
            conn.commit()


    def closed(self, reason):
        print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
        yesterday = datetime.now() - timedelta(1)
        date_str = yesterday.strftime('%y-%m-%d')
        # date_str = current_date.strftime('%Y-%m-%d')
        print('date_srt... ',date_str)
        dd = date_str[8:]
        mm = date_str[5:7]
        yyyy = date_str[:4]
        sdate = yesterday.strftime('%d-%m-%Y')
        # sdate = dd + '-' + mm + '-' + yyyy
        print('INFO::latestScrapeDate:: ',sdate)
        check_query = "SELECT COUNT(*) FROM jsonuotputSkysport WHERE scrapedate = %s;"

        conn = pymysql.connect(
            host='localhost',
            # port=server.local_bind_port,
            user='root',
            password='root',
            db='scrape'
        )

        cursor = conn.cursor()
        cursor.execute(check_query, (date_str,))
        exists = cursor.fetchone()[0] > 0
        if exists:
            print(sdate,' data json already exit')

        else:
            # Insert a new record
            # conn = connection()
            # cursor = conn.cursor()
            cursor.execute("SELECT * FROM skysport_race_from_april_2023 rc "
                           "JOIN skysport_horse_from_april_2023 hr "
                           "ON rc.raceid = hr.raceId WHERE rc.raceurl like '%" + sdate + "%';")
            # data = cursor.fetchall()
            # print('json.dumps(data)')
            # print('aaaaaaa', api_key)
            races = []
            ids = []
            venues = []
            alldatajoin = cursor.fetchall()

            creeeeeeetttt = ''
            upppppppppppd = ''
            for row in alldatajoin:
                # print('get join query')
                raceurl = row[0]
                venues.append(row[6])
                # print('after query')
                ids.append(row[1])
            mylist = list(set(ids))
            venueall = list(set(venues))
            print('venuessssss',venueall)
            ######## new sturcure begin ##################
            jsonven = []
            #for one venues data set
            for vn in venueall:
                timeslots = []
                query = """
                SELECT * FROM skysport_race_from_april_2023
                WHERE venue = %s AND raceurl LIKE %s;
                """
                cursor.execute(query, (vn, f"%{sdate}%"))
                # cursor.execute(
                #     "SELECT * FROM skysport_race_from_april_2023 WHERE venue = '" + vn + "' and raceurl like '%" + sdate + "%';")
                allmeetingsdateof = cursor.fetchall()
                for row in allmeetingsdateof:
                    timemeeting = row[5].replace(':', '.').replace(' ', '')
                    timeslots.append(timemeeting)
                # print(vn)
                # print(timeslots)
                x = np.array(timeslots)
                y = x.astype(np.float)
                miny = ("%.2f" % min(y))
                # print("Smallest element is:",miny )
                index = timeslots.index(str(miny))
                # print(index)
                meetingfirsturl = allmeetingsdateof[index][0]

                # print(vn)
                vnue = ''
                for r in alldatajoin:
                    if str(vn) in str(r[6]):
                        vnue = r
                        break
                vnreces = []
                for l in alldatajoin:
                    if str(vn) in l[6]:
                        #get the venue races
                        vnreces.append(l[1])
                raceall = list(set(vnreces))
                # print('racccccccccaaaaaaaaaaaaaaaaal',raceall)
                races = []
                #for races in one venue
                for i in raceall:
                    # print('raceid', i)
                    oneracehorses = []
                    horses = []
                    race = ''
                    for r in alldatajoin:
                        if str(i) in str(r[1]):
                            race = r
                            break
                    # print(race[0])
                    creeeeeeetttt = race[13]
                    upppppppppppd = race[14]
                    for d in alldatajoin:
                        if str(i) in str(d[1]):
                            oneracehorses.append(d[18])
                            hname = d[19].split('(')[0]
                            clr = d[24].split(' ')[0]
                            sexa = ['Stallion', 'Mare', 'Gelding','Colt','Filly','Stud','Broodmare','Dam','Ridgling','Cryptorchid']
                            sex = ''
                            for t in sexa:
                                if t in d[24]:
                                    sex= t
                            horses.append(
                                {"runner": {
                                    "id": d[18]
                                },
                                    "created": d[13],"updated": d[14],"tabNo": d[38],"barrier": d[39],"name": hname,"age": d[22],
                                    "sex": sex,"yob": d[23],"country":d[21],"colour": clr,"sire": d[26], "sireCountry": d[27],
                                    "dam": d[28],"damCountry": d[29],"damSire": d[30],"damSireCountry": d[31],
                                    "jockeyID": "","jockey": d[35],"trainerID": "","trainer": d[34],"owners": d[32],"margin": "",
                                    "finalPosition": d[33],"prizeMoney": "","prizeMoneyCurrency": "","scratched": "","scratchingType": "",
                                    "weightAllocated": "", "allowanceWeight": "","apprenticeIndicator": False,"silksColour": "",
                                    "comment": d[36],"url":d[15],"prices": {"source": "", "price": d[20],"type": "OP" },"sireYoB":"",
                                    "damYoB":"","damSireYoB":"", "sireSireName":"", "sireSireYob":""
                                }
                            )

                    hh= re.split(r'(\d+)', race[11])
                    pricurency = hh[0]
                    primony = ''.join(hh[1:])
                    races.append(
                        {"id":race[1],"created": race[13],"updated":race[14],"startTime":race[5],"number": "","name": race[4],
                         "starters": race[7],"comment": "","distance": race[8],"status": race[12],"prizeMoney": primony,
                         "prizeMoneyCurrency": pricurency,"class": "","trackCondition":race[9],"weather": "","weightType": "",
                         "surfaceType": race[10],"restrictions": { "age": "","sex": "","jockey": ""},
                         "url":race[0],"competitors":horses
                         }
                    )

                try:
                    trccounty= vnue[6].split('(')[1].replace(')', '')
                except:
                    trccounty = ""
                try:
                    trcname = vn.split('(')[0]
                except:
                    trcname = ''
                jsonven.append({"id":vnue[1],"created": creeeeeeetttt,"updated": upppppppppppd,"tabIndicator": "","railPosition": "",
                                "hasVision": "","type": "","url": meetingfirsturl,"date":sdate,"source": "SP",
                                "track": {"id":vnue[1],"created": vnue[13],
                                          "updated": vnue[14],"name": trcname,"country": trccounty,
                                          "state": "","surface": vnue[10],"abbreviation": "","location": ""}
                                   ,"races":races})
            json_string = json.dumps({"data": {'meetingsDated':jsonven}},default=self.datetime_converter, indent=4)

            insert_query = """
            INSERT INTO jsonuotputSkysport (scrapedate, jsonstring)
            VALUES (%s, %s);
            """
            cursor.execute(insert_query, (date_str, json_string))
            conn.commit()
            print('json creat succes.................')

        yesterday = datetime.now() - timedelta(1)
        datadbdate = datetime.strftime(yesterday, '%d-%m-%Y')
        query_races = "SELECT COUNT(*) AS total_races FROM skysport_race_from_april_2023 WHERE raceUrl LIKE '%"+str(datadbdate)+"%';"
        cursor.execute(query_races)
        total_races = cursor.fetchone()[0]

        # SQL Query to get the total horses
        query_horses = "SELECT COUNT(*) AS total_horses FROM skysport_horse_from_april_2023 WHERE raceUrl LIKE '%"+str(datadbdate)+"%';"
        cursor.execute(query_horses)
        total_horses = cursor.fetchone()[0]

        # Formatting the Slack message
        aest = pytz.timezone('Australia/Sydney')
        current_time_aest = datetime.now(aest)
        formatted_time = current_time_aest.strftime('%Y-%m-%d %H:%M:%S (%Z)')  # Replace with your actual formatted time
        yesterday = datetime.now() - timedelta(1)
        apidate = datetime.strftime(yesterday, '%Y%m%d')
        json_data = {
            'text': f"{formatted_time} Sky Sports Scrape Completed.",
            'scrape_date': apidate,
            'api_endpoint': 'https://apig1.akva.io/api/query?date='+apidate+'&source=SP',
            'total_races': total_races,
            'total_horses': total_horses
        }

        # Slack Webhook URL
        webhook_url = 'YOUR_SLACK_WEBHOOK_URL'  # Replace with your Slack Webhook URL

        # Formatting message for Slack
        message = {
            "blocks": [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f"*{json_data['text']}*\n\n*Scrape Date:* {json_data['scrape_date']}\n*API Endpoint:* {json_data['api_endpoint']}\n*Total Races:* {json_data['total_races']}\n*Total Horses:* {json_data['total_horses']}"
                    }
                }
            ]
        }

        # aest = pytz.timezone('Australia/Sydney')
        # current_time_aest = datetime.now(aest)
        # formatted_time = current_time_aest.strftime('%Y-%m-%d %H:%M:%S (%Z)')
        # headers = {'Content-type': 'application/json',}
        # json_data = {
        #     # 'text': 'Hello, World!Text 1111111111111111111111',
        #     'text':formatted_time+' Sky Sports Scrape Completed.'
        # }
        print(message)
        response = requests.post('https://hooks.slack.com/services/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', headers=headers, json=message)
        conn.close()
        if conn:
            conn.close()

    def datetime_converter(self,o):
        if isinstance(o, datetime):
            return o.isoformat()
