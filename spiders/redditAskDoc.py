import scrapy
import csv
import pandas as pd
import requests
from datetime import datetime


class WorkSpider(scrapy.Spider):
    name = 'redditAskDoc'
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'link','name','postcontent','postheading','scrapetime','posttime','comments','votes'
        ]
    };

    def start_requests(self):
        col_list = ["link","pp"]
        df = pd.read_csv("D:\\Songs\\amazonWeekly\\redditAskDoc_Link_16_5_23.csv", usecols=col_list)
        urls = df["link"]
        prtnum = df["pp"]
        for i in range(0,len(urls)):
            u = urls[i]
            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:103.0) Gecko/20100'+str(i)+' Firefox/103.0'},meta={'partnumber':prtnum[i]})

    def parse(self, response):
        searchnumber = response.meta['partnumber']
        name = response.css('.author-name').xpath('.//text()').extract_first()
        postcontent = ''.join([x.strip() for x in response.xpath('//div[contains(@id, "post-rtjson-content")][1]//text()').extract()])
        postheading = ''.join([x.strip() for x in response.xpath('//div[contains(@id, "post-title-")][1]//text()').extract()])
        scrpetime = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        comment = response.xpath('//shreddit-post[contains(@id, "t3_")]//@comment-count').extract_first()
        votes = response.xpath('//shreddit-post[contains(@id, "t3_")]//@score').extract_first()
        posttime = response.xpath('//time[contains(@title, "Wednesday, May 17, 2023")]').extract()
        t=response.xpath('//shreddit-post[contains(@id, "t3_")]//@created-timestamp').extract_first().split('.')[0]
        scrpetime2 = datetime.strptime(scrpetime, "%Y-%m-%d %H:%M:%S")
        tt2 = datetime.strptime(t, "%Y-%m-%dT%H:%M:%S")
        timeee = scrpetime2-tt2
        yield {
            'link': response.url,
            'name': name,
            'postcontent': postcontent,
            'postheading': postheading,
            'scrapetime':scrpetime,
            'posttime': str(timeee.days)+' days and '+str(timeee.seconds // 3600)+' hours ago',
            'comments':comment,
            'votes': votes
        }
