import scrapy
import re
import json
import csv

# from difflib import SequenceMatcher


class workspider(scrapy.Spider):
    name = "bearmach"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': ["URL",
            "Description",
            "Image",
            "Part No",
            "Supercedes",
            "Brand",
            "Categories",
            "Model",
            "Suitable For Application",
            "Suitable for Fitment",
            "Price"

                               ]
    };



    def start_requests(self):

        f = open("bearmach.csv", "r")
        contents = f.read().split('\n')
        i = 0

        for c in contents:
            i += 1
            if i == 1:
                continue

            yield scrapy.Request(url=c.strip(), callback=self.scrape_product,headers={'User-Agent': 'Mozilla Firefox 12.0'})


    def scrape_product(self, response):

        # catLink = response.meta["catLink"]
        description = response.xpath('.//h1[@itemprop="name"]/text()').extract_first()
        price= response.xpath('//div[@class="product-shop"]/div[@class="price-info mainprice"]/div[@class="price-box globale-pricing"]/span[@class="regular-price"]/span/text()').extract_first()

        part_no = response.xpath('.//*[@itemprop="sku"]/@content').extract_first()
        image = ""
        try:
            image = response.xpath('//img[@itemprop="image"]/@src').extract_first()
        except:
            pass

        table = response.css('.data-table')

        trs = table.xpath('./tbody/tr')

        supercedes = ""
        brand = ""
        categories = ""
        products = ""
        model = ""
        suitable_for_application = ""
        suitable_for_fitment = ""

        for tr in trs:
            label = tr.css('.label').xpath('./text()').extract_first().strip()
            value = ''.join(tr.css('.data').xpath('.//text()').extract()).strip()

            if label == 'Supercedes':
                supercedes = value
            elif label == 'Brands':
                brand = value
            elif label == 'Categories':
                categories = value
            elif label == 'Products':
                products = value
            elif label == 'Suitable for Models':
                model = value
            elif label == 'Suitable for Application':
                suitable_for_application = value
            elif label == 'Suitable for Fitment':
                suitable_for_fitment = value

        if categories and products:
            categories = categories + '/' + products

        yield {
            # "CatLink": catLink,
            "URL": response.url,
            "Description": description,
            "Image": image,
            "Part No": part_no,
            "Supercedes": supercedes,
            "Brand": brand,
            "Categories": categories,
            "Model": model,
            "Suitable For Application": suitable_for_application,
            "Suitable for Fitment": suitable_for_fitment,
            "Price":price

        }
