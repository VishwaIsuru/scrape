# import scrapy
# import csv
# # from sshtunnel import SSHTunnelForwarder
# import pymysql
# from datetime import datetime, timedelta
# # server = SSHTunnelForwarder(
# #     '158.220.104.93',
# #     ssh_username='root',
# #     ssh_password='wnw5UHBq75Yb4fB',
# #     remote_bind_address=('127.0.0.1', 3306)
# # )
# # server.start()
#
# conn = pymysql.connect(
#     host='localhost',
#     # port=server.local_bind_port,
#     user='root',
#     password='root',
#     db='scrape'
# )
# #
# cursor = conn.cursor()
#
# class workspider(scrapy.Spider):
#     name = "unamityjpn_mysql_cronjob"
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': [
#             'horseid','formdate','formhorseJocky'
#                 ]
#     };
#     urls = []
#     yesterday = datetime.now() - timedelta(1)
#     previausdate = datetime.strftime(yesterday, '%Y/%m/%d')
#     ur1= 'https://umanity.jp/en/racedata/race_5.php?date='+str(previausdate)
#     urls.append(ur1)
#     # M = ['07']  #'02','03','04','05','06','07','08','09','10','11','12'
#     # D = ['11']
#     # for d1 in D:
#     #     for m in M:
#     #         for y in range(2024, 2025):
#     #             ur1 = 'https://umanity.jp/en/racedata/race_5.php?date='+str(y)+'/'+str(m)+'/'+str(d1)
#     #             urls.append(ur1)
#
#     # for d in range(13,32):
#     #     for m in M:
#     #         for y in range(2012,2013):
#     #             ur = 'https://umanity.jp/en/racedata/race_5.php?date='+str(y)+'/'+str(m)+'/'+str(d)
#     #             urls.append(ur)
#
#     def start_requests(self):
#         for u in self.urls:
#             yield scrapy.Request(url=u, callback=self.parse,
#                                  # headers= headers,cookies= cookies
#                                  )
#
#     def parse(self,response):
#         allraces1 = response.xpath('//a[contains(@class, "race")]//@href').extract()
#         allraces = list(set(allraces1))
#         # print(allraces)
#         for race in allraces:
#             racelink = 'https://umanity.jp'+race
#             yield scrapy.Request(url=racelink, callback=self.toAllRace,
#                                  # headers= headers, cookies = cookies,dont_filter=True
#                                  )
#
#     def toAllRace(self,response):
#         eventURL = response.url
#         eventId = eventURL.split('=')[-1]
#         raceyear = eventId[0:4]
#         m = ''.join([x for x in response.xpath('//time[@datetime]//text()').extract()])
#         meetingYear = m.split(' ')[0]
#         meetingVenue = m.split(' ')[1]
#         eventime = m.split(' ')[-1]
#         eventNameAll = ''.join([x for x in response.xpath('//h2[@class="racecard"]//text()').extract()])
#         eventNumber = eventNameAll.split(' ')[0]
#         eventName = ' '.join(eventNameAll.split(' ')[1:])
#         try:
#             eventWeather = response.xpath('//td[@class="ttl"][3]//img//@src').extract_first().replace('/common/img/r_','').replace('.gif','')
#         except:
#             eventWeather = ''
#         eventTotalRunner = len(response.xpath('//tr[@class="odd-row" or @class="even-row"]').extract())
#         k = ''.join([x for x in response.xpath('//div[@style="clear:both;"]//span[@class="tx_aqu"]//text()').extract()])
#         eventDistance = k.split(' ')[1]
#         eventDistanceUnit = eventDistance[-1]
#         eventclass = ' '.join(k.split(' ')[2:])
#         runarage = k.split(' ')[2][0:3]
#         eventTrackSurface = k.split(' ')[0]
#         p = response.xpath('//div[contains(text(),"Added Money")]//text()').extract_first()
#         try:
#             eventPriceMoneyCurrency = p.split(' ')[2].split(':')[0].replace('(', '').replace(')', '')
#         except:
#             eventPriceMoneyCurrency = ''
#         try:
#             eventPrizeMoney = p.split(' ')[2].split(':')[1]
#         except:
#             eventPrizeMoney = ''
#         eventTrackCondition = ''.join([x for x in response.xpath('//b[@class="tx_gre"]//text()').extract()])
#         eventStakesLevel = ''
#         eventStatus = ''
#         eventCreatedAt = ''
#         eventUpdatedAt = ''
#
#         # csvfilnm1 = 'UnamityRaceDetailData2012DB.csv'
#         # csvRow1 = (eventId, eventURL,eventName,eventNumber,eventime,eventTotalRunner,eventStatus,eventclass,
#         #            eventStakesLevel,meetingVenue,eventTrackCondition,eventTrackSurface,eventWeather,eventDistance,
#         #            eventDistanceUnit,eventPrizeMoney,eventPriceMoneyCurrency,eventCreatedAt,eventUpdatedAt)
#         # with open(csvfilnm1, "a", encoding="utf-8", newline='') as fp1:
#         #     wr = csv.writer(fp1, dialect='excel')
#         #     wr.writerow(csvRow1)
#
#         # with connection.cursor() as cursor:
#         tim = datetime.now()
#         sql = "INSERT INTO unamityJPNRaceDetail_from_april_2023 (eventId, eventURL,eventName,eventNumber,eventime,eventTotalRunner," \
#               "eventStatus,eventclass,eventStakesLevel,meetingVenue,eventTrackCondition,eventTrackSurface," \
#               "eventWeather,eventDistance,eventDistanceUnit,eventPrizeMoney,eventPriceMoneyCurrency,eventCreatedAt," \
#               "eventUpdatedAt,created,updated) " \
#               "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s)"
#
#         sql = sql % ("'"+eventId+"'", "'"+eventURL+"'", "'"+eventName+"'", "'"+eventNumber+"'", "'"+eventime+"'",  "'"+str(eventTotalRunner)+"'",
#                      "'"+eventStatus+"'", "'"+eventclass+"'","'"+eventStakesLevel+"'","'"+meetingVenue+"'","'"+eventTrackCondition+"'",
#                      "'"+eventTrackSurface+"'","'"+eventWeather+"'","'"+eventDistance+"'","'"+eventDistanceUnit+"'","'"+eventPrizeMoney+"'",
#                      "'"+eventPriceMoneyCurrency+"'","'"+eventCreatedAt+"'","'"+eventUpdatedAt+"'","'"+str(tim)+"'","'"+str(tim)+"'")
#
#         print(sql)
#         cursor.execute(sql)
#         conn.commit()
#
#         reatb = response.xpath('//tr[@class="odd-row" or @class="even-row" or @class="odd" or @class="even"]')
#         for tr in reatb:
#             horsePosition = tr.xpath('.//td[1]//text()').extract_first()
#             horseBarrierNumber = tr.xpath('.//td[2]//text()').extract_first()
#             horseMargin = tr.xpath('.//td[15]//text()').extract_first()
#             horseTime = tr.xpath('.//td[14]//text()').extract_first()
#             horseWeight = ''.join([x for x in tr.xpath('.//td[10]//text()').extract()])
#             horseStartingPrice = tr.xpath('.//td[11]//text()').extract_first()
#             horseNumber = tr.xpath('.//td[3]//text()').extract_first()
#             horseWeightCarried = tr.xpath('.//td[6]//text()').extract_first()
#             horseName = tr.xpath('.//td[4]//@href').extract_first().replace("'","´")
#             horselink = 'https:' + horseName.replace('top','pedigree')
#             horseJocky = ''.join([x for x in tr.xpath('.//td[7]//text()').extract()]).replace("'","´")
#             horseTrainer = ''.join([x for x in tr.xpath('.//td[8]//text()').extract()]).replace("'","´")
#             horseSex = ''.join([x for x in tr.xpath('.//td[5]//text()').extract()])
#             yield scrapy.Request(url=horselink, callback=self.parse_inside_horse, dont_filter=True,
#                                  meta={'eventURL': eventURL, 'eventId': eventId, 'horseJocky': horseJocky, 'sex': horseSex,
#                                        'rage': runarage, 'horsePosition': horsePosition,'horseNumber':horseNumber,'horseMargin':horseMargin,
#                                        'horseWeight': horseWeight,'horseWeightCarried':horseWeightCarried,'horseTime':horseTime,
#                                        'horseBarrierNumber':horseBarrierNumber,'horseStartingPrice':horseStartingPrice
#                                        }
#                                  )
#
#         # reatb =response.xpath('//tr[@class="odd-row" or @class="even-row" or @class="odd" or @class="even"]//td')
#         # print('trscount',len(reatb))
#         # for trtd in range(len(reatb)):
#         #     horseName = reatb[trtd].xpath('.//a//@href').extract_first()
#         #     horseJocky = ''
#         #     horseSex = ''
#         #     horsePosition = ''
#         #     if horseName is not None and '/horse_top.php' in horseName:
#         #         print('hhhhh', horseName)
#         #         horsePosition = reatb[trtd - 4].xpath('.//text()').extract_first()
#         #         sexage = reatb[trtd+1].xpath('.//text()').extract_first()
#         #         if len(sexage) == 2:
#         #             horseSex = sexage
#         #
#         #
#         #         horseJockyhref = reatb[trtd+4].xpath('.//a//@href').extract_first()
#         #         if '/database_jockey_' in horseJockyhref:
#         #             horseJocky = reatb[trtd + 4].xpath('.//a//text()').extract_first()
#         #             # print('horseJocky nm',horseJocky)
#         #     #from horse links /horse data
#         #     # if horseName is not None and '/horse_top.php' in horseName:
#         #         horselink = 'https:' + horseName.rehorsePosition('top','pedigree')
#         #         print('hourseurl',horselink)
#
#     def parse_inside_horse(self, response):
#         eventURL = response.meta['eventURL']
#         eventId = response.meta['eventId']
#         horseJocky = response.meta['horseJocky']
#         horseSex = response.meta['sex']
#         horseage = int(''.join(filter(str.isdigit, horseSex)))
#         horseNumber = response.meta['horseNumber']
#         horseMargin = response.meta['horseMargin']
#         horseWeight = response.meta['horseWeight']
#         horseWeightCarried = response.meta['horseWeightCarried']
#         horsePosition = response.meta['horsePosition']
#         horseTime = response.meta['horseTime']
#         horseBarrierNumber = response.meta['horseBarrierNumber']
#         horseStartingPrice = response.meta['horseStartingPrice']
#         horseurl = response.url
#         horseid = horseurl.split('=')[-1]
#         horsename = ''.join([x for x in response.xpath('//table[@class="h2_table"]//h2//text()').extract()]).replace("'","´")
#         lp = ''.join([x for x in response.xpath('//td[@width="250"]//text()').extract()]).replace('\n', '')
#         horseDOB = lp.split()[-1]
#         horseYOB = horseDOB[-4:]
#         if len(lp.split()) == 3:
#             horseColour = lp.split()[1]
#         else:
#             horseColour = lp.split()[1]+' '+lp.split()[2]
#         breeder = ''.join([x for x in response.xpath('//th[contains(text(),"Breeder")]/following-sibling::td//text()').extract()])
#         horseOwner = ''.join([x for x in response.xpath('//th[contains(text(),"Owner")]/following-sibling::td//text()').extract()]).replace("'","´")
#         horseTrainer = ''.join([x for x in response.xpath('//th[contains(text(),"Trainer(Belong)")]/following-sibling::td//text()').extract()]).replace("'","´")
#         horsePriceMoneyWon1 = ''.join([x for x in response.xpath('//th[contains(text(),"Earnings")]/following-sibling::td//text()').extract()])
#         horsePriceMoneyWon = horsePriceMoneyWon1.split('(')[0]
#         horsePriceMoneyWonCurrency = horsePriceMoneyWon1.split('(')[1].replace(')', '')
#         horseSire = response.css('tr.blank:nth-child(1) > td:nth-child(1)').xpath('.//text()').extract()[0].replace("'","´")
#         horseSireYOB = response.css('tr.blank:nth-child(1) > td:nth-child(1)').xpath('.//text()').extract()[1][:4]
#         horseDam = response.css('tr.blank:nth-child(17) > td:nth-child(1)').xpath('.//text()').extract()[0].replace("'","´")
#         horseDamYOB = response.css('tr.blank:nth-child(17) > td:nth-child(1)').xpath('.//text()').extract()[1][:4]
#         horseSireSire = response.css('tr.blank:nth-child(1) > td:nth-child(2)').xpath('.//text()').extract()[0].replace("'","´")
#         horseSireSireYOB = response.css('tr.blank:nth-child(1) > td:nth-child(2)').xpath('.//text()').extract()[1][:4]
#         horseSireDam = response.css('tr.blank:nth-child(9) > td:nth-child(1)').xpath('.//text()').extract()[0].replace("'","´")
#         horseSireDamYOB = response.css('tr.blank:nth-child(9) > td:nth-child(1)').xpath('.//text()').extract()[1][:4]
#         horseDamSire = response.css('tr.blank:nth-child(17) > td:nth-child(2)').xpath('.//text()').extract()[0].replace("'","´")
#         horseDamSireYOB = response.css('tr.blank:nth-child(17) > td:nth-child(2)').xpath('.//text()').extract()[1][:4]
#         horseDamDam = response.css('tr.blank:nth-child(25) > td:nth-child(1)').xpath('.//text()').extract()[0].replace("'","´")
#         horseDamDamYOB = response.css('tr.blank:nth-child(25) > td:nth-child(1)').xpath('.//text()').extract()[1][:4]
#         horseDamDamSire = response.css('tr.blank:nth-child(25) > td:nth-child(2)').xpath('.//text()').extract()[0].replace("'","´")
#         horseDamDamSireYOB = response.css('tr.blank:nth-child(25) > td:nth-child(2)').xpath('.//text()').extract()[1][:4]
#         horseCOB = ''
#         horseSireCOB = ''
#         horseDamCOB = ''
#         horseWeightCarriedUnit = 'Kg'
#         horseCreatedAt = ''
#         horseUpdatedAt = ''
#         horseWeightUnit = 'Kg'
#         runnersp = ''
#         horseComment = ''
#
#         # csvfilnm2 = 'UnamityHorseData2012DB.csv'
#         # csvRow2 = (horseid,eventId,horseurl,horseNumber,horsename,horseSex,horseCOB,horseage,horseDOB,horseYOB,horseColour,
#         #            horseSire,horseSireYOB,horseDam,horseDamYOB,horseDamSire,horseDamSireYOB,horseSireSire,horseSireSireYOB,
#         #            horseSireDam,horseSireDamYOB,horseDamDam,horseDamDamYOB,horseDamDamSire,horseDamDamSireYOB,
#         #            horseStartingPrice,horsePosition,horseMargin,horsePriceMoneyWon,horsePriceMoneyWonCurrency,
#         #            horseWeightCarried,horseWeightCarriedUnit,horseJocky,horseTrainer,horseOwner,horseComment,
#         #            horseCreatedAt,horseUpdatedAt,horseWeight,horseWeightUnit,horseTime,horseBarrierNumber)
#         # with open(csvfilnm2, "a", encoding="utf-8", newline='') as fp2:
#         #     wr = csv.writer(fp2, dialect='excel')
#         #     wr.writerow(csvRow2)
#
#         # with connection.cursor() as cursor:
#         sql = "INSERT INTO unamityJPNHorse_from_april_2023(horseid,eventId,horseurl,horseNumber,horsename,horseSex,horseCOB,horseAge," \
#               "horseDOB,horseYOB,horseColour,horseSire,horseSireYOB,horseDam,horseDamYOB,horseDamSire,horseDamSireYOB," \
#               "horseSireSire,horseSireSireYOB,horseSireDam,horseSireDamYOB,horseDamDam,horseDamDamYOB,horseDamDamSire," \
#               "horseDamDamSireYOB,horseStartingPrice,horsePosition,horseMargin,horsePriceMoneyWon,horsePriceMoneyWonCurrency," \
#               "horseWeightCarried,horseWeightCarriedUnit,horseJocky,horseTrainer,horseOwner,horseComment," \
#               "horseCreatedAt,horseUpdatedAt,horseWeight,horseWeightUnit,horseTime,horseBarrierNumber)" \
#               "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s , %s,%s, %s, %s, %s, %s , %s,%s, %s, %s, %s, %s, %s, %s," \
#               " %s, %s, %s, %s , %s,%s, %s, %s, %s, %s , %s,%s, %s, %s, %s , %s,%s, %s, %s)"
#
#         sql = sql % (horseid,"'"+eventId+"'","'"+horseurl+"'",horseNumber,"'"+horsename+"'","'"+horseSex+"'",
#                      "'"+horseCOB+"'",horseage,"'"+horseDOB+"'",horseYOB,"'"+horseColour+"'","'"+horseSire+"'",
#                      horseSireYOB,"'"+horseDam+"'",horseDamYOB,"'"+horseDamSire+"'",horseDamSireYOB,
#                      "'"+horseSireSire+"'",horseSireSireYOB,
#                      "'"+horseSireDam+"'",horseSireDamYOB,"'"+horseDamDam+"'",horseDamDamYOB,"'"+horseDamDamSire+"'",
#                      horseDamDamSireYOB,"'"+horseStartingPrice+"'","'"+horsePosition+"'","'"+str(horseMargin)+"'","'"+horsePriceMoneyWon+"'",
#                      "'"+horsePriceMoneyWonCurrency+"'",horseWeightCarried,"'"+horseWeightCarriedUnit+"'","'"+horseJocky+"'",
#                      "'"+horseTrainer+"'","'"+horseOwner+"'","'"+horseComment+"'","'"+horseCreatedAt+"'","'"+horseUpdatedAt+"'","'"+horseWeight+"'",
#                      "'"+horseWeightUnit+"'","'"+horseTime+"'",horseBarrierNumber)
#
#         print(sql)
#         cursor.execute(sql)
#         conn.commit()
#
#         formdataurl = horseurl.replace('pedigree','top')
#         print('formurl.....',formdataurl)
#         yield scrapy.Request(url=formdataurl, callback=self.parse_inside_horse_formdata, dont_filter=True,
#                              meta={'horseurl':horseurl,'horseOwner':horseOwner,'horsename':horsename,'horseYOB':horseYOB,'horseDam':horseDam}
#                              )
#
#     def parse_inside_horse_formdata(self,response):
#         horseHistoryURL = response.meta['horseurl']
#         horseid = horseHistoryURL.split('=')[-1]
#         horseHistoryOwner = response.meta['horseOwner']
#         horseHistoryHorseName = response.meta['horsename']
#         horseHistoryYOB = response.meta['horseYOB']
#         horseHistoryDam = response.meta['horseDam']
#         horseHistoryCreatedAt = ''
#         horseHistoryUpdatedAt = ''
#         # lp = ''.join([x for x in response.xpath('//td[@width="250"]//text()').extract()]).rehorsePosition('\n', '')
#         # horseDOB = response.meta['horseDOB']
#         horsename = ''.join([x for x in response.xpath('//table[@class="h2_table"]//h2//text()').extract()])
#         formtb = response.xpath('//td[@class="border_color_gray"]//tr[@class="white2" or @class="gray"]')
#         for t in formtb:
#             horseHistoryDate = t.xpath('.//td[1]//text()').extract_first()
#             horseHistoryYear = horseHistoryDate[-4:]
#             horseHistoryPosition = t.xpath('.//td[9]//text()').extract_first()
#             horseHistoryTotalRun = t.xpath('.//td[4]//text()').extract_first()
#             horseHistoryWeight = t.xpath('.//td[12]//text()').extract_first()
#             horseHistoryWinner = t.xpath('.//td[20]//text()').extract_first().replace("'","´")
#             horseHistoryJocky = t.xpath('.//td[13]//a[1]//text()').extract_first().replace("'","´")
#             try:
#                 horseHistoryTrainer = t.xpath('.//td[13]//a[2]//text()').extract_first().replace("'","´")
#             except:
#                 horseHistoryTrainer = ''
#             try:
#                 horseHistoryRaceDetail = t.xpath('.//td[2]//a/@href').extract_first().replace("'","´")
#             except:
#                 horseHistoryRaceDetail = ''
#             horseHistoryBeatenBy = ''
#
#             # csvfilnm3 = 'UnamityHorseFormData2012DB.csv'
#             # csvRow3 = (horseid,horseHistoryURL,horseHistoryYear,horseHistoryDate,horseHistoryPosition,horseHistoryTotalRun,
#             #            horseHistoryWeight,horseHistoryWinner,horseHistoryBeatenBy,horseHistoryOwner,horseHistoryTrainer,
#             #            horseHistoryJocky,horseHistoryRaceDetail,horseHistoryHorseName,horseHistoryYOB,horseHistoryDam,
#             #            horseHistoryCreatedAt,horseHistoryUpdatedAt)
#             # with open(csvfilnm3, "a", encoding="utf-8", newline='') as fp3:
#             #     wr = csv.writer(fp3, dialect='excel')
#             #     wr.writerow(csvRow3)
#
#             # with connection.cursor() as cursor:
#             sql = "INSERT INTO unamityJPNHorseForm_from_april_2023(horseid,horseHistoryURL,horseHistoryYear,horseHistoryDate," \
#                   "horseHistoryPosition," \
#                   "horseHistoryTotalRun,horseHistoryWeight,horseHistoryWinner,horseHistoryBeatenBy,horseHistoryOwner," \
#                   "horseHistoryTrainer,horseHistoryJocky,horseHistoryRaceDetail,horseHistoryHorseName," \
#                   "horseHistoryYOB,horseHistoryDam,horseHistoryCreatedAt,horseHistoryUpdatedAt)" \
#                   "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s , %s,%s, %s, %s, %s, %s , %s,%s, %s)"
#
#             sql = sql % (horseid,"'"+horseHistoryURL+"'",horseHistoryYear,"'"+horseHistoryDate+"'","'"+horseHistoryPosition+"'",
#                          horseHistoryTotalRun,horseHistoryWeight,"'"+horseHistoryWinner+"'","'"+horseHistoryBeatenBy+"'",
#                          "'"+horseHistoryOwner+"'","'"+horseHistoryTrainer+"'","'"+horseHistoryJocky+"'","'"+str(horseHistoryRaceDetail)+"'",
#                          "'"+horseHistoryHorseName+"'",horseHistoryYOB,"'"+horseHistoryDam+"'","'"+horseHistoryCreatedAt+"'","'"+horseHistoryUpdatedAt+"'")
#
#             print(sql)
#             cursor.execute(sql)
#             conn.commit()
#
#         # try:
#         #     totalresulit = response.xpath('//td[contains(text()," of ")]//text()').extract_first().split()[-1]
#         # except:
#         #     totalresulit = '2'
#         # pgs = int(totalresulit)/20
#         # if pgs>1 and int(totalresulit)>20:
#         #     print('in post request...............................................................')
#         #     for pg in range(2,int(pgs)+2):
#         #         data = {
#         #             'type': '1',
#         #             'code': str(horseid),
#         #             'grade': '0',
#         #             'page': str(pg),
#         #             'max_row': '20',
#         #         }
#         #
#         #         headers = {
#         #             'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36',
#         #             'Accept': 'text/plain, */*; q=0.01',
#         #             'Accept-Language': 'en-US,en;q=0.5',
#         #             'X-Requested-With': 'XMLHttpRequest',
#         #             'Origin': 'https://umanity.jp',
#         #             'Connection': 'keep-alive',
#         #             'Referer': 'https://umanity.jp/en/racedata/db/horse_top.php?code='+str(horseid),
#         #             'Sec-Fetch-Dest': 'empty',
#         #             'Sec-Fetch-Mode': 'cors',
#         #             'Sec-Fetch-Site': 'same-origin',
#         #         }
#         #         cookies = {
#         #             # '_ga': 'GA1.2.220645592.1650624015',
#         #             # '__gads': 'ID=e808c327b0e95721-2222e6769cd200ea:T=1650624018:RT=1650624018:S=ALNI_Mb2QCMOE7CBqMy_YVaG0gQAEEMoWA',
#         #             'VIEWSITE': 'PC',
#         #             # '__gpi': 'UID=0000066328608b46:T=1654498668:RT=1655091715:S=ALNI_MYfUipNneWnFifKfJPERdcYKDMCKA',
#         #             # 'racecard_jra': 'race_8_1',
#         #             # '_gid': 'GA1.2.1625339252.1655091717',
#         #         }
#         #         yield FormRequest(url='https://umanity.jp/en/racedata/db/update_result_view.php', callback=self.parse_inside_horse_formdata, dont_filter=True,
#         #                           headers = headers,
#         #                           # cookies=cookies,
#         #                           formdata = data,
#         #                              meta={'dam': dam,'horseDOB':horseDOB,'horseurl':horseurl}
#         #                              )
