# import scrapy
# import csv
# from csv import reader
# from scrapy_scrapingbee import ScrapingBeeRequest
# import pymysql.cursors
# import datetime
#
# connection = pymysql.connect(host='localhost',
#                              user='root',
#                              password='root',
#                              db='scrape', use_unicode=True, charset="utf8")
#
#
# headers = {
#     'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
#     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
#     'Accept-Language': 'en-US,en;q=0.5',
#     'Connection': 'keep-alive',
# }
#
# class workspider(scrapy.Spider):
#     name = "ricadoscrape"
#
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': [
#             'Url','Main Category','Subcategory 1','Subcategory 2','Subcategory 3','Subcategory 4',
#             'Name','Price','Description'
#         ]
#     };
#
#     def start_requests(self):
#         maincats = []
#         subcat1s = []
#         subcat2s = []
#         subcat3s = []
#         subcat4s = []
#         links = []
#         with open('ricadoAppleCategory.csv', 'r', encoding="utf8",errors='ignore') as read_obj:
#             csv_reader = reader(read_obj)
#             for row in csv_reader:
#                 maincat = row[0]
#                 maincats.append(maincat)
#                 subcat1 = row[1]
#                 subcat1s.append(subcat1)
#                 subcat2 = row[2]
#                 subcat2s.append(subcat2)
#                 subcat3 = row[3]
#                 subcat3s.append(subcat3)
#                 subcat4 = row[4]
#                 subcat4s.append(subcat4)
#                 link = row[5]
#                 links.append(link)
#         for i in range(1, len(links)):
#             ur = links[i]
#             yield ScrapingBeeRequest(url=ur, callback=self.parse_inside, dont_filter=True,headers=headers,
#                                      params={'render_js':True,'premium_proxy':False,'country_code':'us'},
#                                      meta={'maincat':maincats[i],'sub1cat': subcat1s[i],'sub2cat':subcat2s[i],
#                                            'sub3cat':subcat3s[i],'sub4cat':subcat4s[i]})
#
#     def parse_inside(self,response):
#         url = response.url
#         maincat = response.meta['maincat']
#         sub1cat = response.meta['sub1cat']
#         sub2cat = response.meta['sub2cat']
#         sub3cat = response.meta['sub3cat']
#         sub4cat = response.meta['sub4cat']
#         allpages = response.xpath("//a[contains(@class, 'MuiButtonBase-root MuiPaginationItem-root')]//text()").extract()
#         # print(allpages)
#
#         try:
#             lspage = allpages[-1]
#         except:
#             lspage = '1'
#         for j in range(1,int(lspage)+1):
#             pgurl = url+'?page='+str(j)
#             yield ScrapingBeeRequest(url=pgurl, callback=self.parse_inside_products, dont_filter=True, headers=headers,
#                                      params={'render_js': True, 'premium_proxy': False, 'country_code': 'us'},
#                                      meta={'maincat':maincat,'sub1cat':sub1cat,
#                                            'sub2cat':sub2cat,'sub3cat':sub3cat,'sub4cat':sub4cat})
#
#     def parse_inside_products(self, response):
#         maincat = response.meta['maincat']
#         sub1cat = response.meta['sub1cat']
#         sub2cat = response.meta['sub2cat']
#         sub3cat = response.meta['sub3cat']
#         sub4cat = response.meta['sub4cat']
#         productlinks =response.xpath("//*[contains(@class, 'MuiGrid-root MuiGrid-item ')]//@href").extract()
#         # print('products lins.................',productlinks)
#         for l in productlinks:
#             link = 'https://www.ricardo.ch'+l
#             yield ScrapingBeeRequest(url=link, callback=self.parseInsidePrdData,dont_filter=True,
#                                      params={'render_js':True,'premium_proxy':False,'country_code':'us'},
#                                      meta={'maincat': maincat, 'sub1cat': sub1cat,
#                                            'sub2cat': sub2cat, 'sub3cat': sub3cat, 'sub4cat': sub4cat}
#                                      )
#
#     def parseInsidePrdData(self, response):
#         maincat = response.meta['maincat']
#         sub1cat = response.meta['sub1cat']
#         sub2cat = response.meta['sub2cat']
#         sub3cat = response.meta['sub3cat']
#         sub4cat = response.meta['sub4cat']
#         name = response.xpath('//h1[@class="style_title__bX9DL"]//text()').extract_first()
#         price = response.xpath("//*[contains(@class, 'style_price__')]//text()").extract_first() or response.xpath("//div[contains(@class, 'jss77 style_price__')]//text()").extract_first()
#         description = '\n'.join([x.strip() for x in response.xpath('//div[@name="description-panel"]//text()').extract()]).replace('\n\n','')
#         # breadcrumb = response.xpath('//li[@itemprop="itemListElement"]//text()').extract()
#         url = response.url
#         articalnumber = url.split('/')[-2].split('-')[-1]
#         ct = datetime.datetime.now()
#         # print(ct,"current time")
#         yield {
#             'Url': response.url,
#             'Main Category': maincat,
#             'Subcategory 1': sub1cat,
#             'Subcategory 2': sub2cat,
#             'Subcategory 3': sub3cat,
#             'Subcategory 4': sub4cat,
#             'Name': name,
#             'Price': price,
#             'Description': description
#         }
#
#         with connection.cursor() as cursor:
#             sql = "SELECT * FROM `ricado` WHERE `article_number`=%s"
#             cursor.execute(sql, (articalnumber))
#             result = cursor.fetchone()
#             # print(result)
#             # connection.commit()
#             if result is None:
#                 sql = "INSERT INTO ricado (url,article_number,main_category,subcategory_1,subcategory_2,subcategory_3," \
#                       "subcategory_4,name,price,description,first_scrape_time)" \
#                       "VALUES (%s,%s, %s, %s, %s, %s, %s, %s, %s, %s , %s)"
#
#                 cursor.execute(sql,
#                                (url,articalnumber,maincat,sub1cat,sub2cat,sub3cat,sub4cat,name,price,description,ct)
#                                )
#
#                 connection.commit()
#
#             else:
#                 sql = "UPDATE ricado SET  main_category = %s,subcategory_1 = %s ,subcategory_2 = %s,subcategory_3 = %s," \
#                       "subcategory_4 = %s,name = %s,last_price = %s," \
#                       "description = %s,last_scrape_time = %s " \
#                       "WHERE article_number =%s"
#
#                 cursor.execute(sql, (maincat,sub1cat,sub2cat,sub3cat,sub4cat,name,price,description,
#                                      ct,articalnumber))
#                 connection.commit()
