import scrapy
import csv

class WorkSpider(scrapy.Spider):
    name = 'healthtourism'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            # 'Hospital Link','Hospital Name','Location','Business Top Accreditations','Overview',
            # 'Departments and specialty centers','Services','Accommodations','Facilities','Certifications','Languages',
            # 'Liability','Numbers','Doctors Link'

            'drurl','hospital','detail','speciality','languages','graduated','about','education','certifications',
            'professionalExperience','memberships'
        ]
    };

    urls = []
    for p in range(1,6):
        l = 'https://www.health-tourism.com/hospitals-india/&p='+str(p)
        urls.append(l)

    def start_requests(self):
        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        hslinks = response.xpath('//h2//a//@href').extract()
        for hslink in hslinks:
            l = 'https://www.health-tourism.com'+hslink
            yield scrapy.Request(url=l,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'})
        # print l ,dont_filter=True


    def parseInside(self,response):
        hsLink = response.url
        hsName = response.xpath('//h1//text()').extract_first()
        location = response.xpath('//h2[contains(text(),"Location")]/following-sibling::p//text()').extract_first()
        businusTopAccelets = ','.join([x.strip() for x in response.xpath('//span[@class="businessTopAccreditationText"]//text()').extract()]).replace(',,',',')
        b = '\n'.join([x.strip() for x in response.xpath('//div[@class="contentCon"]//text()').extract()])
        overview = b.split('Overview')[1].split('Departments and specialty centers at')[0].replace('\n\n','\n')
        depAndSpeciality = '\n'.join([x.strip() for x in response.xpath('//div[@class="specialtyCenters"]//ul//text()').extract()]).replace('\n\n','\n')
        services = '\n'.join([x.strip() for x in response.xpath('//div[@class="services"]//ul//text()').extract()]).replace('\n\n','\n')
        accomodation = '\n'.join([x.strip() for x in response.xpath('//h3[contains(text(),"Accommodations:")]/following-sibling::ul//text()').extract()]).replace('\n\n','\n')
        facilities = '\n'.join([x.strip() for x in response.xpath('//h3[contains(text(),"Facilities:")]/following-sibling::ul//text()').extract()]).replace('\n\n','\n')
        certifications = '\n'.join([x.strip() for x in response.xpath('//h3[contains(text(),"Certifications:")]/following-sibling::ul//text()').extract()]).replace('\n\n','\n')
        languages = '\n'.join([x.strip() for x in response.xpath('//h3[contains(text(),"Languages:")]/following-sibling::ul//text()').extract()]).replace('\n\n','\n')
        liability = '\n'.join([x.strip() for x in response.xpath('//h3[contains(text(),"Liability:")]/following-sibling::ul//text()').extract()]).replace('\n\n','\n')
        numbers = '\n'.join([x.strip() for x in response.xpath('//h3[contains(text(),"Numbers:")]/following-sibling::ul//text()').extract()]).replace('\n\n','\n')
        doctorslink = hsLink + 'doctors/'

        # yield {
        #     'Hospital Link': hsLink,
        #     'Hospital Name': hsName,
        #     'Location': location,
        #     'Business Top Accreditations': businusTopAccelets,
        #     'Overview': overview,
        #     'Departments and specialty centers': depAndSpeciality,
        #     'Services': services,
        #     'Accommodations': accomodation,
        #     'Facilities': facilities,
        #     'Certifications': certifications,
        #     'Languages': languages,
        #     'Liability': liability,
        #     'Numbers': numbers,
        #     'Doctors Link': doctorslink
        # }
        yield scrapy.Request(url=doctorslink, callback=self.parseInsideDoctors, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                             meta={'hospital':hsName})

    def parseInsideDoctors(self,response):
        drhslink = response.url
        hospital = response.meta['hospital']
        try:
            pagnum = ''.join([x.strip() for x in response.xpath('//div[@class="paginationCon"]//h4//text()').extract()]).split('Page 1 of ')[1].split(')')[0]
        except:
            pagnum = '1'
        for p in range(1,int(pagnum)+1):
            pagination = drhslink+'&p='+str(p)
            yield scrapy.Request(url=pagination, callback=self.parseInsideDoctorsLinks,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},meta={'hospital':hospital})

    def parseInsideDoctorsLinks(self,response):
        hospital = response.meta['hospital']
        drslinks = response.xpath('//h1//a//@href').extract()
        for drl in drslinks:
            drdetail = 'https://www.health-tourism.com'+drl
            yield scrapy.Request(url=drdetail, callback=self.parseInsideDoctorsDetail,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},meta={'hospital':hospital})

    def parseInsideDoctorsDetail(self,response):
        hospital = response.meta['hospital']
        drlink = response.url
        drname = response.xpath('//h1[@class="staffName"]//text()').extract_first()
        detail = '\n'.join([x.strip() for x in response.xpath('//td[@class="TopDetails"]//text()').extract()]).replace(drname,'').replace('\n\n','\n')
        speciality = ''.join([x.strip() for x in response.xpath('//td[contains(text(),"Speciality")]/following-sibling::td//text()').extract()])
        languages = ''.join([x.strip() for x in response.xpath('//td[contains(text(),"Languages")]/following-sibling::td//text()').extract()])
        graduated = ''.join([x.strip() for x in response.xpath('//td[contains(text(),"Graduated")]/following-sibling::td//text()').extract()])
        about = ''.join([x.strip() for x in response.xpath('//td[contains(text(),"About")]/following-sibling::td//text()').extract()])
        education = ''.join([x.strip() for x in response.xpath('//td[contains(text(),"Education")]/following-sibling::td//text()').extract()])
        certifications = ''.join([x.strip() for x in response.xpath('//td[contains(text(),"Certifications")]/following-sibling::td//text()').extract()])
        professionalExperience = ''.join([x.strip() for x in response.xpath('//td[contains(text(),"Professional Experience")]/following-sibling::td//text()').extract()])
        memberships = ''.join([x.strip() for x in response.xpath('//td[contains(text(),"Memberships")]/following-sibling::td//text()').extract()])

        yield {
            'drurl': drlink,
            'hospital': hospital,
            'detail': detail,
            'speciality': speciality,
            'languages': languages,
            'graduated': graduated,
            'about': about,
            'education': education,
            'certifications': certifications,
            'professionalExperience': professionalExperience,
            'memberships': memberships
        }

