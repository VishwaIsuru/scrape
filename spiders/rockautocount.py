import scrapy
import csv

class WorkSpider(scrapy.Spider):
    name = 'rockautocount'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': ['manufacture','partGroup','partType','numberOfParts','partNUmbers']
    };

    urls = ["https://www.rockauto.com/docs/warranty"]

    def start_requests(self):
        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):
        dlinks = response.xpath('//table[@class="tblz w100"]//td[@style="padding-left: 20px; width: 45%;"]//a//@href').extract()
        for dlink in dlinks:
            l = "https://www.rockauto.com"+dlink
            yield scrapy.Request(url=l,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'})
        # print l ,dont_filter=True

    def parseInside(self,response):
        drLink = response.url
        # drName = ''.join([x.strip() for x in response.xpath('//h1[@id="ga_doctor_name"]//text()').extract()])
        catslinks = response.xpath('//td[@class="nlabel nlbl-docolumns"]//a//@href').extract()
        for clink in catslinks:
            l = "https://www.rockauto.com"+clink
            yield scrapy.Request(url=l,callback=self.parseInsideFinal,headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parseInsideFinal(self,response):
        manuacture = response.xpath('//select[@id="manufacturer_partsearch_007"]//option[@selected]//text()').extract_first()
        partgroup = response.xpath('//select[@id="partgroup_partsearch_007"]//option[@selected]//text()').extract_first()
        parttype = response.xpath('//select[@id="parttype_partsearch_007"]//option[@selected]//text()').extract_first()
        partnumberscount = len(response.xpath('//td[@class="nlabel nlbl-docolumns"]//text()').extract())
        partnumbers = response.xpath('//td[@class="nlabel nlbl-docolumns"]//text()').extract()

        yield {
            'manufacture': manuacture,
            'partGroup': partgroup,
            'partType': parttype,
            'numberOfParts' : partnumberscount,
            'partNUmbers': partnumbers
        }