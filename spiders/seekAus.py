import scrapy
import csv
import pandas as pd
import re
# from sshtunnel import SSHTunnelForwarder
import pymysql
from datetime import datetime
import requests
# server = SSHTunnelForwarder(
#     '89.116.31.168',
#     ssh_username='root',
#     ssh_password='3Pu142SrTsJKWmLQ',
#     remote_bind_address=('127.0.0.1', 3306)
# )
# server.start()
#
# conn = pymysql.connect(
#     host='localhost',
#     port=server.local_bind_port,
#     user='root',
#     password='root',
#     db='scrape_seek'
# )

# cursor = conn.cursor()

class WorkSpider(scrapy.Spider):
    name = 'seek'
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'jobId','jobUrl','jobTitle','advertiserName','jobDetailLocation','jobDetailClassifications','jobDetailWorkType',
            'jobDetailSalary','posted','scrapeDate','emails','phone_numbers','phone_numbers_2','companyOverview',
            'thePosition','responsibilities','requirments',
            'jobAdDetails'
        ]
    };

    def start_requests(self):
        urls = [
            ############### Construction ##########################
            'https://www.seek.com.au/jobs-in-construction/contracts-management?daterange=1',
            # 'https://www.seek.com.au/jobs-in-construction/estimating?daterange=1',
            # 'https://www.seek.com.au/jobs-in-construction/foreperson-supervisors?daterange=1',
            # 'https://www.seek.com.au/jobs-in-construction/health-safety-environment?daterange=1',
            # 'https://www.seek.com.au/jobs-in-construction/management?daterange=1',
            # 'https://www.seek.com.au/jobs-in-construction/planning-scheduling?daterange=1',
            # 'https://www.seek.com.au/jobs-in-construction/plant-machinery-operators?daterange=1',
            # 'https://www.seek.com.au/jobs-in-construction/project-management?daterange=1',
            # 'https://www.seek.com.au/jobs-in-construction/quality-assurance-control?daterange=1',
            # 'https://www.seek.com.au/jobs-in-construction/surveying?daterange=1',
            # 'https://www.seek.com.au/jobs-in-construction/other?daterange=1',
            # ############### Engineering ##########################
            # 'https://www.seek.com.au/jobs-in-engineering/civil-structural-engineering?daterange=1',
            # 'https://www.seek.com.au/jobs-in-engineering/electrical-electronic-engineering?daterange=1',
            # 'https://www.seek.com.au/jobs-in-engineering/engineering-drafting?daterange=1',
            # 'https://www.seek.com.au/jobs-in-engineering/field-engineering?daterange=1',
            # 'https://www.seek.com.au/jobs-in-engineering/industrial-engineering?daterange=1',
            # 'https://www.seek.com.au/jobs-in-engineering/maintenance?daterange=1',
            # 'https://www.seek.com.au/jobs-in-engineering/management?daterange=1',
            # 'https://www.seek.com.au/jobs-in-engineering/materials-handling-engineering?daterange=1',
            # 'https://www.seek.com.au/jobs-in-engineering/process-engineering?daterange=1',
            'https://www.seek.com.au/jobs-in-engineering/project-engineering?daterange=1'
            # 'https://www.seek.com.au/jobs-in-engineering/project-management?daterange=1',
            # 'https://www.seek.com.au/jobs-in-engineering/supervisors?daterange=1',
            # 'https://www.seek.com.au/jobs-in-engineering/systems-engineering?daterange=1',
            # 'https://www.seek.com.au/jobs-in-engineering/water-waste-engineering?daterange=1',
            # 'https://www.seek.com.au/jobs-in-engineering/other?daterange=1',
            # ############### Mining, Resources & Energy ##########################
            # 'https://www.seek.com.au/jobs-in-mining-resources-energy/health-safety-environment?daterange=1',
            # 'https://www.seek.com.au/jobs-in-mining-resources-energy/management?daterange=1',
            # 'https://www.seek.com.au/jobs-in-mining-resources-energy/mining-engineering-maintenance?daterange=1',
            # 'https://www.seek.com.au/jobs-in-mining-resources-energy/mining-operations?daterange=1',
            # 'https://www.seek.com.au/jobs-in-mining-resources-energy/mining-processing?daterange=1',
            # 'https://www.seek.com.au/jobs-in-mining-resources-energy/natural-resources-water?daterange=1',
            # 'https://www.seek.com.au/jobs-in-mining-resources-energy/oil-gas-drilling?daterange=1',
            # 'https://www.seek.com.au/jobs-in-mining-resources-energy/oil-gas-engineering-maintenance?daterange=1',
            # 'https://www.seek.com.au/jobs-in-mining-resources-energy/oil-gas-operations?daterange=1',
            # 'https://www.seek.com.au/jobs-in-mining-resources-energy/oil-gas-production-refinement?daterange=1',
            # 'https://www.seek.com.au/jobs-in-mining-resources-energy/power-generation-distribution?daterange=1',
            # 'https://www.seek.com.au/jobs-in-mining-resources-energy/surveying?daterange=1',
            # 'https://www.seek.com.au/jobs-in-mining-resources-energy/other?daterange=1',
            ################# Trades & Services ##################
            # 'https://www.seek.com.au/jobs-in-trades-services/technicians?daterange=1',
            # 'https://www.seek.com.au/jobs-in-trades-services/welders-boilermakers?daterange=1'

        ]
        i=0
        print('INFO::startScrape...................')
        for u in urls:
            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/2010540'+str(i),
                                          'Connection':'keep-alive',
                                 }
                                 )
            i+=1

    def parse(self, response):
        jobs = ''.join([x.strip() for x in response.xpath("//h1[@id='SearchSummary']//text()").extract()]).replace('jobs','').replace(',','')
        print(jobs,'rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr')
        try:
            pgcount = int(jobs)/20
        except:
            pgcount = 1
        print(pgcount,'pgggggggg')
        if pgcount < 25:
            for pg in range(1,int(pgcount)+2):
                pgurl = response.url+'&page='+str(pg)
                print(pgurl,'ffffffffffffffffffffffffffffff')
                yield scrapy.Request(url=pgurl,callback=self.parseInsideJobs)
        else:
            salaryrange = ['0-30000','30000-40000','40000-50000','50000-60000','60000-70000','70000-80000','80000-100000',
                           '100000-120000','120000-150000','150000-200000','200000-250000','250000-350000']
            for slrang in salaryrange:
                pgurl = response.url+'&salaryrange='+slrang+'&salarytype=annual'
                yield scrapy.Request(url=pgurl,callback=self.parseInsidePaging)

    def parseInsidePaging(self, response):
        jobs = ''.join([x.strip() for x in response.xpath("//h1[@id='SearchSummary']//text()").extract()]).replace('jobs','').replace(',','')
        pgcount = int(jobs)/20
        if pgcount < 25:
            for pg in range(1,int(pgcount)+2):
                pgurl = response.url+'&page='+str(pg)
                yield scrapy.Request(url=pgurl,callback=self.parseInsideJobs)
        else:
            for pg in range(1,25):
                pgurl = response.url+'&page='+str(pg)
                yield scrapy.Request(url=pgurl,callback=self.parseInsideJobs)

    def parseInsideJobs(self, response):
        print('immmmmmmmmmmmmmmmm heerrrrrrrrrrrrrrrrrrrrrrrrrr')
        jobsids = response.xpath('//article//@data-job-id').extract()
        print(jobsids,'jobids')
        for jobid in jobsids:
            # check_query = "SELECT COUNT(*) FROM ScrapeSeekJobs WHERE jobid = %s"
            # cursor.execute(check_query, (jobid,))
            # # Fetch the result
            # (result,) = cursor.fetchone()

            if result == 0:
                joburlids = 'https://www.seek.com.au/job/'+str(jobid)
                yield scrapy.Request(url=joburlids,callback=self.parseInsideJobsData)
            else:
                print("Jobid already exists.")

    def parseInsideJobsData(self, response):
        scrapeDate = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        jobTitle = ''.join([x.strip() for x in response.xpath("//h1[@data-automation='job-detail-title']//text()").extract()])
        advertiser_name = ''.join([x.strip() for x in response.xpath("//*[@data-automation='advertiser-name']//text()").extract()])
        job_detail_location = ''.join([x.strip() for x in response.xpath("//*[@data-automation='job-detail-location']//text()").extract()])
        job_detail_classifications = ''.join([x.strip() for x in response.xpath("//*[@data-automation='job-detail-classifications']//text()").extract()])
        job_detail_work_type = ''.join([x.strip() for x in response.xpath("//*[@data-automation='job-detail-work-type']//text()").extract()])
        job_detail_salary = ''.join([x.strip() for x in response.xpath("//*[@data-automation='job-detail-salary']//text()").extract()])
        posted = response.xpath('//*[contains(text(),"Posted")]//text()').extract_first()
        # textfull = str(response.body)
        textfull = ''.join([x.strip() for x in response.xpath('//text()').extract()])
        email_pattern = r'[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}'
        emails = ", ".join(set(re.findall(email_pattern, textfull)))
        # phone_pattern = r'\+?61-\d{4}-\d{3}-\d{3}|\+?61-\d{1,4}-\d{3}-\d{3}|\(?\+?61\)? ?[2-478]? ?\d{4} ?\d{4}|\(?\d{2}\)? ?\d{4} ?\d{4}|\b\d{4} ?\d{3} ?\d{3}\b'
        # phone_pattern = r'\(?\+?61\)? ?[2-478]? ?\d{4} ?\d{4}|\(?\d{2}\)? ?\d{4} ?\d{4}|\b\d{4} ?\d{3} ?\d{3}\b'
        phone_pattern = r'\b\d{4} \d{3} \d{3}\b|\+\d{2}-\d{4}-\d{3}-\d{3}|\+\d{2} \d{4} \d{3} \d{3}|\b\d{10}\b'
        phone_numbers = ", ".join(set(re.findall(phone_pattern, textfull)))
        phone_numbers_2 = textfull.split('phoneNumber')[1].split(',')[0].replace('":','')
        companyOverview = response.xpath('//p[strong[text()="Company Overview:"]]/following-sibling::p[1]//text()').extract_first()
        thePosition = response.xpath('//p[strong[text()="The Position:"]]/following-sibling::p[1]//text()').extract_first()
        responsibilities = ', '.join([x.strip() for x in response.xpath('//p[contains(., "Responsibilities")]/following-sibling::ul[1]//text()').extract()])
        requirments = ', '.join([x.strip() for x in response.xpath('//p[contains(., "Requirements")]/following-sibling::ul[1]//text()').extract()])
        jobAdDetails = ''.join([x.strip() for x in response.xpath("//div[@data-automation='jobAdDetails']//text()").extract()])

        jobid_to_check = response.url.split('/')[-1]
        jobUrl = response.url
        # check_query = "SELECT COUNT(*) FROM ScrapeSeekJobs WHERE jobid = %s"
        # cursor.execute(check_query, (jobid_to_check,))
        # # Fetch the result
        # (result,) = cursor.fetchone()
        # if result == 0:
            # If jobid does not exist, insert the data
        # insert_query = "INSERT INTO ScrapeSeekJobs (jobid, jobUrl, jobTitle, advertiserName, jobDetailLocation, " \
        #                "jobDetailClassifications, jobDetailWorkType, jobDetailSalary, posted, scrapeDate, emails, " \
        #                "phone_numbers, phone_numbers_2, companyOverview, thePosition, responsibilities, requirments, " \
        #                "jobAdDetails) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # cursor.execute(insert_query, (jobid_to_check, jobUrl, jobTitle, advertiser_name, job_detail_location,
        #                               job_detail_classifications, job_detail_work_type, job_detail_salary, posted, scrapeDate, emails,
        #                               phone_numbers, phone_numbers_2, companyOverview, thePosition, responsibilities, requirments,
        #                               jobAdDetails))
        # conn.commit()
        print("Data inserted successfully.", jobid_to_check)
        # else:
        #     print("Jobid already exists.")

    def closed(self, reason):
        print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
        current_datetime = datetime.now()
        datetime_string = current_datetime.strftime('%Y-%m-%d %H:%M:%S')
        print(datetime_string)
        headers = {'Content-type': 'application/json',}
        json_data = {
            # 'text': 'Hello, World!Text 1111111111111111111111',
            'text':datetime_string+'Sky Sports Scrape Initiated.'
        }
        response = requests.post('https://hooks.slack.com/services/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', headers=headers, json=json_data)
    #     query = """
    #     SELECT *
    #     FROM ScrapeSeekJobs
    #     WHERE DATE(scrapeDate) = CURDATE();
    #     """
    #
    #     cursor = conn.cursor()
    #     cursor.execute(query)
    #     rows = cursor.fetchall()
    #
    #     # Get column headers
    #     column_headers = [i[0] for i in cursor.description]
    #
    #     # Define the CSV file name with today's date in DD_MM_YYYY format
    #     csv_filename = './seek_latest_csv_files/'+datetime.now().strftime('seek_data_%d_%m_%Y.csv')
    #
    #     # Write the rows to a CSV file
    #     with open(csv_filename, mode='w', newline='', encoding='utf-8') as file:
    #         writer = csv.writer(file)
    #         writer.writerow(column_headers)  # Write the headers
    #         writer.writerows(rows)  # Write the data
    #
    #     # Close the cursor and the connection
    #     cursor.close()
    #     conn.close()
    #
    #     print(f"Data has been written to {csv_filename}")

        # yield {
        #     'jobId': response.url.split('/')[-1],
        #     'jobUrl': response.url,
        #     'jobTitle': jobTitle,
        #     'advertiserName': advertiser_name,
        #     'jobDetailLocation': job_detail_location,
        #     'jobDetailClassifications': job_detail_classifications,
        #     'jobDetailWorkType': job_detail_work_type,
        #     'jobDetailSalary': job_detail_salary,
        #     'posted': posted,
        #     'scrapeDate': scrapeDate,
        #     'emails': emails,
        #     'phone_numbers': phone_numbers,
        #     'phone_numbers_2': phone_numbers_2,
        #     'companyOverview': companyOverview,
        #     'thePosition': thePosition,
        #     'responsibilities': responsibilities,
        #     'requirments': requirments,
        #     'jobAdDetails': jobAdDetails
        # }
