import scrapy
import csv
import pandas as pd

class workspider(scrapy.Spider):
    name = "amazonAriel2"
    custom_settings = {
        # specifies exported fields and order ASIN tikata. Url, ASIN,
        # product title, price, BSR, seller, category, subcategory, delivery timing, and inventory availability
        'FEED_EXPORT_FIELDS': [
            "ID", "ASIN", "Link",
            # "which_Search",
            "Product Title", "Ships From", "Sold by",
            "Best Seller Ranking in Automotive","Best Seller Name","Sub Category Ranking",
           'Sub Category Name', "Acutal price","Cross off Price","Ratings","Prime Status","BCCat",
           "BCSubCat1","BCSubCat2","BCSubCat3","Brand Name", "Item Weight", "Product Dimension",
           "Item Number", 'Manufacturer',"Manufacturer Number", "Reviews", "Shipping Weight",
           "Date First Available","Landing Image URL", "Availability"
        ]
    };

    def start_requests(self):
        print("heeeeeerrrr")
        col_list = ["ASIN",
                    # "which_Search"
                    ]
        df = pd.read_csv("D:\\Songs\\amazonWeekly\\amazonWeekly_21_08_24\\not_run_asins_21_08_24.csv", usecols=col_list,encoding = "unicode_escape")
        asin1 = df["ASIN"]
        # which_Search = df["which_Search"]
        #Catalog_Scrape_May_WFS
        header = {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64)" + str(
                313311111121111) + " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
            "Referer": "https://www.amazon.com",
            "Origin": "https://www.amazon.com",
            "Connection": "keep-alive",
        }
        # cookie = {"sp-cdn": "L5Z9:CA"}  # "CN for china"  CA for canada

        for i in range(0, len(asin1)):#
            asin = asin1[i]
            u = 'https://www.amazon.com/dp/' + asin
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers=header,
                                 # cookies=cookie,
                                 meta={'ID': str(i), 'LINK': u, 'ASIN': asin,
                                       # 'which_Search':which_Search[i]
                                       },
                                 dont_filter=True)

    def parse(self, response):
        # try:
        #     title = response.css('#productTitle').xpath('./text()').extract_first().strip()
        # except:
        title = ''.join([x.strip() for x in response.xpath('//h1[@id="title"]//text()').extract()])
        ratings = response.xpath('//span[@id="acrCustomerReviewText"]//text()').extract_first()
        primelogo = response.xpath('//div[@class="a-box-inner a-padding-base"]//img//@src').extract_first()
        if 'almPrimeSavings_feature_div' in str(response.body):
            primestatus = 'prime'
        else:
            primestatus = ''
        breadcum = ''.join([x.strip() for x in response.xpath(
            '//div[@id="wayfinding-breadcrumbs_feature_div"]//ul//li//text()').extract()])
        bccat = breadcum.split("›")[0]
        try:
            bcsubcat1 = breadcum.split("›")[1]
        except:
            bcsubcat1 = ''
        try:
            bcsubcat2 = breadcum.split("›")[2]
        except:
            bcsubcat2 = ''
        try:
            bcsubcat3 = breadcum.split("›")[3]
        except:
            bcsubcat3 = ''

        ships_from = ''
        soldBy = ''
        tr_ships_from = response.css('#tabular-buybox-container tr')
        for t in tr_ships_from:
            tdhead = ''.join(t.css('td')[0].xpath('.//text()').extract())
            tdcontent = ''.join(t.css('td')[1].xpath('.//text()').extract())
            if tdhead and 'ships from' in tdhead.lower():
                ships_from = tdcontent
        if ships_from:
            ships_from = ships_from.strip()
        else:
            sf_tr = response.css('.tabular-buybox-container span')
            for sf in range(len(sf_tr)):
                try:
                    if sf_tr[sf].xpath('./text()').extract_first().lower().strip() == 'ships from':
                        try:
                            ships_from = sf_tr[sf + 1].xpath('./text()').extract_first()
                        except:
                            pass
                    elif sf_tr[sf].xpath('./text()').extract_first().lower().strip() == 'sold by' and soldBy == '':
                        try:
                            soldBy = ''.join([x.strip() for x in sf_tr[sf + 1].xpath('.//text()').extract()])
                        except:
                            pass
                except:
                    pass
        if not soldBy or soldBy == '':
            soldBy = ''.join(
                response.css('#shipsFromSoldByInsideBuyBox_feature_div').xpath('.//text()').extract()).strip()
        if soldBy and soldBy == '':
            soldBy = response.css('#sellerProfileTriggerId').xpath('./text()').extract_first()
            if not soldBy:
                tr_solds = response.css('#tabular-buybox-container tr')
                for t in tr_solds:
                    tdhead = ''.join(t.css('td')[0].xpath('.//text()').extract())
                    tdcontent = ''.join(t.css('td')[1].xpath('.//text()').extract())
                    if 'sold by' in tdhead.lower():
                        soldBy = tdcontent
        if soldBy and 'P.when("seller-register-popover")' in soldBy:
            soldBy = soldBy.split('P.when("seller-register-popover")')[0].strip().split('\.')[0].split('\n')[0]
        try:
            s = soldBy.split(" by Amazon")[0]
            if s != soldBy:
                soldBy = soldBy.split(" by Amazon")[0] + " by Amazon."
            if len(soldBy) > 100:
                print(soldBy)
                return
        except:
            pass

        if len(ships_from)==0:
            ships_from = ''.join([x.strip() for x in response.xpath("//div[@id='fulfillerInfoFeature_feature_div']//span[@class='a-size-small offer-display-feature-text-message']//text()").extract()])

        if len(soldBy) == 0:
            soldBy = response.xpath('//div[@id="merchantInfoFeature_feature_div"]//span[@class="a-size-small offer-display-feature-text-message"]//text()').extract_first()

        additionalInfo = response.css('#productDetails_db_sections')

        trs = additionalInfo.xpath('.//table//tr')

        bestSellerRankInAutomative = ''
        subCategory = ''
        # later added
        brandName = ''
        itemWeight = ''
        productDimensions = ''
        itemNumber = ''
        manufectureNumber = ''
        manufacturer = ''
        reviews = ''
        shippingWeight = ''
        dateAvailable = ''
        landingImageUrl = ''
        availability = ''
        subCategoryName = ''
        for tr in trs:
            thStr = ''.join(tr.css('th').xpath('.//text()').extract()).lower()
            tdStr = ''.join(tr.css('td').xpath('.//text()').extract())

            if 'best sellers rank' in thStr:
                try:
                    subCategoryName = tdStr.split(" in ")[-1].strip()
                except:
                    pass
                bestSellerRankInAutomative = tdStr.split("in Automotive")[0].replace(',', '').strip().split("in")[
                    0].strip()
                try:
                    subCategory = '#' + tdStr.split(')')[1].split('#')[1].split('in')[0].strip()
                except:
                    pass
            elif 'review' in thStr.lower():
                rs = tr.css('td').xpath('./text()').extract()
                for r in rs:
                    if 'of' in r:
                        reviews = r.strip();
                        break
            elif 'shipping weight' in thStr.lower():
                shippingWeight = tdStr.strip()
            elif 'date' in thStr.lower() and 'available' in thStr.lower():
                dateAvailable = tdStr.strip()

        landingImageUrl = response.xpath('//img[@id="landingImage"]//@src').extract_first()
        availability = response.css('#availabilityInsideBuyBox_feature_div').xpath('.//text()').extract()
        availability = [a.strip() for a in availability]
        availability = ' '.join(availability)

        try:
            availability = availability.strip().split('.avail')[0]
        except:
            pass
        if not availability or availability == "":
            availability = \
            ''.join(response.css('#availability span').xpath('.//text()').extract()).strip().split('.avail')[0]
            if len(availability) > 30:
                availability = ''
        techInfo = response.css('#productDetails_techSpec_section_1')
        trs = techInfo.xpath('.//tr')

        for tr in trs:
            thStr = ''.join(tr.css('th').xpath('.//text()').extract()).lower()
            tdStr = ''.join(tr.css('td').xpath('.//text()').extract())

            if thStr.strip() == 'brand' or thStr.strip() == 'manufacturer':
                brandName = tdStr.strip().replace('?', '')
            elif 'item weight' in thStr.lower():
                itemWeight = tdStr.strip()
            elif 'product dimension' in thStr:
                productDimensions = tdStr.strip().replace('?', '')
            elif 'item' in thStr or 'part' in thStr and 'number' in thStr:
                itemNumber = tdStr.strip().replace('?', '')
            if 'manufacturer part number' in thStr or 'manufacturer' in thStr and 'number' in thStr:
                manufectureNumber = tdStr.strip().replace('?', '')
            if thStr.strip() == 'brand' or thStr.strip() == 'manufacturer' and 'number' not in thStr:
                manufacturer = tdStr.strip().replace('?', '')

        # Sometimes left has all
        techInfo = response.css('#productDetails_detailBullets_sections1')
        trs = techInfo.xpath('.//tr')

        for tr in trs:
            thStr = ''.join(tr.css('th').xpath('.//text()').extract()).lower()
            tdStr = ''.join(tr.css('td').xpath('.//text()').extract())

            if thStr.strip() == 'brand' or thStr.strip() == 'manufacturer':
                brandName = tdStr.strip().replace('?', '')
            elif 'item weight' in thStr.lower():
                itemWeight = tdStr.strip().replace('?', '')
            elif 'product dimension' in thStr.lower():
                productDimensions = tdStr.strip().replace('?', '')
            elif 'item' in thStr or 'part' in thStr and 'number' in thStr.lower():
                itemNumber = tdStr.strip().replace('?', '')
            elif thStr.strip() == 'brand' or thStr.strip() == 'manufacturer' and 'number' not in thStr:
                manufacturer = tdStr.strip().replace('?', '')
            if 'manufacturer part number' in thStr or 'manufacturer' in thStr and 'number' in thStr:
                manufectureNumber = tdStr.strip().replace('?', '')
            elif 'best sellers rank' in thStr.lower():
                try:
                    subCategoryName = tdStr.split(" in ")[-1].strip().replace('?', '')
                except:
                    pass
                bestSellerRankInAutomative = tdStr.split("in Automotive")[0].replace(',', '').strip().split("in")[
                    0].strip().replace('?', '')
                try:
                    subCategory = '#' + tdStr.split(')')[1].split('#')[1].split('in')[0].strip().replace('?', '')
                except:
                    pass
            elif 'review' in thStr.lower():
                rs = tr.css('td').xpath('./text()').extract()
                for r in rs:
                    if 'of' in r:
                        reviews = r.strip();
                        break
            elif 'shipping weight' in thStr.lower():
                shippingWeight = tdStr.strip().replace('?', '')
            elif 'date' in thStr.lower() and 'available' in thStr.lower():
                dateAvailable = tdStr.strip().replace('?', '')

        techInfo = response.xpath('//table[@class="a-normal a-spacing-micro"]')
        trs = techInfo.xpath('.//tr')

        for tr in trs:
            thStr = ''.join(tr.xpath('.//td[1]').xpath('.//text()').extract()).lower()
            tdStr = ''.join(tr.xpath('.//td[2]').xpath('.//text()').extract())

            if 'brand' in thStr.strip() or 'manufacturer' in thStr.strip():
                brandName = tdStr.strip().replace('?', '')

        # Sometimes its a ul
        techInfo = response.css('#detailBullets_feature_div')
        trs = techInfo.xpath('.//li')

        for tr in trs:
            try:
                thStr = ''.join(tr.css('span')[0].xpath('.//text()').extract()).lower().split(':')[0].strip()
                tdStr = ''.join([x.strip() for x in ':'.join(''.join(tr.css('span')[0].xpath('.//text()').extract()).split(':')[1:])])

                if 'brand' in thStr.strip() or 'manufacturer' in thStr.strip() :
                    brandName = tdStr.strip()
                elif 'item weight' in thStr:
                    itemWeight = tdStr.strip()
                elif 'product dimension' in thStr or 'package dimensions' in thStr:
                    productDimensions = tdStr.strip()
                elif 'item' in thStr or 'part' in thStr and 'number' in thStr:
                    itemNumber = tdStr.strip()
                elif thStr.strip() == 'brand' or thStr.strip() == 'manufacturer' and 'number' not in thStr:
                    manufacturer = tdStr.strip().replace('?', '')
                if 'manufacturer part number' in thStr or 'manufacturer' in thStr and 'number' in thStr:
                    manufectureNumber = tdStr.strip()
                elif 'best sellers rank' in thStr:
                    try:
                        subCategoryName = tdStr.split(" in ")[-1].strip()
                    except:
                        pass
                    bestSellerRankInAutomative = \
                        tdStr.split("in Automotive")[0].replace(',', '').strip().split("in")[
                            0].strip()
                    try:
                        subCategory = '#' + tdStr.split(')')[1].split('#')[1].split('in')[0].strip()
                    except:
                        pass
                elif 'review' in thStr.lower():
                    rs = tr.css('td').xpath('./text()').extract()
                    for r in rs:
                        if 'of' in r:
                            reviews = r.strip();
                            break
                elif 'shipping weight' in thStr.lower():
                    shippingWeight = tdStr.strip()
                elif 'date' in thStr.lower() and 'available' in thStr.lower():
                    dateAvailable = tdStr.strip()
            except:
                pass

        # priceStr = response.css('#price,#corePrice_desktop').xpath('.//text()').extract()
        priceStr = response.css('.basisPrice').xpath('.//text()').extract()

        priceBefore = ''
        priceNow = ''
        priceStr = [p.strip() for p in priceStr]

        priceAct = []
        # print(priceStr)
        # print(priceAct)
        try:
            for p in priceStr:
                if 'was:' == p or 'Was:' == p or '$' in p or 'Price:' == p or 'price:' == p or 'List Price:' == p:
                    priceAct.append(p)

            if 'was' in priceAct[0] or 'Was' in priceAct[0] or 'List Price' in priceAct[0]:
                priceBefore = priceAct[1].split()[0]
            if 'price' == priceAct[0].lower().replace(':', '').strip():
                priceNow = priceAct[1].split()[0]
            elif 'price' == priceAct[2].lower().replace(':', '').strip():
                priceNow = priceAct[3].split()[0]
            elif 'price' == priceAct[3].lower().replace(':', '').strip():
                priceNow = priceAct[4].split()[0]
        except:
            pass
        if priceNow.strip() == '':
            try:
                pps = [s.strip() for s in response.css('#corePrice_feature_div').xpath('.//text()').extract() if
                       s.strip() != '' and '$' in s]
                if len(pps) > 0:
                    priceNow = pps[0]
            except:
                pass
        if 'try free for 7 days' in priceNow.strip():
            priceNow = response.xpath('//div[@class="a-section a-spacing-none aok-align-center aok-relative"]//span//text()').extract_first()

        similarItems = response.css('._p13n-desktop-sims-fbt_fbt-desktop_product-box__3PBxY')

        fItemDesctriptions = []
        fItemPrices = []
        fItemLinks = []

        for li in range(0, len(similarItems)):
            text1 = similarItems[li].xpath(
                './/span[@class="_p13n-desktop-sims-fbt_fbt-desktop_title-truncate__1pPAM"]//text()').extract()
            text = [t.strip() for t in similarItems[li].xpath('.//text()').extract()]
            fItemDesctriptions.append(' '.join(text1).strip().replace('This item: ', ''))
            b = False
            for t in text:
                if '$' in t:
                    fItemPrices.append(t.strip())
                    b = True
                    break
            if not b:
                fItemPrices.append('')
            a = ''
            try:
                end_parts = similarItems[li].xpath('.//a/@href').extract()
                end_part = ''
                for en in end_parts:
                    if '/dp/' in en:
                        end_part = en
                a = 'https://www.amazon.com' + end_part
            except:
                pass
            fItemLinks.append(a)

        count = 3 - len(fItemDesctriptions)
        if count > 0:
            for i in range(count):
                fItemDesctriptions.append('')
                fItemPrices.append('')
                fItemLinks.append('')
        fItemLinks[0] = ''

        carosals = response.css('.a-carousel-container')

        vps = response.css('#view_to_purchase-sims-feature ul li')
        if not vps:
            for car in carosals:
                heading = car.css('.a-carousel-header-row').xpath('.//text()').extract_first()
                if heading and 'customers also viewed these products' in heading.lower():
                    vps = car.css('ol li')

        vpsItemDescription = []
        vpsItemPrices = []
        vpsItemLinks = []

        for v in vps:
            desc = v.xpath('.//text()').extract()
            desc = [d.strip() for d in desc]
            vpsItemDescription.append(' '.join(desc).strip())
            b = False
            for vp in desc:
                if '$' in vp:
                    vpsItemPrices.append(vp)
                    b = True
                    break
            if not b:
                vpsItemPrices.append('')
            a = 'https://www.amazon.com' + v.xpath('.//a/@href').extract_first().split('?')[0]
            vpsItemLinks.append(a)

        count = 5 - len(vpsItemDescription)
        if count > 0:
            for i in range(count):
                vpsItemDescription.append('')
                vpsItemPrices.append('')
                vpsItemLinks.append('')
        print('view items')
        viewItems = response.css('#desktop-dp-sims_session-similarities-sims-feature ol li')
        if not viewItems:
            for car in carosals:
                heading = car.css('.a-carousel-header-row').xpath('.//text()').extract_first()
                if heading and 'customers who viewed this item also viewed' in heading.lower():
                    viewItems = car.css('ol li')
            print(viewItems)
        viewItemDescriptions = []
        viewItemPrices = []
        viewItemLinks = []

        for v in viewItems:
            texts = v.xpath('.//text()').extract()
            texts = [t.strip() for t in texts]
            viewItemDescriptions.append(' '.join(texts).strip())
            print(viewItemDescriptions)
            b = False
            for text in texts:
                text = text.strip()
                if '$' in text:
                    viewItemPrices.append(text)
                    b = True
                    break
            if not b:
                viewItemPrices.append('')
            a = 'https://www.amazon.com' + v.xpath('.//a/@href').extract_first()
            viewItemLinks.append(a)

        count = 5 - len(viewItemDescriptions)
        if count > 0:
            for i in range(count):
                viewItemDescriptions.append('')
                viewItemPrices.append('')
                viewItemLinks.append('')

        ###########
        custAlsoBought = []
        if not custAlsoBought:
            for car in carosals:
                heading = car.css('.a-carousel-header-row').xpath('.//text()').extract_first()
                print(heading)
                if heading and 'customers who bought this item also bought' in heading.lower():
                    custAlsoBought = car.css('ol li')
        custAlsoBoughtDescriptions = []
        custAlsoBoughtPrices = []
        custAlsoBoughtLinks = []

        for v in custAlsoBought:
            texts = v.xpath('.//text()').extract()
            texts = [t.strip() for t in texts]
            custAlsoBoughtDescriptions.append(' '.join(texts).strip())

            b = False
            for text in texts:
                text = text.strip()
                if '$' in text:
                    custAlsoBoughtPrices.append(text)
                    b = True
                    break
            if not b:
                custAlsoBoughtPrices.append('')
            a = 'https://www.amazon.com' + v.xpath('.//a/@href').extract_first()
            custAlsoBoughtLinks.append(a)

        count = 5 - len(custAlsoBoughtDescriptions)
        if count > 0:
            for i in range(count):
                custAlsoBoughtDescriptions.append('')
                custAlsoBoughtPrices.append('')
                custAlsoBoughtLinks.append('')

        #######
        productsRelated = []
        if not productsRelated:
            for car in carosals:
                heading = car.css('.a-carousel-header-row').xpath('.//text()').extract_first()
                if heading and 'products related to this item' in heading.lower():
                    productsRelated = car.css('ol li')
        productsRelatedDescriptions = []
        productsRelatedPrices = []
        productsRelatedLinks = []

        for v in productsRelated:
            texts = v.xpath('.//text()').extract()
            texts = [t.strip() for t in texts]
            des = ' '.join(texts).strip()
            if '}));' in des:
                des = des.split('}));')[-1]
                des = ' '.join(des.split())
                texts = des.split(' ')

            productsRelatedDescriptions.append(des)

            b = False
            for text in texts:
                text = text.strip()
                if '$' in text:
                    productsRelatedPrices.append(text)
                    b = True
                    break
            if not b:
                productsRelatedPrices.append('')
            a = ""
            try:
                a = [al for al in v.xpath('.//a/@href').extract() if 'amazon.com' in al][0]
            except:
                pass
            productsRelatedLinks.append(a)

        count = 5 - len(productsRelatedDescriptions)
        if count > 0:
            for i in range(count):
                productsRelatedDescriptions.append('')
                productsRelatedPrices.append('')
                productsRelatedLinks.append('')

        compareWithSimilarItems = response.css('#HLCXComparisonTable')

        compareWithSimilarItemsDescriptions = compareWithSimilarItems.css('th .a-size-base').xpath(
            './/text()').extract()
        compareWithSimilarItemsPrices = compareWithSimilarItems.css('.a-price').xpath('./span[1]/text()').extract()
        compareWithSimilarItemsLinks = compareWithSimilarItems.css('th').xpath('@data-asin').extract()
        compareWithSimilarItemsLinks = ['https://www.amazon.com/dp/' + asin for asin in compareWithSimilarItemsLinks]
        try:
            compareWithSimilarItemsDescriptions = compareWithSimilarItemsDescriptions[
                                                  1:len(compareWithSimilarItemsPrices) + 1]
        except:
            compareWithSimilarItemsDescriptions = []
        count = 5 - len(compareWithSimilarItemsDescriptions)
        if count > 0:
            for i in range(count):
                compareWithSimilarItemsDescriptions.append('')
                compareWithSimilarItemsPrices.append('')
                compareWithSimilarItemsLinks.append('')

        ###########
        a4sa = []
        if not a4sa:
            for car in carosals:
                heading = car.css('.a-carousel-header-row').xpath('.//text()').extract_first()
                print(heading)
                if heading and '4 stars and above' in heading.lower():
                    a4sa = car.css('ol li')
        a4asDescriptions1 = []
        a4asPrices1 = []
        a4asLinks1 = []

        for v in a4sa:
            texts = v.xpath('.//text()').extract()
            texts = [t.strip() for t in texts]
            a4asDescriptions1.append(' '.join(texts).strip())

            b = False
            for text in texts:
                text = text.strip()
                if '$' in text:
                    a4asPrices1.append(text)
                    b = True
                    break
            if not b:
                a4asPrices1.append('')
            a = 'https://www.amazon.com' + v.xpath('.//a/@href').extract_first()
            a4asLinks1.append(a)

        count = 5 - len(a4asDescriptions1)
        if count > 0:
            for i in range(count):
                a4asDescriptions1.append('')
                a4asPrices1.append('')
                a4asLinks1.append('')

        a4asDescriptions = []
        a4asPrices = []
        a4asLinks = []
        for k in a4asDescriptions1:
            try:
                asinas4 = 'https://www.amazon.com/dp/' + \
                          str(k.split('highly_rated_')[1].split('_')[0]).split('"')[0]
                a4asLinks.append(asinas4)
            except:
                asinas4 = ''
                a4asLinks.append(asinas4)
            # a4link = +str(asinas4)

            try:
                pricea4a = k.split('$')[-1].split(' ')[0]

            except:
                pricea4a = ''
            if len(pricea4a) > 1:
                a4asPrices.append('$ ' + str(pricea4a))
            else:
                a4asPrices.append('')

            try:
                descra4a = k.split('Feedback')[1].split('#')[0].replace('  ', '')
            except:
                descra4a = ''
            a4asDescriptions.append(descra4a)
        #######

        deliveryto = ''.join([x.strip() for x in response.xpath('//span[@id="contextualIngressPtLabel"]//text()').extract()])

        ###### new 2024 06 25 for all catageries ####
        bestSellerName = ''
        bestsellerranktext2 = ''.join([x.strip() for x in response.xpath('//*[contains(text(), "Best Sellers Rank")]/following-sibling::td//text()').extract()])
        print('INFO::bestsellerankText::',bestsellerranktext2)
        try:
            bestSellerName = bestsellerranktext2.split(' in ')[1].split('(')[0]
        except:
            pass
        if bestSellerRankInAutomative == '':
            if bestSellerRankInAutomative == '':
                bestsellerranktext = ''.join([x.strip() for x in response.xpath('//span[contains(text(), "Best Sellers Rank:")]/parent::span//text()').extract()])
                try:
                    bestSellerRankInAutomative = bestsellerranktext.split(":")[1].split(' in ')[0]
                except:
                    bestSellerRankInAutomative = ''
                try:
                    bestSellerName = bestsellerranktext.split(":")[1].split(' in ')[1].split('#')[0].split('(')[0]
                except:
                    bestSellerName = ''
                try:
                    subCategory = '#'+bestsellerranktext.split(":")[1].split('#')[-1].split('in')[0]
                except:
                    subCategory = ''
                try:
                    subCategoryName = bestsellerranktext.split('#')[-1].split('(')[0].split('in')[-1]
                except:
                    subCategoryName = ''
                reviews = ''.join([x.strip() for x in response.xpath('//span[contains(text(), " Customer Reviews: ")]/parent::span//@title').extract()])
                print("INFO::reviews",reviews)

        yield {
            "ID": response.meta["ID"], "ASIN": response.meta['ASIN'], "Link": response.meta['LINK'],
            # "which_Search": response.meta['which_Search'],
            "Product Title": title, "Ships From": ships_from, "Sold by": soldBy,
            "Best Seller Ranking in Automotive": bestSellerRankInAutomative,"Best Seller Name": bestSellerName,
            "Sub Category Ranking": subCategory, 'Sub Category Name': subCategoryName, "Acutal price": priceNow,
            "Cross off Price": priceBefore,"Ratings": ratings,"Prime Status": primestatus,"BCCat":bccat,
            "BCSubCat1":bcsubcat1,"BCSubCat2":bcsubcat2,"BCSubCat3":bcsubcat3,
            "Brand Name": brandName, "Item Weight": itemWeight, "Product Dimension": productDimensions,
            "Item Number": itemNumber, 'Manufacturer': brandName,
            "Manufacturer Number": manufectureNumber, "Reviews": reviews, "Shipping Weight": shippingWeight,
            "Date First Available": dateAvailable,
            "Landing Image URL": landingImageUrl, "Availability": availability,
        }



