import scrapy
import csv
import codecs

class WorkSpider(scrapy.Spider):
    name = 'abcapteka'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            "url", "Product_Name", "cat", "Main_Image", "Product_Price", "Producent",
            "Id","EAN",
            # "Description"

        ]
    };

    urls = [
        "https://abcapteka.pl/",
    ]

    def start_requests(self):

        f = codecs.open("/home/vishwa/myproject/abcaptekaNew1.xml", "w+", encoding='utf8')
        contents = "\n".join([
                                 '<?xml version="1.0" encoding="utf-8"?><offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1">',
                                 '</offers>'])
        f.write(contents)
        f.flush()
        f.close()

        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        details = response.xpath('//ul[@class="dl-menu"]//a')

        links = details.xpath('.//@href').extract()
        lenarr = len(links)+1
        # print (links)

        for x in links:
            l = 'https://abcapteka.pl'+x
            yield scrapy.Request(url=l.strip(), callback=self.parseInside, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )

    def parseInside(self, response):

        pagingUrls = []

        try:
            pagings = response.xpath('//ul[@class="pagination"]//a/@href').extract()
            paging = pagings[-2]
            lastpgnum = response.xpath('//ul[@class="pagination"]//a//text()').extract()
            valuelastpg = lastpgnum[-2]
            pagingother = paging.split('=')[0]

            i = 1
            while i < int(valuelastpg):
                pgurl = "https://abcapteka.pl" + pagingother + "=" + str(i)
                pagingUrls.append(pgurl)
                i += 1
        except:
            pagingUrls.append(response.url)

        for pagesofprdt in pagingUrls:
            yield scrapy.Request(url=pagesofprdt, callback=self.toAllPrdt,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

    def toAllPrdt(self, response):

        prducturl = response.xpath('//a[@class="product-icon align_row"]')
        prdlinks = prducturl.xpath('./@href').extract()

        for prdlink in prdlinks:
            linn = 'https://abcapteka.pl'+prdlink
            yield scrapy.Request(url=linn, callback=self.prdDetails, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )

    def prdDetails(self,response):
        print('I m herrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr')
        prdname = response.xpath('//h1/text()').extract_first()
        cat = response.xpath('//div[@class="breadcrumbs"]//li//text()').extract()[-2]
        try:
            imageurlsrc = response.xpath('//img[@class="photo"]/@src').extract_first()
        except:
            imageurlsrc = '-'
        imageurl = 'https://abcapteka.pl'+ str(imageurlsrc)
        prdprice1 = response.xpath('//strong[@id="projector_price_value"]//text()').extract_first().replace(',','.')
        prdprice = prdprice1.split()[0]
        producer = response.xpath('//div[@class="producer"]//a/text()').extract_first()
        ean = response.xpath('//div[@class="code"]//strong/text()').extract_first()
        kodproducer = 'BBBB'
        descri = '\n'.join([x.strip('') for x in
                            response.xpath('//div[@id="component_projector_longdescription"]//text()').extract() if
                            x.strip() != ''])
        id = response.url.split('/')[-1].split('-')[-1].replace('.html','')

        line = '<o id="%s" price="%s" url="%s"><cat><![CDATA[%s]]></cat><name><![CDATA[%s]]></name><imgs><main url="%s"/></imgs><attrs><a name="Producent"><![CDATA[%s]]></a><a name="Kod_producenta"><![CDATA[%s]]></a><a name="EAN"><![CDATA[%s]]></a></attrs></o>' % (
            id, prdprice, response.url, cat, prdname, imageurl, producer, kodproducer,ean
        )

        yield {
            "url":response.url,
            "Product_Name":prdname,
            "cat":cat,
            "Main_Image":imageurl,
            "Product_Price":prdprice,
            "Producent":producer,
            "ID": id,
            "EAN": ean,
            "Kod_producenta": kodproducer,
            "Description": descri
        }

        f = codecs.open("/home/vishwa/myproject/abcaptekaNew1.xml", "r", encoding='utf8')
        contents = f.readlines()
        contents = [a for a in contents if a.strip() != '']
        f.close()

        contents.insert(len(contents) - 1, line.strip())

        f = codecs.open("/home/vishwa/myproject/abcaptekaNew1.xml", "w", encoding='utf8')
        contents = "\n".join(contents)
        f.write(contents)
        f.flush()
        f.close()


