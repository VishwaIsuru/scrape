import scrapy
import csv

class workspider(scrapy.Spider):
    name = "shopitheme"
    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Url", "ThemeTitle", "ShortDescription",
            "Price", "FeatureImageDesktop", "FeatureImageMobile",
            "BuyThemeLink", "ThemeDeveloperLink",
            "ThemeFeaturetitle1", "ThemeFeatureShortDescription1",
            "ThemeFeaturetitle2", "ThemeFeatureShortDescription2",
            "ThemeFeaturetitle3", "ThemeFeatureShortDescription3",
            "ThemeFeaturetitle4", "ThemeFeatureShortDescription4",
            "ThemeFeaturetitle5", "ThemeFeatureShortDescription5",
            "ThemeFeaturetitle6", "ThemeFeatureShortDescription6",
            "StoreUsingFeatureImage1", "StoreUsingLinkToStore1",
            "StoreUsingFeatureImage2", "StoreUsingLinkToStore2",
            "StoreUsingFeatureImage3", "StoreUsingLinkToStore3",
            "StoreUsingFeatureImage4", "StoreUsingLinkToStore4"
                ]
    };
    urls = ["https://themes.shopify.com/themes?page=1","https://themes.shopify.com/themes?page=2",
            "https://themes.shopify.com/themes?page=3","https://themes.shopify.com/themes?page=4"]

    def start_requests(self):

        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )

    def parse(self, response):
        allthemes = response.xpath('//a[@class="theme__link"]/@href').extract()

        for themes in allthemes:
            themelink = 'https://themes.shopify.com'+themes
            yield scrapy.Request(url=themelink, callback=self.toAllThemes,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'}, dont_filter=True)

    def toAllThemes(self, response):
        themetitle = response.xpath('//h1[@class="section-heading__heading gutter-bottom--half"]//text()').extract_first()
        shortdisc = response.css('.theme-meta__tagline').xpath('.//text()').extract_first()
        price = response.xpath('//p[@class="heading--3"]//text()').extract_first()
        desimg = response.xpath('//div[@class="main-screenshot main-screenshot--details-carousel"]//img/@src').extract_first()
        mobileimg = response.xpath('//div[@class="iphone__screenshot"]//img//@src').extract_first()
        try:
            buythemelink = 'https://themes.shopify.com'+response.xpath('//a[@id="MobileBuyTheme"]/@href').extract_first()
        except:
            buythemelink = ''
        themedevlink = ''
        try:
            themedevlink = response.xpath('//a[@rel="nofollow"]/@href').extract_first()
        except:
            themedevlink = response.xpath('//p[@class="text-major theme-text-major gutter-bottom--reset"]//text()').extract_first()

        themfettitle1 = response.xpath('//div[@class="social-proof__feature-text"][1]//h3//text()').extract_first()
        thefetshordiscr1 = response.xpath('//div[@class="social-proof__feature-text"][1]//p//text()').extract_first()
        themfettitle2 = response.css('div.grid__item--tablet-up-half:nth-child(3) > div:nth-child(1) > div:nth-child(2) > h3:nth-child(1)').xpath('.//text()').extract_first()
        thefetshordiscr2 = response.css('div.grid__item--tablet-up-half:nth-child(3) > div:nth-child(1) > div:nth-child(2) > p:nth-child(2)').xpath('.//text()').extract_first()
        themfettitle3 = response.css('div.grid__item--tablet-up-half:nth-child(4) > div:nth-child(1) > div:nth-child(2) > h3:nth-child(1)').xpath('.//text()').extract_first()
        thefetshordiscr3 = response.css('div.grid__item--tablet-up-half:nth-child(4) > div:nth-child(1) > div:nth-child(2) > p:nth-child(2)').xpath('.//text()').extract_first()
        themfettitle4 = response.css('div.grid__item--tablet-up-half:nth-child(5) > div:nth-child(1) > div:nth-child(2) > h3:nth-child(1)').xpath('.//text()').extract_first()
        thefetshordiscr4 = response.css('div.grid__item--tablet-up-half:nth-child(5) > div:nth-child(1) > div:nth-child(2) > p:nth-child(2)').xpath('.//text()').extract_first()
        themfettitle5 = response.css('div.grid__item--tablet-up-half:nth-child(6) > div:nth-child(1) > div:nth-child(2) > h3:nth-child(1)').xpath('.//text()').extract_first()
        thefetshordiscr5 = response.css('div.grid__item--tablet-up-half:nth-child(6) > div:nth-child(1) > div:nth-child(2) > p:nth-child(2)').xpath('.//text()').extract_first()
        themfettitle6 = response.css('div.grid__item--tablet-up-half:nth-child(7) > div:nth-child(1) > div:nth-child(2) > h3:nth-child(1)').xpath('.//text()').extract_first()
        thefetshordiscr6 = response.css('div.grid__item--tablet-up-half:nth-child(7) > div:nth-child(1) > div:nth-child(2) > p:nth-child(2)').xpath('.//text()').extract_first()

        storefeaturelink1 = response.xpath('//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][1]//a/@href').extract_first()
        strofeatureimg1 = response.xpath('//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][1]//img/@src').extract_first()
        storefeaturelink2 = response.xpath(
            '//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][2]//a/@href').extract_first()
        strofeatureimg2 = response.xpath(
            '//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][2]//img/@src').extract_first()
        storefeaturelink3 = response.xpath(
            '//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][3]//a/@href').extract_first()
        strofeatureimg3 = response.xpath(
            '//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][3]//img/@src').extract_first()
        storefeaturelink4 = response.xpath(
            '//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][4]//a/@href').extract_first()
        strofeatureimg4 = response.xpath(
            '//div[@class="grid__item grid__item--mobile-up-2 grid__item--tablet-up-2 grid__item--desktop-up-3 theme-live-example gutter-bottom--half"][4]//img/@src').extract_first()

        yield {
            "Url": response.url,"ThemeTitle": themetitle,"ShortDescription":shortdisc,
            "Price": price,"FeatureImageDesktop":desimg,"FeatureImageMobile": mobileimg,
            "BuyThemeLink": buythemelink,"ThemeDeveloperLink":themedevlink,
            "ThemeFeaturetitle1":themfettitle1,"ThemeFeatureShortDescription1":thefetshordiscr1,
            "ThemeFeaturetitle2": themfettitle2, "ThemeFeatureShortDescription2": thefetshordiscr2,
            "ThemeFeaturetitle3": themfettitle3, "ThemeFeatureShortDescription3": thefetshordiscr3,
            "ThemeFeaturetitle4": themfettitle4, "ThemeFeatureShortDescription4": thefetshordiscr4,
            "ThemeFeaturetitle5": themfettitle5, "ThemeFeatureShortDescription5": thefetshordiscr5,
            "ThemeFeaturetitle6": themfettitle6, "ThemeFeatureShortDescription6": thefetshordiscr6,
            "StoreUsingFeatureImage1": strofeatureimg1,"StoreUsingLinkToStore1": storefeaturelink1,
            "StoreUsingFeatureImage2": strofeatureimg2, "StoreUsingLinkToStore2": storefeaturelink2,
            "StoreUsingFeatureImage3": strofeatureimg3, "StoreUsingLinkToStore3": storefeaturelink3,
            "StoreUsingFeatureImage4": strofeatureimg4, "StoreUsingLinkToStore4": storefeaturelink4,
        }