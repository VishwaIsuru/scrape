# import scrapy
# import re
# import csv
# import psycopg2
#
# csv.register_dialect('myDialect', delimiter='/', quoting=csv.QUOTE_NONE)
# connection = psycopg2.connect(
#     host="wineke-rds.cfijrpvd3af5.us-east-1.rds.amazonaws.com",
#     database="postgres",
#     port=5432,
#     user="postgres",
#     password="MrNimbus")
#
# cookies = {
#     'cookie_enabled': 'true',
#     'COOKIE_ID': '3P81CT6DGD100LR',
#     # 'visit': '3P81CT6DGD100LR%7C20210903094718%7C%2Ffind%2Fmonochrome%2Bwines%2B%2F-%2F-%2F-%2Fx%7C%7Cend%20',
#     # 'search': 'start%7Ccoastal%2Bestates%2Bred%2Bwine%7C1%7Cany%7CUSD%7C%7C%7C%7C%7C%7C%7C%7C%7Ce%7Ci%7C%7C%7CN%7C%7Cx%7C%7C%7C%7CCUR%7Cend',
#     # '_pxhd': 'qDcq6/x9pL0-QtNs0//Y3ScJKiR77v0-z/Lc5UD9m3oTsw1mmHYr5DU19l0LZcG5BrDGB5CajJ6C8qG2EdEnsg==:/9ya5Dpgc2aHXaWYCKLcPobwQaQ-WqF09cH5rzy6cxjOwEk/XfTXweoXf0LEmxyh3TGSfGY12O-xZIS-MIp82d0iljcxxt2Wa4zEzn9emoE=',
#     # '_pxvid': '8b4d963b-0c93-11ec-b862-74786148497a',
#     # '_ga_M0W3BEYMXL': 'GS1.1.1631078506.11.1.1631078973.50',
#     # '_ga': 'GA1.2.1732147093.1630658840',
#     # 'NPS_bc24fa74_last_seen': '1630658840963',
#     # '_fbp': 'fb.1.1630658841677.548659916',
#     # '__gads': 'ID=75d447d710b5a7c0:T=1630658842:S=ALNI_MYCMYtogd_ID0NXUl6FUobLzF4dzg',
#     # '_gid': 'GA1.2.382931789.1631002482',
#     # 'ID': '1H01CGSRG4G0B2J',
#     # 'IDPWD': 'I77556313',
#     # 'user_status': 'A%7C',
#     # '_csrf': 'Dsthg8toKU9b7tXiwJa7dBp5aOFeeDcP',
#     # 'pxcts': '62a491e0-1060-11ec-9fb7-83f01d077532',
#     # 'NPS_bc24fa74_throttle': '1631121921691',
#     # 'find_tab': '%23t2',
#     # '_px3': '7d58d1564db0b6ad548eb59591ab10de0f762cb3ff04cc9c3e2287b30718b628:UEyfULbCHDgToKRy49nUItWJsCa09nXdkaKRzyjTl3W8LiA6/Ma4dPJ1i1edePmOcZ81XcTpxcFaTi68fYv2dQ==:1000:h+22Zsc/ZitxiJAGqQ2YDRWn4XPu3OR2qt14u/FTaBPyj3HpfmgfyOdPAOiOHdxOhxNwjSsLXbdgf2xiaNNKGq1HBtEKK+0OUOCsJ1oRUfRwkUv4dx/oZzmNV9y+6XaNGnI/VtI7v0ta+iwB+F8ljPNVIsind00vqG+90Mi5F+VW968tKeKkm/fdeyrBS2XTujUApA9p1abu+ybpnDmRtg==',
#     # '_px2': 'eyJ1IjoiYmY0YjMwYzAtMTA2NS0xMWVjLTg4NGQtYTM3N2Y3NTc1NzUyIiwidiI6IjhiNGQ5NjNiLTBjOTMtMTFlYy1iODYyLTc0Nzg2MTQ4NDk3YSIsInQiOjE1Mjk5NzEyMDAwMDAsImgiOiI5MmE3YzNhMTU5ZDIzZTg2ZTcwYTFiMzgyMTExMzVlYTk3OGEyNjAwNDZiZDJhZGIwZWQzYjRmMDM2NWM1NzYwIn0=',
# }
#
# class WorkSpider(scrapy.Spider):
#     name = 'winesearcher'
#
#     custom_settings = {
#         # specifies exported fields and order
#         'FEED_EXPORT_FIELDS': [
#             'url','label_id','label_name','producer','varietal','wine_type','case_size','item_number',
#             'price','qty_on_hand','qty_on_order','bottle_size','is_sparkling','cru','village','appellation',
#             'subregion','region','state','country','vintage'
#         ]
#     };
#
#     def start_requests(self):
#         col_list = ["Brand Name","Brand Number"]
#         urls = []
#         itemNumbers = []
#         with open('brandinput.csv','r',encoding='utf-8') as file:
#             reader=csv.DictReader(file)
#             for row in reader:
#                 urls.append(row['Brand Name'])
#                 itemNumbers.append(row['Brand Number'])
#
#         d=len(urls)+1
#         for i in range(0,d):
#             string = urls[i]
#             itemNumber = itemNumbers[i]
#             re.search(r'\b(19|20)\d{1,2}', string)
#             re.search(r'\b(6|12)(/|pk| pack)', string)
#             re.search(r'\b(375|750|1500)', string)
#             try:
#                 s = re.findall(r"\d{4}", string)[0]
#             except:
#                 s =''
#
#             str2 = string.replace(s,'')
#             chars = "'\""
#
#             for c in chars:
#                 if c in string:
#                     str2 = str2.replace(c, "")
#
#             str3 = str2.replace(" ", "+").lower()
#             searchurl = "https://www.wine-searcher.com/find/" + str3 + "/-/-/-/x"
#
#             yield scrapy.Request(url=searchurl, callback=self.parseInside, dont_filter=True,
#                                  headers={
#                                      'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20200'+str(i)+' Firefox/74.0',
#                                      'Accept-Encoding': 'gzip, deflate, br',
#                                      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
#                                      'Accept-Language': 'en-US,en;q=0.5',
#                                      'Connection': 'keep-alive',
#                                      'Upgrade-Insecure-Requests': '1',
#                                      'Sec-Fetch-Dest': 'document',
#                                      'Sec-Fetch-Mode': 'navigate',
#                                      'Sec-Fetch-Site': 'none',
#                                      'Sec-Fetch-User': '?1',
#                                      'Cache-Control': 'max-age=0',
#                                      'TE': 'trailers',
#                                  },
#                                  # cookies=cookies,
#                                  meta={'searchurl': searchurl,'itemNumber':itemNumber,'index':i})
#
#     def parseInside(self, response):
#         searchurl = response.meta['searchurl']
#         itemNumber = response.meta['itemNumber']
#         index = response.meta['index']+2
#         headers = {
#             'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/200001'+str(index)+' Firefox/91.0',
#             'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
#             'Accept-Language': 'en-US,en;q=0.5',
#             'Referer': searchurl,
#             'Connection': 'keep-alive',
#             'Upgrade-Insecure-Requests': '1',
#             'Sec-Fetch-Dest': 'document',
#             'Sec-Fetch-Mode': 'navigate',
#             'Sec-Fetch-Site': 'same-origin',
#             'Sec-Fetch-User': '?1',
#             'Cache-Control': 'max-age=0',
#             'TE': 'trailers',
#         }
#
#         firstproduct = response.xpath('//div[@class="card-product__name"]//a/@href').extract_first()
#         # print(response.url)
#         if firstproduct is not None:
#             yield scrapy.Request(url='https://www.wine-searcher.com'+firstproduct, callback=self.parseInsideProduct, dont_filter=True,
#                                  headers=headers,meta={'itemNumber':itemNumber},
#                                  # cookies=cookies
#                                  )
#
#     def parseInsideProduct(self,response):
#         itemNumber = response.meta['itemNumber']
#         ws_label = response.xpath('//h1[contains(@class, "product-details__product-name")]//span//text()').extract_first()
#         the_producer = response.xpath('//div[@class="card-text"]//strong//text()').extract_first().replace("'","´")
#         try:
#             the_producer_split = the_producer.replace("'","´").split()
#         except:
#             the_producer_split = []
#         try:
#             varietal_label = response.xpath('//div[contains(text(), "Grape / Blend")]/following-sibling::div[1]/text()').extract_first().replace("'","´")
#         except:
#             varietal_label = ''
#         try:
#             wine_type = response.xpath('//div[contains(text(), "Wine Style")]/following-sibling::div[1]/text()').extract_first().split('-')[0]
#         except:
#             wine_type = ''
#         appellation_label = response.xpath('//div[contains(text(), "Region or Appellation")]/following-sibling::div[1]//a//text()').extract_first().strip().replace("'","´")
#         countyherachi = response.xpath(
#             '//div[contains(text(), "Region or Appellation")]/following-sibling::div[2]//a//text()').extract()
#         remove_list = [the_producer, varietal_label, appellation_label
#                        ] + the_producer_split
#
#         country = ''
#         state = ''
#         region = ''
#         subregion = ''
#         village = ''
#         cru = ''
#         if (countyherachi[-1] == 'USA'):
#             country = countyherachi[-1]
#             remove_list.append(country)
#             try:
#                 state = countyherachi[-2]
#                 remove_list.append(state)
#             except:
#                 state = ''
#             try:
#                 region = countyherachi[-3]
#                 remove_list.append(region)
#             except:
#                 region = ''
#             try:
#                 subregion = countyherachi[-4]
#                 remove_list.append(subregion)
#             except:
#                 subregion = ''
#         elif (countyherachi[-1] != 'USA'):
#             country = countyherachi[-1]
#             remove_list.append(country)
#             try:
#                 region = countyherachi[-2]
#                 remove_list.append(region)
#             except:
#                 region = ''
#             try:
#                 subregion = countyherachi[-3]
#                 remove_list.append(subregion)
#             except:
#                 subregion = ''
#             try:
#                 village = countyherachi[-4]
#                 remove_list.append(village)
#             except:
#                 village = ''
#             try:
#                 cru = countyherachi[-5]
#                 remove_list.append(cru)
#             except:
#                 cru = ''
#
#         for j in remove_list:
#             ws_label = ws_label.replace(j, '')
#         the_label = ws_label.strip().replace("'","´")
#         label_id = ''
#         price = ''
#         qty_on_hand = ''
#         qty_on_order = ''
#         bottle_size = ''
#         is_sparkling = ''
#         case_size = ''
#         vintage = ''
#         url = response.url
#         # (label_name, producer, varietal, wine_type, case_size, item_number, price, qty_on_hand, qty_on_order,
#         #  bottle_size, is_sparkling, cru, village, appellation, subregion, region, state, country, vintage)
#         yield {
#             'url': url,
#             'label_id': label_id,
#             'label_name': the_label,
#             'producer': the_producer,
#             'varietal': varietal_label,
#             'wine_type': wine_type,
#             'case_size':case_size,
#             'item_number': itemNumber,
#             'price': price,
#             'qty_on_hand': qty_on_hand,
#             'qty_on_order': qty_on_order,
#             'bottle_size': bottle_size,
#             'is_sparkling': is_sparkling,
#             'cru': cru,
#             'village': village,
#             'appellation': appellation_label,
#             'subregion': subregion,
#             'region': region,
#             'state': state,
#             'country': country,
#             'vintage': vintage
#         }
#         with connection.cursor() as cursor:
#             sql = "SELECT * FROM labels WHERE item_number='%s'"
#             cursor.execute(sql % (itemNumber))
#             result = cursor.fetchone()
#             print(result)
#             # connection.commit()
#             try:
#                 if result is None:
#                     sql = "INSERT INTO labels (url,label_id, label_name, producer, varietal, wine_type, case_size, item_number," \
#                           "price, qty_on_hand, qty_on_order, bottle_size, is_sparkling, cru, village, appellation, subregion, " \
#                           "region, state, country, vintage)" \
#                           "VALUES ('%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', " \
#                           "'%s','%s', '%s''%s', '%s', '%s', '%s', '%s' , '%s','%s', '%s')"
#                     sql_processed = sql % (
#                         url,label_id, the_label, the_producer, varietal_label, wine_type, case_size, itemNumber,
#                         price, qty_on_hand, qty_on_order, bottle_size, is_sparkling,cru,village,appellation_label,subregion,
#                         region,state,country,vintage
#                     )
#                     print(sql_processed)
#                     cursor.execute(sql_processed)
#                     connection.commit()
#
#                 else:
#                     sql = "UPDATE labels SET  url='%s',label_id='%s' ,label_name = '%s',producer = '%s'," \
#                           "varietal = '%s',wine_type = '%s',case_size = '%s'," \
#                           "price = '%s',qty_on_hand = '%s',qty_on_order = '%s'," \
#                           "bottle_size = '%s',is_sparkling='%s',cru='%s',village='%s'," \
#                           "appellation='%s',subregion='%s',region='%s',state='%s'," \
#                           "country='%s',vintage='%s'" \
#                           "WHERE item_number = '%s'"
#
#                     # cursor.execute(sql,
#                     #                (rating,review,btype,discrip,loctin,locatedin,hours,menuurl,tel,weblink,get_url,title))
#                     # print(sql)
#                     # connection.commit()
#                     sql_processed = sql % (
#                         url,label_id, the_label, the_producer, varietal_label, wine_type, case_size,
#                         price, qty_on_hand, qty_on_order, bottle_size, is_sparkling, cru, village, appellation_label,
#                         subregion,region, state, country, vintage,itemNumber
#                     )
#
#                     cursor.execute(sql_processed)
#                     connection.commit()
#             except:
#                 connection.rollback()