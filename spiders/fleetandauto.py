import scrapy
import csv
import requests
import scrapy
from scrapy.http import request
from scrapy.http.request.form import FormRequest
from scrapy.http import FormRequest
import json

class WorkSpider(scrapy.Spider):
    name = 'fleetandauto'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': ['url','title','price','replaces','ship_info','minimum_purchase',
                               'product_info','cross_references'

        ]
    };

    urls = []
    for i in range(1,22):
        urls.append('https://fleetandauto.com/accessories/?page='+str(i))
    for i in range(1,8):
        urls.append('https://fleetandauto.com/belts-and-pulleys/?page='+str(i))
    for i in range(1,124):
        urls.append('https://fleetandauto.com/body/?page='+str(i))
    for i in range(1,40):
        urls.append('https://fleetandauto.com/brakes/?page='+str(i))
    for i in range(1,23):
        urls.append('https://fleetandauto.com/cooling-system/?page='+str(i))
    for i in range(1,28):
        urls.append('https://fleetandauto.com/drivetrain-and-transmission/?page='+str(i))
    for i in range(1,67):
        urls.append('https://fleetandauto.com/electrical/?page='+str(i))
    for i in range(1,32):
        urls.append('https://fleetandauto.com/engine/?page='+str(i))
    for i in range(1,16):
        urls.append('https://fleetandauto.com/exhaust-and-emission/?page='+str(i))
    for i in range(1,25):
        urls.append('https://fleetandauto.com/fuel-and-air/?page='+str(i))
    for i in range(1,13):
        urls.append('https://fleetandauto.com/heating-and-cooling/?page='+str(i))
    for i in range(1,7):
        urls.append('https://fleetandauto.com/ignition/?page='+str(i))
    for i in range(1,22):
        urls.append('https://fleetandauto.com/interior/?page='+str(i))
    for i in range(1,24):
        urls.append('https://fleetandauto.com/lighting/?page='+str(i))
    for i in range(1,9):
        urls.append('https://fleetandauto.com/lines-and-clamps/?page='+str(i))
    for i in range(1,32):
        urls.append('https://fleetandauto.com/miscellaneous-hardware/?page='+str(i))
    for i in range(1,7):
        urls.append('https://fleetandauto.com/security/?page='+str(i))
    for i in range(1,18):
        urls.append('https://fleetandauto.com/steering/?page='+str(i))
    for i in range(1,21):
        urls.append('https://fleetandauto.com/suspension/?page='+str(i))
    for i in range(1,12):
        urls.append('https://fleetandauto.com/wheels-and-hubs/?page='+str(i))
    for i in range(1,12):
        urls.append('https://fleetandauto.com/wiper-and-washer/?page='+str(i))

    def start_requests(self):
        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):
        prdulrs = response.xpath('//h4[@class="card-title"]//a//@href').extract()
        for plink in prdulrs:
            yield scrapy.Request(url=plink,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'},dont_filter=True,
                                     )

    def parseInside(self,response):
        title = ''.join([x.strip() for x in response.xpath('//h1[@class="productView-title"]//text()').extract()])
        price = response.xpath('//span[@class="price price--withoutTax"]//text()').extract_first()
        replaces = ''.join([x.strip() for x in response.xpath('//dd[@data-product-sku=""]//text()').extract()])
        shipinfo = ''.join([x.strip() for x in response.xpath('//dt[@data-product-part-type-code=""]//text()').extract()])
        minimun_purchase = response.xpath('//dt[contains(text(),"Minimum Purchase:")]/following-sibling::dd//text()').extract_first()
        product_discription = ' '.join([x.strip() for x in response.xpath('//div[@id="tab-description"]//text()').extract()]).replace(' Product Description  ','')
        # cros = ' '.join([x.strip() for x in response.xpath('//dd[@id="ymmcrossref"]//text()').extract()])
        pid = response.xpath('//input[@name="product_id"]//@value').extract_first()
        pshu = response.xpath('//input[@name="productsku"]//@value').extract_first()
        form_data = {"productid":str(pid),"productsku":str(pshu)}
        request_body = json.dumps(form_data)
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/118.0',
                   'Accept': 'application/json, text/plain, */*',
                   'Accept-Language': 'en-US,en;q=0.5',
                   'Content-Type': 'application/json; charset=UTF-8',
                   'Origin': 'https://fleetandauto.com',
                   'Connection': 'keep-alive',
                   'Referer': 'https://fleetandauto.com/',
                   'Sec-Fetch-Dest': 'empty',
                   'Sec-Fetch-Mode': 'cors',
                   'Sec-Fetch-Site': 'cross-site',
                   }

        yield scrapy.Request('https://fleetandauto.apacatapult.com/apa_getcrossreference',
                             method="POST",body=request_body,headers=headers,
                             meta={'purl':response.url,'title':title,'price':price,'replaces':replaces,'shipinfo':shipinfo,
                                   'minimumperchase':minimun_purchase,'productdescri':product_discription},
                             callback=self.parsePost)

    def parsePost(self, response):
        purl = response.meta['purl']
        title = response.meta['title']
        price = response.meta['price']
        replaces = response.meta['replaces']
        shipinfo = response.meta['shipinfo']
        minimumperchase = response.meta['minimumperchase']
        productdescri = response.meta['productdescri']
        d = response.body
        j = json.loads(d)
        crossreferences = j['result']

        yield {
            'url':purl,
            'title': title,
            'price':price,'replaces':replaces,'ship_info':shipinfo,'minimum_purchase': minimumperchase,
            'product_info':productdescri,'cross_references':crossreferences
        }
