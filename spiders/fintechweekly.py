import scrapy
import csv

class WorkSpider(scrapy.Spider):
    name = 'finteckweekly'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'Name','Catagory','Country'
        ]
    };

    urls = ["https://fintechweekly.com/fintech-companies",]

    def start_requests(self):
        i = 0
        for u in self.urls:

            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):
        # l = response.xpath("//*[contains(text(), 'Total Funding:')]//text()").getall()
        g = response.xpath('//div[@class="collapsable collapsable_first-open"]')
        for k in g:
            name = k.xpath('.//div[@class="col-xs-11"]//h3//text()').extract_first()
            catagory = ''.join([x.strip() for x in k.xpath(".//div[@class='o-section__description']//ul//li[1]//text()").extract()]).replace("\n"," ")
            countrys = k.xpath(".//div[@class='o-section__description']//ul//li[5]")
            country = ''
            for t in countrys:
                tx = ''.join([x.strip() for x in t.xpath('.//text()').extract()])
                if 'Country' in tx:
                    country = tx.replace('\n','')


            yield {
                'Name':name,
                'Catagory': catagory,
                'Country': country
            }