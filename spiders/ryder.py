import scrapy
import csv
import json

class WorkSpider(scrapy.Spider):
    name = 'ryder'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "url","Title","Brand","Product Number","Availability","List Price","Sell Price","Description", "Category","Cross References",
            "Rating","Reviews"

        ]
    };

    urls = [
        # "https://www.ryderfleetproducts.com/heavy-duty-truck-parts-c-9608",
        "https://www.ryderfleetproducts.com/shop-supplies-c-8263","https://www.ryderfleetproducts.com/truck-maintenance-tools-c-9061",
        "https://www.ryderfleetproducts.com/truck-wash-equipment-c-12791","https://www.ryderfleetproducts.com/safety-items-c-10413",



    ]

    def start_requests(self):


        i=0
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse)
            i+=1

    def parse(self, response):
        details = response.xpath('//div[@class="content grid-100 grid-parent"]/div[@class="grid-20 tablet-grid-20 mobile-grid-50"]//a[@class="product-img"]')


        links= details.xpath('./@href').extract()

        for link in links:

            l="https://www.ryderfleetproducts.com"+link
            yield scrapy.Request(url=l, callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parseInside(self,response):

        # prdone=response.xpath('//div[@class="grid-25 tablet-grid-50"]//a[@class="product-img"]')

        prdone=response.xpath('//div[@class="grid-20 tablet-grid-20 mobile-grid-50"]//a[@class="product-img"]')

        fstprdliks=prdone.xpath('./@href').extract()

        for prdoneurl in fstprdliks:
            prdoneurls= "https://www.ryderfleetproducts.com"+prdoneurl


            yield scrapy.Request(url=prdoneurls, callback=self.parseInsideMoreproductpages, headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parseInsideMoreproductpages(self,response):
        t = response.xpath('//div[@class="grid-100 margin-bottom"]/span/text()').extract_first()
        t = t.split('of')[1]
        s = int(t) / 20

        pgurls=[]
        i=0
        while i<=s:
            pgurl=response.url+"?pageNumber="+str(i)
            pgurls.append(pgurl)
            i+=1
            i = 0
            for pu in pgurls:

                yield scrapy.Request(url=pu, callback=self.parseInsideMoreproduct,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'})
                i += 1

    def parseInsideMoreproduct(self,response):
        pruuu=response.xpath('//div[@class="grid-25 tablet-grid-50"]//a[@class="product-img"]')
        fstpruuu=pruuu.xpath('./@href').extract()

        for pruuuurl in fstpruuu:
            pruuuurls= "https://www.ryderfleetproducts.com"+pruuuurl


            yield scrapy.Request(url=pruuuurls, callback=self.parseInsideMore, headers={'User-Agent': 'Mozilla Firefox 12.0'})

    def parseInsideMore(self,response):

        title=response.xpath('//div[@class="product-info grid-65 tablet-grid-65"]/h1/text()').extract()
        brand=response.xpath('//div[@class="product-meta middle-color grid-55"]//table/tr[2]/td[2]/a/text()').extract()
        productnum=response.xpath('//div[@class="product-meta middle-color grid-55"]//table/tr[3]/td[2]//text()').extract()
        availability=''.join([x.strip() for x in response.xpath('//div[@class="product-meta middle-color grid-55"]//table/tr[4]//td[@class="active-color"]//text()').extract()])
        lisrpri=response.xpath('//div[@class="product-price active-color grid-45"]/del/text()').extract()
        sellpri=response.xpath('//div[@class="product-price active-color grid-45"]/strong/text()').extract()
        description='\n'.join([x.strip() for x in response.xpath('//div[@id="tab-description"]//text()').extract()])
        crossrefer=''
        try:
            crossrefer=description.split('Cross Reference:')[1]
        except:
            pass
        category='>'.join([x.strip() for x in response.xpath('//div[@class="breadcrumbs grid-100 middle-color"]//text()').extract()])





        pid=response.xpath('//form[@action="#"]/input/@value').extract_first()

        ratingjson="https://display.powerreviews.com/m/8008/l/en_US/product/"+pid+"/reviews?apikey=b0b524ba-7ba2-493a-a835-a18cfaec749b"

        # prdlinks = response.xpath('//div[@class="grid-25 tablet-grid-50"]/a[@class="product-img"]/@href').extract()

        yield scrapy.Request(url=ratingjson, callback=self.allDetails,
                             headers={"User-Agent": "Mozilla Firefox 12.0"}, meta={"Title":title,"Brand":brand,"Product Number":productnum,"Availability":availability,
            "List Price":lisrpri,
            "Sell Price":sellpri,"Description":description,"Category":category,"Cross References":crossrefer,"url":response.url})

    def allDetails(self,response):

        d = response.body
        j = json.loads(d)
        dataset = j['results']
        rating='0.0'
        reviewcount='No Review'
        for re in dataset:
            review = re['reviews']
            try:
                roll=re['rollup']
                reviewcount=roll['review_count']
            except:
                pass
            try:
                for me in review:
                    metri=me['metrics']

                    rating=metri['rating']

            except:
                rating='0.0'

        yield {
                "url":response.meta['url'],
                "Title":response.meta['Title'],
                "Brand":response.meta['Brand'],
                "Product Number":response.meta['Product Number'],
                "Availability":response.meta['Availability'],
                "List Price":response.meta['List Price'],
                "Sell Price":response.meta['Sell Price'],
                "Description":response.meta['Description'],
                "Category":response.meta['Category'],
                "Cross References":response.meta['Cross References'],

                "Rating": rating,
                "Reviews": reviewcount
            }
