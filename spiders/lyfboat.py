import scrapy
import csv

class WorkSpider(scrapy.Spider):
    name = 'lyfboat'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            "Dr Link","Dr Name","Qualifications","Specialization","Department","Designation","Location",
            "Hospital","Career Profile","Education & Training","Experience","Clinical Focus","Membership"
        ]
    };

    urls = []
    for p in range(1,5):
        l = 'https://www.lyfboat.com/doctors/best-doctors-in-india/?pageview='+str(p)+'&show=10'
        urls.append(l)

    def start_requests(self):
        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u,callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        dlinks = response.xpath('//h2//a//@href').extract()
        for dlink in dlinks:
            l = dlink
            yield scrapy.Request(url=l,callback=self.parseInside,headers={'User-Agent': 'Mozilla Firefox 12.0'})
        # print l ,dont_filter=True


    def parseInside(self,response):
        drLink = response.url
        drName = ''.join([x.strip() for x in response.xpath('//h1[@id="ga_doctor_name"]//text()').extract()])
        qulificatins = response.xpath('//strong[contains(text(),"Qualification - ")]/following-sibling::span/text()').extract_first()
        specilization = response.xpath('//strong[contains(text(),"Specialization - ")]/following-sibling::span/text()').extract_first()
        department = response.xpath('//strong[contains(text(),"Department - ")]/following-sibling::span/text()').extract_first()
        designation = response.xpath('//strong[contains(text(),"Designation - ")]/following-sibling::span/text()').extract_first()
        location = ''.join([x.strip() for x in response.xpath('//strong[contains(text(),"Location - ")]/following-sibling::span//text()').extract()])
        hospital = ''.join([x.strip() for x in response.xpath('//strong[contains(text(),"Hospital(s) - ")]/following-sibling::span//text()').extract()])
        careerProfile = ''.join([x.strip() for x in response.xpath('//h2[contains(text(),"Career Profile")]/following-sibling::div//text()').extract()])
        educationAndTraining = ''.join([x.strip() for x in response.xpath('//h2[contains(text(),"Education & Training")]/following-sibling::ul[1]//text()').extract()])
        experiance = ''.join([x.strip() for x in response.xpath('//h2[contains(text(),"Experience")]/following-sibling::ul[1]//text()').extract()])
        clinicalForcus = ''.join([x.strip() for x in response.xpath('//h2[contains(text(),"Clinical Focus")]/following-sibling::ul[1]//text()').extract()])
        membership = ''.join([x.strip() for x in response.xpath('//h2[contains(text(),"Memberships")]/following-sibling::ul[1]//text()').extract()])

        yield {
            "Dr Link": drLink,
            "Dr Name": drName,
            "Qualifications": qulificatins,
            "Specialization": specilization,
            "Department": department,
            "Designation": designation,
            "Location": location,
            "Hospital": hospital,
            "Career Profile": careerProfile,
            "Education & Training": educationAndTraining,
            "Experience": experiance,
            "Clinical Focus": clinicalForcus,
            "Membership": membership
        }
