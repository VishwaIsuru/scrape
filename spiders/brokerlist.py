import scrapy
import csv
import codecs

class WorkSpider(scrapy.Spider):
    name = 'brokerlist'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [

            "Url","Name","Email","OfficeNumber","FaxNumber","CellPhoneNumber"

        ]
    };
    # 298
    urls = []
    for j in range(1,299):
        ur = "https://thebrokerlist.com/search_profiles?lightbox=0&order=profiles.last_sign_in_at+DESC&page="+str(j)
        urls.append(ur)

    def start_requests(self):

        i = 0
        for u in self.urls:
            yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        brourls = response.xpath('//div[@class="avatar"]//a//@href').extract()
        for brurl in brourls:
            l = 'https://thebrokerlist.com'+brurl
            yield scrapy.Request(url=l.strip(), callback=self.parseInside, headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )

    def parseInside(self,response):

        name = response.xpath('//h1[@itemprop="name"]//text()').extract_first()
        try:
            email = response.xpath('//div[@class="email"]//text()').extract_first().replace('Email: ','')
        except:
            email = ''

        phonum = response.xpath('//div[@class="phone_numbers row"]//div[@class=""]')
        officenum = ''
        cellnum = ''
        faxnum = ''
        for photx in phonum:
            phontype = photx.xpath('.//div[@class="phone_type"]//text()').extract_first()
            phonval = photx.xpath('.//span[@itemprop="tel"]//text()').extract_first()
            if 'Office:' in phontype:
                officenum = phonval
            if 'Cell:' in phontype:
                cellnum = phonval
            if 'Fax:' in phontype:
                faxnum = phonval
        yield{
            "Url": response.url,
            "Name": name,
            "Email": email,
            "OfficeNumber": officenum,
            "FaxNumber": faxnum,
            "CellPhoneNumber": cellnum
        }