import scrapy
import csv
import codecs

class WorkSpider(scrapy.Spider):
    name = 'amazonbreadcum'

    custom_settings = {
        # specifies exported fields and order
        'FEED_EXPORT_FIELDS': [
            'lastcat','pagin','sndpgurl'
        ]
    };

    def start_requests(self):
        i = 0
        with open('D:\\Vishwapreviousprojects\\seliniumPython\\amazon_55k_Toys_and_Games.csv') as file_obj:
            reader_obj = csv.reader(file_obj)
            for row in reader_obj:
                u = row[1]
                yield scrapy.Request(url=u, callback=self.parse,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )
            i += 1

    def parse(self, response):

        fclinks = response.xpath('//li[@class="a-spacing-micro s-navigation-indent-2"]//@href').extract()
        for fc in fclinks:
            fscat = 'https://www.amazon.com' + fc
            yield scrapy.Request(url=fscat, callback=self.parseSecnd,
                                 headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                 )

    def parseSecnd(self,response):
        snlinks = response.xpath('//li[@class="a-spacing-micro s-navigation-indent-2"]//@href').extract()
        if len(snlinks)>0:
            for u in snlinks:
                yield scrapy.Request(url='https://www.amazon.com'+str(u), callback=self.parseSecnd,
                                     headers={'User-Agent': 'Mozilla Firefox 12.0'},
                                     )
        else:
            try:
                lspg = response.xpath('//span[@class="s-pagination-item s-pagination-disabled"]//text()').extract_first()
            except:
                lspg = '3'
            sndpaglin = response.xpath('//a[@class="s-pagination-item s-pagination-button"]//@href').extract_first()
            yield {
                'lastcat': response.url,
                'pagin': lspg,
                'sndpgurl': sndpaglin
            }

